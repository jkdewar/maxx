local folder = _OS
if _OPTIONS.platform == "ios" then
  folder = "ios"
end

solution "maxx"
  configurations { "Debug", "Release" }
  location(folder)
  targetdir(folder.."/bin")

project "maxx"
  kind "StaticLib"
  language "C++"
  files { "../src/**.h", "../src/**.cpp" }
  includedirs {
    "../src",
  }
  defines {
    "ED_ENABLED",
  }

  configuration "ios"
    files { "../src/**.mm" }
    includedirs { 
      "../../third-party/include/lua",
      "../../third-party/include/box2d",
      "../../third-party/include/sdl",
      "../../third-party/include/sdl-image",
    }
    defines{
      "__IPHONEOS__",
    }
    libdirs {
    }
    links {
    }

  if _OPTIONS.platform ~= "ios" then
  configuration "macosx"
    platforms { "x32" }
    files { "../src/**.mm" }
    includedirs { 
      "../../third-party/include/ogg",
      "../../third-party/include/vorbis",
      "../../third-party/include/lua",
      "../../third-party/include/box2d",
      "../../third-party/include/sdl",
      "../../third-party/include/sdl-image",
      "../../third-party/include/sdl-net",
    }
    defines {
      "__MACOSX__",
      "MAXX_SERVER",
    }
    libdirs {
    }
    links {
    }
  end
    
  configuration "linux"
    includedirs {
      "/usr/include/lua5.1",
      "/usr/local/include/SDL2",
      "/usr/local/include/Box2D",
    }
    defines {
      "__LINUX__",
      "MAXX_SERVER",
    }
    libdirs {
    }
    links {
    }
    
  configuration "windows"
    includedirs { 
      "../../third-party/include/ogg",
      "../../third-party/include/vorbis",
      "../../third-party/include/lua",
      "../../third-party/include/box2d",
      "../../third-party/include/sdl",
      "../../third-party/include/sdl-image",
      "../../third-party/include/sdl-net",
      "../../third-party/include/openal",
    }
    defines {
      "__WIN32__",
      "MAXX_SERVER",
    }
    libdirs {
    }
    links {
    }
  
  configuration "Debug"
    defines { "_DEBUG", "DEBUG" }
    flags { "Symbols" }

  configuration "Release"
    defines { "NDEBUG" }
    flags { "Optimize" }
