#if defined(__IPHONEOS__)
// On iOS, compile audio.cpp with the Objective-C++ compiler.
#define INCLUDING_AUDIO_CPP_FROM_MM
#include "audio/audio.cpp"
#undef INCLUDING_AUDIO_CPP_FROM_MM
#endif