#if !defined(__IPHONEOS__) || defined(INCLUDING_AUDIO_CPP_FROM_MM)
// I used this tutorial:
// http://www.devmaster.net/articles/openal-tutorials/lesson1.php

#include "audio/audio.h"
#include "audio/music-player-oggvorbis.h"
#include "audio/music-player-ios.h"
#include "audio/wav-file.h"
#include "utils/assert.h"
#include "utils/guid.h"
#include "math/math.h"

namespace Maxx {

//--------------------------------------------------------------------
/*static*/ Audio* Audio::Create(int& argc, char** argv)
{
  bool success = false;
  Audio* audio = new Audio(success);
  if (!success)
  {
    delete audio; 
    audio = 0;
  }
  return audio;
}

//--------------------------------------------------------------------
Audio::Audio(bool& success)
  : device_(0)
  , context_(0)
  , musicPlayer_(0)
  , soundVolume_(1.0)
  , noRetrigger_(5)
{
  // init OpenAL
  device_ = alcOpenDevice(0);
  if (!device_)
  {
    Trace("ERROR: alcOpenDefaultDevice failed\n");
    success = false;
    return;
  }
  context_ = alcCreateContext(device_, 0);
  if (!context_)
  {
    success = false;
    return;
  }
  alcMakeContextCurrent(context_);
  alGetError(); // clear error code
  
  alDistanceModel(AL_NONE); // no distance attenuation
  
#if defined(__IPHONEOS__)
  musicPlayer_ = new MusicPlayerIos;
#else
  musicPlayer_ = new MusicPlayerOggVorbis;
#endif
  
  success = true;
  return;
}

//--------------------------------------------------------------------
Audio::~Audio()
{ 
  delete musicPlayer_; musicPlayer_ = 0;
  Trace("--- ALL USED SOUNDS ---\n");
  for (FileToBuffer::iterator i = fileToBuffer_.begin(); i != fileToBuffer_.end(); ++i)
  {
    Trace("\"%s\",\n", i->first.c_str());
    ALuint buffer = i->second;
    alDeleteBuffers(1, &buffer);
  }
  fileToBuffer_.clear();
  Trace("-----------------------\n");

  delete musicPlayer_; musicPlayer_ = 0;
  
  if (context_)
  {
    alcMakeContextCurrent(0);
    alcDestroyContext(context_); context_ = 0;
  }
  if (device_)
  {
    alcCloseDevice(device_); device_ = 0;
  }
}

//--------------------------------------------------------------------
double Audio::GetSoundVolume() const
{
  return soundVolume_;
}

//--------------------------------------------------------------------
void Audio::SetSoundVolume(double v)
{
  soundVolume_ = v;
}

//--------------------------------------------------------------------
bool Audio::GetMusicMuted() const
{
  if (!musicPlayer_)
    return true;
  return musicPlayer_->GetMuted();
}

//--------------------------------------------------------------------
void Audio::SetMusicMuted(bool v)
{
  if (musicPlayer_)
    musicPlayer_->SetMuted(v);
}

//--------------------------------------------------------------------
double Audio::GetMusicVolume() const
{
  if (!musicPlayer_)
    return 0.0;
  return musicPlayer_->GetVolume();
}
//--------------------------------------------------------------------
void Audio::SetMusicVolume(double v)
{
  if (musicPlayer_)
    musicPlayer_->SetVolume(v);
}

//--------------------------------------------------------------------
std::string Audio::PlayWav(const std::string& fileName)
{
  return PlayWavAt(fileName, 0.0, 0.0);
}

//--------------------------------------------------------------------
std::string Audio::PlayWavAt(const std::string& fileName, double x, double y)
{
  // If the sound was triggered recently, ignore
  if (!noRetrigger_.Trigger(fileName))
    return "";

  // Load the wav file if not already loaded.
  FileToBuffer::iterator i = fileToBuffer_.find(fileName);
  if (i == fileToBuffer_.end())
  {
    bool loadOk = LoadWav(fileName);
    if (!loadOk)
    {
      printf("ERROR: Couldn't load wav file %s\n", fileName.c_str());
      return "";
    }
    i = fileToBuffer_.find(fileName);
    Assert(i != fileToBuffer_.end());
    if (i == fileToBuffer_.end())
      return "";
  }
  
  // Create a new source
  std::string guid = Utils::NewGuid();
  ALuint buffer = i->second;
  ALuint source;
  alGenSources(1, &source);
  if (alGetError() != AL_NO_ERROR)
    return "";
  sources_.insert(std::make_pair(guid, source));
  //Trace("%s x=%f y=%f\n",fileName.c_str(),x,y);
  ALfloat SourcePos[] = { x, y, 0.0 };
  ALfloat SourceVel[] = { 0.0, 0.0, 0.0 };
  alSourcei (source, AL_BUFFER,   buffer   );
  alSourcef (source, AL_PITCH,    1.0f );
  alSourcef (source, AL_GAIN,     (float)soundVolume_);
  alSourcefv(source, AL_POSITION, SourcePos);
  alSourcefv(source, AL_VELOCITY, SourceVel);
  alSourcei (source, AL_LOOPING,  false     );
  if (alGetError() != AL_NO_ERROR)
    return "";

  // Play wav buffer
  alSourcePlay(source);
  if (alGetError() != AL_NO_ERROR)
    return "";
  
  return guid;
}

//--------------------------------------------------------------------
void Audio::StopWav(const std::string& guid)
{
  SourceMap::iterator i = sources_.find(guid);
  if (i == sources_.end())
    return;
  ALuint source = i->second;
  alSourceStop(source);
  sources_.erase(i);
}

//--------------------------------------------------------------------
void Audio::StopAllWavs()
{
  for (SourceMap::iterator i = sources_.begin(); i != sources_.end(); ++i)
  {
    StopWav(i->first);
  }
}

//--------------------------------------------------------------------
bool Audio::PlayMusic(const std::string& fileName, double seek)
{
  if (!musicPlayer_)
    return false;
  return musicPlayer_->Play(fileName, seek);
}

//--------------------------------------------------------------------
void Audio::StopMusic()
{
  musicPlayer_->Stop();
}

//--------------------------------------------------------------------
double Audio::GetMusicLength() const
{
  if (!musicPlayer_)
    return 0.0;
  return musicPlayer_->GetMusicLength();
}

//--------------------------------------------------------------------
void Audio::SeekMusic(double t)
{
  if (musicPlayer_)
    musicPlayer_->SeekTo(t);
}

//--------------------------------------------------------------------
void Audio::Update()
{
  // set up the listener
  alListener3f(AL_POSITION, 0.0, 0.0, 6.0);
  float directionvect[6];
  directionvect[0] = 0;
  directionvect[1] = 0;
  directionvect[2] = -1;
  directionvect[3] = 0;
  directionvect[4] = 1;
  directionvect[5] = 0;
  alListenerfv(AL_ORIENTATION, directionvect);
  
  noRetrigger_.Tick();
  if (musicPlayer_)
    musicPlayer_->Update();
  
  // free finished sound sources
  for (SourceMap::iterator i = sources_.begin(); i != sources_.end(); /**/)
  {
    ALuint source = i->second;
    ALint sourceState;
    alGetSourcei(source, AL_SOURCE_STATE, &sourceState);
    if (sourceState != AL_PLAYING)
    {
      alDeleteSources(1, &source);
      sources_.erase(i++);
    }
    else
    {
      ++i;
    }
  }
}

//--------------------------------------------------------------------
bool Audio::LoadWav(const std::string& fileName)
{
  ALuint buffer;
  
  alGenBuffers(1, &buffer);
  if (alGetError() != AL_NO_ERROR)
    return false;

  WavInfo info;
  bool ok = LoadWavFile(fileName, info);
  if (!ok)
    return false;

  // figure out the OpenAL format based on the wav header
  ALenum format;
  if (info.bits_per_sample == 8)
  {
    if (info.channels == 1)
      format = AL_FORMAT_MONO8;
    else //if(channels == 2)
    {
      Assert(info.channels == 2);
      format = AL_FORMAT_STEREO8;
    }
  }
  else if (info.bits_per_sample == 16)
  {
    if (info.channels == 1)
      format = AL_FORMAT_MONO16;
    else //if(channels == 2)
    {
      Assert(info.channels == 2);
      format = AL_FORMAT_STEREO16;
    }
  }

  ALsizei size = (ALsizei)info.data_size;
  ALvoid* data = (ALvoid*)info.sound_buffer;
  ALsizei freq = (ALsizei)info.sample_rate;

  alBufferData(buffer, format, data, size, freq);
  delete [] info.sound_buffer; info.sound_buffer = 0;
  
  fileToBuffer_[fileName] = buffer;
  
  return true;
}
  
} // Maxx
#endif
