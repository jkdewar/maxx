#pragma once
#include "cross-platform/openal-headers.h"
#include "audio/no-retrigger.h"
#include "audio/i-music-player.h"
#include <string>
#include <map>
#include <list>

class ogg_stream;

namespace Maxx {

class Audio
{
public:
  static Audio* Create(int& argc, char** argv);
  ~Audio();
  
  // volume
  double GetSoundVolume() const;
  void SetSoundVolume(double v);
  double GetMusicVolume() const;
  void SetMusicVolume(double v);
  bool GetMusicMuted() const;
  void SetMusicMuted(bool v);
  
  // sound effects
  bool LoadWav(const std::string& fileName);
  std::string PlayWav(const std::string& fileName);
  std::string PlayWavAt(const std::string& fileName, double x, double y);
  void StopWav(const std::string& guid);
  void StopAllWavs();
  
  // music
  bool PlayMusic(const std::string& fileName, double seek = 0.0);
  void StopMusic();
  double GetMusicLength() const;
  void SeekMusic(double t);
  
  void Update();
  
private:
  Audio(bool& success);
  
private:
  ALCdevice* device_;
  ALCcontext* context_;
  typedef std::map<std::string, ALuint> FileToBuffer;
  FileToBuffer fileToBuffer_;
  IMusicPlayer* musicPlayer_;
  double soundVolume_;
  NoRetrigger noRetrigger_;
  typedef std::map<std::string, ALuint> SourceMap;
  SourceMap sources_;
};

} // Maxx
