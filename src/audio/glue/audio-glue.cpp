#include "audio-glue.h"
#include "audio/audio.h"
#include "engine/maxx.h"

namespace Maxx {
namespace Glue {

//--------------------------------------------------------------------
// self, fileName
// returns boolean
int Audio_loadWav(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Audio* audio = Maxx::Glue::GetUserData<Audio>(L, 1);
  GLUE_VERIFY(audio);
  std::string fileName = lua_tostring(L, 2);
  bool ret = audio->LoadWav(fileName);
  lua_pushboolean(L, ret);
  return 1;
}

//--------------------------------------------------------------------
// self, fileName
// returns guid
int Audio_playWav(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Audio* audio = Maxx::Glue::GetUserData<Audio>(L, 1);
  GLUE_VERIFY(audio);
  std::string fileName = lua_tostring(L, 2);
  std::string guid = audio->PlayWav(fileName);
  lua_pushstring(L, guid.c_str());
  return 1;
}

//--------------------------------------------------------------------
// self, guid
int Audio_stopWav(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Audio* audio = Maxx::Glue::GetUserData<Audio>(L, 1);
  GLUE_VERIFY(audio);
  std::string guid = lua_tostring(L, 2);
  audio->StopWav(guid);
  return 1;
}

//--------------------------------------------------------------------
// self
int Audio_stopAllWavs(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Audio* audio = Maxx::Glue::GetUserData<Audio>(L, 1);
  GLUE_VERIFY(audio);
  audio->StopAllWavs();
  return 0;
}

//--------------------------------------------------------------------
// self, fileName, (seek)
// returns boolean
int Audio_playMusic(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2 || lua_gettop(L) == 3);
  Audio* audio = Maxx::Glue::GetUserData<Audio>(L, 1);
  GLUE_VERIFY(audio);
  std::string fileName = lua_tostring(L, 2);
  double seek = 0.0;
  if (lua_gettop(L) >= 3)
  {
    GLUE_VERIFY(lua_isnumber(L, 3));
    seek = lua_tonumber(L, 3);
  }
  bool ret = audio->PlayMusic(fileName, seek);
  lua_pushboolean(L, ret);
  return 1;
}

//--------------------------------------------------------------------
// self
// returns number
int Audio_getSoundVolume(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Audio* audio = Maxx::Glue::GetUserData<Audio>(L, 1);
  GLUE_VERIFY(audio);
  double volume = audio->GetSoundVolume();
  lua_pushnumber(L, volume);
  return 1;
}

//--------------------------------------------------------------------
// self
// returns number
int Audio_getMusicVolume(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Audio* audio = Maxx::Glue::GetUserData<Audio>(L, 1);
  GLUE_VERIFY(audio);
  double volume = audio->GetMusicVolume();
  lua_pushnumber(L, volume);
  return 1;
}

//--------------------------------------------------------------------
// self, volume
int Audio_setSoundVolume(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Audio* audio = Maxx::Glue::GetUserData<Audio>(L, 1);
  GLUE_VERIFY(audio);
  double volume = lua_tonumber(L, 2);
  audio->SetSoundVolume(volume);
  return 0;
}

//--------------------------------------------------------------------
// self, volume
int Audio_setMusicVolume(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Audio* audio = Maxx::Glue::GetUserData<Audio>(L, 1);
  GLUE_VERIFY(audio);
  double volume = lua_tonumber(L, 2);
  audio->SetMusicVolume(volume);
  return 0;
}

//--------------------------------------------------------------------
// self
// returns boolean
int Audio_getMusicMuted(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Audio* audio = Maxx::Glue::GetUserData<Audio>(L, 1);
  GLUE_VERIFY(audio);
  bool muted = audio->GetMusicMuted();
  lua_pushboolean(L, muted);
  return 1;
}

//--------------------------------------------------------------------
// self, muted
int Audio_setMusicMuted(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Audio* audio = Maxx::Glue::GetUserData<Audio>(L, 1);
  GLUE_VERIFY(audio);
  bool muted = lua_toboolean(L, 2);
  audio->SetMusicMuted(muted);
  return 0;
}



//--------------------------------------------------------------------
// self
// returns total length of music, in seconds
int Audio_getMusicLength(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Audio* audio = Maxx::Glue::GetUserData<Audio>(L, 1);
  GLUE_VERIFY(audio);
  double musicLength = audio->GetMusicLength();
  lua_pushnumber(L, musicLength);
  return 1;
}

//--------------------------------------------------------------------
// self, t
int Audio_seekMusic(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Audio* audio = Maxx::Glue::GetUserData<Audio>(L, 1);
  GLUE_VERIFY(audio);
  double t = lua_tonumber(L, 2);
  audio->SeekMusic(t);
  return 0;
}

//--------------------------------------------------------------------
int luaopen_Audio(lua_State* L)
{
  static luaL_Reg table[] =
  {
    {"loadWav", Audio_loadWav},
    {"playWav", Audio_playWav},
    {"stopWav", Audio_stopWav},
    {"stopAllWavs", Audio_stopWav},
    {"playMusic", Audio_playMusic},
    {"getSoundVolume", Audio_getSoundVolume},
    {"getMusicVolume", Audio_getMusicVolume},
    {"setSoundVolume", Audio_setSoundVolume},
    {"setMusicVolume", Audio_setMusicVolume},
    {"getMusicMuted", Audio_getMusicMuted},
    {"setMusicMuted", Audio_setMusicMuted},
    {"getMusicLength", Audio_getMusicLength},
    {"seekMusic", Audio_seekMusic},
    {0, 0}
  };
  Maxx::Glue::RegisterType<Audio>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}
  
} // Glue
} // Maxx
