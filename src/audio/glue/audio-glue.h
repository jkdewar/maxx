#pragma once

#include "engine/glue.h"

namespace Maxx {
namespace Glue {

int luaopen_Audio(lua_State*);

} // Glue
} // Maxx
