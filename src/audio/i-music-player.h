#pragma once
#include <string>

namespace Maxx {

class IMusicPlayer
{
public:
  virtual ~IMusicPlayer() {}

  virtual void Update() = 0;
  virtual bool Play(const std::string& fileName, double seek) = 0;
  virtual void Stop() = 0;
  virtual double GetMusicLength() const = 0;
  virtual void SeekTo(double seek) = 0;
  virtual double GetVolume() const = 0;
  virtual void SetVolume(double volume) = 0;
  virtual bool GetMuted() const = 0;
  virtual void SetMuted(bool muted) = 0;
};
  
} // Maxx
