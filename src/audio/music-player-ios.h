#if defined(__IPHONEOS__)
#pragma once
#include "audio/i-music-player.h"
#import <AVFoundation/AVAudioPlayer.h>

namespace Maxx {

//--------------------------------------------------------------------
class MusicPlayerIos : public IMusicPlayer
{
public:
  MusicPlayerIos();
  virtual ~MusicPlayerIos();
  
  virtual void Update();
  virtual bool Play(const std::string& fileName, double seek);
  virtual void Stop();
  virtual double GetMusicLength() const;
  virtual void SeekTo(double seek);
  virtual double GetVolume() const;
  virtual void SetVolume(double volume);
  virtual bool GetMuted() const;
  virtual void SetMuted(bool muted);
  
private:
  void PushVolume();
  
private:
  AVAudioPlayer* avAudioPlayer_;
  double volume_;
  bool muted_;
};

} // Maxx
#endif