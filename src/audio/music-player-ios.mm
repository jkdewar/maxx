#if defined(__IPHONEOS__)
#include "audio/music-player-ios.h"
#include "utils/assert.h"
#include "utils/trace.h"
#import <Foundation/Foundation.h>
#import <AVFoundation/AVAudioSession.h>

// Code based on:
// http://www.raywenderlich.com/259/audio-101-for-iphone-developers-playing-audio-programmatically

namespace Maxx {
  
//--------------------------------------------------------------------
MusicPlayerIos::MusicPlayerIos()
  : avAudioPlayer_(0)
  , volume_(0.0)
  , muted_(false)
{
}

//--------------------------------------------------------------------
MusicPlayerIos::~MusicPlayerIos()
{
  Stop();
}

//--------------------------------------------------------------------
void MusicPlayerIos::Update()
{
}

//--------------------------------------------------------------------
bool MusicPlayerIos::Play(const std::string& fileName, double seek)
{
  NSString* str = [NSString stringWithUTF8String:(fileName+".caf").c_str()];
  NSURL* url = [NSURL fileURLWithPath:str];
  NSError* error = 0;
  avAudioPlayer_ = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
  //NSLog(@"  %@", [error localizedDescription]);
  avAudioPlayer_.volume = 1.0;
  avAudioPlayer_.numberOfLoops = -1;
  //[error release];
  //[url release];
  //[str release];
  [avAudioPlayer_ prepareToPlay];
  [avAudioPlayer_ play];
  PushVolume();
  return true;
}

//--------------------------------------------------------------------
void MusicPlayerIos::Stop()
{
  [avAudioPlayer_ release]; avAudioPlayer_ = 0;
}

//--------------------------------------------------------------------
double MusicPlayerIos::GetMusicLength() const
{
  if (!avAudioPlayer_)
    return 0.0;
  return avAudioPlayer_.duration;
}

//--------------------------------------------------------------------
void MusicPlayerIos::SeekTo(double seek)
{
  if (avAudioPlayer_)
    avAudioPlayer_.currentTime = seek;
}

//--------------------------------------------------------------------
double MusicPlayerIos::GetVolume() const
{
  return volume_;
}

//--------------------------------------------------------------------
void MusicPlayerIos::SetVolume(double volume)
{
  volume_ = volume;
  PushVolume();
}

//--------------------------------------------------------------------
bool MusicPlayerIos::GetMuted() const
{
  return muted_;
}

//--------------------------------------------------------------------
void MusicPlayerIos::SetMuted(bool muted)
{
  muted_ = muted;
  PushVolume();
}

//--------------------------------------------------------------------
void MusicPlayerIos::PushVolume()
{
  if (avAudioPlayer_)
    avAudioPlayer_.volume = muted_ ? 0.0 : volume_;
}
  
} // Maxx

#endif
