#if !defined(__IPHONEOS__)
#include "audio/music-player-oggvorbis.h"
#include "audio/ogg-stream.h"
#include "utils/assert.h"
#include "utils/trace.h"

namespace Maxx {
  
//--------------------------------------------------------------------
MusicPlayerOggVorbis::MusicPlayerOggVorbis()
  : oggStream_(0)
  , volume_(0.0)
  , muted_(false)
{
}

//--------------------------------------------------------------------
MusicPlayerOggVorbis::~MusicPlayerOggVorbis()
{
  Stop();
}

//--------------------------------------------------------------------
void MusicPlayerOggVorbis::Update()
{
  if (!oggStream_)
    return;
  bool result = oggStream_->update();
  if (!result)
  {
    // Music is over. Loop back to the beginning.
    oggStream_->Seek(0.0);
  }
}

//--------------------------------------------------------------------
bool MusicPlayerOggVorbis::Play(const std::string& fileName, double seek)
{
  // TODO: respect the seek parameter
  ogg_stream* oggStream = ogg_stream::create(fileName+".ogg");
  if (!oggStream)
    return false;
  Stop();
  oggStream_ = oggStream;
  oggStream_->Seek(seek);
  oggStream_->playback();
  PushVolume();
  return true;
}

//--------------------------------------------------------------------
void MusicPlayerOggVorbis::Stop()
{
  delete oggStream_; oggStream_ = 0;
}

//--------------------------------------------------------------------
double MusicPlayerOggVorbis::GetMusicLength() const
{
  if (!oggStream_)
    return 0.0;
  return oggStream_->GetLength();
}

//--------------------------------------------------------------------
void MusicPlayerOggVorbis::SeekTo(double seek)
{
  if (!oggStream_)
    return;
  oggStream_->Seek(seek);
}

//--------------------------------------------------------------------
double MusicPlayerOggVorbis::GetVolume() const
{
  return volume_;
}

//--------------------------------------------------------------------
void MusicPlayerOggVorbis::SetVolume(double volume)
{
  volume_ = volume;
  PushVolume();
}

//--------------------------------------------------------------------
bool MusicPlayerOggVorbis::GetMuted() const
{
  return muted_;
}

//--------------------------------------------------------------------
void MusicPlayerOggVorbis::SetMuted(bool muted)
{
  muted_ = muted;
  PushVolume();
}

//--------------------------------------------------------------------
void MusicPlayerOggVorbis::PushVolume()
{
  if (oggStream_)
    alSourcef(oggStream_->GetSource(), AL_GAIN, muted_ ? 0.0f : (float)volume_);
}
  
} // Maxx

#endif
