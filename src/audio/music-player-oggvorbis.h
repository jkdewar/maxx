#pragma once
#include "audio/i-music-player.h"

class ogg_stream;

namespace Maxx {

//--------------------------------------------------------------------
class MusicPlayerOggVorbis : public IMusicPlayer
{
public:
  MusicPlayerOggVorbis();
  virtual ~MusicPlayerOggVorbis();
  
  virtual void Update();
  virtual bool Play(const std::string& fileName, double seek);
  virtual void Stop();
  virtual double GetMusicLength() const;
  virtual void SeekTo(double seek);
  virtual double GetVolume() const;
  virtual void SetVolume(double volume);
  virtual bool GetMuted() const;
  virtual void SetMuted(bool muted);
  
private:
  void PushVolume();
  
private:
  ogg_stream* oggStream_;
  double volume_;
  bool muted_;
};
  
} // Maxx
