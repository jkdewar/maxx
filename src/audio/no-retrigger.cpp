#include "audio/no-retrigger.h"

namespace Maxx {
  
//--------------------------------------------------------------------
bool NoRetrigger::Trigger(const std::string& key)
{
  Map::iterator i = map_.find(key);
  if (i == map_.end())
  {
    // ok to trigger
    map_.insert(std::make_pair(key, minTicks_));
    return true;
  }
  // not ok to trigger
  return false;
}

//--------------------------------------------------------------------
void NoRetrigger::Tick()
{
  for (Map::iterator i = map_.begin(); i != map_.end(); /**/)
  {
    int& ticks = i->second;
    --ticks;
    if (ticks <= 0)
    {
      Map::iterator temp = i;
      ++i;
      map_.erase(temp);
    }
    else
    {
      ++i;
    }
  }
}
  
} // Maxx
