#pragma once
#include <string>
#include <map>

namespace Maxx {

//--------------------------------------------------------------------
class NoRetrigger
{
public:
  NoRetrigger(int minTicks) : minTicks_(minTicks) {}
  ~NoRetrigger() {}
  bool Trigger(const std::string& key);
  void Tick();
  
private:
  typedef std::map<std::string, int> Map;
  Map map_;
  int minTicks_;
};

} // Maxx
