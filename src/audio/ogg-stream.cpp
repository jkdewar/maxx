#if !defined(__IPHONEOS__)
#include "audio/audio.h"
#include "ogg-stream.h"
#include "utils/trace.h"
#include "utils/assert.h"
#include "engine/maxx.h"

using namespace std;

// http://www.devmaster.net/articles/openal-tutorials/lesson8.php

static const int BUFFER_SIZE = (100000);

/*static*/ ogg_stream* ogg_stream::create(string path)
{
  ogg_stream* s = new ogg_stream;
  bool openOk = s->open(path);
  if (!openOk)
  {
  delete s; s = 0;
  }
  return s;
}

ogg_stream::ogg_stream()
  : source(0)
  , opened(false)
{
}

ogg_stream::~ogg_stream()
{
   if (opened)
     release();
   opened = false;
}

bool ogg_stream::open(string path)
{
  int result;
  
  if((result = ov_fopen(path.c_str(), &oggStream)) != 0)
  {
    Trace("Could not open Ogg stream. %s", errorString(result).c_str());
    return false;
  }

  vorbisInfo = ov_info(&oggStream, -1);
  vorbisComment = ov_comment(&oggStream, -1);

  if(vorbisInfo->channels == 1)
    format = AL_FORMAT_MONO16;
  else
    format = AL_FORMAT_STEREO16;
    
    
  alGenBuffers(2, buffers);
  check();
  alGenSources(1, &source);
  check();
  
  alSource3f(source, AL_POSITION,         0.0, 0.0, 0.0);
  alSource3f(source, AL_VELOCITY,         0.0, 0.0, 0.0);
  alSource3f(source, AL_DIRECTION,        0.0, 0.0, 0.0);
  alSourcef (source, AL_ROLLOFF_FACTOR,   0.0      );
  alSourcei (source, AL_SOURCE_RELATIVE,  AL_TRUE    );
  alSourcef (source, AL_GAIN,             Maxx::Get()->GetAudio()->GetMusicVolume()     );
  opened = true;
  return true;
}




void ogg_stream::release()
{
  alSourceStop(source);
  empty();
  alDeleteSources(1, &source);
  check();
  alDeleteBuffers(1, buffers);
  check();

  ov_clear(&oggStream);
}




void ogg_stream::display()
{
  cout
    << "version     " << vorbisInfo->version     << "\n"
    << "channels    " << vorbisInfo->channels    << "\n"
    << "rate (hz)     " << vorbisInfo->rate      << "\n"
    << "bitrate upper   " << vorbisInfo->bitrate_upper   << "\n"
    << "bitrate nominal " << vorbisInfo->bitrate_nominal << "\n"
    << "bitrate lower   " << vorbisInfo->bitrate_lower   << "\n"
    << "bitrate window  " << vorbisInfo->bitrate_window  << "\n"
    << "\n"
    << "vendor " << vorbisComment->vendor << "\n";
    
  for(int i = 0; i < vorbisComment->comments; i++)
    cout << "   " << vorbisComment->user_comments[i] << "\n";
    
  cout << endl;
}




bool ogg_stream::playback()
{
  if(playing())
    return true;
    
  if(!stream(buffers[0]))
  {
    Assert(false);
    return false;
  }
    
  if(!stream(buffers[1]))
  {
    Assert(false);
    return false;
  }
  
  alSourceQueueBuffers(source, 2, buffers);
  check();
  alSourcePlay(source);
  check();
  
  return true;
}




bool ogg_stream::playing()
{
  ALenum state;
  
  alGetSourcei(source, AL_SOURCE_STATE, &state);
  
  return (state == AL_PLAYING);
}




bool ogg_stream::update()
{
  int processed = 0;
  bool active = true;

  alGetSourcei(source, AL_BUFFERS_PROCESSED, &processed);
  
  while(processed--)
  {
    ALuint buffer;
    
    alSourceUnqueueBuffers(source, 1, &buffer);
    check();

    active = stream(buffer);

    alSourceQueueBuffers(source, 1, &buffer);
    check();
  }

  return active;
}



bool ogg_stream::stream(ALuint buffer)
{
  char pcm[BUFFER_SIZE];
  int  size = 0;
  int  section;
  int  result;
  
  while(size < BUFFER_SIZE)
  {
    result = ov_read(&oggStream, pcm + size, BUFFER_SIZE - size, 0, 2, 1, &section);
    if(result > 0)
      size += result;
    else if(result < 0)
    {   
      Trace("ERROR: %s\n", errorString(result).c_str());
      return false;
    }
    else // result == 0
      break;
  }
  
  if(size == 0)
  {
    return false;
  }
    
  alBufferData(buffer, format, pcm, size, vorbisInfo->rate);
  check();
    
  return true;
}




void ogg_stream::empty()
{
  int queued;
  
  alGetSourcei(source, AL_BUFFERS_QUEUED, &queued);
  
  while(queued--)
  {
    ALuint buffer;
  
    alSourceUnqueueBuffers(source, 1, &buffer);
    check();
  }
}




void ogg_stream::check()
{
	int error = alGetError();

  if(error != AL_NO_ERROR)
    Trace("OpenAL error was raised.\n");
}



string ogg_stream::errorString(int code)
{
  switch(code)
  {
    case OV_EREAD:
      return string("Read from media.");
    case OV_ENOTVORBIS:
      return string("Not Vorbis data.");
    case OV_EVERSION:
      return string("Vorbis version mismatch.");
    case OV_EBADHEADER:
      return string("Invalid Vorbis header.");
    case OV_EFAULT:
      return string("Internal logic fault (bug or heap/stack corruption.");
    default:
      return string("Unknown Ogg error.");
  }
}

double ogg_stream::GetLength() const
{
  double length = ov_time_total(&((ogg_stream*)this)->oggStream, -1);
  return length;
}

void ogg_stream::Seek(double t)
{
  ov_time_seek(&oggStream, t);
}
#endif
