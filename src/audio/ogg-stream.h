#if !defined(__IPHONEOS__)
// http://www.devmaster.net/articles/openal-tutorials/lesson8.php

#pragma once

#include <string>
#include <iostream>

#include "cross-platform/openal-headers.h"
#include "cross-platform/ogg-vorbis-headers.h"


class ogg_stream
{
    public:
        static ogg_stream* create(std::string path);
        ~ogg_stream();

        void display();
        bool playback();
        bool playing();
        bool update();
        
        double GetLength() const;
        void Seek(double t);
        
        ALuint GetSource() const { if (opened) return source; return 0; }

    protected:
        ogg_stream();
        bool open(std::string path);
        void release();
        bool stream(ALuint buffer);
        void empty();
        void check();
        std::string errorString(int code);

    private:

        FILE*           oggFile;
        OggVorbis_File  oggStream;
        vorbis_info*    vorbisInfo;
        vorbis_comment* vorbisComment;

        ALuint buffers[2];
        ALuint source;
        ALenum format;
        
        bool opened;
};
#endif