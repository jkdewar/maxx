// Based on code from http://www.cpp-home.com/tutorials/333_1.htm
// Beware, I fixed many bugs... the code on that site is hella bad.

#include "audio/wav-file.h"
#include <stdio.h>

namespace Maxx {

bool LoadWavFile(const std::string& fileName, WavInfo& info)
{ 
  FILE* fp = fopen(fileName.c_str(), "rb"); 
  if (!fp)
  {
    printf("Error: loading wav file '%s'\n", fileName.c_str());
    goto fail;
  }
  
  unsigned char id[4]; //four bytes to hold 'RIFF' 
  unsigned int size; // 32 bit value to hold file size 

  fread(id, 1, 4, fp); //read in first four bytes 
  if (!(id[0]=='R' && id[1]=='I' && id[2]=='F' && id[3]=='F'))
  {
    printf("Error: not a RIFF file\n"); 
    goto fail;
  }
  fread(&size, 4, 1, fp); //read in 32bit size value 
  fread(id, 1, 4, fp); //read in 4 byte string now
  if (!(id[0]=='W' && id[1]=='A' && id[2]=='V' && id[3]=='E'))
  {
    printf("Error: RIFF file but not a wave file\n"); 
    goto fail;
  }
  fread(id, 1, 4, fp); //read in 4 bytes "fmt "; 
  fread(&info.format_length, 4,1,fp); 
  fread(&info.format_tag, 2, 1, fp); //check mmreg.h (i think?) for other 
  // possible format tags like ADPCM 
  fread(&info.channels, 2,1,fp); //1 mono, 2 stereo 
  fread(&info.sample_rate, 4, 1, fp); //like 44100, 22050, etc... 
  fread(&info.avg_bytes_sec, 4, 1, fp); //probably won't need this
  fread(&info.block_align, 2, 1, fp); //probably won't need this 
  fread(&info.bits_per_sample, 2, 1, fp); //8 bit or 16 bit file? 
  fread(id, 1, 4, fp); //read in 'data'
  fread(&info.data_size, 4, 1, fp); //how many bytes of sound data we have 
  info.sound_buffer = new unsigned char [info.data_size]; //set aside sound buffer space 
  fread(info.sound_buffer, 1, info.data_size, fp); //read in our whole sound data chunk   
  fclose(fp);
  return true;

fail:
  if (fp)
  {
    fclose(fp); fp = 0;
  }
  return false;
}
  
} // Maxx
