#pragma once
#include <string>

namespace Maxx {

struct WavInfo
{
  unsigned char* sound_buffer;
  short format_tag, channels, block_align, bits_per_sample;    // 16-bit
  unsigned int format_length, sample_rate, avg_bytes_sec, data_size; // 32-bit
};

bool LoadWavFile(const std::string& fileName, WavInfo& info);

} // Maxx
