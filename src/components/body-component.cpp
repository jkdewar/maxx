#include "components/body-component.h"
#include "components/transform-component.h"
#include "components/fixture-component.h"
#include "cross-platform/box2d-headers.h"
#include "physics/physics.h"
#include "engine/maxx.h"
#include "engine/entity.h"
#include "utils/assert.h"

namespace Maxx {

//--------------------------------------------------------------------
BodyComponent::BodyComponent()
  : body_(0)
  , bodyDef_()
  , preUpdateBodyX_(0.0)
  , preUpdateBodyY_(0.0)
  , preUpdateBodyAngle_(0.0)
  , preUpdateTransformX_(0.0)
  , preUpdateTransformY_(0.0)
  , preUpdateTransformAngle_(0.0)
  , wasPreUpdated_(false)
{
}

//--------------------------------------------------------------------
BodyComponent::~BodyComponent()
{
  b2World* world = Maxx::Get()->GetPhysics()->Getb2World();
  if (body_ && world)
    world->DestroyBody(body_);
  body_ = 0;
}

//--------------------------------------------------------------------
bool BodyComponent::Init()
{
  return BuildBody();
}

//--------------------------------------------------------------------
std::string BodyComponent::GetType() const
{
  b2BodyType bodyType = body_ ? body_->GetType() : bodyDef_.type;
  std::string str;
  switch (bodyType)
  {
    case b2_staticBody: str = "static"; break;
    case b2_kinematicBody: str = "kinematic"; break;
    case b2_dynamicBody: str = "dynamic"; break;
    default: Assert(false); str = "static"; break;
  }
  return str;
}

//--------------------------------------------------------------------
void BodyComponent::SetType(const std::string& typeString)
{
  b2BodyType bodyType = b2_staticBody;
  if (typeString == "static")
    bodyType = b2_staticBody;
  else if (typeString == "kinematic")
    bodyType = b2_kinematicBody;
  else if (typeString == "dynamic")
    bodyType = b2_dynamicBody;
  else
    Assert(false);
  if (body_)
    body_->SetType(bodyType);
  else
    bodyDef_.type = bodyType;
}

//--------------------------------------------------------------------
double BodyComponent::GetX() const
{
  if (!body_)
    return bodyDef_.position.x;
  return body_->GetPosition().x;
}

//--------------------------------------------------------------------
void BodyComponent::SetX(double x)
{
  if (!body_)
    bodyDef_.position.x = (float32)x;
  else
    body_->SetTransform(b2Vec2((float32)x, (float32)GetY()), (float32)GetAngle());
}

//--------------------------------------------------------------------
double BodyComponent::GetY() const
{
  if (!body_)
    return bodyDef_.position.y;
  return body_->GetPosition().y;
}

//--------------------------------------------------------------------
void BodyComponent::SetY(double y)
{
  if (!body_)
    bodyDef_.position.x = (float32)y;
  else
    body_->SetTransform(b2Vec2((float32)GetX(), (float32)y), (float32)GetAngle());
}

//--------------------------------------------------------------------
double BodyComponent::GetAngle() const
{
  if (!body_)
    return bodyDef_.angle;
  return body_->GetAngle();
}

//--------------------------------------------------------------------
void BodyComponent::SetAngle(double r)
{
  if (!body_)
    bodyDef_.angle = (float32)r;
  else
    body_->SetTransform(b2Vec2((float32)GetX(), (float32)GetY()), (float32)r);
}

//--------------------------------------------------------------------
double BodyComponent::GetLinearVelocityX() const
{
  if (body_)
    return body_->GetLinearVelocity().x;
  return bodyDef_.linearVelocity.x;
}

//--------------------------------------------------------------------
void BodyComponent::SetLinearVelocityX(double vx)
{
  if (body_)
  {
    if (body_->GetType() == b2_dynamicBody)
    {
      const double mass = body_->GetMass();
      body_->ApplyLinearImpulse(b2Vec2((vx-GetLinearVelocityX())*mass,0), body_->GetPosition());
    }
    else
      body_->SetLinearVelocity(b2Vec2((float32)vx, (float32)GetLinearVelocityY()));
  }
  else
    bodyDef_.linearVelocity.x = (float32)vx;
}

//--------------------------------------------------------------------
double BodyComponent::GetLinearVelocityY() const
{
  if (body_)
    return body_->GetLinearVelocity().y;
  return bodyDef_.linearVelocity.y;
}

//--------------------------------------------------------------------
void BodyComponent::SetLinearVelocityY(double vy)
{
  if (body_)
  {
    if (body_->GetType() == b2_dynamicBody)
    {
      const double mass = body_->GetMass();
      body_->ApplyLinearImpulse(b2Vec2(0, (vy-GetLinearVelocityY())*mass), body_->GetPosition());
    }
    else
      body_->SetLinearVelocity(b2Vec2((float32)GetLinearVelocityX(), (float32)vy));
  }
  else
    bodyDef_.linearVelocity.y = (float32)vy;
}

//--------------------------------------------------------------------
double BodyComponent::GetAngularVelocity() const
{
  if (body_)
    return body_->GetAngularVelocity();
  return bodyDef_.angularVelocity;
}

//--------------------------------------------------------------------
void BodyComponent::SetAngularVelocity(double av)
{
  if (body_)
    body_->SetAngularVelocity((float32)av);
  else
    bodyDef_.angularVelocity = (float32)av;
}

//--------------------------------------------------------------------
double BodyComponent::GetLinearDamping() const
{
  if (body_)
    return body_->GetLinearDamping();
  return bodyDef_.linearDamping;
}

//--------------------------------------------------------------------
void BodyComponent::SetLinearDamping(double d)
{
  if (body_)
    body_->SetLinearDamping((float32)d);
  else
    bodyDef_.linearDamping = (float32)d;
}

//--------------------------------------------------------------------
double BodyComponent::GetAngularDamping() const
{
  if (body_)
    return body_->GetAngularDamping();
  return bodyDef_.angularDamping;
}

//--------------------------------------------------------------------
void BodyComponent::SetAngularDamping(double d)
{
  if (body_)
    body_->SetAngularDamping((float32)d);
  else
    bodyDef_.angularDamping = (float32)d;
}

//--------------------------------------------------------------------
bool BodyComponent::GetAllowSleep() const
{
  if (body_)
    return body_->IsSleepingAllowed();
  return bodyDef_.allowSleep;
}

//--------------------------------------------------------------------
void BodyComponent::SetAllowSleep(bool allow)
{
  if (body_)
    body_->SetSleepingAllowed(allow);
  else
    bodyDef_.allowSleep = allow;
}

//--------------------------------------------------------------------
bool BodyComponent::IsAwake() const
{
  if (body_)
    return body_->IsAwake();
  return bodyDef_.allowSleep;
}

//--------------------------------------------------------------------
void BodyComponent::SetAwake(bool isAwake)
{
  if (body_)
    body_->SetAwake(isAwake);
  else
    bodyDef_.awake = isAwake;
}

//--------------------------------------------------------------------
bool BodyComponent::IsFixedRotation() const
{
  if (body_)
    return body_->IsFixedRotation();
  return bodyDef_.fixedRotation;
}

//--------------------------------------------------------------------
void BodyComponent::SetFixedRotation(bool isFixedRotation)
{
  if (body_)
    body_->SetFixedRotation(isFixedRotation);
  else
    bodyDef_.fixedRotation = isFixedRotation;
}

//--------------------------------------------------------------------
bool BodyComponent::IsBullet() const
{
  if (body_)
    return body_->IsBullet();
  return bodyDef_.bullet;
}

//--------------------------------------------------------------------
void BodyComponent::SetBullet(bool isBullet)
{
  if (body_)
    body_->SetBullet(isBullet);
  else
    bodyDef_.bullet = isBullet;
}

//--------------------------------------------------------------------
bool BodyComponent::IsActive() const
{
  if (body_)
    return body_->IsActive();
  return bodyDef_.active;
}

//--------------------------------------------------------------------
void BodyComponent::SetActive(bool isActive)
{
  if (body_)
    body_->SetActive(isActive);
  else
    bodyDef_.active = isActive;
}

//--------------------------------------------------------------------
bool BodyComponent::IsPropertySet(const std::string& name) const
{
  if (name == "linearVelocityX")
  {
    if (GetLinearVelocityX() == 0)
      return false;
  }
  else if (name == "linearVelocityY")
  {
    if (GetLinearVelocityY() == 0)
      return false;
  }
  return ComponentT<BodyComponent>::IsPropertySet(name);
}

//--------------------------------------------------------------------
void BodyComponent::PreUpdate()
{
  Entity* entity = GetEntity();
  if (!entity)
    return;
  TransformComponent* transform = entity->GetTransformComponent();
  if (!transform)
    return;
  
  //Physics* physics = Maxx::Get()->GetPhysics();
  // SetX(physics->PixelsToMeters(transform->GetX()));
  // SetY(physics->PixelsToMeters(transform->GetY()));
  // SetAngle(transform->GetAngle());

  preUpdateBodyX_ = GetX();
  preUpdateBodyY_ = GetY();
  preUpdateBodyAngle_ = GetAngle();
  preUpdateTransformX_ = transform->GetX();
  preUpdateTransformY_ = transform->GetY();
  preUpdateTransformAngle_ = transform->GetAngle();
  wasPreUpdated_ = true;
}

//--------------------------------------------------------------------
void BodyComponent::PostUpdate()
{
  if (!wasPreUpdated_)
    return;
  wasPreUpdated_ = false;
    
  Entity* entity = GetEntity();
  if (!entity)
    return;
  TransformComponent* transform = entity->GetTransformComponent();
  if (!transform)
    return;

  Physics* physics = Maxx::Get()->GetPhysics();
   
  // If our transform was moved during the update then
  double postUpdateTransformX = transform->GetX();
  double postUpdateTransformY = transform->GetY();
  double postUpdateTransformAngle = transform->GetAngle();
  if (postUpdateTransformX != preUpdateTransformX_ ||
      postUpdateTransformY != preUpdateTransformY_)
  {
    // ignore physics change in position and snap to transform.
    SetX(physics->PixelsToMeters(postUpdateTransformX));
    SetY(physics->PixelsToMeters(postUpdateTransformY));
  }
  else
  {
    // If we moved (by physics) then set the transform to our new position.
    double postUpdateBodyX = GetX();
    double postUpdateBodyY = GetY();
    if (postUpdateBodyX != preUpdateBodyX_ || 
        postUpdateBodyY != preUpdateBodyY_)
    {
      transform->SetX(transform->GetX() + physics->MetersToPixels(postUpdateBodyX - preUpdateBodyX_));
      transform->SetY(transform->GetY() + physics->MetersToPixels(postUpdateBodyY - preUpdateBodyY_));
    }
  }

  if (postUpdateTransformAngle != preUpdateTransformAngle_)
  {
    SetAngle(postUpdateTransformAngle);
  }
  else
  {
    double postUpdateBodyAngle = GetAngle();
    transform->SetAngle(transform->GetAngle() + (postUpdateBodyAngle - preUpdateBodyAngle_));
  }
  
  // MarkPropertyAsSet("linearVelocityX");
  // MarkPropertyAsSet("linearVelocityY");
  // MarkPropertyAsSet("angularVelocityY");
}

//--------------------------------------------------------------------
bool BodyComponent::BuildBody()
{
  Entity* entity = GetEntity();
  if (!entity)
    return false;

  b2World* world = Maxx::Get()->GetPhysics()->Getb2World();
  if (!world)
  {
    Trace("BodyComponent::BuildBody: ERROR: No world found.\n");
    return false;
  }

  TransformComponent* transform = entity->GetTransformComponent();
  if (!transform)
  {
    Trace("BodyComponent::BuildBody: ERROR: No transform found.\n");
    return false;
  }
 
  if (body_)
    world->DestroyBody(body_);
  
  Physics* physics = Maxx::Get()->GetPhysics();
  bodyDef_.position.x = physics->PixelsToMeters(transform->GetX());
  bodyDef_.position.y = physics->PixelsToMeters(transform->GetY());
  bodyDef_.angle = transform->GetAngle();
  body_ = world->CreateBody(&bodyDef_);
  if (!body_)
    return false;

//  SetX(physics->PixelsToMeters(transform->GetX()));
//  SetY(physics->PixelsToMeters(transform->GetY()));
//  SetAngle(transform->GetAngle());
  
  preUpdateBodyX_ = GetX();
  preUpdateBodyY_ = GetY();
  preUpdateBodyAngle_ = GetAngle();
  preUpdateTransformX_ = transform->GetX();
  preUpdateTransformY_ = transform->GetY();

  return true;
}

} // Maxx