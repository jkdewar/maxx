#pragma once

#include "engine/component.h"
#include "engine/component-t.h"
#include "engine/component-factory.h"
#include "cross-platform/box2d-headers.h"
//#include <string>

namespace Maxx {

//--------------------------------------------------------------------
class BodyComponent : public ComponentT<BodyComponent>
{
public:
  BodyComponent();
  virtual ~BodyComponent();
  
  bool Init();
  
  std::string GetType() const;
  void SetType(const std::string&);
  double GetLinearVelocityX() const;
  void SetLinearVelocityX(double);
  double GetLinearVelocityY() const;
  void SetLinearVelocityY(double);
  double GetAngularVelocity() const;
  void SetAngularVelocity(double);
  double GetLinearDamping() const;
  void SetLinearDamping(double);
  double GetAngularDamping() const;
  void SetAngularDamping(double);
  bool GetAllowSleep() const;
  void SetAllowSleep(bool);
  bool IsAwake() const;
  void SetAwake(bool);
  bool IsFixedRotation() const;
  void SetFixedRotation(bool);
  bool IsBullet() const;
  void SetBullet(bool);
  bool IsActive() const;
  void SetActive(bool);
  
  virtual bool IsPropertySet(const std::string& name) const;
  
  b2Body* Getb2Body() { return body_; }
  const b2Body* Getb2Body() const { return body_; }
  
  virtual void PreUpdate();
  virtual void PostUpdate();
  
  COMPONENT_TYPE_STRING("BodyComponent")
  
  BEGIN_COMPONENT_PROPERTIES(BodyComponent)
    COMPONENT_PROPERTY_GETTER_SETTER("type", GetType, SetType)
    COMPONENT_PROPERTY_GETTER_SETTER("linearVelocityX", GetLinearVelocityX, SetLinearVelocityX)
    COMPONENT_PROPERTY_GETTER_SETTER("linearVelocityY", GetLinearVelocityY, SetLinearVelocityY)
    COMPONENT_PROPERTY_GETTER_SETTER("angularVelocity", GetAngularVelocity, SetAngularVelocity)
    COMPONENT_PROPERTY_GETTER_SETTER("linearDamping", GetLinearDamping, SetLinearDamping)
    COMPONENT_PROPERTY_GETTER_SETTER("angularDamping", GetAngularDamping, SetAngularDamping)
    COMPONENT_PROPERTY_GETTER_SETTER("allowSleep", GetAllowSleep, SetAllowSleep)
    COMPONENT_PROPERTY_GETTER_SETTER("isAwake", IsAwake, SetAwake)
    COMPONENT_PROPERTY_GETTER_SETTER("isFixedRotation", IsFixedRotation, SetFixedRotation)
    COMPONENT_PROPERTY_GETTER_SETTER("isBullet", IsBullet, SetBullet)
    COMPONENT_PROPERTY_GETTER_SETTER("isActive", IsActive, SetActive)
  END_COMPONENT_PROPERTIES()
  
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("type", GetType, SetType)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("linearVelocityX", GetLinearVelocityX, SetLinearVelocityX)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("linearVelocityY", GetLinearVelocityY, SetLinearVelocityY)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("angularVelocity", GetAngularVelocity, SetAngularVelocity)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("linearDamping", GetLinearDamping, SetLinearDamping)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("angularDamping", GetAngularDamping, SetAngularDamping)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("allowSleep", GetAllowSleep, SetAllowSleep)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("isAwake", IsAwake, SetAwake)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("isFixedRotation", IsFixedRotation, SetFixedRotation)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("isBullet", IsBullet, SetBullet)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("isActive", IsActive, SetActive)
  
private:
  double GetX() const;
  void SetX(double);
  double GetY() const;
  void SetY(double);
  double GetAngle() const;
  void SetAngle(double);

private:
  bool BuildBody();

private:
  b2Body* body_;
  b2BodyDef bodyDef_;
  double preUpdateBodyX_;
  double preUpdateBodyY_;
  double preUpdateBodyAngle_;
  double preUpdateTransformX_;
  double preUpdateTransformY_;
  double preUpdateTransformAngle_;
  bool wasPreUpdated_;
};

} // Maxx
