#pragma once

#include "components/body-component.h"
#include "components/fixture-component.h"
#include "components/image-component.h"
#include "components/script-component.h"
#include "components/transform-component.h"
#include "components/revolute-joint-component.h"
#include "components/distance-joint-component.h"
#include "components/rope-joint-component.h"
#include "components/particle-emitter-component.h"
#include "components/text-component.h"
namespace Maxx {
COMPONENT_FACTORY(BodyComponent)
COMPONENT_FACTORY(FixtureComponent)
COMPONENT_FACTORY(ImageComponent)
COMPONENT_FACTORY(ScriptComponent)
COMPONENT_FACTORY(TransformComponent)
COMPONENT_FACTORY(RevoluteJointComponent)
COMPONENT_FACTORY(DistanceJointComponent)
COMPONENT_FACTORY(RopeJointComponent)
COMPONENT_FACTORY(ParticleEmitterComponent)
COMPONENT_FACTORY(TextComponent)
}