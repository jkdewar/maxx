#include "distance-joint-component.h"
#include "components/body-component.h"
#include "components/transform-component.h"
#include "engine/entity.h"
#include "engine/maxx.h"
#include "physics/physics.h"

namespace Maxx {
  
//--------------------------------------------------------------------
DistanceJointComponent::DistanceJointComponent()
  : distanceJointDef_()
  , distanceJoint_(0)
{
}

//--------------------------------------------------------------------
DistanceJointComponent::~DistanceJointComponent()
{
  b2World* world = Maxx::Get()->GetPhysics()->Getb2World();
  Entity* entityA = Entity::Lookup(entityA_);
  Entity* entityB = Entity::Lookup(entityB_);
  if (distanceJoint_ && entityA && entityB)
    world->DestroyJoint(distanceJoint_);
  distanceJoint_ = 0;
}

//--------------------------------------------------------------------
double DistanceJointComponent::GetLocalAnchorAX() const
{
  Physics* physics = Maxx::Get()->GetPhysics();
  return physics->MetersToPixels(distanceJointDef_.localAnchorA.x);
}

//--------------------------------------------------------------------
void DistanceJointComponent::SetLocalAnchorAX(double v)
{
  Physics* physics = Maxx::Get()->GetPhysics();
  distanceJointDef_.localAnchorA.x = physics->PixelsToMeters(v);
}

//--------------------------------------------------------------------
double DistanceJointComponent::GetLocalAnchorAY() const
{
  Physics* physics = Maxx::Get()->GetPhysics();
  return physics->MetersToPixels(distanceJointDef_.localAnchorA.y);
}

//--------------------------------------------------------------------
void DistanceJointComponent::SetLocalAnchorAY(double v)
{
  Physics* physics = Maxx::Get()->GetPhysics();
  distanceJointDef_.localAnchorA.y = physics->PixelsToMeters(v);
}

//--------------------------------------------------------------------
double DistanceJointComponent::GetLocalAnchorBX() const
{
  Physics* physics = Maxx::Get()->GetPhysics();
  return physics->MetersToPixels(distanceJointDef_.localAnchorB.x);
}

//--------------------------------------------------------------------
void DistanceJointComponent::SetLocalAnchorBX(double v)
{
  Physics* physics = Maxx::Get()->GetPhysics();
  distanceJointDef_.localAnchorB.x = physics->PixelsToMeters(v);
}

//--------------------------------------------------------------------
double DistanceJointComponent::GetLocalAnchorBY() const
{
  Physics* physics = Maxx::Get()->GetPhysics();
  return physics->MetersToPixels(distanceJointDef_.localAnchorB.y);
}

//--------------------------------------------------------------------
void DistanceJointComponent::SetLocalAnchorBY(double v)
{
  Physics* physics = Maxx::Get()->GetPhysics();
  distanceJointDef_.localAnchorB.y = physics->PixelsToMeters(v);
}

//--------------------------------------------------------------------
double DistanceJointComponent::GetLength() const
{
  return Maxx::Get()->GetPhysics()->MetersToPixels(distanceJointDef_.length);
}

//--------------------------------------------------------------------
void DistanceJointComponent::SetLength(double v)
{
  v = Maxx::Get()->GetPhysics()->PixelsToMeters(v);
  distanceJointDef_.length = v;
  if (distanceJoint_)
  distanceJoint_->SetLength(v);
}

//--------------------------------------------------------------------
double DistanceJointComponent::GetFrequencyHz() const
{
  return distanceJointDef_.frequencyHz;
}

//--------------------------------------------------------------------
void DistanceJointComponent::SetFrequencyHz(double v)
{
  distanceJointDef_.frequencyHz = v;
  if (distanceJoint_)
    distanceJoint_->SetFrequency(v);
}

//--------------------------------------------------------------------
double DistanceJointComponent::GetDampingRatio() const
{
  return distanceJointDef_.dampingRatio;
}

//--------------------------------------------------------------------
void DistanceJointComponent::SetDampingRatio(double v)
{
  distanceJointDef_.dampingRatio = v;
  if (distanceJoint_)
    distanceJoint_->SetDampingRatio(v);
}

//--------------------------------------------------------------------
bool DistanceJointComponent::Init()
{
  Assert(!distanceJoint_);
  if (distanceJoint_)
    return false;

  Physics* physics = Maxx::Get()->GetPhysics();
  Entity* entityA = Entity::Lookup(entityA_);
  Entity* entityB = Entity::Lookup(entityB_);
  Assert(entityA);
  Assert(entityB);
  if (!entityA || !entityB)
    return false;
  BodyComponent* bodyComponentA = entityA->GetComponentOfType<BodyComponent>();
  BodyComponent* bodyComponentB = entityB->GetComponentOfType<BodyComponent>();
  b2Body* bodyA = bodyComponentA->Getb2Body();
  b2Body* bodyB = bodyComponentB->Getb2Body();
  Assert(bodyA && bodyB);
  distanceJointDef_.bodyA = bodyA;
  distanceJointDef_.bodyB = bodyB;
//  TransformComponent* transformComponentA = entityA->GetTransformComponent();
//  TransformComponent* transformComponentB = entityB->GetTransformComponent();
//  distanceJointDef_.Initialize(bodyA, bodyB, 
//                          b2Vec2(physics->PixelsToMeters(transformComponentA->GetX()), 
//                                 physics->PixelsToMeters(transformComponentA->GetY())),
//                          b2Vec2(physics->PixelsToMeters(transformComponentB->GetX()), 
//                                 physics->PixelsToMeters(transformComponentB->GetY())));
  b2World* world = Maxx::Get()->GetPhysics()->Getb2World();
  b2Joint* joint = world->CreateJoint(&distanceJointDef_);
  distanceJoint_ = dynamic_cast<b2DistanceJoint*>(joint);
  Assert(joint);
  Assert(distanceJoint_);
  if (!distanceJoint_)
    return false;
  return true;
}

} // Maxx
