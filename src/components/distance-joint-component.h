#pragma once

#include "engine/component.h"
#include "engine/component-t.h"
#include "engine/component-factory.h"
#include "cross-platform/box2d-headers.h"

namespace Maxx {

//--------------------------------------------------------------------
class DistanceJointComponent : public ComponentT<DistanceJointComponent>
{
public:
  DistanceJointComponent();
  virtual ~DistanceJointComponent();

  const std::string& GetEntityA() const { return entityA_; }
  void SetEntityA(const std::string& entityA) { entityA_ = entityA; }
  const std::string& GetEntityB() const { return entityB_; }
  void SetEntityB(const std::string& entityB) { entityB_ = entityB; }
  double GetLocalAnchorAX() const;
  void SetLocalAnchorAX(double);
  double GetLocalAnchorAY() const;
  void SetLocalAnchorAY(double);
  double GetLocalAnchorBX() const;
  void SetLocalAnchorBX(double);
  double GetLocalAnchorBY() const;
  void SetLocalAnchorBY(double);
  double GetLength() const;
  void SetLength(double);
  double GetFrequencyHz() const;
  void SetFrequencyHz(double);
  double GetDampingRatio() const;
  void SetDampingRatio(double);
  
  virtual bool Init();
  
  COMPONENT_TYPE_STRING("DistanceJointComponent")
  
  BEGIN_COMPONENT_PROPERTIES(DistanceJointComponent)
    COMPONENT_PROPERTY_GETTER_SETTER("entityA", GetEntityA, SetEntityA)
    COMPONENT_PROPERTY_GETTER_SETTER("entityB", GetEntityB, SetEntityB)
    COMPONENT_PROPERTY_GETTER_SETTER("localAnchorAX", GetLocalAnchorAX, SetLocalAnchorAX)
    COMPONENT_PROPERTY_GETTER_SETTER("localAnchorAY", GetLocalAnchorAY, SetLocalAnchorAY)
    COMPONENT_PROPERTY_GETTER_SETTER("localAnchorBX", GetLocalAnchorBX, SetLocalAnchorBX)
    COMPONENT_PROPERTY_GETTER_SETTER("localAnchorBY", GetLocalAnchorBY, SetLocalAnchorBY)
    COMPONENT_PROPERTY_GETTER_SETTER("length", GetLength, SetLength)
    COMPONENT_PROPERTY_GETTER_SETTER("frequencyHz", GetFrequencyHz, SetFrequencyHz)
    COMPONENT_PROPERTY_GETTER_SETTER("dampingRatio", GetDampingRatio, SetDampingRatio)
  END_COMPONENT_PROPERTIES()
  
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("entityA", GetEntityA, SetEntityA)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("entityB", GetEntityB, SetEntityB)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("localAnchorAX", GetLocalAnchorAX, SetLocalAnchorAX)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("localAnchorAY", GetLocalAnchorAY, SetLocalAnchorAY)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("localAnchorBX", GetLocalAnchorBX, SetLocalAnchorBX)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("localAnchorBY", GetLocalAnchorBY, SetLocalAnchorBY)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("length", GetLength, SetLength)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("frequencyHz", GetFrequencyHz, SetFrequencyHz)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("dampingRatio", GetDampingRatio, SetDampingRatio)

private:
  std::string entityA_;
  std::string entityB_;
  b2DistanceJointDef distanceJointDef_;
  b2DistanceJoint* distanceJoint_;
};

} // Maxx
