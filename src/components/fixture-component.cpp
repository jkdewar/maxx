#include "fixture-component.h"
#include "body-component.h"
#include "engine/entity.h"
#include "physics/physics.h"
#include "physics/contact-listener.h"
#include "engine/maxx.h"

namespace Maxx {
  
//--------------------------------------------------------------------
FixtureComponent::FixtureComponent()
  : fixture_(0)
  , fixtureDef_()
{
}

//--------------------------------------------------------------------
FixtureComponent::~FixtureComponent()
{
  DestroyFixture();
}

//--------------------------------------------------------------------
PropertyTable FixtureComponent::GetShape() const
{
  PropertyTable table;
  const b2Shape* shape = fixtureDef_.shape;
  if (!shape)
    return table;
  Physics* physics = Maxx::Get()->GetPhysics();
  if (shape->GetType() == b2Shape::e_circle)
  {
    const b2CircleShape* circle = dynamic_cast<const b2CircleShape*>(shape);
    table.Insert("type", "circle");
    table.Insert("x", circle->m_p.x);
    table.Insert("y", circle->m_p.y);
    table.Insert("radius", physics->MetersToPixels(circle->m_radius));
  }
  else if (shape->GetType() == b2Shape::e_polygon)
  {
    const b2PolygonShape* polygon = dynamic_cast<const b2PolygonShape*>(shape);
    table.Insert("type", "polygon");
    table.Insert("verts", PropertyTable());
    PropertyTable* verts = table.GetTable("verts");
    if (verts)
    {
      int vertCount = polygon->GetVertexCount();
      for (int i = 0; i < vertCount; ++i)
      {
        const b2Vec2& b2vert = polygon->GetVertex(i);
        PropertyTable vert;
        vert.Insert("x", physics->MetersToPixels(b2vert.x));
        vert.Insert("y", physics->MetersToPixels(b2vert.y));
        verts->Insert(i + 1, vert);
      }
    }
  }
  else if (shape->GetType() == b2Shape::e_chain)
  {
    // TODO
    int bp=0; ++bp;
  }
  return table;
}

//--------------------------------------------------------------------
void FixtureComponent::SetShape(const PropertyTable& shape)
{
  bool recreate = false;
  if (fixture_ || fixtureDef_.shape)
  {
    DestroyFixture();
    recreate = true;
  }
  
  Physics* physics = Maxx::Get()->GetPhysics();
  const std::string& type = shape.GetString("type", "circle");
  if (type == "circle")
  {
    b2CircleShape* circle = new b2CircleShape;
    circle->m_radius = (float32)physics->PixelsToMeters(shape.GetNumber("radius", 10.0));
    circle->m_p.x = (float32)physics->PixelsToMeters(shape.GetNumber("x", 0.0));
    circle->m_p.y = (float32)physics->PixelsToMeters(shape.GetNumber("y", 0.0));
    fixtureDef_.shape = circle;
  }
  else if (type == "polygon")
  {
    const PropertyTable* vertsTable = shape.GetTable("verts");
    b2PolygonShape* polygon = new b2PolygonShape;
    polygon->m_radius=(float32)physics->PixelsToMeters(shape.GetNumber("radius",3.0));
    if (vertsTable)
    {
      std::vector<b2Vec2> verts(vertsTable->mapFromInt.size());
      int index = 1;
      for (PropertyTable::MapFromInt::const_iterator
           i = vertsTable->mapFromInt.begin(); i != vertsTable->mapFromInt.end(); ++i, ++index)
      {
        const PropertyTable* vertTable = i->second.GetTablePtr();
        if (vertTable)
        {
          double x = physics->PixelsToMeters(vertTable->GetNumber("x", 0.0));
          double y = physics->PixelsToMeters(vertTable->GetNumber("y", 0.0));
          //Trace("index=%d i->first=%d\n", index, (int)i->first);
          verts[i->first - 1] = b2Vec2((float32)x, (float32)y);
        }
      }
      polygon->Set(&verts[0], (int)verts.size());
    }
    fixtureDef_.shape = polygon;
  }
  else if (type == "chain")
  {
    const PropertyTable* vertsTable = shape.GetTable("verts");
    b2ChainShape* chain = new b2ChainShape;
    chain->m_radius=(float32)physics->PixelsToMeters(shape.GetNumber("radius",3.0));
    if (vertsTable)
    {
      std::vector<b2Vec2> verts(vertsTable->mapFromInt.size());
      for (PropertyTable::MapFromInt::const_iterator
           i = vertsTable->mapFromInt.begin(); 
           i != vertsTable->mapFromInt.end(); ++i)
      {
        const PropertyTable* vertTable = i->second.GetTablePtr();
        if (vertTable)
        {
          double x = physics->PixelsToMeters(vertTable->GetNumber("x", 0.0));
          double y = physics->PixelsToMeters(vertTable->GetNumber("y", 0.0));
          //Trace("index=%d i->first=%d\n", index, (int)i->first);
          verts[i->first - 1] = b2Vec2((float32)x, (float32)y);
        }
      }
      chain->CreateChain(&verts[0], (int)verts.size());
    }
    fixtureDef_.shape = chain;    
  }
  else
  {
    Trace("FixtureComponent::SetShape: Unrecognized type '%s'.\n", type.c_str());
    return;
  }
  
  if (recreate)
  {
    bool ok = CreateFixture();
    if (!ok)
      Trace("ERROR: CreateFixture failed\n");
  }
}

//--------------------------------------------------------------------
double FixtureComponent::GetFriction() const
{
  if (fixture_)
    return fixture_->GetFriction();
  return fixtureDef_.friction;
}

//--------------------------------------------------------------------
void FixtureComponent::SetFriction(double friction)
{
  if (fixture_)
    fixture_->SetFriction((float32)friction);
  else
    fixtureDef_.friction = (float32)friction;
}

//--------------------------------------------------------------------
double FixtureComponent::GetRestitution() const
{
  if (fixture_)
    return fixture_->GetRestitution();
  return fixtureDef_.restitution;
}

//--------------------------------------------------------------------
void FixtureComponent::SetRestitution(double restitution)
{
  if (fixture_)
    fixture_->SetRestitution((float32)restitution);
  else
    fixtureDef_.restitution = (float32)restitution;
}

//--------------------------------------------------------------------
double FixtureComponent::GetDensity() const
{
  if (fixture_)
    return fixture_->GetDensity();
  return fixtureDef_.density;
}

//--------------------------------------------------------------------
void FixtureComponent::SetDensity(double density)
{
  if (fixture_)
    fixture_->SetDensity((float32)density);
  else
    fixtureDef_.density = (float32)density;
}

//--------------------------------------------------------------------
bool FixtureComponent::IsSensor() const
{
  if (fixture_)
    return fixture_->IsSensor();
  return fixtureDef_.isSensor;
}

//--------------------------------------------------------------------
void FixtureComponent::SetSensor(bool isSensor)
{
  if (fixture_)
  {
    Trace("FixtureComponent::SetSensor: Cannot modify sensor after the fixture has been initialized.\n");
    return;
  }
  fixtureDef_.isSensor = isSensor;
}

//--------------------------------------------------------------------
uint16 PropertyTableToCategoryBits(const PropertyTable& propTable)
{
  uint16 result = 0;
  for (PropertyTable::MapFromInt::const_iterator i = propTable.mapFromInt.begin(); i != propTable.mapFromInt.end(); ++i)
  {
    int j = (int)i->second.GetNumber();
    result += 1 << (j-1);
  }
  return result;
}

//--------------------------------------------------------------------
PropertyTable CategoryBitsToPropertyTable(uint16 categoryBits)
{
  PropertyTable result;
  int j = 1;
  for (int i = 0; i < 16; ++i)
    if (categoryBits & (1 << i))
      result.Insert(j++, i+1);
  return result;
}

//--------------------------------------------------------------------
PropertyTable FixtureComponent::GetContactCategory() const
{
  if (fixture_)
    return CategoryBitsToPropertyTable(fixture_->GetFilterData().categoryBits);
  return CategoryBitsToPropertyTable(fixtureDef_.filter.categoryBits);
}

//--------------------------------------------------------------------
void FixtureComponent::SetContactCategory(const PropertyTable& contactCategory)
{
  if (fixture_)
  {
    b2Filter b2f = fixture_->GetFilterData();
    b2f.categoryBits = PropertyTableToCategoryBits(contactCategory);
    fixture_->SetFilterData(b2f);
  }
  else
    fixtureDef_.filter.categoryBits = PropertyTableToCategoryBits(contactCategory);
}

//--------------------------------------------------------------------
PropertyTable FixtureComponent::GetContactCategoryMask() const
{
  if (fixture_)
    return CategoryBitsToPropertyTable(fixture_->GetFilterData().maskBits);
  return CategoryBitsToPropertyTable(fixtureDef_.filter.maskBits);
}

//--------------------------------------------------------------------
void FixtureComponent::SetContactCategoryMask(const PropertyTable& contactCategoryMask)
{
  if (fixture_)
  {
    b2Filter b2f = fixture_->GetFilterData();
    b2f.maskBits = PropertyTableToCategoryBits(contactCategoryMask);
    fixture_->SetFilterData(b2f);
  }
  else
    fixtureDef_.filter.maskBits = PropertyTableToCategoryBits(contactCategoryMask);
}

//--------------------------------------------------------------------
bool FixtureComponent::Init()
{
  return CreateFixture();
}

//--------------------------------------------------------------------
bool FixtureComponent::CreateFixture()
{
  Entity* entity = GetEntity();
  if (!entity)
  {
    Trace("FixtureComponent::Init: ERROR: Null entity.\n");
    return false;
  }
  BodyComponent* bodyComponent = entity->GetComponentOfType<BodyComponent>();
  if (!bodyComponent)
  {
    Trace("FixtureComponent::Init: ERROR: BodyComponent not found.\n");
    return false;
  }
  b2Body* body = bodyComponent->Getb2Body();
  if (!body)
  {
    Trace("FixtureComponent::Init: ERROR: BodyComponent has a null body. (Perhaps it was not initialized?)\n");
    return false;
  }
  if (fixture_)
  {
    Trace("FixtureComponent::Init: WARNING: Fixture already created.\n");
    return true;
  }
  fixture_ = body->CreateFixture(&fixtureDef_);
  if (!fixture_)
  {
    Trace("FixtureComponent::Init: ERROR: Failed to create fixture.\n");
    return false;
  }
  
  Maxx::Get()->GetPhysics()->GetContactListener()->RegisterFixtureComponent(this);
  
  return true;
}

//--------------------------------------------------------------------
void FixtureComponent::DestroyFixture()
{
  if (fixture_)
    Maxx::Get()->GetPhysics()->GetContactListener()->UnregisterFixtureComponent(this);
  
  Entity* entity = GetEntity();
  if (entity)
  {
    BodyComponent* bodyComponent = entity->GetComponentOfType<BodyComponent>();
    if (bodyComponent)
    {
      b2Body* body = bodyComponent->Getb2Body();
      if (fixture_)
        body->DestroyFixture(fixture_);
    }
  }
  fixture_ = 0;
  
  delete fixtureDef_.shape; fixtureDef_.shape = 0;
}

} // Maxx
