#pragma once

#include "engine/component.h"
#include "engine/component-t.h"
#include "engine/component-factory.h"
#include "cross-platform/box2d-headers.h"

namespace Maxx {

//--------------------------------------------------------------------
class FixtureComponent : public ComponentT<FixtureComponent>
{
public:
  FixtureComponent();
  virtual ~FixtureComponent();

  PropertyTable GetShape() const;
  void SetShape(const PropertyTable& shape);
  double GetFriction() const;
  void SetFriction(double);
  double GetRestitution() const;
  void SetRestitution(double);
  double GetDensity() const;
  void SetDensity(double);
  bool IsSensor() const;
  void SetSensor(bool);
  PropertyTable GetContactCategory() const;
  void SetContactCategory(const PropertyTable&);
  PropertyTable GetContactCategoryMask() const;
  void SetContactCategoryMask(const PropertyTable&);
  
  virtual bool Init();
  
  COMPONENT_TYPE_STRING("FixtureComponent")
  
  BEGIN_COMPONENT_PROPERTIES(FixtureComponent)
    COMPONENT_PROPERTY_GETTER_SETTER("shape", GetShape, SetShape)
    COMPONENT_PROPERTY_GETTER_SETTER("friction", GetFriction, SetFriction)
    COMPONENT_PROPERTY_GETTER_SETTER("restitution", GetRestitution, SetRestitution)
    COMPONENT_PROPERTY_GETTER_SETTER("density", GetDensity, SetDensity)
    COMPONENT_PROPERTY_GETTER_SETTER("sensor", IsSensor, SetSensor)
    COMPONENT_PROPERTY_GETTER_SETTER("contactCategory", GetContactCategory, SetContactCategory)
    COMPONENT_PROPERTY_GETTER_SETTER("contactCategoryMask", GetContactCategoryMask, SetContactCategoryMask)
  END_COMPONENT_PROPERTIES()
  
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("shape", GetShape, SetShape)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("friction", GetFriction, SetFriction)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("restitution", GetRestitution, SetRestitution)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("density", GetDensity, SetDensity)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("sensor", IsSensor, SetSensor)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("contactCategory", GetContactCategory, SetContactCategory)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("contactCategoryMask", GetContactCategoryMask, SetContactCategoryMask)
  
  b2Fixture* Getb2Fixture() { return fixture_; }

private:
  bool CreateFixture();
  void DestroyFixture();

private:
  b2FixtureDef fixtureDef_;
  b2Fixture* fixture_;
};

} // Maxx
