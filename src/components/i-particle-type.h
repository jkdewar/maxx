#pragma once
#include "components/particle-emitter-component.h"

namespace Maxx {

class PropertyTable;
class IMessageReceiver;
  
//--------------------------------------------------------------------
class IParticleType
{
public:
  virtual ~IParticleType() {}
  virtual void InitParticle(ParticleEmitterComponent::Particle& particle) = 0;
  virtual void UpdateParticles(double dt, ParticleEmitterComponent::ParticleList& particles) = 0;
  virtual void ModifyParticles(ParticleEmitterComponent::ParticleList& particles, PropertyTable& msg, IMessageReceiver* sender) = 0;
};
  
} // Maxx
