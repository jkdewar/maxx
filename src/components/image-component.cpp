#include "image-component.h"
#include "engine/entity.h"
#include "components/transform-component.h"
#include "graphics/image-manager.h"
#include "engine/maxx.h"
#include "cross-platform/file-sys.h"

namespace Maxx {


//--------------------------------------------------------------------
ImageComponent::ImageComponent()
  : filename_("")
  , drawImageParams_()
  , fixRotation_(false)
  , visible_(true)
  , inited_(false)
{
}

//--------------------------------------------------------------------
ImageComponent::~ImageComponent()
{
}

//--------------------------------------------------------------------
bool ImageComponent::Init()
{
  inited_ = true;
  SetFilename(filename_); // to load the image
  return true;
}

//--------------------------------------------------------------------
void ImageComponent::SetFilename(const std::string& filename)
{
  filename_ = filename;
  if (!inited_)
    return;
  if (filename.empty())
  {
    drawImageParams_.SetImage(0);
  }
  else if (GetExtension(filename) == "sheet")
  {
    // load image sheet
    const ImageSheet* imageSheet = Maxx::Get()->GetGraphics()->GetImageManager().GetImageSheet(filename);
    drawImageParams_.SetImageSheet(imageSheet);
    if (!imageSheet)
      Trace("ImageComponent: Error loading imagesheet '%s'\n", filename.c_str());
  }
  else
  {
    // load image
    const Image* image = Maxx::Get()->GetGraphics()->GetImageManager().GetImage(filename);
    drawImageParams_.SetImage(image);
    if (!image)
      Trace("ImageComponent: Error loading image '%s'\n", filename.c_str());
  }
}

//--------------------------------------------------------------------
void ImageComponent::Draw() const
{
  if (!drawImageParams_.GetImage())
    return;
  if (!visible_)
    return;
  Entity* entity = GetEntity();
  if (!entity)
    return;
  DrawImageParams& params = const_cast<ImageComponent*>(this)->drawImageParams_;
  TransformComponent* transform = entity->GetTransformComponent();
  /*#if defined(__IPHONEOS__)
  if (transform && transform->GetY() < -100.0)
      return;
  #endif*/
  params.SetX(transform ? transform->GetX() : 0.0);
  params.SetY(transform ? transform->GetY() : 0.0);
  if (!GetFixRotation())
    params.SetRotation(transform ? transform->GetAngle() : 0.0);
  Maxx::Get()->GetGraphics()->DrawImage(&params, /*deleteParamsForMe=*/false);
}

} // Maxx
