#pragma once

#include "engine/component.h"
#include "engine/component-t.h"
#include "engine/component-factory.h"
#include "graphics/graphics.h"
#include "graphics/image.h"

// double x, y;
// double anchorX, anchorY;
// double offsetX, offsetY;
// double scaleX, scaleY;
// double rotation; // in radians
// bool flipU, flipV;
// double r, g, b, alpha;
// bool filter;

namespace Maxx {

//--------------------------------------------------------------------
class ImageComponent : public ComponentT<ImageComponent>
{
public:
  ImageComponent();
  virtual ~ImageComponent();
  
  virtual bool Init();

  const std::string& GetFilename() const { return filename_; }
  void SetFilename(const std::string& filename);
  const std::string& GetSheetImageName() const { return drawImageParams_.GetSheetImageName(); }
  void SetSheetImageName(const std::string& v) { drawImageParams_.SetSheetImageName(v); }
    
  double GetX() const { return drawImageParams_.GetX(); }
  void SetX(double x) { drawImageParams_.SetX(x); }
  double GetY() const { return drawImageParams_.GetY(); }
  void SetY(double y) { drawImageParams_.SetY(y); }
  double GetAnchorX() const { return drawImageParams_.GetAnchorX(); }
  void SetAnchorX(double x) { drawImageParams_.SetAnchorX(x); }
  double GetAnchorY() const { return drawImageParams_.GetAnchorY(); }
  void SetAnchorY(double x) { drawImageParams_.SetAnchorY(x); }
  double GetOffsetX() const { return drawImageParams_.GetOffsetX(); }
  void SetOffsetX(double x) { drawImageParams_.SetOffsetX(x); }
  double GetOffsetY() const { return drawImageParams_.GetOffsetY(); }
  void SetOffsetY(double x) { drawImageParams_.SetOffsetY(x); }
  double GetScaleX() const { return drawImageParams_.GetScaleX(); }
  void SetScaleX(double sx) { drawImageParams_.SetScaleX(sx); }
  double GetScaleY() const { return drawImageParams_.GetScaleY(); }
  void SetScaleY(double sy) { drawImageParams_.SetScaleY(sy); }
  double GetRotation() const { return drawImageParams_.GetRotation(); }
  void SetRotation(double r) { drawImageParams_.SetRotation(r); }
  bool GetFlipU() const { return drawImageParams_.GetFlipU(); }
  void SetFlipU(bool fu) { drawImageParams_.SetFlipU(fu); }
  bool GetFlipV() const { return drawImageParams_.GetFlipV(); }
  void SetFlipV(bool fv) { drawImageParams_.SetFlipV(fv); }
  double GetOffsetU() const { return drawImageParams_.GetOffsetU(); }
  void SetOffsetU(double o) { drawImageParams_.SetOffsetU(o); }
  double GetOffsetV() const { return drawImageParams_.GetOffsetV(); }
  void SetOffsetV(double o) { drawImageParams_.SetOffsetV(o); }
  double GetR() const { return drawImageParams_.GetR(); } 
  void SetR(double r) { drawImageParams_.SetR(r); }
  double GetG() const { return drawImageParams_.GetG(); }
  void SetG(double g) { drawImageParams_.SetG(g); }
  double GetB() const { return drawImageParams_.GetB(); }
  void SetB(double b) { drawImageParams_.SetB(b); }
  double GetAlpha() const { return drawImageParams_.GetAlpha(); }
  void SetAlpha(double a) { drawImageParams_.SetAlpha(a); }
  bool GetFilter() const { return drawImageParams_.GetFilter(); }
  void SetFilter(bool f) { drawImageParams_.SetFilter(f); }
  bool GetFixRotation() const { return fixRotation_; }
  void SetFixRotation(bool fix) { fixRotation_ = fix; }
  const std::string& GetLayer() const { return drawImageParams_.GetLayer(); }
  void SetLayer(const std::string& layer) { drawImageParams_.SetLayer(layer); }
  double GetLayerZ() const { return drawImageParams_.GetLayerZ(); }
  void SetLayerZ(double layerZ) { drawImageParams_.SetLayerZ(layerZ); }
  bool GetVisible() const { return visible_; }
  void SetVisible(bool visible) { visible_ = visible; }
  const std::string& GetEffect() const { return drawImageParams_.GetEffect(); }
  void SetEffect(const std::string& effect) { drawImageParams_.SetEffect(effect); }
  
  virtual void Draw() const;
  
  COMPONENT_TYPE_STRING("ImageComponent")
  BEGIN_COMPONENT_PROPERTIES(ImageComponent)
    COMPONENT_PROPERTY_GETTER_SETTER("filename", GetFilename, SetFilename)
    COMPONENT_PROPERTY_GETTER_SETTER("sheetImageName", GetSheetImageName, SetSheetImageName)
    COMPONENT_PROPERTY_GETTER_SETTER("x", GetX, SetX)
    COMPONENT_PROPERTY_GETTER_SETTER("y", GetY, SetY)
    COMPONENT_PROPERTY_GETTER_SETTER("anchorX", GetAnchorX, SetAnchorX)
    COMPONENT_PROPERTY_GETTER_SETTER("anchorY", GetAnchorY, SetAnchorY)
    COMPONENT_PROPERTY_GETTER_SETTER("offsetX", GetOffsetX, SetOffsetX)
    COMPONENT_PROPERTY_GETTER_SETTER("offsetY", GetOffsetY, SetOffsetY)
    COMPONENT_PROPERTY_GETTER_SETTER("scaleX", GetScaleX, SetScaleX)
    COMPONENT_PROPERTY_GETTER_SETTER("scaleY", GetScaleY, SetScaleY)
    COMPONENT_PROPERTY_GETTER_SETTER("rotation", GetRotation, SetRotation)
    COMPONENT_PROPERTY_GETTER_SETTER("flipU", GetFlipU, SetFlipU)
    COMPONENT_PROPERTY_GETTER_SETTER("flipV", GetFlipV, SetFlipV)
    COMPONENT_PROPERTY_GETTER_SETTER("offsetU", GetOffsetU, SetOffsetU)
    COMPONENT_PROPERTY_GETTER_SETTER("offsetV", GetOffsetV, SetOffsetV)
    COMPONENT_PROPERTY_GETTER_SETTER("r", GetR, SetR)
    COMPONENT_PROPERTY_GETTER_SETTER("g", GetG, SetG)
    COMPONENT_PROPERTY_GETTER_SETTER("b", GetB, SetB)
    COMPONENT_PROPERTY_GETTER_SETTER("alpha", GetAlpha, SetAlpha)
    COMPONENT_PROPERTY_GETTER_SETTER("filter", GetFilter, SetFilter)
    COMPONENT_PROPERTY_GETTER_SETTER("fixRotation", GetFixRotation, SetFixRotation)
    COMPONENT_PROPERTY_GETTER_SETTER("layer", GetLayer, SetLayer)
    COMPONENT_PROPERTY_GETTER_SETTER("layerZ", GetLayerZ, SetLayerZ)
    COMPONENT_PROPERTY_GETTER_SETTER("visible", GetVisible, SetVisible)
    COMPONENT_PROPERTY_GETTER_SETTER("effect", GetEffect, SetEffect)
  END_COMPONENT_PROPERTIES()
  
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("filename", GetFilename, SetFilename)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("sheetImageName", GetSheetImageName, SetSheetImageName)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("x", GetX, SetX)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("y", GetY, SetY)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("anchorX", GetAnchorX, SetAnchorX)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("anchorY", GetAnchorY, SetAnchorY)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("offsetX", GetOffsetX, SetOffsetX)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("offsetY", GetOffsetY, SetOffsetY)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("scaleX", GetScaleX, SetScaleX)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("scaleY", GetScaleY, SetScaleY)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("rotation", GetRotation, SetRotation)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("flipU", GetFlipU, SetFlipU)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("flipV", GetFlipV, SetFlipV)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("offsetU", GetOffsetU, SetOffsetU)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("offsetV", GetOffsetV, SetOffsetV)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("r", GetR, SetR)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("g", GetG, SetG)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("b", GetB, SetB)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("alpha", GetAlpha, SetAlpha)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("filter", GetFilter, SetFilter)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("fixRotation", GetFixRotation, SetFixRotation)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("layer", GetLayer, SetLayer)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("layerZ", GetLayerZ, SetLayerZ)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("visible", GetVisible, SetVisible)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("effect", GetEffect, SetEffect)
  
protected:
  std::string filename_;
  std::string sheetImageName_;
  DrawImageParams drawImageParams_;
  bool fixRotation_;
  bool visible_;
  bool inited_;
};

} // Maxx
