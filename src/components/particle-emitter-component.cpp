#include "components/particle-emitter-component.h"
#include "components/i-particle-type.h"
#include "graphics/glue/glue-graphics.h"
#include "graphics/image-batcher.h"
#include "graphics/image-batch.h"
#include "math/math.h"
#include <map>
#include <string>
#include <cassert>

namespace Maxx {

typedef std::map<std::string, IParticleType*> ParticleTypeMap;
static ParticleTypeMap particleTypeMap;

//--------------------------------------------------------------------
/*static*/ void ParticleEmitterComponent::RegisterParticleType(const std::string& typeName, IParticleType* type)
{
  ParticleTypeMap::iterator i = particleTypeMap.find(typeName);
  if (i != particleTypeMap.end())
  {
    Trace("Error: RegisterParticleType already called for name '%s'\n", typeName.c_str());
    return;
  }
  // TODO: delete all registered types at shutdown
  particleTypeMap.insert(std::make_pair(typeName, type));
}

//--------------------------------------------------------------------
ParticleEmitterComponent::ParticleEmitterComponent()
  : image_(0)
{
  SUBSCRIBE_ALL()
}

//--------------------------------------------------------------------
ParticleEmitterComponent::~ParticleEmitterComponent()
{
  for (ParticleList::iterator i = particles_.begin(); i != particles_.end(); ++i)
  {
    delete *i;
  }
  particles_.clear();
}

//--------------------------------------------------------------------
bool ParticleEmitterComponent::Init()
{
  if (GetParticleTypeInterface() == 0)
  {
    Trace("Particle type '%s' unknown.\n", particleType_.c_str());
    return false;
  }
  Assert(!imageFilename_.empty());
  image_ = Maxx::Get()->GetGraphics()->GetImageManager().GetImage(imageFilename_);
  Assert(image_);
  
  lua_State* L = Maxx::Get()->GetLuaState();
  imageParamsTable_.PushOnLuaStack(L);
  Glue::Graphics_getDrawImageParamsFromLuaTable(L, lua_gettop(L), imageParams_);
  imageParams_.SetImage(image_);

  lua_pop(L, 1);
  
  return true;
}

//--------------------------------------------------------------------
void ParticleEmitterComponent::Update(double dt)
{
  IParticleType* particleType = GetParticleTypeInterface();
  particleType->UpdateParticles(dt, particles_);
  
  for (ParticleList::iterator i = particles_.begin(); i != particles_.end(); /**/)
  {
    Particle& particle = *(*i);
    particle.elapsed += dt;
    if (particle.die)
    {
      delete *i;
      i = particles_.erase(i);
    }
    else
      ++i;
  }
}

//--------------------------------------------------------------------
void ParticleEmitterComponent::Draw() const
{
  if (!image_)
    return;
  if (particles_.empty())
    return;
  
  Graphics* graphics = Maxx::Get()->GetGraphics();
  ImageBatch* batch = graphics->GetImageBatcher()->GetImageBatch((*particles_.begin())->params);
  if (!batch)
    return;
  for (ParticleList::const_iterator i = particles_.begin(); i != particles_.end(); ++i)
  {
    Particle& particle = *(const_cast<Particle*>(*i));
    batch->Add(particle.params);
  }
}

//--------------------------------------------------------------------
void ParticleEmitterComponent::OnMsgCreateParticle(PropertyTable& msg, IMessageReceiver* sender)
{
  if (msg.GetString("emitterName","") != GetName())
    return;
  Particle* p = new Particle;
  p->params = imageParams_;
  p->params.SetX(msg.GetNumber("x", 0.0));
  p->params.SetY(msg.GetNumber("y", 0.0));
  p->params.SetRotation(msg.GetNumber("rotation", 0.0));
  p->elapsed = 0.0;
  IParticleType* particleType = GetParticleTypeInterface();
  particleType->InitParticle(*p);
  particles_.push_back(p);
}

//--------------------------------------------------------------------
void ParticleEmitterComponent::OnMsgModifyParticles(PropertyTable& msg, IMessageReceiver* sender)
{
  if (msg.GetString("emitterName","") != GetName())
    return;
  IParticleType* particleType = GetParticleTypeInterface();
  particleType->ModifyParticles(particles_, msg, sender);
}

//--------------------------------------------------------------------
IParticleType* ParticleEmitterComponent::GetParticleTypeInterface()
{
  ParticleTypeMap::iterator i = particleTypeMap.find(particleType_);
  if (i != particleTypeMap.end())
    return i->second;
  return 0;
}

} // Maxx
