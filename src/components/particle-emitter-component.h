#pragma once
#include "engine/component.h"
#include "engine/component-t.h"
#include "engine/component-factory.h"
#include "messaging/message-receiver.h"
#include "graphics/graphics.h"
#include <vector>
#include <list>
#include <deque>

#define BEGIN_PARTICLE_LOOP                                                                               \
for (ParticleEmitterComponent::ParticleList::iterator i = particles.begin(); i != particles.end(); ++i)   \
{                                                                                                         \
  ParticleEmitterComponent::Particle& particle = *(*i);                                                   \

#define END_PARTICLE_LOOP                                             \
}                                                                     \

namespace Maxx {
  
class IParticleType;

//--------------------------------------------------------------------
class ParticleEmitterComponent : public ComponentT<ParticleEmitterComponent>
                               , public MessageReceiver<ParticleEmitterComponent>
                                 
{
public:
  
  ParticleEmitterComponent();
  virtual ~ParticleEmitterComponent();
  
  // takes ownership of type
  static void RegisterParticleType(const std::string& typeName, IParticleType* type);
  
  virtual bool Init();
  virtual void Update(double dt);
  virtual void Draw() const;
  
  const std::string& GetParticleType() const { return particleType_; }
  void SetParticleType(const std::string& v) { particleType_ = v; }
  const std::string& GetImageFilename() const { return imageFilename_; }
  void SetImageFilename(const std::string& v) { imageFilename_ = v; }
  const PropertyTable& GetImageParams() const { return imageParamsTable_; }
  void SetImageParams(const PropertyTable& v) { imageParamsTable_ = v; }
  
  COMPONENT_TYPE_STRING("ParticleEmitterComponent")
  BEGIN_COMPONENT_PROPERTIES(ParticleEmitterComponent)
  COMPONENT_PROPERTY_GETTER_SETTER("particleType", GetParticleType, SetParticleType)
    COMPONENT_PROPERTY_GETTER_SETTER("imageFilename", GetImageFilename, SetImageFilename)
    COMPONENT_PROPERTY_GETTER_SETTER("imageParams", GetImageParams, SetImageParams)
  END_COMPONENT_PROPERTIES()
  
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("particleType", GetParticleType, SetParticleType)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("imageFilename", GetImageFilename, SetImageFilename)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("imageParams", GetImageParams, SetImageParams)

  void OnMsgCreateParticle(PropertyTable& msg, IMessageReceiver* sender);
  void OnMsgModifyParticles(PropertyTable& msg, IMessageReceiver* sender);
  
  BEGIN_ONMSG(ParticleEmitterComponent)
    ONMSG("CreateParticle", OnMsgCreateParticle);
    ONMSG("ModifyParticles", OnMsgModifyParticles);
  END_ONMSG()
  
  struct Particle
  {
    Particle() : die(false), elapsed(0.0), percent(0.0) { for (size_t i = 0; i < sizeof(data)/sizeof(data[0]); ++i) data[i] = 0; }
    bool die;
    double elapsed;
    double percent;
    DrawImageParams params;
    double data[3];
  };
  typedef std::list<Particle*> ParticleList;
    
private:
  IParticleType* GetParticleTypeInterface();
  
private:
  std::string particleType_;
  std::string imageFilename_;
  PropertyTable imageParamsTable_;
  DrawImageParams imageParams_;
  const Image* image_;
  ParticleList particles_;
  std::deque<size_t> freeList_;
};

} // Maxx

// {type="ParticleEmitterComponent",properties={
//  createMessageName="CreateSparkle",
//  imageParams={
//    filename="",
//    scaleX=2,
//  },
//  lifetime=2.0,
//  updateFunction="Sparkle_Update", // c++ function
// }}