#include "components/revolute-joint-component.h"
#include "components/body-component.h"
#include "components/transform-component.h"
#include "engine/entity.h"
#include "engine/maxx.h"
#include "physics/physics.h"

namespace Maxx {
  
//--------------------------------------------------------------------
RevoluteJointComponent::RevoluteJointComponent()
  : revoluteJointDef_()
  , revoluteJoint_(0)
{
}

//--------------------------------------------------------------------
RevoluteJointComponent::~RevoluteJointComponent()
{
  b2World* world = Maxx::Get()->GetPhysics()->Getb2World();
  Entity* entityA = Entity::Lookup(entityA_);
  Entity* entityB = Entity::Lookup(entityB_);
  if (revoluteJoint_ && entityA && entityB)
    world->DestroyJoint(revoluteJoint_);
  revoluteJoint_ = 0;
}

// //--------------------------------------------------------------------
// double RevoluteJointComponent::GetLocalAnchorAX() const
// {
//   return revoluteJointDef_.localAnchorA.x;
// }
// 
// //--------------------------------------------------------------------
// void RevoluteJointComponent::SetLocalAnchorAX(double x)
// {
//   revoluteJointDef_.localAnchorA.x = x;
// }
// 
// //--------------------------------------------------------------------
// double RevoluteJointComponent::GetLocalAnchorAY() const
// {
//   return revoluteJointDef_.localAnchorA.y;
// }
// 
// //--------------------------------------------------------------------
// void RevoluteJointComponent::SetLocalAnchorAY(double y)
// {
//   revoluteJointDef_.localAnchorA.y = y;
// }
// 
// //--------------------------------------------------------------------
// double RevoluteJointComponent::GetLocalAnchorBX() const
// {
//   return revoluteJointDef_.localAnchorB.x;
// }
// 
// //--------------------------------------------------------------------
// void RevoluteJointComponent::SetLocalAnchorBX(double x)
// {
//   revoluteJointDef_.localAnchorB.x = x;
// }
// 
// //--------------------------------------------------------------------
// double RevoluteJointComponent::GetLocalAnchorBY() const
// {
//   return revoluteJointDef_.localAnchorB.y;
// }
// 
// //--------------------------------------------------------------------
// void RevoluteJointComponent::SetLocalAnchorBY(double y)
// {
//   revoluteJointDef_.localAnchorB.y = y;
// }

//--------------------------------------------------------------------
double RevoluteJointComponent::GetReferenceAngle() const
{
  return revoluteJointDef_.referenceAngle;
}

//--------------------------------------------------------------------
void RevoluteJointComponent::SetReferenceAngle(double a)
{
  revoluteJointDef_.referenceAngle = a;
}

//--------------------------------------------------------------------
bool RevoluteJointComponent::GetEnableLimit() const
{
  return revoluteJointDef_.enableLimit;
}

//--------------------------------------------------------------------
void RevoluteJointComponent::SetEnableLimit(bool b)
{
  revoluteJointDef_.enableLimit = b;
}

//--------------------------------------------------------------------
double RevoluteJointComponent::GetLowerAngle() const
{
  return revoluteJointDef_.lowerAngle;
}

//--------------------------------------------------------------------
void RevoluteJointComponent::SetLowerAngle(double a)
{
  revoluteJointDef_.lowerAngle = a;
}

//--------------------------------------------------------------------
double RevoluteJointComponent::GetUpperAngle() const
{
  return revoluteJointDef_.upperAngle;
}

//--------------------------------------------------------------------
void RevoluteJointComponent::SetUpperAngle(double a)
{
  revoluteJointDef_.upperAngle = a;
}

//--------------------------------------------------------------------
bool RevoluteJointComponent::GetEnableMotor() const
{
  if (revoluteJoint_)
    return revoluteJoint_->IsMotorEnabled();
  return revoluteJointDef_.enableMotor;
}

//--------------------------------------------------------------------
void RevoluteJointComponent::SetEnableMotor(bool b)
{
  if (revoluteJoint_)
    revoluteJoint_->EnableMotor(b);
  else
    revoluteJointDef_.enableMotor = b;
}

//--------------------------------------------------------------------
double RevoluteJointComponent::GetMotorSpeed() const
{
  if (revoluteJoint_)
    return revoluteJoint_->GetMotorSpeed();
  return revoluteJointDef_.motorSpeed;
}

//--------------------------------------------------------------------
void RevoluteJointComponent::SetMotorSpeed(double s)
{
  if (revoluteJoint_)
    revoluteJoint_->SetMotorSpeed(s);
  else
    revoluteJointDef_.motorSpeed = s;
}

//--------------------------------------------------------------------
double RevoluteJointComponent::GetMaxMotorTorque() const
{
  if (revoluteJoint_)
    return revoluteJoint_->GetMotorTorque(0.0);
  return revoluteJointDef_.maxMotorTorque;
}

//--------------------------------------------------------------------
void RevoluteJointComponent::SetMaxMotorTorque(double m)
{
  if (revoluteJoint_)
    revoluteJoint_->SetMaxMotorTorque(m);
  else
    revoluteJointDef_.maxMotorTorque = m;
}

//--------------------------------------------------------------------
bool RevoluteJointComponent::Init()
{
  Assert(!revoluteJoint_);
  if (revoluteJoint_)
    return false;

  Physics* physics = Maxx::Get()->GetPhysics();
  Entity* entityA = Entity::Lookup(entityA_);
  Entity* entityB = Entity::Lookup(entityB_);
  Assert(entityA);
  Assert(entityB);
  if (!entityA || !entityB)
    return false;
  BodyComponent* bodyComponentA = entityA->GetComponentOfType<BodyComponent>();
  BodyComponent* bodyComponentB = entityB->GetComponentOfType<BodyComponent>();
  b2Body* bodyA = bodyComponentA->Getb2Body();
  b2Body* bodyB = bodyComponentB->Getb2Body();
  Assert(bodyA && bodyB);
  TransformComponent* transformComponent = entityA->GetTransformComponent();
  revoluteJointDef_.Initialize(bodyA, bodyB, 
                          b2Vec2(physics->PixelsToMeters(transformComponent->GetX()), 
                                 physics->PixelsToMeters(transformComponent->GetY())));
  b2World* world = Maxx::Get()->GetPhysics()->Getb2World();
  b2Joint* joint = world->CreateJoint(&revoluteJointDef_);
  revoluteJoint_ = dynamic_cast<b2RevoluteJoint*>(joint);
  Assert(joint);
  Assert(revoluteJoint_);
  if (!revoluteJoint_)
    return false;
  return true;
}

} // Maxx
