#pragma once

#include "engine/component.h"
#include "engine/component-t.h"
#include "engine/component-factory.h"
#include "cross-platform/box2d-headers.h"

namespace Maxx {

//--------------------------------------------------------------------
class RevoluteJointComponent : public ComponentT<RevoluteJointComponent>
{
public:
  RevoluteJointComponent();
  virtual ~RevoluteJointComponent();

  const std::string& GetEntityA() const { return entityA_; }
  void SetEntityA(const std::string& entityA) { entityA_ = entityA; }
  
  const std::string& GetEntityB() const { return entityB_; }
  void SetEntityB(const std::string& entityB) { entityB_ = entityB; }
  
  // double GetLocalAnchorAX() const;
  // void SetLocalAnchorAX(double x);
  // 
  // double GetLocalAnchorAY() const;
  // void SetLocalAnchorAY(double y);
  // 
  // double GetLocalAnchorBX() const;
  // void SetLocalAnchorBX(double x);
  // 
  // double GetLocalAnchorBY() const;
  // void SetLocalAnchorBY(double y);

  double GetReferenceAngle() const;
  void SetReferenceAngle(double);
  
  bool GetEnableLimit() const;
  void SetEnableLimit(bool);
  
  double GetLowerAngle() const;
  void SetLowerAngle(double);
  
  double GetUpperAngle() const;
  void SetUpperAngle(double);
  
  bool GetEnableMotor() const;
  void SetEnableMotor(bool);
  
  double GetMotorSpeed() const;
  void SetMotorSpeed(double);
  
  double GetMaxMotorTorque() const;
  void SetMaxMotorTorque(double);
  
  virtual bool Init();
  
  COMPONENT_TYPE_STRING("RevoluteJointComponent")
  
  BEGIN_COMPONENT_PROPERTIES(RevoluteJointComponent)
    COMPONENT_PROPERTY_GETTER_SETTER("entityA", GetEntityA, SetEntityA)
    COMPONENT_PROPERTY_GETTER_SETTER("entityB", GetEntityB, SetEntityB)
    // COMPONENT_PROPERTY_GETTER_SETTER("localAnchorAX", GetLocalAnchorAX, SetLocalAnchorAX)
    // COMPONENT_PROPERTY_GETTER_SETTER("localAnchorAY", GetLocalAnchorAY, SetLocalAnchorAY)
    // COMPONENT_PROPERTY_GETTER_SETTER("localAnchorBX", GetLocalAnchorBX, SetLocalAnchorBX)
    // COMPONENT_PROPERTY_GETTER_SETTER("localAnchorBY", GetLocalAnchorBY, SetLocalAnchorBY)
    COMPONENT_PROPERTY_GETTER_SETTER("referenceAngle", GetReferenceAngle, SetReferenceAngle)
    COMPONENT_PROPERTY_GETTER_SETTER("enableLimit", GetEnableLimit, SetEnableLimit)
    COMPONENT_PROPERTY_GETTER_SETTER("lowerAngle", GetLowerAngle, SetLowerAngle)
    COMPONENT_PROPERTY_GETTER_SETTER("upperAngle", GetUpperAngle, SetUpperAngle)
    COMPONENT_PROPERTY_GETTER_SETTER("enableMotor", GetEnableMotor, SetEnableMotor)
    COMPONENT_PROPERTY_GETTER_SETTER("motorSpeed", GetMotorSpeed, SetMotorSpeed)
    COMPONENT_PROPERTY_GETTER_SETTER("maxMotorTorque", GetMaxMotorTorque, SetMaxMotorTorque)
  END_COMPONENT_PROPERTIES()
  
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("entityA", GetEntityA, SetEntityA)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("entityB", GetEntityB, SetEntityB)
  // COMPONENT_PROPERTY_GETTER_SETTER_WRAP("localAnchorAX", GetLocalAnchorAX, SetLocalAnchorAX)
  // COMPONENT_PROPERTY_GETTER_SETTER_WRAP("localAnchorAY", GetLocalAnchorAY, SetLocalAnchorAY)
  // COMPONENT_PROPERTY_GETTER_SETTER_WRAP("localAnchorBX", GetLocalAnchorBX, SetLocalAnchorBX)
  // COMPONENT_PROPERTY_GETTER_SETTER_WRAP("localAnchorBY", GetLocalAnchorBY, SetLocalAnchorBY)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("referenceAngle", GetReferenceAngle, SetReferenceAngle)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("enableLimit", GetEnableLimit, SetEnableLimit)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("lowerAngle", GetLowerAngle, SetLowerAngle)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("upperAngle", GetUpperAngle, SetUpperAngle)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("enableMotor", GetEnableMotor, SetEnableMotor)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("motorSpeed", GetMotorSpeed, SetMotorSpeed)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("maxMotorTorque", GetMaxMotorTorque, SetMaxMotorTorque)

private:
  std::string entityA_;
  std::string entityB_;
  b2RevoluteJointDef revoluteJointDef_;
  b2RevoluteJoint* revoluteJoint_;
};

} // Maxx
