#include "rope-joint-component.h"
#include "components/body-component.h"
#include "components/transform-component.h"
#include "engine/entity.h"
#include "engine/maxx.h"
#include "physics/physics.h"

namespace Maxx {
  
//--------------------------------------------------------------------
RopeJointComponent::RopeJointComponent()
  : ropeJointDef_()
  , ropeJoint_(0)
{
}

//--------------------------------------------------------------------
RopeJointComponent::~RopeJointComponent()
{
  b2World* world = Maxx::Get()->GetPhysics()->Getb2World();
  Entity* entityA = Entity::Lookup(entityA_);
  Entity* entityB = Entity::Lookup(entityB_);
  if (ropeJoint_ && entityA && entityB)
    world->DestroyJoint(ropeJoint_);
  ropeJoint_ = 0;
}

//--------------------------------------------------------------------
double RopeJointComponent::GetMaxLength() const
{
  return Maxx::Get()->GetPhysics()->MetersToPixels(ropeJointDef_.maxLength);
}

//--------------------------------------------------------------------
void RopeJointComponent::SetMaxLength(double v)
{
  v = Maxx::Get()->GetPhysics()->PixelsToMeters(v);
  ropeJointDef_.maxLength = v;
  if (ropeJoint_)
    ropeJoint_->SetMaxLength(v);
}

//--------------------------------------------------------------------
bool RopeJointComponent::Init()
{
  Assert(!ropeJoint_);
  if (ropeJoint_)
    return false;

  Physics* physics = Maxx::Get()->GetPhysics();
  Entity* entityA = Entity::Lookup(entityA_);
  Entity* entityB = Entity::Lookup(entityB_);
  Assert(entityA);
  Assert(entityB);
  if (!entityA || !entityB)
    return false;
  BodyComponent* bodyComponentA = entityA->GetComponentOfType<BodyComponent>();
  BodyComponent* bodyComponentB = entityB->GetComponentOfType<BodyComponent>();
  b2Body* bodyA = bodyComponentA->Getb2Body();
  b2Body* bodyB = bodyComponentB->Getb2Body();
  Assert(bodyA);
  Assert(bodyB);
  if (!bodyA || !bodyB)
    return false;
  ropeJointDef_.bodyA = bodyA;
  ropeJointDef_.bodyB = bodyB;
  ropeJointDef_.localAnchorA = b2Vec2(physics->PixelsToMeters(0),
                                      physics->PixelsToMeters(0));
  ropeJointDef_.localAnchorB = b2Vec2(physics->PixelsToMeters(0),
                                      physics->PixelsToMeters(0));
  if (ropeJointDef_.maxLength <= 0)
  {
    TransformComponent* transformComponentA = entityA->GetTransformComponent();
    TransformComponent* transformComponentB = entityB->GetTransformComponent();
    ropeJointDef_.maxLength = physics->PixelsToMeters(Vector(transformComponentA->GetX() - transformComponentB->GetX(), 
                                                             transformComponentA->GetY() - transformComponentB->GetY()).Length());
    ropeJointDef_.maxLength *= 0.7;
  }
  b2World* world = Maxx::Get()->GetPhysics()->Getb2World();
  b2Joint* joint = world->CreateJoint(&ropeJointDef_);
  ropeJoint_ = dynamic_cast<b2RopeJoint*>(joint);
  Assert(joint);
  Assert(ropeJoint_);
  if (!ropeJoint_)
    return false;
  return true;
}

} // Maxx
