#pragma once

#include "engine/component.h"
#include "engine/component-t.h"
#include "engine/component-factory.h"
#include "cross-platform/box2d-headers.h"

namespace Maxx {

//--------------------------------------------------------------------
class RopeJointComponent : public ComponentT<RopeJointComponent>
{
public:
  RopeJointComponent();
  virtual ~RopeJointComponent();

  const std::string& GetEntityA() const { return entityA_; }
  void SetEntityA(const std::string& entityA) { entityA_ = entityA; }
  const std::string& GetEntityB() const { return entityB_; }
  void SetEntityB(const std::string& entityB) { entityB_ = entityB; }
  double GetMaxLength() const;
  void SetMaxLength(double);
  
  virtual bool Init();
  
  COMPONENT_TYPE_STRING("RopeJointComponent")
  
  BEGIN_COMPONENT_PROPERTIES(RopeJointComponent)
    COMPONENT_PROPERTY_GETTER_SETTER("entityA", GetEntityA, SetEntityA)
    COMPONENT_PROPERTY_GETTER_SETTER("entityB", GetEntityB, SetEntityB)
    COMPONENT_PROPERTY_GETTER_SETTER("maxLength", GetMaxLength, SetMaxLength)
  END_COMPONENT_PROPERTIES()
  
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("entityA", GetEntityA, SetEntityA)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("entityB", GetEntityB, SetEntityB)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("maxLength", GetMaxLength, SetMaxLength)

private:
  std::string entityA_;
  std::string entityB_;
  b2RopeJointDef ropeJointDef_;
  b2RopeJoint* ropeJoint_;
};

} // Maxx
