#include "components/script-component.h"
#include "cross-platform/lua-headers.h"
#include "utils/guid.h"
#include "engine/maxx.h"
#include "engine/glue.h"
#include <cassert>
#include <cstdlib>

namespace Maxx {

//--------------------------------------------------------------------
ScriptComponent::ScriptComponent()
  : objectRegistryKey_("")
  , updatable_(true)
  , drawable_(true)
{
}

//--------------------------------------------------------------------
ScriptComponent::~ScriptComponent()
{
}

//--------------------------------------------------------------------
bool ScriptComponent::Init()
{
  lua_State* L = Maxx::Get()->GetLuaState();
  int oldTop = lua_gettop(L);
  
  classname_ = properties_.GetString("classname", "");
  Assert(!classname_.empty());
  
  if (GetName().empty())
  { 
    SetName(classname_ + " " + Maxx::Utils::NewGuid());
  }

  //static unsigned int count = 0;
  //char buf[64];
  //sprintf(buf, "%d", count);
  //objectRegistryKey_ = std::string(buf) + "(" + classname_ + ")";
  objectRegistryKey_ = Maxx::Utils::NewGuid() + "(" + classname_ + ")";
  
  // instance = {}
  // setmetatable(instance, YourLuaClass)
  lua_newtable(L);
  lua_setfield(L, LUA_REGISTRYINDEX, objectRegistryKey_.c_str());
  lua_getfield(L, LUA_REGISTRYINDEX, objectRegistryKey_.c_str());
  lua_getfield(L, LUA_GLOBALSINDEX, classname_.c_str());
  lua_setmetatable(L, -2);

  // instance.userdata = this
  lua_getfield(L, LUA_REGISTRYINDEX, objectRegistryKey_.c_str());
  Maxx::Glue::NewUserData<Component>(L, this, /*own=*/false);
  lua_setfield(L, -2, "userdata");
  lua_pop(L, 1);

  // set properties\\
  lua_getfield(L, LUA_REGISTRYINDEX, objectRegistryKey_.c_str());
  for (PropertyTable::MapFromString::const_iterator
       i = properties_.mapFromString.begin(); i != properties_.mapFromString.end(); ++i)
  {
    const std::string& name = i->first.c_str();
    const Property& value = i->second;
    //Trace("setting %s to %s\n", name.c_str(), value.Stringify().c_str());
    value.PushOnLuaStack(L);
    lua_setfield(L, -2, name.c_str());
  }
  lua_pop(L, 1);
  
  SubscribeToMessages();

  CallStart();
  
  lua_pop(L, lua_gettop(L) - oldTop);

  return true;
}

//--------------------------------------------------------------------
void ScriptComponent::GetAllPropertyNames(PropertyNames& propertyNames) const
{
  if (classname_.empty())
    return;
  lua_State* L = Maxx::Get()->GetLuaState();

  // class.properties
  lua_getfield(L, LUA_GLOBALSINDEX, classname_.c_str());
  lua_getfield(L, -1, "properties");
  if (!lua_isnil(L, -1))
  {
    PropertyTable table;
    PropertyTable::FromLuaStack(L, -1, table);
    for (PropertyTable::MapFromString::const_iterator
         i = table.mapFromString.begin(); i != table.mapFromString.end(); ++i)
    {
      const std::string& key = i->first;
      propertyNames.insert(key.c_str());
    }
  }
  lua_pop(L, 2);

  for (PropertyTable::MapFromString::const_iterator
       i = properties_.mapFromString.begin(); i != properties_.mapFromString.end(); ++i)
  {
    const std::string& key = i->first;
    propertyNames.insert(key.c_str());
  }
}

//--------------------------------------------------------------------
Property ScriptComponent::GetProperty(const std::string& name) const
{
  if (name == "classname")
    return classname_;
  if (objectRegistryKey_.empty())
    return *properties_.Get(name);

  lua_State* L = Maxx::Get()->GetLuaState();

  Property property;
    
  // Try the instance
  lua_getfield(L, LUA_REGISTRYINDEX, objectRegistryKey_.c_str());
  if (!lua_isnil(L, -1))
  {
    lua_pushstring(L, name.c_str());
    lua_rawget(L, -2);
    if (!lua_isfunction(L, -1) && !lua_isnil(L, -1))
    {
      Property::FromLuaStack(L, -1, property);
    }
    lua_pop(L, 1);
  }
  lua_pop(L, 1);

  // Look in the lua class properties
  if (property.IsNull())
  {
    Assert(!classname_.empty());
    if (!classname_.empty())
    {
      lua_getfield(L, LUA_GLOBALSINDEX, classname_.c_str());
      Assert(!lua_isnil(L, -1));
      lua_pushstring(L, "properties");
      lua_rawget(L, -2);
      if (!lua_isnil(L, -1))
      {
        lua_pushstring(L, name.c_str());
        lua_rawget(L, -2);
        if (!lua_isfunction(L, -1) && !lua_isnil(L, -1))
        {
          Property::FromLuaStack(L, -1, property);
        }
      }
      lua_pop(L, 3);
    }
  }
  
  return property;
}

//--------------------------------------------------------------------
void ScriptComponent::SetProperty(const std::string& name, const Property& value)
{
  //Assert(objectRegistryKey_.empty())
  if (objectRegistryKey_.empty())
  {
    properties_.Set(name, value);
  }  
  else
  {
    lua_State* L = Maxx::Get()->GetLuaState();
 
    // Set the property on the instance
    lua_getfield(L, LUA_REGISTRYINDEX, objectRegistryKey_.c_str());
    Assert(!lua_isnil(L, -1));
    lua_pushstring(L, name.c_str());
    value.PushOnLuaStack(L);
    lua_rawset(L, -3);
    lua_pop(L, 1);
  }
  
  ComponentT<ScriptComponent>::MarkPropertyAsSet(name);

  // PropertyTable msg;
  // msg.Insert("property", name);
  // OnMsg("PropertyChanged", msg, /*sender=*/0);
}

//--------------------------------------------------------------------
bool ScriptComponent::IsPropertySet(const std::string& name) const
{
  if (GetEntity() && GetEntity()->IsPrefabInstance() && name=="classname")
    return false;
  return ComponentT<ScriptComponent>::IsPropertySet(name);
}

//--------------------------------------------------------------------
void ScriptComponent::Update(double dt)
{
  if (!updatable_)
    return;
  
  //printf("SCriptComponent updating %s\n", classname_.c_str());
  lua_State* L = Maxx::Get()->GetLuaState();
  int oldTop = lua_gettop(L);

  // instance:update(dt)
  lua_getfield(L, LUA_GLOBALSINDEX, classname_.c_str());
  if (lua_isnil(L, -1))
  {
    Trace("ERROR: Unknown script class \"%s\"\n", classname_.c_str());
    lua_pop(L, lua_gettop(L) - oldTop);
    Maxx::Get()->Quit();
    return;
  }
  lua_getfield(L, -1, "update");
  if (lua_isfunction(L, -1))
  {
    lua_getfield(L, LUA_REGISTRYINDEX, objectRegistryKey_.c_str());
    Assert(!lua_isnil(L, -1));
    lua_pushnumber(L, dt);
    int result = lua_pcall(L, /*nargs=*/2, /*nresults=*/0, /*errorfunc=*/0);
    if (result != 0)
    {
      Trace("ScriptComponent::Update: Lua error\n%s\n", lua_tostring(L, -1));
      lua_pop(L, 1);
    }
  }
  else
  {
    updatable_ = false;
  }
  lua_pop(L, lua_gettop(L) - oldTop);
}

//--------------------------------------------------------------------
void ScriptComponent::Draw() const
{
  if (!drawable_)
    return;
    
  lua_State* L = Maxx::Get()->GetLuaState();
  int oldTop = lua_gettop(L);
    
  // instance:draw()
  lua_getfield(L, LUA_GLOBALSINDEX, classname_.c_str());
  if (lua_isnil(L, -1))
  {
    Trace("ERROR: Unknown script class \"%s\"\n", classname_.c_str());
    lua_pop(L, lua_gettop(L) - oldTop);
    Maxx::Get()->Quit();
    return;
  }
  lua_getfield(L, -1, "draw");
  if (lua_isfunction(L, -1))
  {
    lua_getfield(L, LUA_REGISTRYINDEX, objectRegistryKey_.c_str());
    Assert(!lua_isnil(L, -1));
    int result = lua_pcall(L, /*nargs=*/1, /*nresults=*/0, /*errorfunc=*/0);
    if (result != 0)
    {
      Trace("ScriptComponent::Draw: Lua error\n%s\n", lua_tostring(L, -1));
      lua_pop(L, 1);
    }
  }
  else
  {
    drawable_ = false;
  }
  lua_pop(L, lua_gettop(L) - oldTop);
}

//--------------------------------------------------------------------
void ScriptComponent::OnMsg(const std::string& msgname, PropertyTable& msg, IMessageReceiver* sender)
{
  if (!GetEntity() || !GetEntity()->Inited())
    return;

  lua_State* L = Maxx::Get()->GetLuaState();
  int oldTop = lua_gettop(L);

  Entity* senderEntity = dynamic_cast<Entity*>(sender);
  
  if (classname_.empty())
    return;

  lua_getfield(L, LUA_GLOBALSINDEX, classname_.c_str());
  Assert(!lua_isnil(L, -1));
  std::string luaFunctionName = "onMsg" + msgname;
  lua_pushstring(L, luaFunctionName.c_str());
  lua_rawget(L, -2);
  if (lua_type(L, -1) == LUA_TFUNCTION)
  {
    lua_getfield(L, LUA_REGISTRYINDEX, objectRegistryKey_.c_str());
    Assert(!lua_isnil(L, -1));
    msg.PushOnLuaStack(L);
    int msgRef = luaL_ref(L, LUA_REGISTRYINDEX);
    lua_rawgeti(L, LUA_REGISTRYINDEX, msgRef);
    Maxx::Glue::NewUserData<Entity>(L, senderEntity, /*own=*/false);
    int result = lua_pcall(L, /*nargs=*/3, /*nresults=*/0, /*errorfunc=*/0);
    if (result != 0)
    {
      Trace("%s\n", lua_tostring(L, -1));
      lua_pop(L, 1);
    }
    else
    {
      lua_rawgeti(L, LUA_REGISTRYINDEX, msgRef);
      PropertyTable::FromLuaStack(L, -1, msg);
      lua_pop(L, 1);
    }
  }
  
  lua_pop(L, lua_gettop(L) - oldTop);
}

//--------------------------------------------------------------------
void ScriptComponent::SubscribeToMessages()
{
  // Find all functions in our Lua instance object that start with onMsg
  // and register with the postmaster. We'll forward these messages to
  // Lua.
  
  lua_State* L = Maxx::Get()->GetLuaState();
  int oldTop = lua_gettop(L);
  
  lua_getfield(L, LUA_GLOBALSINDEX, classname_.c_str());
  if (lua_isnil(L, -1))
  {
    Trace("ERROR: Unknown script class \"%s\"\n", classname_.c_str());
    lua_pop(L, lua_gettop(L) - oldTop);
    //Maxx::Get()->Quit();
    return;
  }

  int index = -1;
  lua_pushnil(L);
  while (lua_next(L, index - 1))
  {
    int keyType = lua_type(L, index - 1);
    int valueType = lua_type(L, index);
    if (keyType == LUA_TSTRING && valueType == LUA_TFUNCTION)
    {
      const char* functionName = lua_tostring(L, index - 1);
      if (functionName && 
          functionName[0]=='o'&&
          functionName[1]=='n'&&
          functionName[2]=='M'&&
          functionName[3]=='s'&&
          functionName[4]=='g') // "onMsg"
      {
        Maxx::Get()->GetPostmaster()->SubscribeToMessage(&functionName[5], this);
      }
    }
    lua_pop(L, 1);
  }
  
  lua_pop(L, lua_gettop(L) - oldTop);
}

void ScriptComponent::CallStart()
{
  lua_State* L = Maxx::Get()->GetLuaState();

  lua_getfield(L, LUA_GLOBALSINDEX, classname_.c_str());
  if (lua_isnil(L, -1))
  {
    Trace("ERROR: Unknown script class \"%s\"\n", classname_.c_str());
    //Maxx::Get()->Quit();
    return;
  }

  lua_getfield(L, -1, "start");
  if (!lua_isfunction(L, -1))
    return;

  lua_getfield(L, LUA_REGISTRYINDEX, objectRegistryKey_.c_str());
  int result = lua_pcall(L, /*nargs=*/1, /*nresults=*/0, /*errorfunc=*/0);
  if (result != 0)
  {
    Trace("ScriptComponent::Update: Lua error\n%s\n", lua_tostring(L, -1));
    lua_pop(L, 1);
  }
}

} // Maxx
