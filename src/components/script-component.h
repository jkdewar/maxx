#pragma once

#include "engine/component.h"
#include "engine/component-t.h"
#include "engine/component-factory.h"
#include "engine/property-table.h"
#include "messaging/message-receiver.h"

namespace Maxx {

//--------------------------------------------------------------------
class ScriptComponent : public ComponentT<ScriptComponent>
                      , public MessageReceiver<ScriptComponent>
{
public:
  ScriptComponent();
  virtual ~ScriptComponent();
  
  virtual bool Init();
  
  virtual void Update(double dt);
  virtual void Draw() const;
  
  virtual void      GetAllPropertyNames(PropertyNames& propertyNames) const;
  virtual Property  GetProperty(const std::string& name) const;
  virtual void      SetProperty(const std::string& name, const Property& value);
  virtual bool      IsPropertySet(const std::string& name) const;
  
  void SetUpdateDisabled(bool b) { updatable_ = !b; }
  
  COMPONENT_TYPE_STRING("ScriptComponent")
  
  BEGIN_COMPONENT_PROPERTIES(ScriptComponent)
  END_COMPONENT_PROPERTIES()
  
  virtual void OnMsg(const std::string& msgname, PropertyTable& msg, IMessageReceiver* sender);
  
private:
  void SubscribeToMessages();
  void CallStart();
  
private:
  PropertyTable properties_;
  std::string classname_;
  std::string objectRegistryKey_;
  bool updatable_;
  mutable bool drawable_;
};

} // Maxx
