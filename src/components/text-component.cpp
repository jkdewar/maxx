#include "components/text-component.h"
#include "components/transform-component.h"
#include "engine/maxx.h"
#include "graphics/text/font-manager.h"

namespace Maxx {

//--------------------------------------------------------------------
TextComponent::TextComponent()
  : fontName_()
  , font_(0)
  , text_()
  , textWidth_(0.0)
  , textHeight_(0.0)
  , dips_()
  , dirty_(true)
{
}

//--------------------------------------------------------------------
TextComponent::~TextComponent()
{
}

//--------------------------------------------------------------------
bool TextComponent::Init()
{
  params_.SetFilter(false);
  if (params_.GetLayer().empty())
    params_.SetLayer("ui");
  return true;
}

//--------------------------------------------------------------------
void TextComponent::Draw() const
{
  if (!font_)
    return;
  if (dirty_ || params_.IsDirty())
  {
    dips_.clear();
    font_->BuildText(text_, params_, dips_);
    textWidth_ = font_->GetTextWidth(text_, params_);
    textHeight_ = font_->GetHeight() * params_.GetScaleY();
    dirty_ = false;
  }
  TransformComponent* transform = GetEntity()->GetTransformComponent();
  Graphics* graphics = Maxx::Get()->GetGraphics();
  for (Font::DrawImageParamsList::iterator
       i = dips_.begin(); i != dips_.end(); ++i)
  {
    DrawImageParams& p = *i;
    p.SetX(transform->GetX() /*- textWidth_/2.0 - params_.GetAnchorX()*textWidth_/2.0*/);
    p.SetY(transform->GetY() /*- textHeight_/2.0 - params_.GetAnchorY()*textHeight_/2.0*/);
    p.SetRotation(transform->GetAngle() + params_.GetRotation());
    graphics->DrawImage(&*i, /*deleteParamsForMe=*/false);
  }
}

//--------------------------------------------------------------------
void TextComponent::SetFont(const std::string& v)
{ 
  if (fontName_ != v) 
  { 
    fontName_ = v;
    font_ = Maxx::Get()->GetFontManager()->GetFont(fontName_);
    if (!font_)
      Trace("ERROR: TextComponent: loading font '%s'\n", fontName_.c_str());
    dirty_ = true; 
  }
}

//--------------------------------------------------------------------
void TextComponent::SetText(const std::string& v)
{ 
  if (text_ != v) 
  { 
    text_ = v; 
    dirty_ = true; 
  } 
}

} // Maxx
