#pragma once

#include "engine/component.h"
#include "engine/component-t.h"
#include "engine/component-factory.h"
#include "graphics/graphics.h"
#include "graphics/text/font.h"

namespace Maxx {

class Font;
  
//--------------------------------------------------------------------
class TextComponent : public ComponentT<TextComponent>
{
public:
  TextComponent();
  virtual ~TextComponent();
  
  virtual bool Init();

  virtual void Draw() const;
  
  const std::string& GetFont() const { return fontName_; }
  void SetFont(const std::string& v);
  const std::string& GetText() const { return text_; }
  void SetText(const std::string& v);

  double GetX() const { return params_.GetX(); }
  void SetX(double v) { params_.SetX(v); }
  double GetY() const { return params_.GetY(); }
  void SetY(double v) { params_.SetY(v); }
  double GetAnchorX() const { return params_.GetAnchorX(); }
  void SetAnchorX(double v) { params_.SetAnchorX(v); }
  double GetAnchorY() const { return params_.GetAnchorY(); }
  void SetAnchorY(double v) { params_.SetAnchorY(v); }
  double GetScaleX() const { return params_.GetScaleX(); }
  void SetScaleX(double v) { params_.SetScaleX(v); }
  double GetScaleY() const { return params_.GetScaleY(); }
  void SetScaleY(double v) { params_.SetScaleY(v); }
  const std::string& GetLayer() const { return params_.GetLayer(); }
  void SetLayer(const std::string& layer) { params_.SetLayer(layer); }
  double GetR() const { return params_.GetR(); } 
  void SetR(double r) { params_.SetR(r); }
  double GetG() const { return params_.GetG(); }
  void SetG(double g) { params_.SetG(g); }
  double GetB() const { return params_.GetB(); }
  void SetB(double b) { params_.SetB(b); }
  double GetAlpha() const { return params_.GetAlpha(); }
  void SetAlpha(double a) { params_.SetAlpha(a); }
  double GetRotation() const { return params_.GetRotation(); }
  void SetRotation(double v) { params_.SetRotation(v); }
  
  COMPONENT_TYPE_STRING("TextComponent")
  BEGIN_COMPONENT_PROPERTIES(TextComponent)
    COMPONENT_PROPERTY_GETTER_SETTER("font", GetFont, SetFont)
    COMPONENT_PROPERTY_GETTER_SETTER("text", GetText, SetText)
    COMPONENT_PROPERTY_GETTER_SETTER("x", GetX, SetX)
    COMPONENT_PROPERTY_GETTER_SETTER("y", GetY, SetY)
    COMPONENT_PROPERTY_GETTER_SETTER("anchorX", GetAnchorX, SetAnchorX)
    COMPONENT_PROPERTY_GETTER_SETTER("anchorY", GetAnchorY, SetAnchorY)
    COMPONENT_PROPERTY_GETTER_SETTER("scaleX", GetScaleX, SetScaleX)
    COMPONENT_PROPERTY_GETTER_SETTER("scaleY", GetScaleY, SetScaleY)
    COMPONENT_PROPERTY_GETTER_SETTER("layer", GetLayer, SetLayer)
    COMPONENT_PROPERTY_GETTER_SETTER("r", GetR, SetR)
    COMPONENT_PROPERTY_GETTER_SETTER("g", GetG, SetG)
    COMPONENT_PROPERTY_GETTER_SETTER("b", GetB, SetB)
    COMPONENT_PROPERTY_GETTER_SETTER("alpha", GetAlpha, SetAlpha)
    COMPONENT_PROPERTY_GETTER_SETTER("rotation", GetRotation, SetRotation)
  END_COMPONENT_PROPERTIES()

  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("font", GetFont, SetFont)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("text", GetText, SetText)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("x", GetX, SetX)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("y", GetY, SetY)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("anchorX", GetAnchorX, SetAnchorX)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("anchorY", GetAnchorY, SetAnchorY)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("scaleX", GetScaleX, SetScaleX)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("scaleY", GetScaleY, SetScaleY)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("layer", GetLayer, SetLayer)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("r", GetR, SetR)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("g", GetG, SetG)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("b", GetB, SetB)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("alpha", GetAlpha, SetAlpha)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("rotation", GetRotation, SetRotation)

protected:
  std::string fontName_;
  const Font* font_;
  std::string text_;
  mutable double textWidth_, textHeight_;
  DrawImageParams params_;
  mutable Font::DrawImageParamsList dips_;
  mutable bool dirty_;
};

} // Maxx
