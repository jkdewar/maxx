#include "components/transform-component.h"
#include "components/body-component.h"
#include "math/math.h"
#include "math/vector.h"
#include "graphics/graphics.h"
#include "graphics/debug-lines.h"

namespace Maxx {

//--------------------------------------------------------------------
TransformComponent::TransformComponent()
  : x_(0.0)
  , y_(0.0)
  , angle_(0.0)
  , scaleX_(1.0)
  , scaleY_(1.0)
  
  , edSelected_(false)
{
}

//--------------------------------------------------------------------
TransformComponent::~TransformComponent()
{
}

//--------------------------------------------------------------------
bool TransformComponent::Init()
{
#if defined(ED_ENABLED)
  SUBSCRIBE_ALL()
#endif
  return true;
}

#if defined(ED_ENABLED)
//--------------------------------------------------------------------
void TransformComponent::OnMsgEdBoxSelect(PropertyTable& msg, IMessageReceiver* sender)
{
  bool hit = Math::TestPointInAabb(x_, y_, msg.GetNumber("x1",0.0),msg.GetNumber("y1",0.0),
                                           msg.GetNumber("x2",0.0),msg.GetNumber("y2",0.0));
  if (hit)
    edSelected_ = true;
}

//--------------------------------------------------------------------
void TransformComponent::OnMsgEdSetSelected(PropertyTable& msg, IMessageReceiver* sender)
{
  edSelected_ = (msg.GetString("guid","") == GetEntity()->GetGuid());
}

//--------------------------------------------------------------------
void TransformComponent::OnMsgEdClearSelection(PropertyTable& msg, IMessageReceiver* sender)
{
  //Trace("EdClearSelection received by TransformComponent\n");
  edSelected_ = false;
}

//--------------------------------------------------------------------
void TransformComponent::OnMsgEdMoveSelected(PropertyTable& msg, IMessageReceiver* sender)
{
  if (!edSelected_)
    return;
  double deltaX = msg.GetNumber("deltaX", 0.0);
  double deltaY = msg.GetNumber("deltaY", 0.0);
  x_ += deltaX;
  y_ += deltaY;
}

static const double snapInterval = Math::TwoPi / 16;

//--------------------------------------------------------------------
static double SnapAngle(double angle)
{
  return Math::Floor(angle / snapInterval) * snapInterval;
}

//--------------------------------------------------------------------
void TransformComponent::OnMsgEdRotateSelected(PropertyTable& msg, IMessageReceiver* sender)
{
  if (!edSelected_)
    return;
  double deltaAngle = msg.GetNumber("deltaAngle", 0.0);
  bool snap = msg.GetBool("snap", false);
  if (snap)
  {
    angle_ = SnapAngle(angle_);
    angle_ += deltaAngle * snapInterval;
  }
  else
  {
    angle_ += deltaAngle;
  }
  angle_ = Math::UnwindAngle(angle_);
  SetProperty("angle",angle_);
}

//--------------------------------------------------------------------
void TransformComponent::OnMsgEdDeleteSelected(PropertyTable& msg, IMessageReceiver* sender)
{
  if (!edSelected_)
    return;
  GetEntity()->Die();
}

//--------------------------------------------------------------------
void TransformComponent::OnMsgEdFindSelected(PropertyTable& msg, IMessageReceiver* sender)
{
  if (!edSelected_)
    return;
  PropertyTable response;
  response.Set("guid", GetEntity()->GetGuid());
  GetEntity()->SendMessage("EdImSelected", response);
}

//--------------------------------------------------------------------
void TransformComponent::Draw() const
{
  if (!edSelected_)
    return;
  const double s1 = 5.0;
  const double s2 = s1 + 1.0;
  const double s3 = s2 + 1.0;
  const double s4 = s3 + 1.0;
  double r=1.0,g=0.0,b=1.0;
  if (GetEntity() && GetEntity()->IsPrefabInstance())
  {
    r=0.0;g=1.0;b=0.0;
  }
  Maxx::Get()->GetGraphics()->GetDebugLines()->AddBox(x_ - s1, y_ - s1, x_ + s1, y_ + s1, 0.2,0.2,0.0);
  Maxx::Get()->GetGraphics()->GetDebugLines()->AddBox(x_ - s2, y_ - s2, x_ + s2, y_ + s2, r,g,b);
  Maxx::Get()->GetGraphics()->GetDebugLines()->AddBox(x_ - s3, y_ - s3, x_ + s3, y_ + s3, r,g,b);
  Maxx::Get()->GetGraphics()->GetDebugLines()->AddBox(x_ - s4, y_ - s4, x_ + s4, y_ + s4, 0.2,0.2,0.2);
}

#endif // ED_ENABLED

} // Maxx
