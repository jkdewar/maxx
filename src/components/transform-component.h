#pragma once

#include "engine/component.h"
#include "engine/component-t.h"
#include "engine/component-factory.h"
#include "math/matrix.h"
#include "messaging/message-receiver.h"

namespace Maxx {
  
//--------------------------------------------------------------------
class TransformComponent  : public ComponentT<TransformComponent>
                          , public MessageReceiver<TransformComponent>
{
public:
  TransformComponent();
  virtual ~TransformComponent();
  
  virtual bool Init();
  
  double GetX() const { return x_; }
  void SetX(double x) { x_ = x; }
  double GetY() const { return y_; }
  void SetY(double y) { y_ = y; }
  double GetAngle() const { return angle_; }
  void SetAngle(double r) { angle_ = r; }
  double GetScaleX() const { return scaleX_; }
  void SetScaleX(double sx) { scaleX_ = sx; }
  double GetScaleY() const { return scaleY_; }
  void SetScaleY(double sy) { scaleY_ = sy; }
  
  //Matrix GetMatrix() const;
  //void SetMatrix(const Matrix&);
  
  COMPONENT_TYPE_STRING("TransformComponent")
  
  BEGIN_COMPONENT_PROPERTIES(TransformComponent)
    COMPONENT_PROPERTY_GETTER_SETTER("x", GetX, SetX)
    COMPONENT_PROPERTY_GETTER_SETTER("y", GetY, SetY)
    COMPONENT_PROPERTY_GETTER_SETTER("angle", GetAngle, SetAngle)
    COMPONENT_PROPERTY_GETTER_SETTER("scaleX", GetScaleX, SetScaleX)
    COMPONENT_PROPERTY_GETTER_SETTER("scaleY", GetScaleY, SetScaleY)
  END_COMPONENT_PROPERTIES()

  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("x", GetX, SetX)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("y", GetY, SetY)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("angle", GetAngle, SetAngle)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("scaleX", GetScaleX, SetScaleX)
  COMPONENT_PROPERTY_GETTER_SETTER_WRAP("scaleY", GetScaleY, SetScaleY)
  
#if defined(ED_ENABLED)
  void OnMsgEdBoxSelect(PropertyTable& msg, IMessageReceiver* sender);
  void OnMsgEdSetSelected(PropertyTable& msg, IMessageReceiver* sender);
  void OnMsgEdClearSelection(PropertyTable& msg, IMessageReceiver* sender);
  void OnMsgEdMoveSelected(PropertyTable& msg, IMessageReceiver* sender);
  void OnMsgEdRotateSelected(PropertyTable& msg, IMessageReceiver* sender);
  void OnMsgEdDeleteSelected(PropertyTable& msg, IMessageReceiver* sender);
  void OnMsgEdFindSelected(PropertyTable& msg, IMessageReceiver* sender);
  
  BEGIN_ONMSG(TransformComponent)
    ONMSG("EdBoxSelect", OnMsgEdBoxSelect);
    ONMSG("EdSetSelected", OnMsgEdSetSelected);
    ONMSG("EdClearSelection", OnMsgEdClearSelection);
    ONMSG("EdMoveSelected", OnMsgEdMoveSelected);
    ONMSG("EdRotateSelected", OnMsgEdRotateSelected);
    ONMSG("EdDeleteSelected", OnMsgEdDeleteSelected);
    ONMSG("EdFindSelected", OnMsgEdFindSelected);
  END_ONMSG()

  virtual void Draw() const;

#endif

private:
  double x_, y_;
  double angle_;
  double scaleX_, scaleY_;
  
  bool edSelected_;
};

} // Maxx
