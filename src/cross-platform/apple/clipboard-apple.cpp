#if defined(__MACOSX__) || defined(__IPHONEOS__)

#include "cross-platform/clipboard.h"
#include <string>

namespace Clipboard {

std::string gClipboard;

std::string Read()
{
    return gClipboard;
}

void Write(const std::string& text)
{
	gClipboard = text;
}

} // namespace Clipboard

#endif