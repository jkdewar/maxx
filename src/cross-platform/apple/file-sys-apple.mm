#import <Foundation/Foundation.h>
#include <string>
#include "utils/trace.h"
#include "cross-platform/file-sys.h"
#include "engine/maxx.h"

#if defined(__IPHONEOS__)
//--------------------------------------------------------------------
std::string GetSaveFolder()
{
  static bool firstTime = true;
  static std::string saveFolder;
  if (firstTime)
  {
    firstTime = false;
    
    NSArray *dirPaths = 0;
    NSString *docsDir = 0;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    saveFolder = std::string([docsDir cStringUsingEncoding: NSASCIIStringEncoding]);
  	Trace("Save folder: %s\n", saveFolder.c_str());
    MyMkdir(saveFolder);
	}
	return saveFolder;
}

#elif defined (__MACOSX__)
//--------------------------------------------------------------------
std::string GetSaveFolder()
{
  static bool firstTime = true;
  static std::string saveFolder;
  if (firstTime)
  {
    const std::string& gameName = Maxx::Get()->GetGameName();
    if (gameName.empty())
    {
      Trace("ERROR: You must call maxx.setGameName(\"MySuperCoolGame\") before calling   maxx.filesys.getSaveFolder()\n");
      exit(1);
      return "";
    }
    
    firstTime = false;
    
    NSArray *dirPaths = 0;
    NSString *docsDir = 0;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    saveFolder = std::string([docsDir cStringUsingEncoding: NSASCIIStringEncoding]);
    saveFolder += "/" + gameName;
  	Trace("Save folder: %s\n", saveFolder.c_str());
    MyMkdir(saveFolder);
	}
	return saveFolder;
}

#endif
