#if defined(__MACOSX__) || defined(__IPHONEOS__)

#include "cross-platform/find-files.h"
#include <fstream>
#include <glob.h>
#include "utils/assert.h"
#include <sys/stat.h>
#include <dirent.h>

namespace FindFiles {

class FileInfoImpl
{
public:
    FileInfoImpl() : mNextIndex(0) { mGlob.gl_offs = 0; }
	glob_t mGlob; 
	size_t mNextIndex;
};

FileInfo::~FileInfo()
{
    delete mImpl;
    mImpl = 0;
}

bool MyFindFirstFile(const std::string& path, const std::string& wildcard, FileInfo& fileInfo)
{
    if (fileInfo.mImpl == 0)
    {
        fileInfo.mImpl = new FileInfoImpl();
    }
    int result = glob((path + wildcard).c_str(), 0, 0, &fileInfo.mImpl->mGlob);
	if (result != 0)
	{
		return false;
	}
	return MyFindNextFile(fileInfo);
}

bool MyFindNextFile(FileInfo& fileInfo)
{
    Assert(fileInfo.mImpl);
    if (fileInfo.mImpl == 0)
    {
        return false;
    }
	if (fileInfo.mImpl->mNextIndex >= fileInfo.mImpl->mGlob.gl_pathc)
	{
		return false;
	}
	fileInfo.mName = fileInfo.mImpl->mGlob.gl_pathv[fileInfo.mImpl->mNextIndex];
	++fileInfo.mImpl->mNextIndex;
	fileInfo.mIsFolder = false;
	DIR* dir = opendir(fileInfo.mName.c_str());
	if (dir)
	{
		fileInfo.mIsFolder = true;
		closedir(dir);
	}
	return true;
}

} // namespace FindFiles

#endif
