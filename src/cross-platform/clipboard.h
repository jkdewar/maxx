#pragma once

#pragma warning(disable:4995) // <string> produces 'deprecated' warnings
#include <string>

namespace Clipboard
{
    std::string Read();
    void Write(const std::string& text);
}
