#include "cross-platform/file-sys.h"
#include "utils/trace.h"
#include <fstream>
#include <string>
#include <streambuf>
#include <sstream>

//--------------------------------------------------------------------
bool MyFileExists(const std::string& name)
{
    std::ifstream f(name.c_str());
    bool fileExists = f.is_open();
    return fileExists;
}

//--------------------------------------------------------------------
std::string GetFolderName(const std::string& path)
{
    std::string::size_type i = path.length();
    bool found = false;
    for (;;)
    {
        if (i == 0)
        {
            break;
        }
        if (path[i] == '/' || path[i] == '\\')
        {
            found = true;
            break;
        }
        --i;
    }
    if (found)
    {
        return path.substr(0, i);
    }
    else
    {
        return ".";
    }
}

//--------------------------------------------------------------------
std::string GetBaseNameNoExtension(const std::string& path)
{ 
  std::string ret = path;
  std::string::size_type i = ret.rfind("/");
  if (i != std::string::npos)
    ret = ret.substr(i + 1, ret.size() - i - 1);
  i = ret.rfind(".");
  if (i != std::string::npos)
    ret = ret.substr(0, i);
  return ret;
}

//--------------------------------------------------------------------
std::string GetExtension(const std::string& path)
{
  std::string::size_type i = path.rfind(".");
  if (i != std::string::npos)
    return path.substr(i + 1, path.size() - i - 1);
  return "";
}

//--------------------------------------------------------------------
bool ReadFile(const std::string& fileName, std::string& str)
{
  std::ifstream t(fileName.c_str());
  if (!t)
  {
    Trace("ReadFile: ERROR: Reading file \"%s\" failed.\n", fileName.c_str());
    return false;
  }
  std::stringstream buffer;
  buffer << t.rdbuf();
  str = buffer.str();
  return true;
}

//--------------------------------------------------------------------
bool WriteFile(const std::string& fileName, const std::string& str)
{
  std::ofstream f(fileName.c_str());
  if (!f)
  {
    Trace("WriteFile: ERROR: Writing to file \"%s\" failed.\n", fileName.c_str());
    return false;
  }
  f.write(str.c_str(), str.size());
  return true;
}
