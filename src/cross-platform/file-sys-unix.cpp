#if defined(__MACOSX__) || defined(__IPHONEOS__) || defined(__LINUX__)

#include "cross-platform/file-sys.h"
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fstream>
#include <iostream>

//--------------------------------------------------------------------
bool MyDeleteFile(const std::string& name)
{
	std::string command = "rm \"" + name + "\"";
	int retcode = system(command.c_str());
	bool success = (retcode == 0);
	return success;
}

//--------------------------------------------------------------------
bool MyChdir(const std::string& folder)
{
  int retcode = ::chdir(folder.c_str());
	return (retcode == 0);
}

//--------------------------------------------------------------------
bool MyMkdir(const std::string& folder)
{
	int retcode = ::mkdir(folder.c_str(), (mode_t)0700);
	return (retcode == 0);
}
#endif
