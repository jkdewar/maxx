#pragma once

#include <string>

bool MyFileExists(const std::string& name);
bool MyDeleteFile(const std::string& name);
std::string GetFolderName(const std::string& path);
bool MyChdir(const std::string& folder);
bool MyMkdir(const std::string& folder);
std::string GetBaseNameNoExtension(const std::string& path);
std::string GetExtension(const std::string& path);
std::string GetSaveFolder();
bool ReadFile(const std::string& fileName, std::string& str);
bool WriteFile(const std::string& fileName, const std::string& str);