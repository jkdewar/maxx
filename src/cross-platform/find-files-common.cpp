#include "cross-platform/find-files.h"

namespace FindFiles {

void FindFilesRecursive(const std::string& folder, const std::string& wildcard, std::vector<std::string>& files)
{
    // find files in folder
    {
        FileInfo fileInfo;
        bool done = !MyFindFirstFile(folder, wildcard, fileInfo);
        while (!done)
        {
            if (!fileInfo.IsFolder())
            {
                files.push_back(fileInfo.GetName());
            }
            done = !MyFindNextFile(fileInfo);
        }
    }
    // recursively call on all folders in folder
    {
        FileInfo fileInfo;
        bool done = !MyFindFirstFile(folder, "*", fileInfo);
        while (!done)
        {
            if (fileInfo.IsFolder())
            {
                const std::string& subfolder = fileInfo.GetName();
                if (subfolder.size() > 0 && subfolder[subfolder.size() - 1] != '.') // don't recurse "." or ".."
                {
                    FindFilesRecursive(subfolder + "/", wildcard, files);
                }
            }
            done = !MyFindNextFile(fileInfo);
        }
    }

}

} // namespace FindFiles
