#pragma once

#pragma warning(disable:4995) // <string> produces 'deprecated' warnings
#include <string>
#include <vector>

// Example usage:
//
// #include "cross-platform/find-files.h"
//
// FindFiles::FileInfo fileInfo;
// std::string folderName = "data/images/";
// bool done = !FindFiles::MyFindFirstFile(folderName, "*", fileInfo);
// while (!done)
// {
//     std::cout << "PathAndName: " << fileInfo.GetName() << std::endl;
//     std::cout << "IsFolder: " << (fileInfo.IsFolder() ? "yes" : "no") << std::endl;
//     done = !FindFiles::MyFindNextFile(fileInfo);
// }

namespace FindFiles
{
    class FileInfo;
    class FileInfoImpl;
    
    bool MyFindFirstFile(const std::string& path, const std::string& wildcard, FileInfo& fileInfo);
    bool MyFindNextFile(FileInfo& fileInfo);
    void FindFilesRecursive(const std::string& folder, const std::string& wildcard, std::vector<std::string>& files);

    class FileInfo
    {
        friend bool MyFindFirstFile(const std::string& path, const std::string& wildcard, FileInfo& fileInfo);
        friend bool MyFindNextFile(FileInfo& fileInfo);
    public:
        FileInfo() : mName(""), mIsFolder(false), mImpl(0) {}
        ~FileInfo();
        const std::string& GetName() { return mName; }
        bool IsFolder() const { return mIsFolder; }
    protected:
        std::string mName;
        bool mIsFolder;
        FileInfoImpl* mImpl;
    };
}
