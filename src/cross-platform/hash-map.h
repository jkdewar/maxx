#pragma once
#ifdef __GNUC__
#include <ext/hash_map>
#include <string>
namespace std { using namespace __gnu_cxx; }
namespace __gnu_cxx               
{ 
  // hash function for std::string  
  template<> struct hash< std::string >
  {
    size_t operator()( const std::string& x ) const
    {
      return hash< const char* >()( x.c_str() );
    }
  };
}
#else
#include <hash_map>
#endif
