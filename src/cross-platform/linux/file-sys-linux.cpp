#if defined(__LINUX__)

#include "cross-platform/file-sys.h"
#include "utils/trace.h"
#include "engine/maxx.h"
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fstream>
#include <iostream>

std::string GetSaveFolder()
{
  static bool firstTime = true;
  static std::string saveFolder;
  if (firstTime)
  {
    const std::string& gameName = Maxx::Get()->GetGameName();
    if (gameName.empty())
    {
      Trace("ERROR: You must call maxx.setGameName(\"MySuperCoolGame\") before calling   maxx.filesys.getSaveFolder()\n");
      exit(1);
      return "";
    }

    firstTime = false;
  	std::string homeFolder = ::getenv("HOME");
    std::string dotmaxxFolder = homeFolder + "/.maxx";
    saveFolder = dotmaxxFolder + "/" + gameName;
  	Trace("Save folder: %s\n", saveFolder.c_str());
    MyMkdir(dotmaxxFolder);
    MyMkdir(saveFolder);
	}
	return saveFolder;
}

#endif
