#pragma once

#if defined(__WIN32__)
#include <al.h>
#include <alc.h>
//#include <AL/alut.h>
#elif defined(__LINUX__)
#include <AL/al.h>
#include <AL/alc.h>
#else
#include <OpenAL/al.h>
#include <OpenAL/alc.h>
//#include "AL/alut.h"
#endif
