#pragma once
#if defined(__IPHONEOS__)
#include <OpenGLES/ES1/gl.h>
#elif defined(__MACOSX__)
#include <OpenGL/gl.h>
#define glOrthof glOrtho
#elif defined(__WIN32__)
#include "cross-platform/win32/windows-header.h"
#include <gl/gl.h>
#define glOrthof glOrtho
#elif defined(__LINUX__)
#include <GL/gl.h>
#else
#error unsupported platform
#endif
