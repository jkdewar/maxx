#if !defined(__IPHONEOS__)

#if defined(__WIN32__)
// winsock declares these, and so does SDL_net
#undef INADDR_ANY
#undef INADDR_BROADCAST
#undef INADDR_NONE
#endif

#include "SDL_net.h"

#endif
