#include "cross-platform/swap.h"
#include "cross-platform/sdl-headers.h"
#include <string.h>

#define STANDARD_BYTEORDER  SDL_BIG_ENDIAN
#define NATIVE_BYTEORDER    SDL_BYTEORDER

#if STANDARD_BYTEORDER != NATIVE_BYTEORDER
void SWAP32(void* data)
{
	Uint32 x = *((Uint32*)data);
	x = SDL_Swap32(x);
	memcpy(data, &x, sizeof(Uint32));
}

void SWAP32INT(int* data)
{
	SWAP32(data);
}

void SWAP32FLOAT(float* data)
{
	SWAP32(data);
}

void SWAP32BOOL(bool* data)
{
	*((int*)data) &= 0x01;
	SWAP32(data);
}

void SWAP32ENUM(void* data)
{
	SWAP32INT((int*)data);
}

#else

void SWAP32(void* data) {}
void SWAP32INT(int* data) {}
void SWAP32FLOAT(float* data) {}
void SWAP32BOOL(bool* data) {}
void SWAP32ENUM(void* data) {}

#endif
