#pragma once

#include "cross-platform/sdl-headers.h"

void SWAP32(void* data);
void SWAP32INT(int* data);
void SWAP32FLOAT(float* data);
void SWAP32BOOL(bool* data);
void SWAP32ENUM(void* data);