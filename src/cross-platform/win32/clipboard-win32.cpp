#if defined(__WIN32__)

#include "cross-platform/clipboard.h"
#include "cross-platform/win32/windows-header.h"

namespace Clipboard {

std::string Read()
{
    std::string clipboardText = "";
    if (OpenClipboard(0))
    {
        HANDLE handle = GetClipboardData(CF_TEXT);
        char *data = (char*)GlobalLock(handle);
        clipboardText = data;
        CloseClipboard();
    }
    return clipboardText;
}

void Write(const std::string& text)
{
    if (!OpenClipboard(0))
    {
        return;
    }
    EmptyClipboard();
    HGLOBAL handle = GlobalAlloc(GMEM_FIXED, text.size() + 1);
    char* data = (char*)GlobalLock(handle);
    memcpy(data, text.c_str(), text.size());
    data[text.size()] = '\0';
    SetClipboardData(CF_TEXT, handle);
    GlobalUnlock(handle);
    CloseClipboard();
}

} // namespace Clipboard

#endif
