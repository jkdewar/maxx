#if defined(__WIN32__)

#include "cross-platform/file-sys.h"
#include "cross-platform/win32/windows-header.h"
#include "engine/maxx.h"
#include "utils/trace.h"
#include <fstream>
#include <shlobj.h>

//--------------------------------------------------------------------
bool MyDeleteFile(const std::string& name)
{
  BOOL result = ::DeleteFileA(name.c_str());
  return (result == TRUE);
}

//--------------------------------------------------------------------
bool MyChdir(const std::string& folder)
{
  BOOL ret = ::SetCurrentDirectoryA(folder.c_str());
  return (ret == TRUE);
}

//--------------------------------------------------------------------
bool MyMkdir(const std::string& folder)
{
  SECURITY_ATTRIBUTES sa;
  sa.nLength = sizeof(sa);
  sa.lpSecurityDescriptor = NULL;
  sa.bInheritHandle = FALSE;
  BOOL ret = ::CreateDirectoryA(folder.c_str(), &sa);
  return (ret == TRUE);
}

//--------------------------------------------------------------------
std::string GetSaveFolder()
{
  const std::string& gameName = Maxx::Get()->GetGameName();
  if (gameName.empty())
  {
    Trace("ERROR: You must call maxx.setGameName(\"MySuperCoolGame\") before calling   maxx.filesys.getSaveFolder()\n");
    exit(1);
    return "";
  }
  
  char buf[MAX_PATH+1];
  BOOL result = ::SHGetSpecialFolderPathA(0, buf, CSIDL_PERSONAL, TRUE);
  if (result)
  {
    std::string path = std::string(buf);
    path += "/" + gameName;
    MyMkdir(path);
    return path;
  }
  else
  {
    Trace("ERROR: SHGetSpecialFolderPathA call failed\n");
    exit(1);
    return ""; // to suppress 'no return value' warning
  }
}

#endif
