#if defined(__WIN32__)

#include "cross-platform/find-files.h"
#include "cross-platform/win32/windows-header.h"
#include <fstream>
#include "utils/assert.h"

namespace FindFiles {

class FileInfoImpl
{
public:
    FileInfoImpl(const std::string& path) : path(path), handle(INVALID_HANDLE_VALUE) {}
	std::string path;
    HANDLE handle;
    WIN32_FIND_DATAA findData;
};

FileInfo::~FileInfo()
{
    delete mImpl;
    mImpl = 0;
}

bool MyFindFirstFile(const std::string& path, const std::string& wildcard, FileInfo& fileInfo)
{
    if (path.size() == 0 || path[path.size() - 1] != '/')
    {
        return false;
    }
    if (fileInfo.mImpl == 0)
    {
        fileInfo.mImpl = new FileInfoImpl(path);
    }
    fileInfo.mImpl->handle = ::FindFirstFileA((path + wildcard).c_str(), &fileInfo.mImpl->findData);
    bool found = (fileInfo.mImpl->handle != INVALID_HANDLE_VALUE);
    if (found)
    {
        fileInfo.mName = fileInfo.mImpl->path + std::string(fileInfo.mImpl->findData.cFileName);
        fileInfo.mIsFolder = ((fileInfo.mImpl->findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0);
    }
    return found;
}

bool MyFindNextFile(FileInfo& fileInfo)
{
    Assert(fileInfo.mImpl);
    if (fileInfo.mImpl == 0)
    {
        return false;
    }
    bool found = (::FindNextFileA(fileInfo.mImpl->handle, &fileInfo.mImpl->findData) == TRUE);
    if (found)
    {
        fileInfo.mName = fileInfo.mImpl->path + std::string(fileInfo.mImpl->findData.cFileName);
        fileInfo.mIsFolder = ((fileInfo.mImpl->findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0);
    }
    return found;
}

} // namespace FindFiles

#endif
