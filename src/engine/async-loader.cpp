#include "engine/async-loader.h"
#include "engine/async-worker.h"
#include "utils/assert.h"

namespace Maxx {

//--------------------------------------------------------------------
AsyncLoader::AsyncLoader()
{
}

//--------------------------------------------------------------------
AsyncLoader::~AsyncLoader()
{
}

//--------------------------------------------------------------------
void AsyncLoader::StartLoading(const std::string& fileName)
{
  WorkerMap::iterator i = workers_.find(fileName);
  if (i != workers_.end())
  {
    Assert(false);
    return;
  }
  Trace("AsyncLoader::StartLoading %s\n", fileName.c_str());
  AsyncWorker* worker = new AsyncWorker(fileName);
  workers_.insert(std::make_pair(fileName, worker));
  worker->Start();
}

//--------------------------------------------------------------------
AsyncLoader::LoadState AsyncLoader::GetLoadState(const std::string& fileName) const
{
  WorkerMap::const_iterator i = workers_.find(fileName);
  if (i == workers_.end())
    return NotLoading;
  const AsyncWorker* worker = i->second;
  return worker->GetState();
}

//--------------------------------------------------------------------
Entity* AsyncLoader::TakeLoadedEntity(const std::string& fileName)
{
  WorkerMap::iterator i = workers_.find(fileName);
  if (i == workers_.end())
    return 0;
  AsyncWorker* worker = i->second;
  Entity* entity = worker->TakeLoadedEntity();
  delete worker; worker = 0;
  workers_.erase(i);
  return entity;
}

} // Maxx
