#pragma once
#include "cross-platform/sdl-headers.h"
#include <string>
#include <map>

namespace Maxx {
  
class AsyncWorker;
class Entity;

//--------------------------------------------------------------------
// Loads entities asynchronously (on a separate thread)
class AsyncLoader
{
public:
  AsyncLoader();
  ~AsyncLoader();
  
  enum LoadState
  {
    NotLoading,
    Loading,
    LoadComplete,
    LoadFailed,
  };
  
  void StartLoading(const std::string& fileName);
  LoadState GetLoadState(const std::string& fileName) const;
  Entity* TakeLoadedEntity(const std::string& fileName);
  
private:
  typedef std::map<std::string, AsyncWorker*> WorkerMap;
  WorkerMap workers_;
};
  
} // Maxx
