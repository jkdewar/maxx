#include "engine/async-worker.h"
#include "engine/entity.h"
#include "utils/assert.h"

namespace Maxx {

//--------------------------------------------------------------------
int AsyncWorker_ThreadFunc(void* userData)
{
  //Trace("userData=%p\n",userData);
  AsyncWorker* worker = static_cast<AsyncWorker*>(userData);
  worker->Load();
  return 0;
}

//--------------------------------------------------------------------
AsyncWorker::AsyncWorker(const std::string& fileName)
  : thread_(0)
  , L_(0)
  , state_(AsyncLoader::NotLoading)
  , fileName_(fileName)
  , entity_(0)
{
}

//--------------------------------------------------------------------
AsyncWorker::~AsyncWorker()
{
  if (thread_)
  {
    SDL_WaitThread(thread_, 0); thread_ = 0;
  }
  if (entity_)
  {
    delete entity_; entity_ = 0;
  }
  Assert(!L_);
}

//--------------------------------------------------------------------
void AsyncWorker::Start()
{
  //Trace("this=%p\n",this);
  Assert(L_ == 0);
  L_ = lua_open();
  lua_gc(L_, LUA_GCSTOP, 1);
  thread_ = SDL_CreateThread(&AsyncWorker_ThreadFunc, "AsyncWorker", this);
  Assert(thread_);
}

//--------------------------------------------------------------------
AsyncLoader::LoadState AsyncWorker::GetState() const
{
  return state_;
}
 
//--------------------------------------------------------------------
Entity* AsyncWorker::TakeLoadedEntity()
{
  if (thread_)
  {
    SDL_WaitThread(thread_, 0); thread_ = 0;
  }
  Entity* e = entity_;
  entity_ = 0;
  return e;
}

//--------------------------------------------------------------------
void AsyncWorker::Load()
{
  SetState(AsyncLoader::Loading, 0);
  Entity* entity = Entity::Load(fileName_, /*isPrefab=*/false, L_);
  if (entity)
    SetState(AsyncLoader::LoadComplete, entity);
  else
    SetState(AsyncLoader::LoadFailed, 0);
  
  lua_close(L_); L_ = 0;
}

//--------------------------------------------------------------------
void AsyncWorker::SetState(AsyncLoader::LoadState state, Entity* entity)
{
  state_ = state;
  entity_ = entity;
}

} // Maxx
