#pragma once
#include "engine/async-loader.h"
#include "cross-platform/sdl-headers.h"
#include "cross-platform/lua-headers.h"
#include <string>

namespace Maxx {
  
class Entity;

//--------------------------------------------------------------------
class AsyncWorker
{
public:  
  AsyncWorker(const std::string& fileName);
  ~AsyncWorker();

  void Start();
  AsyncLoader::LoadState GetState() const;
  Entity* TakeLoadedEntity();

private:
  void Load();
  void SetState(AsyncLoader::LoadState state, Entity* entity);
  
private:
  friend int AsyncWorker_ThreadFunc(void*);
  
  SDL_Thread*  thread_;
  lua_State* L_;
  AsyncLoader::LoadState state_;
  std::string fileName_;
  Entity* entity_;
};

} // Maxx
