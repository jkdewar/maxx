#include "engine/component-factory.h"
#include "engine/component.h"
#include "engine/property-table.h"
#include "engine/maxx.h"
#include "utils/guid.h"
#include <sstream>

namespace Maxx {
  
//--------------------------------------------------------------------
ComponentFactory& ComponentFactory::Instance()
{
  static ComponentFactory componentFactory;
  return componentFactory;
}
  
//--------------------------------------------------------------------
void ComponentFactory::RegisterComponentClass(const std::string& className, ComponentFactoryPtr componentFactoryPtr)
{
  //Trace("RegisterComponentClass '%s'\n", className.c_str());
  componentFactoryMap_[className] = componentFactoryPtr;
}

//--------------------------------------------------------------------
Component* ComponentFactory::ConstructComponent(const std::string& str)
{
  PropertyTable table;
  size_t index = 0;
  bool ok = PropertyTable::Unstringify(Maxx::Get()->GetLuaState(), str, index, table);
  if (!ok)
  {
    //Trace("ConstructComponent: Error parsing table\n");
    return 0;
  }
  return ConstructComponent(table);
}

//--------------------------------------------------------------------
Component* ComponentFactory::ConstructComponent(const PropertyTable& table)
{
   // Construct the component
  
  // type
  const std::string& type = table.GetString("type", "");
  if (type.empty())
  {
    Trace("ConstructComponent: ERROR: Missing type on component.\n");
    Trace("table=%s\n", table.Stringify().c_str());
    return 0;
  }
  
  // name
  const std::string& name = table.GetString("name", "");
  // if (name.empty())
  // {
  //   name = "autonamed " + type;
  // }
  
  ComponentFactoryMap::iterator i = componentFactoryMap_.find(type);
  if (i == componentFactoryMap_.end())
  {
    Trace("ConstructComponent: ERROR: Unknown component type '%s'\n", type.c_str());
    return 0;
  }
  const PropertyTable* propertyTable = table.GetTable("properties");
  ComponentFactoryPtr factory = i->second;
  Component* component = (factory)(propertyTable ? *propertyTable : PropertyTable());
  if (!component)
    return 0;
    
  component->SetName(name);
  
  if (propertyTable)
  {
    // Assign component properties
    for (PropertyTable::MapFromString::const_iterator
         i = propertyTable->mapFromString.begin(); i != propertyTable->mapFromString.end(); ++i)
    {
      const std::string& name = i->first;
      const Property& value = i->second;
      component->SetProperty(name, value);
    }
  }
  return component;
}

} // Maxx
