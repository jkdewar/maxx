#pragma once

#include <string>
#include <map>
#include "utils/assert.h"
#include "engine/property-table.h"

namespace Maxx {

class Component;
class PropertyTable;
  
class ComponentFactory
{
public:

  static ComponentFactory& Instance();
  
  typedef Component* (*ComponentFactoryPtr)(const PropertyTable&); // pointer to a function that returns a Component*

  void RegisterComponentClass(const std::string& className, ComponentFactoryPtr componentFactoryPtr);
  
  Component* ConstructComponent(const std::string& table);
  Component* ConstructComponent(const PropertyTable& table);

 
private:
  typedef std::map<std::string, ComponentFactoryPtr> ComponentFactoryMap;
  ComponentFactoryMap componentFactoryMap_;
};

} // Maxx

//--------------------------------------------------------------------
// Use this macro in the .cpp file for your component.
// See system/component-t.h for details
#define COMPONENT_FACTORY(T)                                                            \
  namespace { Maxx::Component* Construct ## T(const Maxx::PropertyTable& propertyTable) \
  { return new T; } }                                                                   \
  namespace namespace ## T {                                                            \
  class Whatever {                                                                      \
  public: Whatever() {                                                                  \
    ComponentFactory::Instance().RegisterComponentClass(#T, &(Construct ##T));          \
    T :: RegisterProperties(); } };                                                     \
    static Whatever whatever;                                                           \
  };                                                                                    \

