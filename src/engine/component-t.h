#pragma once

/*
  // .h
  class MyComponent : public ComponentT<MyComponent>
  {
  public:
    double GetX() const { return x_; }
    std::string GetName() const { return name_; }
    
    BEGIN_COMPONENT_PROPERTIES()
      COMPONENT_PROPERTY_GETTER(MyComponent, "x", GetX)
      COMPONENT_PROPERTY_GETTER(MyComponent, "name", GetName)
    END_COMPONENT_PROPERTIES()
  
    COMPONENT_PROPERTY_GETTER_WRAP(GetX)
    COMPONENT_PROPERTY_GETTER_WRAP(GetName)

  private:
    double x_;
    std::string name_;
  };
  
  // .cpp
  COMPONENT_FACTORY(MyComponent)
*/

#include "engine/component.h"
#include "engine/component-factory.h"
#include "engine/entity.h"
#include <string>
#include <map>
#include "utils/assert.h"
#include <set>
//#include "cross-platform/hash-map.h"

namespace Maxx {

//--------------------------------------------------------------------
class PropertyDescription
{
public:
  virtual ~PropertyDescription() {}
  virtual bool HasGetter() const = 0;
  virtual bool HasSetter() const = 0;
  virtual Property CallGetter(const Component*) const = 0;
  virtual void CallSetter(Component*, const Property&) = 0;
};

//--------------------------------------------------------------------
template <class T>
class PropertyDescriptionT : public PropertyDescription
{
public:
  PropertyDescriptionT(Property (T::*getter)() const, void (T::*setter)(const Property&))
    : getter(getter), setter(setter) {}
  bool HasGetter() const { return (getter != 0); }
  bool HasSetter() const { return (setter != 0); }
  Property CallGetter(const Component* component) const { const T* t = (static_cast<const T*>(component)); return (t->*getter)(); }
  void CallSetter(Component* component, const Property& property) { T* t = (static_cast<T*>(component)); (t->*setter)(property); }
  //bool operator<(const PropertyDescriptionT& d) const { return false; }
  Property (T::*getter)() const;
  void (T::*setter)(const Property&);
};
typedef std::map<std::string, PropertyDescription*> PropertyDescriptionMap;

//--------------------------------------------------------------------
template <class T>
class ComponentT : public Component
{
public:
   
  //--------------------------------------------------------------------
  ComponentT() 
    : Component()
  {
  }
  
  //--------------------------------------------------------------------
  virtual ~ComponentT()
  {
  }

  //--------------------------------------------------------------------
  virtual void GetAllPropertyNames(PropertyNames& propertyNames) const
  {
    for (PropertyDescriptionMap::const_iterator i = GetPropertyDescriptionMap().begin(); 
         i != GetPropertyDescriptionMap().end(); ++i)
    {
      propertyNames.insert(i->first.c_str());
    }
  }
  
  //--------------------------------------------------------------------
  virtual Property GetProperty(const std::string& name) const
  {
    PropertyDescriptionMap::const_iterator i = GetPropertyDescriptionMap().find(name);
    if (i != GetPropertyDescriptionMap().end())
    {
      const PropertyDescription* propertyDescription = i->second;
      if (propertyDescription && propertyDescription->HasGetter())
        return propertyDescription->CallGetter(this);
    }
    return Property();
  }
  
  //--------------------------------------------------------------------
  virtual void SetProperty(const std::string& name, const Property& value)
  {
    PropertyDescriptionMap::const_iterator i = GetPropertyDescriptionMap().find(name);
    if (i == GetPropertyDescriptionMap().end())
    {
      Trace("ComponentT<%s>::SetProperty: Unrecognized property '%s'\n", GetTypeString(), name.c_str());
      Assert(false);
      return;
    }
    PropertyDescription* propertyDescription = i->second;
    if (!propertyDescription || !propertyDescription->HasSetter())
    {
      Trace("ComponentT<%s>::SetProperty: No setter for property '%s'\n", GetTypeString(), name.c_str());
      Assert(false);
    }
    else
    {
      propertyDescription->CallSetter(this, value);
      MarkPropertyAsSet(name);
    }
  }
  
  //--------------------------------------------------------------------
  virtual bool IsPropertySet(const std::string& name) const
  {
    return (definedPropertiesSet_.find(name) != definedPropertiesSet_.end());
  }
    
  //--------------------------------------------------------------------
  virtual void SetPropertySecretly(const std::string& name, const Property& value)
  {
    bool wasSet = IsPropertySet(name);
    SetProperty(name, value);
    if (!wasSet)
    {
      std::set<std::string>::iterator i = definedPropertiesSet_.find(std::string(name));
      if (i != definedPropertiesSet_.end())
        definedPropertiesSet_.erase(i);
    }
  }
  
  //--------------------------------------------------------------------
  virtual void MarkAllPropertiesUnset()
  {
    definedPropertiesSet_.clear();
  }
  
public:
  //--------------------------------------------------------------------
  typedef std::map<std::string, PropertyDescription*> PropertyDescriptionMap;
  static PropertyDescriptionMap& GetPropertyDescriptionMap()
  {
    static PropertyDescriptionMap propertyDescriptionMap;
    return propertyDescriptionMap;
  }
  
  //--------------------------------------------------------------------
  void MarkPropertyAsSet(const std::string& name)
  {
    definedPropertiesSet_.insert(name);
  }
  
protected:
  //--------------------------------------------------------------------
  std::set<std::string> definedPropertiesSet_; // which properties have been set?
};

//--------------------------------------------------------------------
#define BEGIN_COMPONENT_PROPERTIES(CLASS)                                       \
  static void RegisterProperties()                                              \
  {                                                                             \
    typedef CLASS T;

//--------------------------------------------------------------------
#define COMPONENT_PROPERTY_GETTER(NAME, GETTER)                                       \
    GetPropertyDescriptionMap()[NAME] =                                               \
      new Maxx::PropertyDescriptionT<T>(NAME, &T::PropertyGetterWrap_ ## GETTER, 0);  \

//--------------------------------------------------------------------
#define COMPONENT_PROPERTY_GETTER_SETTER(NAME, GETTER, SETTER)                        \
    GetPropertyDescriptionMap()[NAME] =                                               \
      new Maxx::PropertyDescriptionT<T>(&T::PropertyGetterWrap_ ## GETTER,            \
                                        &T::PropertySetterWrap_ ## SETTER);           \

//--------------------------------------------------------------------
#define END_COMPONENT_PROPERTIES()    \
  }                                   \

//--------------------------------------------------------------------
#define COMPONENT_PROPERTY_GETTER_WRAP(GETTER)          \
  Maxx::Property PropertyGetterWrap_ ## GETTER() const  \
  { return Maxx::Property(GETTER()); }                  \
  
#define COMPONENT_PROPERTY_GETTER_SETTER_WRAP(NAME, GETTER, SETTER)     \
  Maxx::Property PropertyGetterWrap_ ## GETTER() const                  \
  { return Maxx::Property(GETTER()); }                                  \
  void PropertySetterWrap_ ## SETTER(const Property& value)             \
  { SETTER(value); MarkPropertyAsSet(NAME); }                           \


} // Maxx
