#include "engine/component.h"
#include "engine/component-factory.h"
#include "engine/entity.h"
#include "utils/assert.h"

namespace Maxx {
  
//--------------------------------------------------------------------
bool Component::AreAnyPropertiesSet() const
{
  PropertyNames propertyNames;
  GetAllPropertyNames(propertyNames);
  for (PropertyNames::const_iterator 
       i = propertyNames.begin();
       i != propertyNames.end();
       ++i)
  {
    if (IsPropertySet(*i))
      return true;
  }
  return false;
}

//--------------------------------------------------------------------
std::string Component::Stringify() const
{
  PropertyTable table;
  Serialize(table);
  return table.Stringify();
}

//--------------------------------------------------------------------
void Component::Serialize(PropertyTable& table) const
{
  const std::string& type = GetTypeString();
  table.Insert("type", type);
  PropertyTable propertyTable;
  
  if (!name_.empty())
    table.Insert("name", name_.c_str());

  PropertyNames propertyNames;
  GetAllPropertyNames(propertyNames);

  for (PropertyNames::const_iterator i = propertyNames.begin(); i != propertyNames.end(); ++i)
  {
    const std::string& name = *i;
    
    if (!IsPropertySet(name))
      continue;
    
    Property value = GetProperty(name);
    propertyTable.mapFromString.insert(std::make_pair(name, value));
  }
  table.Insert("properties", propertyTable);
}

//--------------------------------------------------------------------
/*static*/ Component* Component::Deserialize(const PropertyTable& table)
{
  static ComponentFactory* factory = 0;
  if (!factory)
    factory = &ComponentFactory::Instance();
  Component* component = factory->ConstructComponent(table);
  return component; // Note: component still needs to have Init() called on it
}

} // Maxx
