#pragma once

#include "engine/property.h"
#include "utils/guid.h"
#include <string>
#include <set>

namespace Maxx {

class Entity;

//--------------------------------------------------------------------
class Component
{
public:
  Component() : entity_(0), name_(""), dead_(false) {}
  virtual ~Component() {}
  
  virtual bool Init() { return true; }
  
  virtual const char* GetTypeString() const = 0;
  
  const std::string& GetName() const { return name_; }
  void SetName(const std::string& name) { name_ = name; }
  
  virtual void Update(double dt) {}
  virtual void PreUpdate() {}
  virtual void PostUpdate() {}
  virtual void Draw() const {}
  
  typedef std::set<std::string> PropertyNames;
  virtual void      GetAllPropertyNames(PropertyNames& propertyNames) const = 0;
  virtual Property  GetProperty(const std::string& name) const = 0;
  //virtual void      PushPropertyOnLuaStack(const std::string& name) const = 0;
  virtual void      SetProperty(const std::string& name, const Property& value) = 0;
  virtual bool      IsPropertySet(const std::string& name) const = 0;
  virtual void      SetPropertySecretly(const std::string& name, const Property& value) = 0;
  virtual void      MarkAllPropertiesUnset() = 0;
  bool              AreAnyPropertiesSet() const;
   
  void Die() { dead_ = true; }
  bool IsDead() const { return dead_; }
  
  void SetEntity(Entity* entity) { entity_ = entity; }
  Entity* GetEntity() const { return entity_; }
  
  std::string Stringify() const;
  
  void Serialize(PropertyTable& propertyTable) const;
  static Component* Deserialize(const PropertyTable& propertyTable);
  
private:
  Entity* entity_;
  std::string name_;
  bool dead_;
};

#define COMPONENT_TYPE_STRING(T)                                                                  \
  virtual const char* GetTypeString() const { return T; }   \
  static const char* GetTypeStringStatic() { return T; }    \
    
#define BEGIN_PROPERTY_GETTERS_SETTERS() \

#define PROPERTY_GETTER_SETTER(name, type, get, set) \

#define END_PROPERTY_GETTERS_SETTERS() \

#define BARE_PROPERTY(name, type) \
  
} // Maxx
