#include "engine/entity.h"
#include "engine/component.h"
#include "engine/component-factory.h"
#include "cross-platform/file-sys.h"
#include "file-system/file-cache.h"
#include "engine/maxx.h"
#include "engine/prefab-manager.h"
#include "components/transform-component.h"
#include "components/body-component.h"
#include "messaging/i-message-receiver.h"
#include "messaging/postmaster.h"
#include "cross-platform/box2d-headers.h"
#include "engine/glue.h"
#include "utils/guid.h"
#include "physics/physics.h"
#include "utils/assert.h"
#include "cross-platform/sdl-headers.h"
#include "profiler/profile_scope.h"
#include "audio/audio.h"
#include "graphics/display.h"
#include "engine/property-cache.h"
#include <list>
#include <memory.h>

namespace Maxx {
  
/*static*/ Entity::GuidToEntity Entity::guidToEntity_;
/*static*/ b2Contact* Entity::preSolveContact_ = 0;

//--------------------------------------------------------------------
Entity::Entity(const std::string& guid)
  : components_()
  , children_()
  , dead_(false)
  , updating_(false)
  , guid_(guid)
  , name_("entity")
  , serialized_(true)
  , inited_(false)
  , transformComponent_(0)
{
  if (guid_.empty())
    guid_ = Maxx::Utils::NewGuid();

  GuidToEntity::iterator i = guidToEntity_.find(guid_);
  //Assert(i == guidToEntity_.end());
  if (i != guidToEntity_.end())
  {
    Trace("WARNING: duplicate entity guid %s\n", guid_.c_str());
    guidToEntity_.erase(i);
  }
  guidToEntity_.insert(std::make_pair(guid_, this));
}

//--------------------------------------------------------------------
void Entity::Init()
{
  Assert(!inited_);
  if (inited_)
    return;
  inited_ = true;
}

//--------------------------------------------------------------------
static void DeleteComponents(Entity::ComponentList& components)
{
   for (int pass = 0; pass < 2; ++pass)
   {
     for (Entity::ComponentList::iterator i = components.begin(); i != components.end(); /**/)
     {
       Component* component = *i;
       bool isBody = (strcmp(component->GetTypeString(), "BodyComponent") == 0);
       if ((!isBody && pass == 0) || (isBody && pass == 1))
       {
         delete component;
         i = components.erase(i);
         continue;
       }
       ++i;
     }
   }
}

//--------------------------------------------------------------------
Entity::~Entity()
{
  for (EntityList::iterator i = children_.begin(); i != children_.end(); ++i)
  {
    Entity* entity = *i;
    delete entity;
  }
  for (EntityList::iterator i = pendingChildren_.begin(); i != pendingChildren_.end(); ++i)
  {
    Entity* entity = *i;
    delete entity;
  }
  DeleteComponents(components_);
  DeleteComponents(pendingComponentsAlreadyInited_);
  DeleteComponents(pendingComponentsNotInited_);

  GuidToEntity::iterator i = guidToEntity_.find(guid_);
  if (i != guidToEntity_.end() && i->second == this)
    guidToEntity_.erase(i);
}

//--------------------------------------------------------------------
void Entity::Update(double dt)
{
  updating_ = true;
  ComponentList copy=components_;
  for (ComponentList::iterator i = copy.begin(); i != copy.end(); ++i)
  {
    Component* component = *i;
    component->Update(dt);
  }
  for (EntityList::iterator i = children_.begin(); i != children_.end(); ++i)
  {
    Entity* entity = *i;
    entity->Update(dt);
  }
  RemoveTheDead();
  updating_ = false;

  // TODO: remove
  TransformComponent* transformComponent = GetTransformComponent();
  if (transformComponent)
  {
    if (transformComponent->GetY() > 1200)
      Die();
  }
}

//--------------------------------------------------------------------
void Entity::PreUpdate()
{
  updating_ = true;
  for (ComponentList::iterator i = components_.begin(); i != components_.end(); ++i)
  {
    Component* component = *i;
    component->PreUpdate();
  }
  for (EntityList::iterator i = children_.begin(); i != children_.end(); ++i)
  {
    Entity* entity = *i;
    entity->PreUpdate();
  }
  updating_ = false;
}

//--------------------------------------------------------------------
void Entity::PostUpdate()
{
  AddPending();  
  updating_ = true;
  for (ComponentList::iterator i = components_.begin(); i != components_.end(); ++i)
  {
    Component* component = *i;
    component->PostUpdate();
  }
  for (EntityList::iterator i = children_.begin(); i != children_.end(); ++i)
  {
    Entity* entity = *i;
    entity->PostUpdate();
  }
  updating_ = false;
}

//--------------------------------------------------------------------
void Entity::Draw() const
{
  for (ComponentList::const_iterator i = components_.begin(); i != components_.end(); ++i)
  {
    const Component* component = *i;
    component->Draw();
  }
  for (EntityList::const_iterator i = children_.begin(); i != children_.end(); ++i)
  {
    const Entity* entity = *i;
    entity->Draw();
  }
}

//--------------------------------------------------------------------
bool Entity::AddComponent(Component* component, bool initialize)
{
  if (std::find(pendingComponentsNotInited_.begin(), pendingComponentsNotInited_.end(), component) != pendingComponentsNotInited_.end())
    return false;
  if (std::find(pendingComponentsAlreadyInited_.begin(), pendingComponentsAlreadyInited_.end(), component) != pendingComponentsAlreadyInited_.end())
    return false;
  if (std::find(components_.begin(), components_.end(), component) != components_.end())
    return false;

  component->SetEntity(this);
  if (initialize)
  {
    if (/*updating_ || */Maxx::Get()->GetPhysics()->UpdateInProgress())
    {
      pendingComponentsNotInited_.push_back(component);
    }
    else
    {
      InitComponent(component);
      if (component)
        components_.push_back(component);
      else
        return false;
    }
  }
  else
  {
    if (component)
      components_.push_back(component);
    else
      return false;
  }
  return true;
}

//--------------------------------------------------------------------
void Entity::GetComponentsOfType(const std::string& type, ComponentList& componentList) const
{
  for (ComponentList::const_iterator i = components_.begin(); i != components_.end(); ++i)
  {
    Component* component = *i;
    if (component->GetTypeString() == type)
      componentList.push_back(component);
  }
  for (ComponentList::const_iterator i = pendingComponentsAlreadyInited_.begin(); i != pendingComponentsAlreadyInited_.end(); ++i)
  {
    Component* component = *i;
    if (component->GetTypeString() == type)
      componentList.push_back(component);
  }
  for (ComponentList::const_iterator i = pendingComponentsNotInited_.begin(); i != pendingComponentsNotInited_.end(); ++i)
  {
    Component* component = *i;
    if (component->GetTypeString() == type)
      componentList.push_back(component);
  }
}

//--------------------------------------------------------------------
Component* Entity::GetComponentOfType(const std::string& type) const
{
  for (ComponentList::const_iterator i = components_.begin(); i != components_.end(); ++i)
  {
    Component* component = *i;
    if (component->GetTypeString() == type)
      return component;
  }
  for (ComponentList::const_iterator i = pendingComponentsAlreadyInited_.begin(); i != pendingComponentsAlreadyInited_.end(); ++i)
  {
    Component* component = *i;
    if (component->GetTypeString() == type)
      return component;
  }
  for (ComponentList::const_iterator i = pendingComponentsNotInited_.begin(); i != pendingComponentsNotInited_.end(); ++i)
  {
    Component* component = *i;
    if (component->GetTypeString() == type)
      return component;
  }
  return 0;
}

//--------------------------------------------------------------------
Component* Entity::GetComponentByName(const std::string& type, const std::string& name) const
{
  ComponentList componentList;
  GetComponentsOfType(type, componentList);
  Component* foundComponent = 0;
  for (ComponentList::iterator i = componentList.begin(); i != componentList.end(); ++i)
  {
    Component* component = *i;
    if (component->GetName() == name)
    {
      if (foundComponent)
      {
        //Trace("Entity::GetComponentByName: WARNING: Multiple components of type '%s' with name '%s'\n", type.c_str(), name.c_str());
      }
      foundComponent = component;
    }
  }
  return foundComponent;
}

//--------------------------------------------------------------------
void Entity::GetAllComponents(ComponentList& componentList) const
{
  for (ComponentList::const_iterator i = components_.begin(); i != components_.end(); ++i)
    componentList.push_back(*i);
  for (ComponentList::const_iterator i = pendingComponentsAlreadyInited_.begin(); i != pendingComponentsAlreadyInited_.end(); ++i)
    componentList.push_back(*i);
  for (ComponentList::const_iterator i = pendingComponentsNotInited_.begin(); i != pendingComponentsNotInited_.end(); ++i)
    componentList.push_back(*i);
}

//--------------------------------------------------------------------
TransformComponent* Entity::GetTransformComponent()
{
  if (!transformComponent_)
    transformComponent_ = GetComponentOfType<TransformComponent>();
  return transformComponent_;
}

//--------------------------------------------------------------------
bool Entity::AddChildEntity(Entity* entity)
{
  if (Maxx::Get()->UpdateInProgress())
    pendingChildren_.push_back(entity);
  else
  {
    entity->Init();
    children_.push_back(entity);
  }
  return true;
}

//--------------------------------------------------------------------
void Entity::GetChildren(EntityList& entityList) const
{
  for (EntityList::const_iterator 
       i = children_.begin(); i != children_.end(); ++i)
  {
    entityList.push_back(*i);
  }
  for (EntityList::const_iterator 
       i = pendingChildren_.begin(); i != pendingChildren_.end(); ++i)
  {
    entityList.push_back(*i);
  }
}

//--------------------------------------------------------------------
void Entity::Die()
{ 
  bool firstTime = !dead_; 
  dead_ = true; 
  if (firstTime) 
  { 
    PropertyTable msg; 
    SendMessageTo("Died", msg, this); 
  } 
}

//--------------------------------------------------------------------
/*static*/ Entity* Entity::Load(const std::string& fileName, bool isPrefab, lua_State* L)
{
  if (!L)
    L = Maxx::Get()->GetLuaState();
  Property* prop = Maxx::Get()->GetPropertyCache()->LoadPropertyFromFile(fileName, L);
  if (!prop)
    return 0;
  Entity* entity = Entity::Deserialize(*prop, isPrefab, L);
  if (!entity)
  {
    Trace("Entity::Load: Error deserializing '%s'\n", fileName.c_str());
    return 0;
  }
  return entity;
}

//--------------------------------------------------------------------
/*static*/ bool Entity::Preload(const std::string& fileName, lua_State* L)
{
  if (!L)
    L = Maxx::Get()->GetLuaState();
  Property* prop = Maxx::Get()->GetPropertyCache()->LoadPropertyFromFile(fileName, L);
  return (prop != 0);
}

//--------------------------------------------------------------------
std::string Entity::Stringify() const
{
  PropertyTable table;
  Serialize(table);
  return table.Stringify();
}

//--------------------------------------------------------------------
/*static*/ Entity* Entity::Unstringify(const std::string& str, bool isPrefab, lua_State* L)
{
  if (!L)
    L = Maxx::Get()->GetLuaState();
  PropertyTable table;
  size_t index = 0;
  bool ok = PropertyTable::Unstringify(L, str, index, table);
  if (!ok)
  {
    return 0;
  }
  Entity* entity = Deserialize(table, isPrefab, L);
  return entity;
}

//--------------------------------------------------------------------
// table is the entity table, not the table of components
// returns the 1-based index, or 0 if not found
static int FindCorrespondingComponent(const PropertyTable& table,
                                      const PropertyTable& prefabComponent)
{
  const PropertyTable* components = table.GetTable("components");
  if (!components)
    return 0;
  static std::string typeConstant("type");
  static std::string nameConstant("name");
  const std::string& prefabComponentType = prefabComponent.GetString(typeConstant,"");
  const std::string& prefabComponentName = prefabComponent.GetString(nameConstant,"");
  for (PropertyTable::MapFromInt::const_iterator
       i = components->mapFromInt.begin();
       i != components->mapFromInt.end(); ++i)
  {
    const int index = i->first;
    const Property& component = i->second;
    if (!component.IsTable())
      continue;
    const std::string& componentType = component.GetTable().GetString(typeConstant,"");
    const std::string& componentName = component.GetTable().GetString(nameConstant,"");
    if (componentType == prefabComponentType && 
        componentName == prefabComponentName)
    {
      return index;
    }
  }
  return 0;
}

//--------------------------------------------------------------------
void Entity::Serialize(PropertyTable& table) const
{
  if (!Serialized())
    return;
  
  // TODO: not currently saving the guid... table.Set("guid", GetGuid());
  if (!GetName().empty())
    table.Set("name", GetName());
  
  if (IsPrefabInstance())
    table.Set("prefab", prefabFileName_);

  // Make a table of child entities
  {
    PropertyTable* childrenTable = 0;
    int index = 1;
    for (EntityList::const_iterator i = children_.begin(); i != children_.end(); ++i)
    {
      const Entity* child = *i;
      PropertyTable childTable;
      child->Serialize(childTable);
      if (childTable.IsEmpty())
        continue;
      if (index == 1)
      {
        table.mapFromString["children"] = PropertyTable();
        childrenTable = table.GetTable("children");
      }
      childrenTable->mapFromInt[index] = childTable;
      ++index;
    }
  }

  // make a table of components
  {
    PropertyTable* componentsTable = 0;
    int index = 1;
    for (ComponentList::const_iterator i = components_.begin(); i != components_.end(); ++i)
    {
      const Component* component = *i;
      if (!component->AreAnyPropertiesSet())
        continue;
      if (index == 1)
      {
        table.mapFromString["components"] = PropertyTable();
        componentsTable = table.GetTable("components");
      }
      PropertyTable componentTable;
      componentsTable->mapFromInt[index] = PropertyTable();
      component->Serialize(*componentsTable->mapFromInt[index].GetTablePtr());
      ++index;
    }
  }
}

//--------------------------------------------------------------------
static void BuildComponent(Entity* entity, 
                           const PropertyTable& entityTable,
                           const Property& value, // component table
                           const PropertyTable* prefabTable,  
                           std::list<Component*>& componentsToInit)
{
  if (!value.IsTable())
  {
    Trace("Entity::Deserialize: WARNING: A non-table component was encountered. (skipped)\n");
    return;
  }
  const PropertyTable& table = value.GetTable();
  
  const PropertyTable* prefabComponentsTable = 0;
  if (prefabTable)
    prefabComponentsTable = prefabTable->GetTable("components");

  // Deserialize the component
  Component* component = Component::Deserialize(table);
  if (!component)
  {
    Trace("Entity::Deserialize: WARNING: ConstructComponent failed. (skipped)\n");
    return;
  }

  entity->AddComponent(component, /*initialize=*/false);
  
  // Prefabbing
  if (prefabComponentsTable)
  {
    // Is there a component in the prefab corresponding to the one in the instance?
    int prefabComponentIndex = FindCorrespondingComponent(*prefabTable, table);
    if (prefabComponentIndex >= 1)
    {
      const PropertyTable* prefabComponentTable = prefabComponentsTable->GetTable(prefabComponentIndex);
      const PropertyTable* prefabComponentPropertiesTable = prefabComponentTable->GetTable("properties");
      // Secretly set any properties that are in the prefab but not the instance.
      const PropertyTable* propertiesTable = table.GetTable("properties");
      for (PropertyTable::MapFromString::const_iterator
           i = prefabComponentPropertiesTable->mapFromString.begin();
           i != prefabComponentPropertiesTable->mapFromString.end();
           ++i)
      {
        const char* propertyName = i->first.c_str();
        const Property& propertyValue = i->second;
        if (!propertiesTable->HasKey(propertyName))
        {
          component->SetPropertySecretly(propertyName, propertyValue);
          //printf("secretly set %s\n", propertyName.c_str());
        }
        else
        {
          //printf("didnt set %s\n", propertyName.c_str());
        }
      }
    }
  }
  
  component->SetName(table.GetString("name", ""));
  componentsToInit.push_back(component);
}

//--------------------------------------------------------------------
static void BuildMissingComponentsFromPrefab(Entity* entity, 
                                             const PropertyTable* instanceEntityTable, 
                                             const PropertyTable* prefabTable, 
                                             std::list<Component*>& componentsToInit)
{
  const PropertyTable* prefabComponentsTable = prefabTable ?prefabTable->GetTable("components") : 0;
  if (!prefabComponentsTable)
    return;
  // for each prefab component
  for (PropertyTable::MapFromInt::const_iterator
       i = prefabComponentsTable->mapFromInt.begin(); 
       i != prefabComponentsTable->mapFromInt.end(); ++i)
  {
    //const int prefabComponentIndex = i->first;
    const PropertyTable* prefabComponentTable = i->second.GetTablePtr();
    // is there a corresponding component in the instance?
    if (prefabComponentTable)
    {
      int instanceComponentIndex = FindCorrespondingComponent(*instanceEntityTable, *prefabComponentTable);
      if (instanceComponentIndex >= 1)
        continue;
    }
    // nope. build it
    BuildComponent(entity, *instanceEntityTable, *prefabComponentTable, 0, componentsToInit);
    Component* newComponent = componentsToInit.back();
    newComponent->MarkAllPropertiesUnset();
  }
}

//--------------------------------------------------------------------
/*static*/ Entity* Entity::Deserialize(const PropertyTable& table, bool isPrefab, lua_State* L)
{
  PROFILE_SCOPE("Entity::Deserialize (" + table.GetString("name","") + ")")
  
  if (!L)
    L = Maxx::Get()->GetLuaState();
      
  Assert(!isPrefab); // TODO remove this param

  std::string guid = "";//table.GetString("guid", ""); // TODO: not currently loading guid
  Entity* entity = new Entity(guid);
  
  std::string name = table.GetString("name", "");
  entity->SetName(name);
  
  entity->prefabFileName_ = table.GetString("prefab", "");
  
  // Prefab instance?
  const PropertyTable* prefabTable = 0;
  const PropertyTable* prefabComponentsTable = 0;
  if (!entity->prefabFileName_.empty())
  {
    // Load prefab
    Property* prop = Maxx::Get()->GetPropertyCache()->LoadPropertyFromFile(entity->prefabFileName_, L);
    Assert(prop);
    if (!prop)
      return 0;
    prefabTable = prop->GetTablePtr();
    Assert(prefabTable);
    prefabComponentsTable = prefabTable->GetTable("components");
    if (name.empty() || name=="entity")
    {
      name = prefabTable->GetString("name","");
      entity->SetName(name);
    }
  }
  
  // Build components
  std::list<Component*> componentsToInit;
  const Property* components = table.Get("components");
  const PropertyTable* componentsTable = components ? components->GetTablePtr() : 0;
  if (componentsTable)
  {
    for (PropertyTable::MapFromInt::const_iterator i = 
         componentsTable->mapFromInt.begin(); 
         i != componentsTable->mapFromInt.end(); 
         ++i)
    {
      BuildComponent(entity, table, i->second, prefabTable, componentsToInit);
    }
    if (prefabTable)
      BuildMissingComponentsFromPrefab(entity, &table, prefabTable, componentsToInit);
  }
  
  // We're in the middle of an update. Init these components in AddPending,
  // because Box2D doesn't allow creating new bodies or fixtures while
  // updating.
  for (std::list<Component*>::iterator i = componentsToInit.begin(); i != componentsToInit.end(); ++i)
  {
    entity->pendingComponentsNotInited_.push_back(*i);
    ComponentList::iterator j = std::find(entity->components_.begin(), entity->components_.end(), *i);
    if (j != entity->components_.end())
      entity->components_.erase(j);
  }
  
  // Build child entities
  const Property* children = table.Get("children");
  if (children && children->IsTable())
  {
    const PropertyTable* childrenTable = children->GetTablePtr();
    for (PropertyTable::MapFromInt::const_iterator 
         i = childrenTable->mapFromInt.begin(); i != childrenTable->mapFromInt.end(); ++i)
    {
      const Property& value = i->second;
      if (!value.IsTable())
      {
        Trace("Entity::Deserialize: WARNING: A non-table child entity was encountered. (skipped)\n");
        continue;
      }
      Entity* childEntity = Entity::Deserialize(value.GetTable(), /*isPrefab=*/isPrefab, L);
      if (!childEntity)
      {
        Trace("Entity::Deserialize: WARNING: Deserializing child entity failed. (skipped)\n");
        continue;
      }
      entity->AddChildEntity(childEntity);
    }
  }
  
  return entity;
}

//--------------------------------------------------------------------
void Entity::SendMessage(const std::string& msgname, PropertyTable& msg)
{
  Postmaster* postmaster = Maxx::Get()->GetPostmaster();
  postmaster->SendMessage(msgname, msg, this);
}

//--------------------------------------------------------------------
void Entity::SendMessageTo(const std::string& msgname, PropertyTable& msg, Entity* entity)
{
  // Trace("Entity::SendMessageTo entity=%p, this=%p msgName=%s\n msg=%s\n\n",
  //       entity, this, msgname.c_str(), msg.Stringify().c_str());
  entity->OnMsg(msgname, msg, /*sender=*/this);
  //Trace("directed msg \"%s\"\n", msgname.c_str());
}

//--------------------------------------------------------------------
void Entity::OnMsg(const std::string& msgname, PropertyTable& msg, IMessageReceiver* sender)
{ 
  if (!inited_)
    return;

  // Send message to all components.
  ComponentList componentsCopy = components_;
  for (ComponentList::iterator i = componentsCopy.begin(); i != componentsCopy.end(); ++i)
  {
    Component* component = *i;
    IMessageReceiver* receiver = dynamic_cast<IMessageReceiver*>(component);
    if (!receiver)
      continue;
    receiver->OnMsg(msgname, msg, /*sender=*/sender);
  }
}

//--------------------------------------------------------------------
/*static*/ void Entity::OnBeginContact(b2Contact* contact, 
                                       Entity* entityA, 
                                       Entity* entityB, 
                                       FixtureComponent* fixtureComponentA, 
                                       FixtureComponent* fixtureComponentB)
{
  SendContactMessages("BeginContact", contact, entityA, entityB, fixtureComponentA, fixtureComponentB);
}

//--------------------------------------------------------------------
/*static*/ void Entity::OnEndContact(b2Contact* contact, 
                                     Entity* entityA, 
                                     Entity* entityB, 
                                     FixtureComponent* fixtureComponentA, 
                                     FixtureComponent* fixtureComponentB)
{
  SendContactMessages("EndContact", contact, entityA, entityB, fixtureComponentA, fixtureComponentB);
}

//--------------------------------------------------------------------
/*static*/ void Entity::OnPreSolveContact(b2Contact* contact, 
                                  Entity* entityA, 
                                  Entity* entityB, 
                                  FixtureComponent* fixtureComponentA, 
                                  FixtureComponent* fixtureComponentB)
{
  Assert(!preSolveContact_);
  preSolveContact_ = contact;
  SendContactMessages("PreSolveContact", contact, entityA, entityB, fixtureComponentA, fixtureComponentB);
  preSolveContact_ = 0;
}

//--------------------------------------------------------------------
void Entity::DisableContactFromPreSolve()
{
  Assert(preSolveContact_);
  if (!preSolveContact_)
    return;
  preSolveContact_->SetEnabled(false);
}

//--------------------------------------------------------------------
/*static*/ void Entity::SendContactMessages(const std::string& msgname,
                                            b2Contact* contact, 
                                            Entity* entityA, 
                                            Entity* entityB, 
                                            FixtureComponent* fixtureComponentA, 
                                            FixtureComponent* fixtureComponentB)
{  
   // Put the b2Contact into a msg
  PropertyTable msg;
  msg.Insert("isTouching", contact->IsTouching());
  msg.Insert("enabled", contact->IsEnabled());
  PropertyTable* manifold =  msg.InsertTable("manifold");
  manifold->Insert("localNormalX", contact->GetManifold()->localNormal.x);
  manifold->Insert("localNormalY", contact->GetManifold()->localNormal.y);
  manifold->Insert("localPointX", contact->GetManifold()->localPoint.x);
  manifold->Insert("localPointY", contact->GetManifold()->localPoint.y); 
  std::string type;
  switch (contact->GetManifold()->type)
  {
  case b2Manifold::e_circles: type = "circles"; break;
  case b2Manifold::e_faceA: type = "faceA"; break;
  case b2Manifold::e_faceB: type = "faceB"; break;
  default: type = "???"; break; // TODO
  }
  manifold->Insert("type", type); 
  manifold->Insert("pointCount", contact->GetManifold()->pointCount);
  PropertyTable* points = manifold->InsertTable("points");
  for (int i = 0; i < contact->GetManifold()->pointCount; ++i)
  {
    PropertyTable* p = points->InsertTable(i+1);
    p->Insert("localPointX", contact->GetManifold()->points[i].localPoint.x);
    p->Insert("localPointY", contact->GetManifold()->points[i].localPoint.y);
    p->Insert("normalImpulse", contact->GetManifold()->points[i].normalImpulse);
    p->Insert("tangentImpulse", contact->GetManifold()->points[i].tangentImpulse);
  }
  b2Vec2 worldNormal;
  worldNormal = entityA->GetComponentOfType<BodyComponent>()->Getb2Body()->GetWorldVector(contact->GetManifold()->localNormal);
  manifold->Insert("worldNormalX", worldNormal.x);
  manifold->Insert("worldNormalY", worldNormal.y);
  b2Vec2 worldPoint;
  worldPoint = entityA->GetComponentOfType<BodyComponent>()->Getb2Body()->GetWorldPoint(contact->GetManifold()->localPoint);
  worldPoint.x *= Maxx::Get()->GetPhysics()->GetPixelsPerMeter();
  worldPoint.y *= Maxx::Get()->GetPhysics()->GetPixelsPerMeter();
  manifold->Insert("worldPointX", worldPoint.x);
  manifold->Insert("worldPointY", worldPoint.y);

  msg.Insert("myFixtureName", fixtureComponentB->GetName());
  msg.Insert("otherFixtureName", fixtureComponentA->GetName());
  entityA->SendMessageTo(msgname, msg, /*to=*/entityB);
  
  msg.Insert("myFixtureName", fixtureComponentA->GetName());
  msg.Insert("otherFixtureName", fixtureComponentB->GetName());
  entityB->SendMessageTo(msgname, msg, /*to=*/entityA);
}

//--------------------------------------------------------------------
void Entity::RemoveTheDead()
{
  for (EntityList::iterator i = children_.begin(); i != children_.end(); /**/)
  {
    Entity* child = *i;
    if (child->IsDead())
    {
      delete child; child = 0;
      i = children_.erase(i);
    }
    else
    {
      ++i;
    }
  }
  for (int pass = 0; pass < 2; ++pass)
  {
    for (ComponentList::iterator i = components_.begin(); i != components_.end(); /**/)
    {
      Component* component = *i;
      if (component->IsDead())
      {
        bool isBody = (strcmp(component->GetTypeString(),"BodyComponent") == 0);
        if ((!isBody && pass == 0) || (isBody && pass == 1))
        {
          delete component; component = 0;
          i = components_.erase(i);
          continue;
        }
      }
      ++i;
    }
  }
}

//--------------------------------------------------------------------
void Entity::AddPending()
{
  Assert(!updating_);
  if (updating_)
    return;

  if (!pendingComponentsAlreadyInited_.empty())
  {
    ComponentList initedCopy = pendingComponentsAlreadyInited_;
    pendingComponentsAlreadyInited_.clear();
    for (ComponentList::iterator i = initedCopy.begin(); i != initedCopy.end(); ++i)
    {
      Component* component = *i;
      AddComponent(component, /*initialize=*/false);
    }
  }
  
  if (!pendingComponentsNotInited_.empty())
  {
    ComponentList notInitedCopy = pendingComponentsNotInited_;
    pendingComponentsNotInited_.clear();
    for (ComponentList::iterator i = notInitedCopy.begin(); i != notInitedCopy.end(); ++i)
    {
      Component* component = *i;
      AddComponent(component, /*initialize=*/true);
    }
  }

  for (EntityList::iterator i = pendingChildren_.begin(); i != pendingChildren_.end();
       i = pendingChildren_.erase(i))
  {
    Entity* child = *i;
    child->Init();
    children_.push_back(child);
  }
}

//--------------------------------------------------------------------
void Entity::MoveAll(double dx, double dy)
{
  std::vector<TransformComponent*> transformComponents;
  GetComponentsOfType(transformComponents);
  for (EntityList::iterator i = children_.begin(); i != children_.end(); ++i)
    (*i)->GetComponentsOfType(transformComponents);
  for (EntityList::iterator i = pendingChildren_.begin(); i != pendingChildren_.end(); ++i)
    (*i)->GetComponentsOfType(transformComponents);
  for (std::vector<TransformComponent*>::iterator i = transformComponents.begin(); i != transformComponents.end(); ++i)
  {
    TransformComponent* tf = *i;
    tf->SetX(tf->GetX() + dx);
    tf->SetY(tf->GetY() + dy);
  }
}

//--------------------------------------------------------------------
void Entity::PlayWav(const std::string& fileName)
{
  Audio* audio = Maxx::Get()->GetAudio();
  TransformComponent* tf = GetTransformComponent();
  if (!tf)
  {
    // not positional
    audio->PlayWav(fileName);
    return;
  }
  else
  {
    // positional
    const double unitScale = 1.0/100.0;
    Display* display = Maxx::Get()->GetDisplay();
    double x = (tf->GetX() - display->GetScreenSizeX() / 2.0) * unitScale;
    double y = (tf->GetY() - display->GetScreenSizeY() / 2.0) * unitScale;
    audio->PlayWavAt(fileName, x, -y);
  }
}

//--------------------------------------------------------------------
void Entity::ApplyForce(double forceX, double forceY, double pointX, double pointY)
{
  BodyComponent* body = GetComponentOfType<BodyComponent>();
  if (!body)
    return;
  body->Getb2Body()->ApplyForce(b2Vec2(forceX,forceY),b2Vec2(pointX,pointY));
  
}

//--------------------------------------------------------------------
void Entity::ApplyTorque(double torque)
{
  BodyComponent* body = GetComponentOfType<BodyComponent>();
  if (!body)
    return;
  body->Getb2Body()->ApplyTorque(torque);
}

//--------------------------------------------------------------------
void Entity::ApplyLinearImpulse(double impulseX, double impulseY, double pointX, double pointY)
{
  BodyComponent* body = GetComponentOfType<BodyComponent>();
  if (!body)
    return;
  Physics* physics = Maxx::Get()->GetPhysics();
  pointX = physics->PixelsToMeters(pointX);
  pointY = physics->PixelsToMeters(pointY);
  body->Getb2Body()->ApplyLinearImpulse(b2Vec2(impulseX,impulseY),b2Vec2(pointX,pointY));
}

//--------------------------------------------------------------------
void Entity::ApplyAngularImpulse(double impulse)
{
  BodyComponent* body = GetComponentOfType<BodyComponent>();
  if (!body)
    return;
  body->Getb2Body()->ApplyAngularImpulse(impulse);
}

//--------------------------------------------------------------------
bool Entity::InitComponent(Component*& component)
{
  if (!component)
    return false;
  Assert(component->GetEntity() == 0 || component->GetEntity() == this);
  component->SetEntity(this);
  bool initOk = component->Init();
  if (!initOk)
  {
      Trace("ERROR: Component of type \"%s\" failed to initialize.\n",
             component->GetTypeString());
      delete component;
      component = 0;
  }
  return initOk;
}

//--------------------------------------------------------------------
/*static*/ Entity* Entity::Lookup(const std::string& guid)
{
  GuidToEntity::iterator i = guidToEntity_.find(guid);
  if (i == guidToEntity_.end())
    return 0;
  Entity* entity = i->second;
  return entity;
}


} // Maxx
