#pragma once

#include "math/matrix.h"
#include "messaging/i-message-receiver.h"
#include "engine/property-table.h"
#include <list>
#include <string>
#include <vector>
#include <map>

class b2Contact;

namespace Maxx {

class Component;
class FixtureComponent;
class TransformComponent;

//--------------------------------------------------------------------
// Do not derive from this class. -- The Management
class Entity : public IMessageReceiver
{
public:
  typedef std::vector<Component*> ComponentList;
  typedef std::list<Entity*> EntityList;
  
  Entity(const std::string& guid = "");
  virtual ~Entity();
  
  void Init();
  bool Inited() const { return inited_; }
  
  void Update(double dt);
  void PreUpdate();
  void PostUpdate();
  void Draw() const;
  
  bool AddComponent(Component* component, bool initialize = true);
  
  void GetComponentsOfType(const std::string& type, ComponentList& componentList) const;
  template <class T> void GetComponentsOfType(std::vector<T*>& componentList) const;
  Component* GetComponentOfType(const std::string& type) const;
  template <class T> T* GetComponentOfType() const;
  Component* GetComponentByName(const std::string& type, const std::string& name) const;
  void GetAllComponents(ComponentList& componentList) const;
  TransformComponent* GetTransformComponent();
    
  bool AddChildEntity(Entity* entity);
  void GetChildren(EntityList& entityList) const;
  
  void Die(); 
  bool IsDead() const { return dead_; }
  
  static Entity* Load(const std::string& fileName, bool isPrefab = false, lua_State* L = 0);
  static bool Preload(const std::string& fileName, lua_State* L = 0);

  std::string Stringify() const;
  static Entity* Unstringify(const std::string&, bool isPrefab = false, lua_State* L = 0);
  
  void Serialize(PropertyTable& table) const;
  static Entity* Deserialize(const PropertyTable& table, bool isPrefab = false, lua_State* L = 0);
  
  bool IsPrefabInstance() const { return !prefabFileName_.empty(); }
  
  void SendMessage(const std::string& msgname, PropertyTable& msg);
  void SendMessageTo(const std::string& msgname, PropertyTable& msg, Entity* entity);
  
  virtual void OnMsg(const std::string& msgname, PropertyTable& msg, IMessageReceiver* sender);
  
  static void OnBeginContact(b2Contact* contact, 
                             Entity* entityA, 
                             Entity* entityB, 
                             FixtureComponent* fixtureComponentA, 
                             FixtureComponent* fixtureComponentB);
  static void OnEndContact(b2Contact* contact, 
                           Entity* entityA, 
                           Entity* entityB, 
                           FixtureComponent* fixtureComponentA, 
                           FixtureComponent* fixtureComponentB);
  static void OnPreSolveContact(b2Contact* contact, 
                                Entity* entityA, 
                                Entity* entityB, 
                                FixtureComponent* fixtureComponentA, 
                                FixtureComponent* fixtureComponentB);
  void DisableContactFromPreSolve();
   
  const std::string& GetGuid() const { return guid_; }
  static Entity* Lookup(const std::string& guid);
  
  const std::string& GetName() const { return name_; }
  void SetName(const std::string& name) { name_ = name; }

  Component* GetCorrespondingComponent(const Component* prefabComponent) const;

  // Should this entity be serialized (saved to .entity file)?
  bool Serialized() const { return serialized_; }
  void SetSerialized(bool should) { serialized_ = should; }

  void MoveAll(double dx, double dy);
  
  void PlayWav(const std::string& fileName);
  
  void ApplyForce(double forceX, double forceY, double pointX, double pointY);
  void ApplyTorque(double torque);
  void ApplyLinearImpulse(double impulseX, double impulseY, double pointX, double pointY);
  void ApplyAngularImpulse(double impulse);

private:
  static void SendContactMessages(const std::string& msgname,
                                  b2Contact* contact, 
                                  Entity* entityA, 
                                  Entity* entityB, 
                                  FixtureComponent* fixtureComponentA,
                                  FixtureComponent* fixtureComponentB);
  void RemoveTheDead();
  void AddPending();

  bool InitComponent(Component*& component);
  
private:
  ComponentList components_;
  EntityList children_;
  bool dead_;
  bool updating_;
  ComponentList pendingComponentsAlreadyInited_;
  ComponentList pendingComponentsNotInited_;
  EntityList pendingChildren_;
  std::string prefabFileName_;
  std::string guid_;
  std::string name_;
  bool serialized_;
  bool inited_;
  TransformComponent* transformComponent_;
    
  typedef std::map<std::string, Entity*> GuidToEntity;
  static GuidToEntity guidToEntity_;
  static b2Contact* preSolveContact_;
};

//--------------------------------------------------------------------
template <class T>
void Entity::GetComponentsOfType(std::vector<T*>& componentList) const
{
  ComponentList temp;
  GetComponentsOfType(T::GetTypeStringStatic(), temp);
  for (ComponentList::iterator i = temp.begin(); i != temp.end(); ++i)
  {
    T* t = dynamic_cast<T*>(*i);
    Assert(t);
    componentList.push_back(t);
  }
}

//--------------------------------------------------------------------
template <class T>
T* Entity::GetComponentOfType() const
{
  Component* component = GetComponentOfType(T::GetTypeStringStatic());
  T* t = dynamic_cast<T*>(component);
  return t;
}

} // Maxx
