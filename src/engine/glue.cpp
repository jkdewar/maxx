#include "glue.h"

namespace Maxx {
namespace Glue {

//--------------------------------------------------------------------
void PrintStack(lua_State * L)
{
  Trace("Printing stack:\n");
  for (int i = 1; i <= lua_gettop(L); ++i)
    Trace("%d - %s\n", i, luaL_typename(L, i));
}

} // Glue
} // Maxx
