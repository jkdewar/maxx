#pragma once

#include "cross-platform/lua-headers.h"
#include <string>
#include <typeinfo>
#include "utils/assert.h"

namespace Maxx {
namespace Glue {

//--------------------------------------------------------------------
void PrintStack(lua_State * L);

//--------------------------------------------------------------------
template <class T>
const char* TypeString()
{
  static std::string* s = 0;
  if (!s)
  {
    s = new std::string("maxx.glue.");
    *s += typeid(T).name();
  }
  return s->c_str();
}

//--------------------------------------------------------------------
// This class is stored in userdata. It holds a C++ object, and
// optionally deletes it during garbage collection.
class HolderBase
{
public:
  virtual ~HolderBase() {}
};
template <class T>
class Holder : public HolderBase
{
public:
  Holder(T* obj, bool own) : obj(obj), own(own) {}
  virtual ~Holder() { if (own) delete obj; obj = 0; }
  T* obj;
  bool own;
};

//--------------------------------------------------------------------
// returns the userdata on the stack
template <class T> 
int NewUserData(lua_State* L, T* obj, bool own = true)
{
  HolderBase** holderBase = (HolderBase**)lua_newuserdata(L, sizeof(HolderBase*));
  Assert(holderBase);
  if (!holderBase)
    return 0;
  *holderBase = new Holder<T>(obj, own);

  // get class metatable from the registry
  lua_pushstring(L, TypeString<T>());
  lua_gettable(L, LUA_REGISTRYINDEX);
  Assert(!lua_isnil(L, -1));
  
  // set userdata's metatable to the class metatable
  lua_setmetatable(L, -2);
  
  // return userdata
  return 1;
}

//--------------------------------------------------------------------
template <class T>
T* GetUserData(lua_State* L, int index = 1)
{
  HolderBase** holderBase = (HolderBase**)lua_touserdata(L, index);
  if (!holderBase)
    return 0;
  Assert(*holderBase);
  if (!*holderBase)
    return 0;
  Holder<T>* holder = dynamic_cast<Holder<T>*>(*holderBase);
  if (!holder)
    return 0;
  return holder->obj;
}

//--------------------------------------------------------------------
template <class T>
int GC(lua_State* L)
{
  HolderBase** holderBase = (HolderBase**)lua_touserdata(L, 1);
  Assert(holderBase);
  if (!holderBase)
    return 0;
  Assert(*holderBase);
  if (!*holderBase)
    return 0;
  Holder<T>* holder = dynamic_cast<Holder<T>*>(*holderBase);
  Assert(holder);
  //Trace("__gc own=%s %s\n", holder->own?"y":"n", TypeString<T>().c_str());  
  delete holder;
  return 0;
}

//--------------------------------------------------------------------
template <class T>
int Index(lua_State* L)
{
  // table, key are the args. (key is arg 2)
  const char* key = lua_tolstring(L, 2, 0);

  // get class metatable from the registry
  lua_pushstring(L, TypeString<T>());
  lua_gettable(L, LUA_REGISTRYINDEX);

  // look up key in metatable and return it
  lua_getfield(L, -1, key);
  return 1;
}

//--------------------------------------------------------------------
// returns the metatable on the stack
template <class T>
int RegisterType(lua_State* L, luaL_Reg* f)
{
  luaL_newmetatable(L, TypeString<T>());

  lua_pushcfunction(L, GC<T>);
  lua_setfield(L, -2, "__gc");

  lua_pushcfunction(L, Index<T>);
  lua_setfield(L, -2, "__index");

  if (f != 0)
    luaL_register(L, 0, f);

  // returns the metatable
  return 1;
}

#define GLUE_VERIFY(self) do { if (!(self)) { Trace("%s:%d: GLUE_VERIFY failed: method aborted\n", __FILE__, __LINE__); Assert(false); return 0; } } while(false)

} // Glue
} // Maxx
