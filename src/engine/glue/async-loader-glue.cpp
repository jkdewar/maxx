#include "engine/glue/async-loader-glue.h"
#include "engine/async-loader.h"
#include "engine/entity.h"
#include "cross-platform/lua-headers.h"

namespace Maxx {
namespace Glue {
  
//--------------------------------------------------------------------
// self, filename
int AsyncLoader_startLoading(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  AsyncLoader* asyncLoader = Maxx::Glue::GetUserData<AsyncLoader>(L, 1);
  GLUE_VERIFY(asyncLoader);
  GLUE_VERIFY(lua_isstring(L, 2));
  const std::string filename = lua_tostring(L, 2);
  asyncLoader->StartLoading(filename);
  return 0;
}

//--------------------------------------------------------------------
// self, filename
// returns state
int AsyncLoader_getLoadState(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  AsyncLoader* asyncLoader = Maxx::Glue::GetUserData<AsyncLoader>(L, 1);
  GLUE_VERIFY(asyncLoader);
  GLUE_VERIFY(lua_isstring(L, 2));
  const std::string filename = lua_tostring(L, 2);
  AsyncLoader::LoadState state = asyncLoader->GetLoadState(filename);
  std::string s;
  switch (state)
  {
    case AsyncLoader::NotLoading: s = "NotLoading"; break;
    case AsyncLoader::Loading: s = "Loading"; break;
    case AsyncLoader::LoadComplete: s = "LoadComplete"; break;
    case AsyncLoader::LoadFailed: s = "LoadFailed"; break;
    default: Assert(false); break;
  }
  lua_pushstring(L, s.c_str());
  return 1;
}

//--------------------------------------------------------------------
// self, filename
// returns entity
int AsyncLoader_takeLoadedEntity(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  AsyncLoader* asyncLoader = Maxx::Glue::GetUserData<AsyncLoader>(L, 1);
  GLUE_VERIFY(asyncLoader);
  GLUE_VERIFY(lua_isstring(L, 2));
  const std::string filename = lua_tostring(L, 2);
  Entity* entity = asyncLoader->TakeLoadedEntity(filename);
  if (!entity)
    return 0;
  Maxx::Glue::NewUserData<Entity>(L, entity, /*own=*/false);
  return 1;
}

//--------------------------------------------------------------------
int luaopen_AsyncLoader(lua_State* L)
{
  static luaL_Reg table[] =
  {
    {"startLoading", AsyncLoader_startLoading},
    {"getLoadState", AsyncLoader_getLoadState},
    {"takeLoadedEntity", AsyncLoader_takeLoadedEntity},
    {0, 0}
  };
  Maxx::Glue::RegisterType<AsyncLoader>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}

} // Glue
} // Maxx
