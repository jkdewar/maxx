#pragma once

#include "engine/glue.h"

namespace Maxx {
namespace Glue {

int luaopen_AsyncLoader(lua_State*);

} // Glue
} // Maxx
