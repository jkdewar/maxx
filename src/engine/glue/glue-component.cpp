#include "engine/glue/glue-component.h"
#include "engine/component.h"
#include "engine/entity.h"
#include "components/script-component.h"

namespace Maxx {
namespace Glue {

//--------------------------------------------------------------------
// self
// returns table
int Component_serialize(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Component* component = Maxx::Glue::GetUserData<Component>(L, 1);
  GLUE_VERIFY(component);
  PropertyTable t;
  component->Serialize(t);
  t.PushOnLuaStack(L);
  return 1;
}

//--------------------------------------------------------------------
// table
// returns component
int Component_deserialize(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  GLUE_VERIFY(lua_istable(L, 1));
  PropertyTable t;
  PropertyTable::FromLuaStack(L, 1, t);
  Component* component = Component::Deserialize(t);
  Maxx::Glue::NewUserData<Component>(L, component, /*own=*/false);
  return 1;
}

//--------------------------------------------------------------------
// table, key
int Component___index(lua_State* L)
{
  const char* key = lua_tostring(L, 2);
  
  lua_getfield(L, LUA_REGISTRYINDEX, TypeString<Component>());
  lua_getfield(L, -1, key);
  if (!lua_isnil(L, -1))
  {
    return 1;
  }
  
  Component* component = Maxx::Glue::GetUserData<Component>(L, 1);
  GLUE_VERIFY(component);
  //if (component->HasProperty(key))
  {
    Property value = component->GetProperty(key);
    value.PushOnLuaStack(L);
    //component->PushPropertyOnLuaStack(key, L);
  }
  //else
  //{
  //  lua_pushnil(L);
  //}
  return 1;
}

//--------------------------------------------------------------------
// table, key, value
int Component___newindex(lua_State* L)
{
  const char* key = lua_tostring(L, 2);
  Component* component = Maxx::Glue::GetUserData<Component>(L, 1);
  GLUE_VERIFY(component);
  GLUE_VERIFY(lua_isstring(L, 2));
  Property value;
  Property::FromLuaStack(L, 3, value);
  component->SetProperty(key, value);
  return 0;
}

//--------------------------------------------------------------------
int Component_getEntity(lua_State* L)
{
  Component* component = Maxx::Glue::GetUserData<Component>(L, 1);
  GLUE_VERIFY(component);
  Entity* entity = component->GetEntity();
  Maxx::Glue::NewUserData<Entity>(L, entity, /*own=*/false);
  return 1;
}

//--------------------------------------------------------------------
int Component_die(lua_State* L)
{
  Component* component = Maxx::Glue::GetUserData<Component>(L, 1);
  GLUE_VERIFY(component);
  component->Die();
  return 0;
}

//--------------------------------------------------------------------
// self, name, value
int Component_setProperty(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 3);
  Component* component = Maxx::Glue::GetUserData<Component>(L, 1);
  GLUE_VERIFY(component);
  GLUE_VERIFY(lua_isstring(L, 2));
  std::string name = lua_tostring(L, 2);
  int type = lua_type(L, 3);
  switch(type)
  {
    case LUA_TNIL:
      component->SetProperty(name.c_str(),Property());
      break;
    case LUA_TNUMBER:
      component->SetProperty(name.c_str(),lua_tonumber(L,3));
      break;
    case LUA_TBOOLEAN:
      component->SetProperty(name.c_str(),lua_toboolean(L,3)!=0);
      break;
    case LUA_TSTRING:
      component->SetProperty(name.c_str(),lua_tostring(L,3));
      break;
    case LUA_TTABLE:
    {
      Property value;
      Property::FromLuaStack(L, 3, value);
      component->SetProperty(name.c_str(), value);
    }
    default:
      Assert(false);
      break;
  }
  //Trace("setting %s to %s\n", name.c_str(), value.Stringify().c_str());
  return 0;
}

//--------------------------------------------------------------------
// self
// returns type as a string (ex. "TransformComponent")
int Component_getType(lua_State* L)
{
  Component* component = Maxx::Glue::GetUserData<Component>(L, 1);
  GLUE_VERIFY(component);
  const char* type = component->GetTypeString();
  lua_pushstring(L, type);
  return 1;
}

//--------------------------------------------------------------------
// self
// returns name
int Component_getName(lua_State* L)
{
  Component* component = Maxx::Glue::GetUserData<Component>(L, 1);
  GLUE_VERIFY(component);
  std::string name = component->GetName();
  lua_pushstring(L, name.c_str());
  return 1;
}

//--------------------------------------------------------------------
// self,name
int Component_setName(lua_State* L)
{
  Component* component = Maxx::Glue::GetUserData<Component>(L, 1);
  GLUE_VERIFY(component);
  std::string name = lua_tostring(L, 2);
  component->SetName(name);
  return 0;
}

//--------------------------------------------------------------------
// self
// returns list of property names, possibly empty
int Component_getAllPropertyNames(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Component* component = Maxx::Glue::GetUserData<Component>(L, 1);
  GLUE_VERIFY(component);
  Component::PropertyNames propertyNames;
  component->GetAllPropertyNames(propertyNames);

  // put property names into a lua table on the stack
  {
    lua_newtable(L);
    int index = 1;
    for (Component::PropertyNames::const_iterator i = propertyNames.begin(); i != propertyNames.end(); ++i)
    {
      const std::string& propertyName = *i;
      lua_pushstring(L, propertyName.c_str()); 
      lua_rawseti(L, -2, index);
      ++index;
    }
  }
  return 1;
}

//--------------------------------------------------------------------
// self, propertyName
// returns property value, possibly nil
int Component_getProperty(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Component* component = Maxx::Glue::GetUserData<Component>(L, 1);
  GLUE_VERIFY(component);
  GLUE_VERIFY(lua_isstring(L, 2));
  const char* propertyName = lua_tostring(L, 2);
  Property propertyValue = component->GetProperty(propertyName);
  propertyValue.PushOnLuaStack(L);
  return 1;
}

//--------------------------------------------------------------------
// self, propertyName
// returns bool
int Component_isPropertySet(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Component* component = Maxx::Glue::GetUserData<Component>(L, 1);
  GLUE_VERIFY(component);
  GLUE_VERIFY(lua_isstring(L, 2));
  const char* propertyName = lua_tostring(L, 2);
  bool isPropertySet = component->IsPropertySet(propertyName);
  lua_pushboolean(L, isPropertySet);
  return 1;
}

//--------------------------------------------------------------------
// self, disabled
// self must be a ScriptComponent
int Component_setUpdateDisabled(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Component* component = Maxx::Glue::GetUserData<Component>(L, 1);
  GLUE_VERIFY(component);
  bool disabled = lua_toboolean(L, 2)!=0;
  ScriptComponent* scriptComponent = dynamic_cast<ScriptComponent*>(component);
  GLUE_VERIFY(scriptComponent);
  scriptComponent->SetUpdateDisabled(disabled);
  return 0;
}

//--------------------------------------------------------------------
int luaopen_Component(lua_State* L)
{
  // remember, anything added here needs to be added in init-maxx.lua
  // as well (registerScriptClass)
  static luaL_Reg table[] =
  {
    {"serialize", Component_serialize},
    {"deserialize", Component_deserialize},
    {"__index", Component___index},
    {"__newindex", Component___newindex},
    {"getEntity", Component_getEntity},
    {"die", Component_die},
    {"setProperty", Component_setProperty},
    {"getType", Component_getType},
    {"getName", Component_getName},
    {"setName", Component_setName},
    {"getAllPropertyNames", Component_getAllPropertyNames},
    {"getProperty", Component_getProperty},
    {"isPropertySet", Component_isPropertySet},
    {"setUpdateDisabled", Component_setUpdateDisabled},
    {0, 0}
  };
  Maxx::Glue::RegisterType<Component>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}

} // Glue
} // Maxx
