#pragma once

#include "engine/glue.h"

namespace Maxx {
namespace Glue {

int luaopen_Component(lua_State* L);

int Component___index(lua_State* L);
int Component___newindex(lua_State* L);
int Component_getEntity(lua_State* L);
int Component_die(lua_State* L);

} // Glue
} // Maxx