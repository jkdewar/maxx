
#include "engine/glue/glue-component.h"
#include "engine/entity.h"
#include "engine/component.h"

namespace Maxx {
namespace Glue {

//--------------------------------------------------------------------
// fileName
// returns entity
int Entity_load(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  GLUE_VERIFY(lua_isstring(L, 1));
  std::string fileName = lua_tostring(L, 1);
  Entity* entity = Entity::Load(fileName, /*isPrefab=*/false);
  if (!entity)
  {
    lua_pushnil(L);
    return 1;
  }
  Maxx::Glue::NewUserData<Entity>(L, entity, /*own=*/false);
  return 1;
}

//--------------------------------------------------------------------
// fileName
// returns true if successful
int Entity_preload(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  GLUE_VERIFY(lua_isstring(L, 1));
  std::string fileName = lua_tostring(L, 1);
  bool ok = Entity::Preload(fileName);
  lua_pushboolean(L, ok);
  return 1;
}

//--------------------------------------------------------------------
// self=entity, type
// returns component or nil
int Entity_getComponent(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  GLUE_VERIFY(lua_isstring(L, 2));
  std::string type = lua_tostring(L, 2);
  Component* component = entity->GetComponentOfType(type);
  if (!component)
  {
    lua_pushnil(L);
    return 1;
  }
  Maxx::Glue::NewUserData<Component>(L, component, /*own=*/false);
  return 1;
}

//--------------------------------------------------------------------
// self=entity, type, name
// returns component or nil
int Entity_getComponentByName(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 3);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  GLUE_VERIFY(lua_isstring(L, 2));
  std::string type = lua_tostring(L, 2);
  GLUE_VERIFY(lua_isstring(L, 3));
  std::string name = lua_tostring(L, 3);
  Component* component = entity->GetComponentByName(type, name);
  if (!component)
  {
    lua_pushnil(L);
    return 1;
  }
  Maxx::Glue::NewUserData<Component>(L, component, /*own=*/false);
  return 1;
}

//--------------------------------------------------------------------
// self=entity, type
// returns a table of components (possibly empty)
int Entity_getComponentsOfType(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  GLUE_VERIFY(lua_isstring(L, 2));
  std::string type = lua_tostring(L, 2);

  Entity::ComponentList components;
  entity->GetComponentsOfType(type, components);

  // put components into a lua table on the stack
  {
    lua_newtable(L);
    int index = 1;
    for (Entity::ComponentList::iterator
         i = components.begin(); i != components.end(); ++i)
    {
      Component* component = *i;
      Maxx::Glue::NewUserData<Component>(L, component, /*own=*/false);
      lua_rawseti(L, -2, index);
      ++index;
    }
  }
  
  return 1;
}

//--------------------------------------------------------------------
// self=entity
// returns a table of components (possibly empty)
int Entity_getAllComponents(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);

  Entity::ComponentList components;
  entity->GetAllComponents(components);

  // put components into a lua table on the stack
  {
    lua_newtable(L);
    int index = 1;
    for (Entity::ComponentList::iterator
         i = components.begin(); i != components.end(); ++i)
    {
      Component* component = *i;
      Maxx::Glue::NewUserData<Component>(L, component, /*own=*/false);
      lua_rawseti(L, -2, index);
      ++index;
    }
  }
  
  return 1;
}

//--------------------------------------------------------------------
// self=entity, component
int Entity_addComponent(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  Component* component = Maxx::Glue::GetUserData<Component>(L, 2);
  GLUE_VERIFY(component);
  bool ret = entity->AddComponent(component);
  lua_pushboolean(L, ret);
  return 1;
}

//--------------------------------------------------------------------
// self=entity, childEntity
int Entity_addChildEntity(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  Entity* childEntity = Maxx::Glue::GetUserData<Entity>(L, 2);
  GLUE_VERIFY(childEntity);
  bool ret = entity->AddChildEntity(childEntity);
  lua_pushboolean(L, ret);
  return 1;
}

//--------------------------------------------------------------------
// self=entity
// returns a table of entities (possibly empty)
int Entity_getChildren(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);

  Entity::EntityList children;
  entity->GetChildren(children);

  // put entities into a lua table on the stack
  {
    lua_newtable(L);
    int index = 1;
    for (Entity::EntityList::iterator
         i = children.begin(); i != children.end(); ++i)
    {
      Entity* child = *i;
      Maxx::Glue::NewUserData<Entity>(L, child, /*own=*/false);
      lua_rawseti(L, -2, index);
      ++index;
    }
  }
  
  return 1;
}

//--------------------------------------------------------------------
// self=entity
// returns string
int Entity_stringify(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  std::string str = entity->Stringify();
  lua_pushstring(L, str.c_str());
  return 1;
}

//--------------------------------------------------------------------
// string
// returns entity
int Entity_unstringify(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  GLUE_VERIFY(lua_isstring(L, 1));
  std::string str = lua_tostring(L, 1);
  Entity* entity = Entity::Unstringify(str, /*isPrefab=*/false);
  if (!entity)
  {
    lua_pushnil(L);
    return 1;
  }
  Maxx::Glue::NewUserData<Entity>(L, entity, /*own=*/false);
  return 1;
}

//--------------------------------------------------------------------
// self=entity
// returns table
int Entity_serialize(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  PropertyTable table;
  entity->Serialize(table);
  table.PushOnLuaStack(L);
  return 1;
}

//--------------------------------------------------------------------
// table
// returns entity
int Entity_deserialize(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  GLUE_VERIFY(lua_istable(L, 1));
  PropertyTable propertyTable;
  PropertyTable::FromLuaStack(L, -1, propertyTable);
  Entity* entity = Entity::Deserialize(propertyTable, /*isPrefab=*/false);
  if (!entity)
  {
    lua_pushnil(L);
    return 1;
  }
  Maxx::Glue::NewUserData<Entity>(L, entity, /*own=*/false);
  return 1;
}
//--------------------------------------------------------------------
// self=entity
int Entity_die(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  if (entity)
    entity->Die();
  return 0;
}

//--------------------------------------------------------------------
// self=entity
int Entity_isDead(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  bool isDead = entity->IsDead();
  lua_pushboolean(L, isDead);
  return 1;
}

//--------------------------------------------------------------------
// self=entity, msgname, msg
int Entity_sendMessage(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 3);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  GLUE_VERIFY(lua_isstring(L, 2));
  std::string msgname = lua_tostring(L, 2);
  //GLUE_VERIFY(lua_istable(L, 3));
  PropertyTable msg;
  if (lua_istable(L,3))
    PropertyTable::FromLuaStack(L, 3, msg);
  entity->SendMessage(msgname, msg);
  // TODO: Somehow modify the msg param to reflect the PropertyTable msg.
  return 0;
}

//--------------------------------------------------------------------
// self=entity, msgname, msg, entity
int Entity_sendMessageTo(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 4);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  GLUE_VERIFY(lua_isstring(L, 2));
  std::string msgname = lua_tostring(L, 2);
  Entity* to = Maxx::Glue::GetUserData<Entity>(L, 4);
  GLUE_VERIFY(to);
  
  // TODO: you lazy bastard
  // For some reason, any parameters following the table parameter
  // must be popped before retrieving the table from the stack,
  // otherwise the table comes back shifted over strangely.
  // Shiiiit. picklepicklepickle
  lua_pop(L,1);
  GLUE_VERIFY(lua_istable(L, 3));
  PropertyTable msg;
  PropertyTable::FromLuaStack(L, 3, msg);
  
  entity->SendMessageTo(msgname, msg, to);
  return 0;
}

//--------------------------------------------------------------------
// self=entity
// returns guid
int Entity_getGuid(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  lua_pushstring(L, entity->GetGuid().c_str());
  return 1;
}

//--------------------------------------------------------------------
// guid
// returns entity
int Entity_lookup(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  if (lua_isnil(L, 1))
  {
    lua_pushnil(L);
    return 1;
  }
  GLUE_VERIFY(lua_isstring(L, 1));
  std::string guid = lua_tostring(L, 1);
  Entity* entity = Entity::Lookup(guid);
  if (entity)
    Maxx::Glue::NewUserData<Entity>(L, entity, /*own=*/false);
  else
   lua_pushnil(L);
  return 1;
}

//--------------------------------------------------------------------
// self
// returns name
int Entity_getName(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  lua_pushstring(L, entity->GetName().c_str());
  return 1;
}

//--------------------------------------------------------------------
// self, name
int Entity_setName(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  std::string name = lua_tostring(L, 2);
  entity->SetName(name);
  return 0;
}

//--------------------------------------------------------------------
// self, serialized
int Entity_setSerialized(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  GLUE_VERIFY(lua_isboolean(L, 2));
  bool serialized = lua_toboolean(L, 2) != 0;
  entity->SetSerialized(serialized);
  return 0;
}

//--------------------------------------------------------------------
// self, dx, dy
int Entity_moveAll(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 3);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  double dx = lua_tonumber(L, 2);
  double dy = lua_tonumber(L, 3);
  entity->MoveAll(dx, dy);
  return 0;
}

//--------------------------------------------------------------------
// self
int Entity_disableContactFromPreSolve(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  entity->DisableContactFromPreSolve();
  return 0;
}

//--------------------------------------------------------------------
// self, filename
int Entity_playWav(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  GLUE_VERIFY(lua_isstring(L, 2));
  std::string fileName = lua_tostring(L, 2);
  entity->PlayWav(fileName);
  return 0;
}

//--------------------------------------------------------------------
// self, forceX, forceY, pointX, pointY
int Entity_applyForce(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 5);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  double forceX=lua_tonumber(L,2);
  double forceY=lua_tonumber(L,3);
  double pointX=lua_tonumber(L,4);
  double pointY=lua_tonumber(L,5);
  entity->ApplyForce(forceX,forceY,pointX,pointY);
  return 0;
}

//--------------------------------------------------------------------
// self, torque
int Entity_applyTorque(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  double torque=lua_tonumber(L,2);
  entity->ApplyTorque(torque);
  return 0;
}

//--------------------------------------------------------------------
// self, impulseX, impulseY, pointX, pointY
int Entity_applyLinearImpulse(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 5);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  double impulseX=lua_tonumber(L,2);
  double impulseY=lua_tonumber(L,3);
  double pointX=lua_tonumber(L,4);
  double pointY=lua_tonumber(L,5);
  entity->ApplyLinearImpulse(impulseX,impulseY,pointX,pointY);
  return 0;
}

//--------------------------------------------------------------------
// self, impulse
int Entity_applyAngularImpulse(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(entity);
  double impulse=lua_tonumber(L,2);
  entity->ApplyAngularImpulse(impulse);
  return 0;
}

//--------------------------------------------------------------------
int luaopen_Entity(lua_State* L)
{
  static luaL_Reg table[] =
  {
    {"load", Entity_load},
    {"preload", Entity_preload},
    {"addComponent", Entity_addComponent},
    {"getComponent", Entity_getComponent},
    {"getComponentsOfType", Entity_getComponentsOfType},
    {"getComponentByName", Entity_getComponentByName},
    {"getAllComponents", Entity_getAllComponents},
    {"addChildEntity", Entity_addChildEntity},
    {"getChildren", Entity_getChildren},
    {"stringify", Entity_stringify},
    {"unstringify", Entity_unstringify},
    {"serialize", Entity_serialize},
    {"deserialize", Entity_deserialize},
    {"die", Entity_die},
    {"isDead", Entity_isDead},
    {"sendMessage", Entity_sendMessage},
    {"sendMessageTo", Entity_sendMessageTo},
    {"getGuid", Entity_getGuid},
    {"lookup", Entity_lookup},
    {"getName", Entity_getName},
    {"setName", Entity_setName},
    {"setSerialized", Entity_setSerialized},
    {"moveAll", Entity_moveAll},
    {"disableContactFromPreSolve", Entity_disableContactFromPreSolve},
    {"playWav", Entity_playWav},
    {"applyForce", Entity_applyForce},
    {"applyTorque", Entity_applyTorque},
    {"applyLinearImpulse", Entity_applyLinearImpulse},
    {"applyAngularImpulse", Entity_applyAngularImpulse},
    {0, 0}
  };
  Maxx::Glue::RegisterType<Entity>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}

} // Glue
} // Maxx
