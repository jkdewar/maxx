#pragma once

#include "engine/glue.h"

namespace Maxx {
namespace Glue {

int luaopen_Entity(lua_State*);

} // Glue
} // Maxx
