#include "engine/glue/glue-maxx.h"
#include "engine/maxx.h"
#include "engine/entity.h"

namespace Maxx {
namespace Glue {

//--------------------------------------------------------------------
int Maxx_setGameName(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  GLUE_VERIFY(lua_isstring(L, 1));
  std::string gameName = lua_tostring(L, 1);
  Maxx::Get()->SetGameName(gameName);
  return 0;
}

//--------------------------------------------------------------------
int Maxx_quit(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 0);
  Maxx::Get()->Quit();
  return 0;
}

//--------------------------------------------------------------------
int Maxx_reset(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 0);
  Maxx::Get()->Reset();
  return 0;
}

//--------------------------------------------------------------------
int Maxx_runLuaFile(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  const char* fileName = lua_tostring(L, 1);
  GLUE_VERIFY(fileName);
  Maxx::Get()->RunLuaFile(fileName);
  return 0;
}

//--------------------------------------------------------------------
int Maxx_getScene(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 0);
  Entity* entity = Maxx::Get()->GetScene();
  Maxx::Glue::NewUserData<Entity>(L, entity, /*own=*/false);
  return 1;
}

//--------------------------------------------------------------------
int Maxx_getNextScene(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 0);
  Entity* entity = Maxx::Get()->GetNextScene();
  Maxx::Glue::NewUserData<Entity>(L, entity, /*own=*/false);
  return 1;
}

//--------------------------------------------------------------------
int Maxx_changeScene(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Entity* scene = Maxx::Glue::GetUserData<Entity>(L, 1);
  GLUE_VERIFY(scene);
  Maxx::Get()->ChangeScene(scene);
  return 0;
}

//--------------------------------------------------------------------
int Maxx_setFocus(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Entity* entity = Maxx::Glue::GetUserData<Entity>(L, 1);
  Maxx::Get()->SetFocus(entity);
  return 0;
}

//--------------------------------------------------------------------
int Maxx_isFocus(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 0);
  bool isFocus = Maxx::Get()->GetFocus() != 0;
  lua_pushboolean(L, isFocus);
  return 1;
}

//--------------------------------------------------------------------
int Maxx_setFrameSkip(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  GLUE_VERIFY(lua_isnumber(L, 1));
  int frameSkip = (int)lua_tonumber(L, 1);
  Maxx::Get()->SetFrameSkip(frameSkip);
  return 0;
}

//--------------------------------------------------------------------
int Maxx_getFrameSkip(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 0);
  int frameSkip = Maxx::Get()->GetFrameSkip();
  lua_pushnumber(L, (double)frameSkip);
  return 1;
}

//--------------------------------------------------------------------
int Maxx_getTime(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 0);
  lua_pushnumber(L, Maxx::Get()->GetTime());
  return 1;
}

//--------------------------------------------------------------------
int Maxx_getPlatform(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 0);
#if defined(__WINDOWS__)
  lua_pushstring(L, "win");
#elif defined(__MACOSX__)
  lua_pushstring(L, "mac");
#elif defined(__LINUX__)
  lua_pushstring(L, "linux");
#elif defined(__IPHONEOS__)
  lua_pushstring(L, "ios");
#else
#error unknown platform
  lua_pushstring(L, "?");
#endif
  return 1;
}

//--------------------------------------------------------------------
int luaopen_MaxxInstance(lua_State* L)
{
  static luaL_Reg table[] =
  {
    {"setGameName", Maxx_setGameName},
    {"quit", Maxx_quit},
    {"reset", Maxx_reset},
    {"runLuaFile", Maxx_runLuaFile},
    {"getScene", Maxx_getScene},
    {"getNextScene", Maxx_getNextScene},
    {"changeScene", Maxx_changeScene},
    {"setFocus", Maxx_setFocus},
    {"isFocus", Maxx_isFocus},
    {"setFrameSkip", Maxx_setFrameSkip},
    {"getFrameSkip", Maxx_getFrameSkip},
    {"getTime", Maxx_getTime},
    {"getPlatform", Maxx_getPlatform},
    {0, 0}
  };
  Maxx::Glue::RegisterType<Instance>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}

} // Glue
} // Maxx
