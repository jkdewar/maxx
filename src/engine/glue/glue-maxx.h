#pragma once

#include "engine/glue.h"

namespace Maxx {
namespace Glue {

int luaopen_MaxxInstance(lua_State*);

} // Glue
} // Maxx
