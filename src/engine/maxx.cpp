#include "engine/maxx.h"
#include "cross-platform/sdl-headers.h"
#include "cross-platform/file-sys.h"
#include "engine/server.h"

#include "engine/glue/glue-maxx.h"
#include "input/glue/glue-mouse.h"
#include "input/glue/glue-keyboard.h"
#include "input/glue/glue-accelerometer.h"
#include "input/glue/glue-touch-input.h"
#include "graphics/glue/glue-graphics.h"
#include "graphics/glue/glue-display.h"
#include "graphics/glue/glue-image.h"
#include "graphics/glue/camera-glue.h"
#include "graphics/text/glue/font-glue.h"
#include "graphics/text/glue/font-manager-glue.h"
#include "math/glue/glue-matrix.h"
#include "math/glue/glue-vector.h"
#include "utils/glue/glue-utils.h"
#include "messaging/glue/glue-postmaster.h"
#include "physics/glue/physics-glue.h"
#include "audio/glue/audio-glue.h"
#include "file-system/glue/file-system-glue.h"
#include "engine/glue/glue-component.h"
#include "engine/glue/glue-entity.h"
#include "engine/glue/async-loader-glue.h"

#include "input/mouse.h"
#include "input/touch-input.h"
#include "input/keyboard.h"
#include "input/accelerometer.h"
#include "graphics/graphics.h"
#include "graphics/display.h"
#include "graphics/image.h"
#include "graphics/text/font.h"
#include "graphics/text/font-manager.h"
#include "math/matrix.h"
#include "math/vector.h"
#include "math/math.h"
#include "cross-platform/box2d-headers.h"
#include "engine/entity.h"
#include "engine/component.h"
#include "engine/prefab-manager.h"
#include "messaging/postmaster.h"
#include "physics/physics.h"
#include "audio/audio.h"
#include "components/script-component.h"
#include "engine/async-loader.h"
#include "engine/property-cache.h"
#include "profiler/profiler.h"
#include "profiler/profile_scope.h"

#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>
#include <ctime>

namespace Maxx {

//--------------------------------------------------------------------
static Instance* instance_ = 0;

//--------------------------------------------------------------------
Instance* Initialize(int& argc, char** argv)
{
  delete instance_; instance_ = 0;
  instance_ = Instance::Create(argc, argv);
  return instance_;
}

//--------------------------------------------------------------------
void ShutDown()
{
  delete instance_; instance_ = 0;
}

//--------------------------------------------------------------------
Instance* Get()
{
  return instance_;
}

//--------------------------------------------------------------------
Instance* Instance::Create(int& argc, char** argv)
{
  bool success = false;
  Instance* instance = new Instance(success, argc, argv);
  if (!success)
  {
    delete instance; instance = 0;
  }
  return instance;
}

//--------------------------------------------------------------------
Instance::Instance(bool& success, int& argc, char** argv)
  : gameName_("")
  , server_(0)
  , display_(0)
  , graphics_(0)
  , fontManager_(0)
  , keyboard_(0)
  , mouse_(0)
  , touchInput_(0)
  , accelerometer_(0)
  , prefabManager_(0)
  , postMaster_(0)
  , physics_(0)
  , audio_(0)
  , propertyCache_(0)
  , profiler_(0)
  , L_(0)
  , quit_(false)
  , reset_(false)
  , sceneStack_()
  , nextScene_(0)
  , updateInProgress_(false)
  , focus_("")
  , timeScale_(1.0)
  , backgrounded_(false)
{
  success = false;
  instance_ = this; // hack
  
//#if defined(__IPHONEOS__)
//  SDL_SetHint( "SDL_HINT_ORIENTATIONS", "Portrait" );
//#endif

  //SDL_Init(SDL_INIT_EVERYTHING);
#if 0//!defined(__IPHONEOS__)
  int ttfok = TTF_Init();
  //Trace("TTF_Init returned %d\n", ttfok);
#endif
  
  const char* hello = 
  "MAXX build " __DATE__ " " __TIME__ "\n";
  Trace("%s\n", hello);
  //SDL_WM_SetCaption("MAXX", "");
#if !defined(__IPHONEOS__)
  SDLNet_Init();
#endif

  Math::SeedRandom((unsigned int)time(0));
  
  // Create engines
  mouse_ = new Mouse;
  touchInput_ = new TouchInput;
  keyboard_ = new Keyboard;
  accelerometer_ = new Accelerometer;
  display_ = Display::Create();
  graphics_ = new Graphics;
  fontManager_ = new FontManager;
  physics_ = new Physics;
  audio_ = Audio::Create(argc, argv);
  prefabManager_ = new PrefabManager;
  postMaster_ = new Postmaster;
  asyncLoader_ = new AsyncLoader;
  propertyCache_ = new PropertyCache;
  profiler_ = new Profiler;
  if (!display_)
    return;

  PerformReset();

#if !defined(__IPHONEOS__)
  // Start server
  if (argc > 1 && strcmp(argv[1], "-s") == 0)
  {
    Trace("MAXX starting server...\n");
    server_ = new Server;
  }
#endif
  
  success = true;
}

//--------------------------------------------------------------------
Instance::~Instance()
{
#if !defined(__IPHONEOS__)
  // Destroy server
  delete server_; server_ = 0;
#endif
  
  for (SceneStack::iterator i = sceneStack_.begin(); i != sceneStack_.end(); ++i)
    delete *i;
  sceneStack_.clear();
  delete nextScene_; nextScene_ = 0;

  if (L_)
    lua_close(L_);
  L_ = 0;

  delete prefabManager_; prefabManager_ = 0;
  delete fontManager_; fontManager_ = 0;
  delete graphics_; graphics_ = 0;
  delete display_; display_ = 0;
  delete keyboard_; keyboard_ = 0;
  delete mouse_; mouse_ = 0;
  delete touchInput_; touchInput_ = 0;
  delete accelerometer_; accelerometer_ = 0;
  delete postMaster_; postMaster_ = 0;
  delete physics_; physics_ = 0;
  delete audio_; audio_ = 0;
  delete asyncLoader_; asyncLoader_ = 0;
  delete propertyCache_; propertyCache_ = 0;
  delete profiler_; profiler_ = 0;
  
#if 0//!defined(__IPHONEOS__)
  TTF_Quit();
#endif
#if !defined(__IPHONEOS__)
  SDLNet_Quit();
#endif
  SDL_Quit();
}

//--------------------------------------------------------------------
void Instance::SetGameName(const std::string& gameName)
{
  gameName_ = gameName;
  if (display_)
    SDL_SetWindowTitle(display_->GetSdlWindow(), gameName.c_str());
}

//--------------------------------------------------------------------
void Instance::Quit()
{
  quit_ = true;
}

//--------------------------------------------------------------------
bool Instance::CheckQuit() const
{
  return quit_;
}

//--------------------------------------------------------------------
void Instance::Update()
{
  profiler_->BeginFrame();

  PROFILE_SCOPE("maxx update")
  //Trace("FRAME\n");
  
  updateInProgress_ = true;
  
  // Framerate limiting
  static double idealElapsedTime = 1.0 / 60.0;//TODO:make a member var
    
  uint64_t frameStartTime = SDL_GetPerformanceCounter();

  if (reset_)
  {
    PerformReset();
    reset_ = false;
  }
  
  // SDL events
  SDL_Event ev;
  while (SDL_PollEvent(&ev))
  {
    if (ev.type == SDL_QUIT)
      Quit();
    if (ev.type == SDL_WINDOWEVENT)
    {
      if (ev.window.event == SDL_WINDOWEVENT_FOCUS_LOST)
      {
        backgrounded_ = true;
      }
      else if (ev.window.event == SDL_WINDOWEVENT_FOCUS_GAINED)
      {
        backgrounded_ = false;
      }
    }
    else if (ev.type == SDL_KEYDOWN || ev.type == SDL_KEYUP || ev.type == SDL_TEXTINPUT)
      keyboard_->HandleEvent(ev);
    touchInput_->HandleEvent(ev);
  }
  
  if (backgrounded_)
  {
    SDL_Delay(250);
    updateInProgress_ = false;
    return;
  }
  
  if (nextScene_)
  {
    for (SceneStack::iterator i = sceneStack_.begin(); i != sceneStack_.end(); ++i)
      delete *i;
    sceneStack_.clear();
    sceneStack_.push_back(nextScene_);
    nextScene_->Init();
    //nextScene_->PreUpdate();
    //nextScene_->Update(0.0);
    //nextScene_->PostUpdate();
    nextScene_ = 0;
  }
  
  double dt = idealElapsedTime;

  // slow motion
  static bool slowMotion = false;
  if (Maxx::Get()->GetKeyboard()->IsPressed(SDL_SCANCODE_EQUALS))
    slowMotion = !slowMotion;
  if (slowMotion) 
    dt *= 0.25;
    
  Entity* scene = GetScene();
  
  // Pre-update
  if (scene)
    scene->PreUpdate();
  
  mouse_->Update();
  keyboard_->Update();
  audio_->Update();
  
  {
    PROFILE_SCOPE("scene update");
    scene->Update(dt);
  }
  {
    PROFILE_SCOPE("physics");
    physics_->Update(dt);
  }
  
  // Lua update
  {
    PROFILE_SCOPE("lua update");
    lua_getfield(L_, LUA_GLOBALSINDEX, "maxx");
    lua_getfield(L_, -1, "update");
    lua_pushnumber(L_, idealElapsedTime);
    int result = lua_pcall(L_, 1, 0, 0);
    //if (result != 0)
    //  Trace("Maxx: Lua error during update.\n");
    lua_pop(L_, 1);
    //Assert(lua_gettop(L_) == 0);
    lua_pop(L_, lua_gettop(L_));
    //Trace("lua memory usage: %d Kb\n", lua_gc(L_,LUA_GCCOUNT,0));

    #if 1
    const int gcMilliSeconds = 4;
    lua_gc(L_, LUA_GCSETSTEPMUL, gcMilliSeconds);
    lua_gc(L_, LUA_GCSTEP, 0);
    #endif
  }

  // Post-update
  if (scene)
    scene->PostUpdate();
    
  touchInput_->Update();
  
  bool skipRender = false;
#if 0
  {
    uint64_t frameEndTime = SDL_GetPerformanceCounter();
    float frameElapsedTime = (frameEndTime - frameStartTime) / (float)SDL_GetPerformanceFrequency();
    int ms = (int)(frameElapsedTime*1000);
    if (ms>10)
    {
      Trace("!!!!!!!! SKIP RENDER !!!!!!!!\n");
      skipRender = true;
    }
  }
#endif
  
  
  // Render
if (!skipRender)
{
  {
    PROFILE_SCOPE("render");
    display_->Clear();
    graphics_->BeginFrame();
    {
      for (SceneStack::iterator i = sceneStack_.begin(); i != sceneStack_.end(); ++i)
        (*i)->Draw();
    }
    {
      int oldTop = lua_gettop(L_);
      lua_getfield(L_, LUA_GLOBALSINDEX, "maxx");
      lua_getfield(L_, -1, "draw");
      lua_pcall(L_, 0, 0, 0);
      lua_pop(L_, lua_gettop(L_) - oldTop);
    }
    physics_->Draw();
    graphics_->EndFrame();
    graphics_->Draw();
  }
  
  // print frame time
  {
    uint64_t frameEndTime = SDL_GetPerformanceCounter();
    float frameElapsedTime = (frameEndTime - frameStartTime) / (float)SDL_GetPerformanceFrequency();
    int ms = (int)(frameElapsedTime*1000);
    static int count = 0;
    if (ms>17)
      printf("[%d] frame time: %d ms\n", ++count, ms);
  }

  // Flip
  {
    PROFILE_SCOPE("flip");
    display_->Flip();
  }
}

  {
    double elapsed = profiler_->EndFrame();
    if (elapsed >= 0.03 || skipRender)
      profiler_->PrintFrameInfo();
  }

#if 1
 #if !defined(__IPHONEOS__) //&& !defined(__WIN32__)
   // busy loop wait
   for (;;)
   {     
     uint64_t frameEndTime = SDL_GetPerformanceCounter();
     double frameElapsedTime = (frameEndTime - frameStartTime) / (double)SDL_GetPerformanceFrequency();
     double timeToWaste = 1.0/60.0 - frameElapsedTime;
     if (timeToWaste <= 0.0)
       break;
      if (timeToWaste >= 0.1) // 10 ms
        SDL_Delay(1);
   }
 #endif
#endif

#if 0
  // variable timestep
  uint64_t frameEndTime =  SDL_GetPerformanceCounter();
  double frameElapsedTime = (frameEndTime - frameStartTime) / (double)SDL_GetPerformanceFrequency();
  if (frameElapsedTime > 1.0/60.0)
    idealElapsedTime = 2.0/60.0;
  else
    idealElapsedTime = 1.0/60;
#endif
  
  // // print frame time
  // {
  //   static int count = 0;
  //   ++count;
  //   if (count >= 1)
  //   {
  //     count = 0;
  //     uint64_t frameEndTime = SDL_GetPerformanceCounter();
  //     float frameElapsedTime = (frameEndTime - frameStartTime) / (float)SDL_GetPerformanceFrequency();
  //     //if (frameElapsedTime > 1.0f/60.0f)
  //       Trace("%d milliseconds\n", (int)(frameElapsedTime*1000));
  //    }
  // }
  
  if (keyboard_->IsPressed(SDL_SCANCODE_Q) && keyboard_->IsDown(SDL_SCANCODE_LSHIFT))
    Quit();

#if !defined(__IPHONEOS__)
  if (server_)
    server_->Update();
#endif
  
  updateInProgress_ = false;
}

//--------------------------------------------------------------------
double Instance::GetTime() const
{
  uint64_t counter = SDL_GetPerformanceCounter();
  double frequency = (double)SDL_GetPerformanceFrequency();
  double seconds = counter / frequency;
  return seconds;
}

//--------------------------------------------------------------------
void Instance::Reset()
{
  reset_ = true;  
}

//--------------------------------------------------------------------
void Instance::RunLuaFile(const std::string& luaFileName)
{
  int oldTop = lua_gettop(L_);
  int result = luaL_dofile(L_, luaFileName.c_str());
  if (result != 0)
  {
    Trace("Error running Lua file: %s\n", luaFileName.c_str());
    if (!lua_isnil(L_, -1))
	  Trace("%s\n", lua_tostring(L_, -1));
    RunLuaCode("print(debug.stacktrace)");
  }
  else
    Assert(lua_gettop(L_) == oldTop);
  lua_pop(L_, lua_gettop(L_) - oldTop);
}

//--------------------------------------------------------------------
bool Instance::RunLuaCode(const std::string& luaCode, Property* result)
{
  if (result)
    *result = Property();
  int oldTop = lua_gettop(L_);
  int loadResult = luaL_loadstring(L_, luaCode.c_str());
  if (loadResult != 0)
  {
    lua_pop(L_, lua_gettop(L_) - oldTop);
    return false;
  }
  int callResult = lua_pcall(L_, 0, LUA_MULTRET, 0);
  if (callResult != 0)
  {
    lua_pop(L_, lua_gettop(L_) - oldTop);
    return false;
  }
  if (result)
    Property::FromLuaStack(L_, -1, *result);
  lua_pop(L_, lua_gettop(L_) - oldTop);
  return true;
}

//--------------------------------------------------------------------
Entity* Instance::GetScene()
{
  if (sceneStack_.empty())
    return 0;
  return sceneStack_[sceneStack_.size() - 1];
}

//--------------------------------------------------------------------
Entity* Instance::GetNextScene()
{
  return nextScene_;
}

//--------------------------------------------------------------------
void Instance::ChangeScene(Entity* scene)
{
  delete nextScene_;
  nextScene_ = scene;
}

//--------------------------------------------------------------------
void Instance::PushScene(Entity* scene)
{
  sceneStack_.push_back(scene);
}

//--------------------------------------------------------------------
void Instance::PopScene()
{
  if (sceneStack_.empty())
    return;
  delete sceneStack_.back();
  sceneStack_.pop_back();
}

//--------------------------------------------------------------------
void Instance::SetFocus(Entity* focus)
{
  if (!focus)
    focus_ = "";
  else
    focus_ = focus->GetGuid();
}

//--------------------------------------------------------------------
Entity* Instance::GetFocus() const
{
  if (focus_.empty())
    return 0;
  return Entity::Lookup(focus_);
}

//--------------------------------------------------------------------
void Instance::PerformReset()
{
  audio_->StopAllWavs();
  
  display_->SetClearColor(0.0, 0.4, 0.8);
  
  touchInput_->Reset();
  
  for (SceneStack::iterator i = sceneStack_.begin(); i != sceneStack_.end(); ++i)
    delete *i;
  sceneStack_.clear();
  delete nextScene_; nextScene_ = 0;
  
  delete prefabManager_;
  prefabManager_ = new PrefabManager;
  
  if (L_)
  {
    lua_close(L_);
    L_ = 0;
  }

  L_ = luaL_newstate();
  lua_State* L = L_;
  
  lua_gc(L_, LUA_GCSTOP, 1);
  
  // Open Lua libraries

#define LUAOPEN(x, y)\
  lua_pushcfunction(L, luaopen_ ## x);\
  lua_pushliteral(L, y);\
  lua_call(L, 1, 0);
  
  LUAOPEN(base, "");
  LUAOPEN(table, LUA_TABLIBNAME);
  LUAOPEN(io, LUA_IOLIBNAME);   // TODO: sandbox?
  LUAOPEN(os, LUA_OSLIBNAME);   // TODO: sandbox?
  LUAOPEN(string, LUA_STRLIBNAME);
  LUAOPEN(math, LUA_MATHLIBNAME);
  LUAOPEN(debug, LUA_DBLIBNAME);
  LUAOPEN(package, LUA_LOADLIBNAME);

#undef LUAOPEN

  // Add Maxx stuff to Lua
  
  Maxx::Glue::luaopen_MaxxInstance(L);
  
  Maxx::Glue::luaopen_Mouse(L);
  Maxx::Glue::luaopen_Keyboard(L);
  Maxx::Glue::luaopen_Accelerometer(L);
  Maxx::Glue::luaopen_TouchInput(L);
  
  Maxx::Glue::luaopen_Graphics(L);
  Maxx::Glue::luaopen_Display(L);
  Maxx::Glue::luaopen_Image(L);
  Maxx::Glue::luaopen_Camera(L);
  Maxx::Glue::luaopen_Font(L);
  Maxx::Glue::luaopen_FontManager(L);
  
  Maxx::Glue::luaopen_Matrix(L);
  Maxx::Glue::luaopen_Vector(L);
  
  Maxx::Glue::luaopen_Utils(L);
  
  Maxx::Glue::luaopen_Physics(L);
  
  Maxx::Glue::luaopen_Audio(L);
  
  Maxx::Glue::luaopen_FileSystem(L);
  
  Maxx::Glue::luaopen_Component(L);
  Maxx::Glue::luaopen_Entity(L);
  Maxx::Glue::luaopen_Postmaster(L);
  
  Maxx::Glue::luaopen_AsyncLoader(L);
  
  InitializeMaxxTableForLua();
  
  // Create empty scene
  sceneStack_.push_back(new Entity());
  
  // Lua side initialization
  RunLuaFile("maxx/init-maxx.lua");
  
  // User entry point
  RunLuaFile("main.lua");
}

//--------------------------------------------------------------------
void Instance::InitializeMaxxTableForLua()
{
  lua_State* L = L_;
  
  // Make the maxx global in lua
  lua_newtable(L);
  lua_setfield(L, LUA_GLOBALSINDEX, "maxx");
  
  // maxx.metatable = class metatable
  lua_pushstring(L, "maxx");
  lua_gettable(L, LUA_GLOBALSINDEX);
  lua_pushstring(L, Maxx::Glue::TypeString<Instance>());
  lua_gettable(L, LUA_REGISTRYINDEX);
  lua_setmetatable(L, -2);
  
  // clear the stack
  lua_pop(L, lua_gettop(L));

  // _G.maxx.Image = class metatable
  lua_pushstring(L, "maxx");
  lua_gettable(L, LUA_GLOBALSINDEX);
  lua_pushstring(L, Maxx::Glue::TypeString<Image>());
  lua_gettable(L, LUA_REGISTRYINDEX);
  lua_setfield(L, -2, "Image");

  // _G.maxx.Font = class metatable
  lua_pushstring(L, "maxx");
  lua_gettable(L, LUA_GLOBALSINDEX);
  lua_pushstring(L, Maxx::Glue::TypeString<Font>());
  lua_gettable(L, LUA_REGISTRYINDEX);
  lua_setfield(L, -2, "Font");
  
  // _G.maxx.Matrix = class metatable
  lua_pushstring(L, "maxx");
  lua_gettable(L, LUA_GLOBALSINDEX);
  lua_pushstring(L, Maxx::Glue::TypeString<Matrix>());
  lua_gettable(L, LUA_REGISTRYINDEX);
  lua_setfield(L, -2, "Matrix");

  // _G.maxx.Vector = class metatable
  lua_pushstring(L, "maxx");
  lua_gettable(L, LUA_GLOBALSINDEX);
  lua_pushstring(L, Maxx::Glue::TypeString<Vector>());
  lua_gettable(L, LUA_REGISTRYINDEX);
  lua_setfield(L, -2, "Vector");
  
  // _G.maxx.Component = class metatable
  lua_pushstring(L, "maxx");
  lua_gettable(L, LUA_GLOBALSINDEX);
  lua_pushstring(L, Maxx::Glue::TypeString<Component>());
  lua_gettable(L, LUA_REGISTRYINDEX);
  lua_setfield(L, -2, "Component");
  
  // _G.maxx.Entity = class metatable
  lua_pushstring(L, "maxx");
  lua_gettable(L, LUA_GLOBALSINDEX);
  lua_pushstring(L, Maxx::Glue::TypeString<Entity>());
  lua_gettable(L, LUA_REGISTRYINDEX);
  lua_setfield(L, -2, "Entity");

  // Push _G.maxx onto the stack so we can set fields
  lua_pushstring(L, "maxx");
  lua_gettable(L, LUA_GLOBALSINDEX);
  
  // _G.maxx.keyboard = Maxx::Get()->GetKeyboard()
  Maxx::Glue::NewUserData<Maxx::Keyboard>(L, Maxx::Get()->GetKeyboard(), /*own=*/false);
  lua_setfield(L, -2, "keyboard");
  
  // _G.maxx.mouse = Maxx::Get()->GetMouse()
  Maxx::Glue::NewUserData<Maxx::Mouse>(L, Maxx::Get()->GetMouse(), /*own=*/false);
  lua_setfield(L, -2, "mouse");

  // _G.maxx.accelerometer = Maxx::Get()->GetAccelerometer()
  Maxx::Glue::NewUserData<Maxx::Accelerometer>(L, Maxx::Get()->GetAccelerometer(), /*own=*/false);
  lua_setfield(L, -2, "accelerometer");

  // _G.maxx.touchInput = Maxx::Get()->GetTouchInput()
  Maxx::Glue::NewUserData<Maxx::TouchInput>(L, Maxx::Get()->GetTouchInput(), /*own=*/false);
  lua_setfield(L, -2, "touchInput");
    
  // _G.maxx.postmaster = Maxx::Get()->GetPostmaster()
  Maxx::Glue::NewUserData<Maxx::Postmaster>(L, Maxx::Get()->GetPostmaster(), /*own=*/false);
  lua_setfield(L, -2, "postmaster");

  // _G.maxx.physics = Maxx::Get()->GetPhysics()
  Maxx::Glue::NewUserData<Maxx::Physics>(L, Maxx::Get()->GetPhysics(), /*own=*/false);
  lua_setfield(L, -2, "physics");
  
  // _G.maxx.camera = Maxx::Get()->GetGraphics()->GetCamera()
  Maxx::Glue::NewUserData<Maxx::Camera>(L, &Maxx::Get()->GetGraphics()->GetCamera(), /*own=*/false);
  lua_setfield(L, -2, "camera");

  // _G.maxx.graphics = new table
  lua_newtable(L);
  lua_setfield(L, -3, "graphics");
  // _G.maxx.graphics.metatable = class metatable
  lua_pushstring(L, "graphics");
  lua_gettable(L, -3);
  lua_pushstring(L, Maxx::Glue::TypeString<Maxx::Glue::Graphics_DummyClass>());
  lua_gettable(L, LUA_REGISTRYINDEX);
  lua_setmetatable(L, -2);
  lua_pop(L, 1); // get _G.maxx back to stack -2

  // _G.maxx.display = Maxx::Get()->GetDisplay()
  Maxx::Glue::NewUserData<Maxx::Display>(L, Maxx::Get()->GetDisplay(), /*own=*/false);
  lua_setfield(L, -2, "display");

  // _G.maxx.utils = new table
  lua_newtable(L);
  lua_setfield(L, -3, "utils");
  // _G.maxx.utils.metatable = class metatable
  lua_pushstring(L, "utils");
  lua_gettable(L, -3);
  lua_pushstring(L, Maxx::Glue::TypeString<Maxx::Glue::Utils_DummyClass>());
  lua_gettable(L, LUA_REGISTRYINDEX);
  lua_setmetatable(L, -2);
  lua_pop(L, 1); // get _G.maxx back to stack -2
  
  // _G.maxx.audio = Maxx::Get()->GetAudio()
  Maxx::Glue::NewUserData<Maxx::Audio>(L, Maxx::Get()->GetAudio(), /*own=*/false);
  lua_setfield(L, -2, "audio");
  
  // _G.maxx.filesys = new table
  lua_newtable(L);
  lua_setfield(L, -3, "filesys");
  // _G.maxx.utils.metatable = class metatable
  lua_pushstring(L, "filesys");
  lua_gettable(L, -3);
  lua_pushstring(L, Maxx::Glue::TypeString<Maxx::Glue::FileSystem_DummyClass>());
  lua_gettable(L, LUA_REGISTRYINDEX);
  lua_setmetatable(L, -2);
  lua_pop(L, 1); // get _G.maxx back to stack -2
  
  // _G.maxx.fontManager = Maxx::Get()->GetFontManager()
  Maxx::Glue::NewUserData<Maxx::FontManager>(L, Maxx::Get()->GetFontManager(), /*own=*/false);
  lua_setfield(L, -2, "fontManager");

  // _G.maxx.asyncLoader = Maxx::Get()->GetAsyncLoader()
  Maxx::Glue::NewUserData<Maxx::AsyncLoader>(L, Maxx::Get()->GetAsyncLoader(), /*own=*/false);
  lua_setfield(L, -2, "asyncLoader");
  
  // clear the stack
  lua_pop(L, lua_gettop(L));
}

} // Maxx
