#pragma once

#include "cross-platform/lua-headers.h"
#include <string>
#include <vector>

class b2World;

namespace Maxx {

class Display;
class Graphics;
class FontManager;
class Keyboard;
class Mouse;
class TouchInput;
class Accelerometer;
class PrefabManager;
class Postmaster;
class Physics;
class Entity;
class Server;
class Property;
class Audio;
class AsyncLoader;
class PropertyCache;
class Profiler;

//--------------------------------------------------------------------
class Instance
{
public:
  //--------------------------------------------------------------------
  static Instance* Create(int& argc, char** argv);
  ~Instance();
  
  //--------------------------------------------------------------------
  void SetGameName(const std::string& gameName);
  const std::string& GetGameName() const { return gameName_; }

  //--------------------------------------------------------------------
  Display* GetDisplay() { return display_; }
  Graphics* GetGraphics() { return graphics_; }
  FontManager* GetFontManager() { return fontManager_; }
  Keyboard* GetKeyboard() { return keyboard_; }
  Mouse* GetMouse() { return mouse_; }
  TouchInput* GetTouchInput() { return touchInput_; }
  PrefabManager* GetPrefabManager() { return prefabManager_; }
  Accelerometer* GetAccelerometer() { return accelerometer_; }
  Postmaster* GetPostmaster() { return postMaster_; }
  Physics* GetPhysics() { return physics_; }
  Audio* GetAudio() { return audio_; }
  AsyncLoader* GetAsyncLoader() { return asyncLoader_; }
  PropertyCache* GetPropertyCache() { return propertyCache_; }
  Profiler* GetProfiler() { return profiler_; }

  //--------------------------------------------------------------------
  void Update();
  bool UpdateInProgress() const { return updateInProgress_; }

  //--------------------------------------------------------------------
  double GetTime() const;
  void SetFrameSkip(int frameSkip) { frameSkip_ = frameSkip; }
  int GetFrameSkip() const { return frameSkip_; }
  double GetTimeScale() const { return timeScale_; }
  void SetTimeScale(double timeScale) { timeScale_ = timeScale; }
  
  //--------------------------------------------------------------------
  void Quit();
  bool CheckQuit() const;
  void Reset();

  //--------------------------------------------------------------------
  void RunLuaFile(const std::string& luaFileName);
  bool RunLuaCode(const std::string& luaCode, Property* result = 0);
  
  //--------------------------------------------------------------------
  Entity* GetScene();
  Entity* GetNextScene();
  void ChangeScene(Entity* scene);
  void PushScene(Entity* scene);
  void PopScene();

  //--------------------------------------------------------------------
  lua_State* GetLuaState() { return L_; }
  
  //--------------------------------------------------------------------
  Entity* GetFocus() const;
  void SetFocus(Entity* focus);
  
private:
  Instance(bool& success, int& argc, char** argv);

  void PerformReset();
  void InitializeMaxxTableForLua();
  
  void StepPhysics(double dt);
  
private:
  std::string gameName_;
  Server* server_;
  Display* display_;
  Graphics* graphics_;
  FontManager* fontManager_;
  Keyboard* keyboard_;
  Mouse* mouse_;
  TouchInput* touchInput_;
  Accelerometer* accelerometer_;
  PrefabManager* prefabManager_;
  Postmaster* postMaster_;
  Physics* physics_;
  Audio* audio_;
  AsyncLoader* asyncLoader_;
  PropertyCache* propertyCache_;
  Profiler* profiler_;
  lua_State* L_;
  bool quit_;
  bool reset_;
  typedef std::vector<Entity*> SceneStack;
  SceneStack sceneStack_;
  Entity* nextScene_;
  bool updateInProgress_;
  std::string focus_; // entity guid
  double timeScale_;
  int frameSkip_;
  int frameSkipCounter_;
  bool backgrounded_;
};

Instance* Initialize(int& argc, char** argv);
void ShutDown();
Instance* Get();

} // Maxx

