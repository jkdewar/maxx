#include "engine/prefab-manager.h"
#include "engine/entity.h"
#include "engine/property-table.h"

namespace Maxx {
  
// TODO: Remove PrefabManager

//--------------------------------------------------------------------
PrefabManager::PrefabManager()
{
}

//--------------------------------------------------------------------
PrefabManager::~PrefabManager()
{
  for (Map::iterator i = map_.begin(); i != map_.end(); ++i)
  {
    delete i->second;
  }
  map_.clear();
}

//--------------------------------------------------------------------
Entity* PrefabManager::GetPrefab(const std::string& fileName)
{
  Assert(false);
  Map::iterator i = map_.find(fileName);
  if (i != map_.end())
    return i->second;
  Entity* prefab = Entity::Load(fileName, /*isPrefab=*/true);
  //Trace("\nprefab \"%s\":\n%s", fileName.c_str(), prefab->Stringify().c_str());
  if (!prefab)
  {
    Trace("PrefabManager::GetPrefab: ERROR: Couldn't load prefab '%s'\n", fileName.c_str());
    return 0;
  }
  map_.insert(std::make_pair(fileName, prefab));
  return prefab;
}

//--------------------------------------------------------------------
Entity* PrefabManager::CreateInstanceOfPrefab(const std::string& fileName)
{
  PropertyTable table;
  table.Set("prefab", fileName);
  Entity* instance = Entity::Deserialize(table);
  return instance;
}

} // Maxx
