#pragma once

#include <map>
#include <string>

namespace Maxx {
  
class Entity;

//--------------------------------------------------------------------
// TODO: Remove PrefabManager.
class PrefabManager
{
public:
  PrefabManager();
  ~PrefabManager();
  
  Entity* GetPrefab(const std::string& fileName);
  Entity* CreateInstanceOfPrefab(const std::string& fileName);

private:
  typedef std::map<std::string, Entity*> Map;
  Map map_;
};

} // Maxx
