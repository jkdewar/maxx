#if 0
#include "engine/property.h"
#include "engine/property-table.h"
#include "math/math.h"
#include "utils/assert.h"
#include "engine/maxx.h"
#include "cross-platform/lua-headers.h"
#include <sstream>

namespace Maxx {

//--------------------------------------------------------------------
Property::Property() : value_() {}
Property::Property(const std::string& val) : value_(val) {}
Property::Property(const char* val) : value_(std::string(val)) {}
Property::Property(double val) : value_(val) {}
Property::Property(bool val) : value_(val) {}
Property::Property(const PropertyTable& val) : value_(val) {}

//--------------------------------------------------------------------
Property::~Property() {}

//--------------------------------------------------------------------
bool Property::IsNull() const { return this->value_.IsNull(); }
bool Property::IsString() const { return this->value_.IsType<std::string>(); }
bool Property::IsNumber() const { return this->value_.IsType<double>(); }
bool Property::IsBool() const { return this->value_.IsType<bool>(); }
bool Property::IsTable() const { return this->value_.IsType<PropertyTable>(); }

//--------------------------------------------------------------------
Property& Property::operator=(const Property& prop) { this->value_ = prop.value_; return *this; }
Property& Property::operator=(const std::string& val) { this->value_ = val; return *this; }
Property& Property::operator=(const char* val) { this->value_ = std::string(val); return *this; }
Property& Property::operator=(double val) { this->value_ = val; return *this; }
Property& Property::operator=(bool val) { this->value_ = val; return *this; }
Property& Property::operator=(const PropertyTable& val) { this->value_ = val; return *this; }
const std::string& Property::GetString() const { Assert(IsString()); return this->value_.Get<std::string>(); }
double Property::GetNumber() const { Assert(IsNumber()); return this->value_.Get<double>(); }
bool Property::GetBool() const { Assert(IsBool()); return this->value_.Get<bool>(); }
const PropertyTable& Property::GetTable() const { Assert(IsTable()); return this->value_.Get<PropertyTable>(); }

//--------------------------------------------------------------------
PropertyTable* Property::GetTablePtr()
{
  if (!IsTable())
    return 0;
  return &this->value_.Get<PropertyTable>();
}

} // Maxx
#endif
