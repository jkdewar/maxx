#pragma once

#include "utils/any.h"
#include <string>
#include "utils/assert.h"

struct lua_State;

namespace Maxx {
  
class PropertyTable;

//--------------------------------------------------------------------
// A Lua-style value.
// Has a value of one of the following types:
// Null   - (no value)
// String - std:string
// Number - double
// bool   - bool
// Table  - PropertyTable (keyed by std::string and/or int)
class Property
{
public:
  Property();
  Property(const std::string&);
  Property(const char*);
  Property(double);
  explicit Property(bool);
  Property(const PropertyTable&);
  ~Property();

  bool IsNull() const;
  bool IsString() const;
  bool IsNumber() const;
  bool IsBool() const;
  bool IsTable() const;
  
  Property& operator=(const Property&);
  Property& operator=(const std::string&);
  Property& operator=(const char*);
  Property& operator=(double);
  Property& operator=(bool);
  Property& operator=(const PropertyTable&);

  operator bool () const { return GetBool(); }
  operator double () const {return GetNumber(); }
  operator const std::string& () const { return GetString(); }
  operator const PropertyTable& () const { return GetTable(); }

  const std::string& GetString() const;
  double GetNumber() const;
  bool GetBool() const;
  const PropertyTable& GetTable() const;
  PropertyTable* GetTablePtr(); // avoids copying
  const PropertyTable* GetTablePtr() const { return const_cast<Property*>(this)->GetTablePtr(); }
  
  std::string Stringify(const std::string& indent = "") const;
  static bool Unstringify(lua_State* L, const std::string& str, size_t& index, Property& to);
  
  void PushOnLuaStack(lua_State* L) const;
  static void FromLuaStack(lua_State* L, int index, Property& ret);

private:
  Maxx::Utils::Any value_;
};

} // Maxx
