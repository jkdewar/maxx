#include "engine/property-cache.h"
#include "engine/property.h"
#include "engine/property-table.h"
#include "engine/maxx.h"
#include "cross-platform/file-sys.h"
#include "cross-platform/sdl-headers.h"

namespace Maxx {

//--------------------------------------------------------------------
PropertyCache::PropertyCache()
  : mutex_(0)
{
  mutex_ = SDL_CreateMutex();
}

//--------------------------------------------------------------------
PropertyCache::~PropertyCache()
{
  SDL_DestroyMutex(mutex_); mutex_ = 0;
  for (Map::iterator i = map_.begin(); i != map_.end(); ++i)
  {
    delete i->second;
  }
  map_.clear();
}

//--------------------------------------------------------------------
Property* PropertyCache::LoadPropertyFromFile(const std::string& fileName, lua_State* L)
{
//  Map::iterator i = map_.find(fileName);
//  if (i != map_.end())
//  {
//    return i->second;
//  }

  std::string fileContents;
  //Trace("READING %s\n", fileName.c_str());
  bool readOk = ReadFile(fileName, fileContents);
  if (!readOk)
  {
    Trace("ERROR: loading '%s'\n", fileName.c_str());
    return 0;
  }
  Property* prop = new Property;
  size_t dontCare = 0;
  bool unstringifyOk = Property::Unstringify(L, fileContents, dontCare, *prop);
  if (!unstringifyOk)
  {
    Trace("ERROR: unstringifying '%s'\n", fileName.c_str());
  }
  
  if (!prop || !prop->IsTable() || prop->GetTable().GetBool("cacheable",true))
    map_.insert(std::make_pair(fileName, prop));
//  // TODO: non-cacheable properties leak
  
  return prop;
}

} // Maxx
