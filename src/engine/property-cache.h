#pragma once
#include <string>
#include <map>
#include "cross-platform/lua-headers.h"

struct SDL_mutex;

namespace Maxx {

class Property;

class PropertyCache
{
public:
  PropertyCache();
  ~PropertyCache();

  Property* LoadPropertyFromFile(const std::string& fileName, lua_State* L);

private:
  typedef std::map<std::string, Property*> Map;
  Map map_;
  SDL_mutex* mutex_;
};

} // Maxx
