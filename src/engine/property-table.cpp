#include "engine/property-table.h"
#include "cross-platform/lua-headers.h"
#include <sstream>

namespace Maxx {

//--------------------------------------------------------------------
static bool IsDigit(char c)
{
  if (c >= '0' && c <= '9')
    return true;
  return false;
}

// //--------------------------------------------------------------------
// static bool IsWhitespace(char c)
// {
//   if (c == ' ' || c == '\r' || c == '\n' || c == '\t' || c == '\0')
//     return true;
//   return false;  
// }
// 
// //--------------------------------------------------------------------
// static int GetDigit(char c)
// {
//  switch (c)
//  {
//    case '0': return 0;
//    case '1': return 1;
//    case '2': return 2;
//    case '3': return 3;
//    case '4': return 4;
//    case '5': return 5;
//    case '6': return 6;
//    case '7': return 7;
//    case '8': return 8;
//    case '9': return 9;
//    default: return -1;
//  }
// }

//--------------------------------------------------------------------
static bool IsValidVariableFirstChar(char c)
{
  if (c >= 'a' && c <= 'z')
    return true;
  if (c >= 'A' && c <= 'Z')
    return true;
  return false;
}

//--------------------------------------------------------------------
static bool IsValidVariableChar(char c)
{
  if (IsValidVariableFirstChar(c))
    return true;
  if (IsDigit(c))
    return true;
  return false;
}

//--------------------------------------------------------------------
static bool IsValidVariableName(const std::string& str)
{
  if (str.empty())
    return false;
  if (!IsValidVariableFirstChar(str[0]))
    return false;
  for (size_t i = 1; i < str.size(); ++i)
  {
    if (!IsValidVariableChar(str[i]))
      return false;
  }
  return true;
}
//--------------------------------------------------------------------
std::string PropertyTable::Stringify(const std::string& indent) const
{
  std::stringstream ss;
  ss << "{\n";
  for (MapFromString::const_iterator i = mapFromString.begin(); i != mapFromString.end(); ++i)
  {
    const std::string& name = i->first;
    const Property& value = i->second;
    bool useLongFormKey = false;
    if (!IsValidVariableName(name))
      useLongFormKey = true;
    if (useLongFormKey)
      ss << indent << "  " << "[" << Property(name).Stringify() << "] = ";
    else
      ss << indent << "  " << name << " = ";
    ss << value.Stringify(indent + "  ") << ",\n";
  }
  for (MapFromInt::const_iterator i = mapFromInt.begin(); i != mapFromInt.end(); ++i)
  {
    int index = i->first;
    const Property& value = i->second;
    ss << indent << "  " << "[" << index << "] = ";
    ss << value.Stringify(indent + "  ") << ",\n";
  }
  ss << indent << "}";
  return ss.str();
}

//--------------------------------------------------------------------
/*static*/ bool PropertyTable::Unstringify(lua_State* L, const std::string& input, size_t& index, PropertyTable& to)
{
  Property p;
  if (!Property::Unstringify(L, input, index, p))
    return false;
  if (!p.IsTable())
    return false;
  to = p;
  return true;
}

//--------------------------------------------------------------------
PropertyTable* PropertyTable::GetTable(const std::string& key)
{
  Property* property = Get(key);
  if (property && property->IsTable())
    return property->GetTablePtr();
  return 0;
}

//--------------------------------------------------------------------
const PropertyTable* PropertyTable::GetTable(const std::string& key) const
{
  return const_cast<PropertyTable*>(this)->GetTable(key);
}

//--------------------------------------------------------------------
const std::string& PropertyTable::GetString(const std::string& key, const std::string& defaultValue) const
{
  const Property* property = Get(key);
  if (property && property->IsString())
    return property->GetString();
  return defaultValue;
}

//--------------------------------------------------------------------
double PropertyTable::GetNumber(const std::string& key, double defaultValue) const
{
  const Property* property = Get(key);
  if (property && property->IsNumber())
    return property->GetNumber();
  return defaultValue;
}

//--------------------------------------------------------------------
bool PropertyTable::GetBool(const std::string& key, bool defaultValue) const
{
  const Property* property = Get(key);
  if (property && property->IsBool())
    return property->GetBool();
  return defaultValue;
}

//--------------------------------------------------------------------
PropertyTable* PropertyTable::GetTable(int key)
{
  Property* property = Get(key);
  if (property && property->IsTable())
    return property->GetTablePtr();
  return 0;
}

//--------------------------------------------------------------------
const PropertyTable* PropertyTable::GetTable(int key) const
{
  return const_cast<PropertyTable*>(this)->GetTable(key);
}

//--------------------------------------------------------------------
const std::string& PropertyTable::GetString(int key, const std::string& defaultValue) const
{
  const Property* property = Get(key);
  if (property && property->IsString())
    return property->GetString();
  return defaultValue;
}

//--------------------------------------------------------------------
double PropertyTable::GetNumber(int key, double defaultValue) const
{
  const Property* property = Get(key);
  if (property && property->IsNumber())
    return property->GetNumber();
  return defaultValue;
}

//--------------------------------------------------------------------
bool PropertyTable::GetBool(int key, bool defaultValue) const
{
  const Property* property = Get(key);
  if (property && property->IsBool())
    return property->GetBool();
  return defaultValue;
}

//--------------------------------------------------------------------
void PropertyTable::Append(const PropertyTable& table)
{
  Assert(this != &table);
  mapFromString.insert(table.mapFromString.begin(), table.mapFromString.end());
  mapFromInt.insert(table.mapFromInt.begin(), table.mapFromInt.end());
}

//--------------------------------------------------------------------
void PropertyTable::PushOnLuaStack(lua_State* L) const
{
  int oldTop = lua_gettop(L);

  lua_newtable(L);

  for (MapFromInt::const_iterator i = mapFromInt.begin(); i != mapFromInt.end(); ++i)
  {
    int index = i->first;
    const Property& value = i->second;
    value.PushOnLuaStack(L);
    lua_rawseti(L, -2, index);
  }
  
  for (MapFromString::const_iterator i = mapFromString.begin(); i != mapFromString.end(); ++i)
  {
    const std::string& key = i->first;
    const Property& value = i->second;
    lua_pushstring(L, key.c_str());
    value.PushOnLuaStack(L);
    lua_rawset(L, -3);
  }

  Assert(lua_gettop(L) - oldTop == 1);

  return;
}

//--------------------------------------------------------------------
/*static*/ void PropertyTable::FromLuaStack(lua_State* L, int index, PropertyTable& table)
{
  int oldTop = lua_gettop(L);

  if (index >= 0)
    index = -(lua_gettop(L)+1 - index);
  Assert(index <= -1);

  if (!lua_isnil(L, index))
  {
    lua_pushnil(L);
    while (lua_next(L, index - 1))
    {
      Property key;
      Property::FromLuaStack(L, index - 1, key);
      if (key.IsString())
        table.Insert(key.GetString(), Property());
      else
        table.Insert((int)key.GetNumber(), Property());
      Property& value = key.IsString() ? *table.Get(key.GetString()) : *table.Get((int)key.GetNumber());
      Property::FromLuaStack(L, index, value);
      lua_pop(L, 1);
    }
  }

  Assert(lua_gettop(L) == oldTop);
}

} // Maxx
