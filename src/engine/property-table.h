#pragma once
#include "engine/property.h"
#include <map>
#include <string>


namespace Maxx {

//--------------------------------------------------------------------
class PropertyTable
{
public:
  std::string Stringify(const std::string& indent = "") const;
  static bool Unstringify(lua_State* L, const std::string& str, size_t& index, PropertyTable& to);
  
  bool operator==(const PropertyTable&) const { return false; }
  
  bool HasKey(const std::string& key) const { return mapFromString.find(key) != mapFromString.end(); }
  bool HasKey(int key) const { return mapFromInt.find(key) != mapFromInt.end(); }
  void Insert(const std::string& key, const Property& value) { /*mapFromString.insert(std::make_pair(key, value));*/Set(key,value); }
  void Insert(int key, const Property& value) { /*mapFromInt.insert(std::make_pair(key, value));*/Set(key,value); }
  PropertyTable* InsertTable(const std::string& key, const PropertyTable& propertyTable = PropertyTable()) { Insert(key, propertyTable); return GetTable(key); }
  PropertyTable* InsertTable(int key, const PropertyTable& propertyTable = PropertyTable()) { Insert(key, propertyTable); return GetTable(key); }

  //Property GetValue(const std::string& key) const { if (!HasKey(key)) return Property(); return mapFromString.find(key)->second; }
  Property* Get(const std::string& key) { if (!HasKey(key)) return 0; return &mapFromString[key]; }
  Property* Get(int key) { if (!HasKey(key)) return 0; return &mapFromInt[key]; }
  const Property* Get(const std::string& key) const { MapFromString::const_iterator i = mapFromString.find(key); if (i == mapFromString.end()) return 0; return &(i->second); }
  const Property* Get(int key) const { MapFromInt::const_iterator i = mapFromInt.find(key); if (i == mapFromInt.end()) return 0; return &(i->second); }
  void Set(const std::string& key, const Property& value) { mapFromString[key] = value; }
  void Set(int key, const Property& value) { mapFromInt[key] = value; }
  
  const PropertyTable* GetTable(const std::string& key) const;
  PropertyTable* GetTable(const std::string& key);
  const std::string& GetString(const std::string& key, const std::string& defaultValue = "") const;
  double GetNumber(const std::string& key, double defaultValue = 0.0) const;
  bool GetBool(const std::string& key, bool defaultValue = false) const;
  const PropertyTable* GetTable(int key) const;
  PropertyTable* GetTable(int key);
  const std::string& GetString(int key, const std::string& defaultValue = "") const;
  double GetNumber(int key, double defaultValue = 0.0) const;
  bool GetBool(int key, bool defaultValue = false) const;
  
  void Append(const PropertyTable& table);
  
  bool IsEmpty() { return (mapFromString.empty() && mapFromInt.empty()); }
  
  void PushOnLuaStack(lua_State* L) const;
  static void FromLuaStack(lua_State* L, int index, PropertyTable& ret);
  
  typedef std::map<std::string, Property> MapFromString;
  typedef std::map<int, Property> MapFromInt;
  
  MapFromString mapFromString;
  MapFromInt    mapFromInt;
  
private:
//  PropertyTable(const PropertyTable&);
//  PropertyTable& operator=(const PropertyTable&);
};

} // Maxx
