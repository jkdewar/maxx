#if 1
#include "engine/property-union.h"
#include "engine/property-table.h"

namespace Maxx {

PropertyTable* Property::NewPropertyTable(const PropertyTable& p)
{
  PropertyTable* t = new PropertyTable(p);
  return t;
}

void Property::DeleteTable()
{
  delete table_; table_ = 0;
  //table_.reset();
}
  
} // Maxx

#endif
