#pragma once

#include <string>
#include "utils/assert.h"

struct lua_State;

namespace Maxx {
  
class PropertyTable;

//--------------------------------------------------------------------
// A Lua-style value.
// Has a value of one of the following types:
// Null   - (no value)
// String - std:string
// Number - double
// bool   - bool
// Table  - PropertyTable (keyed by std::string and/or int)
class Property
{
public:
  Property() : type_(Type_Null), table_(0) {}
  Property(const Property& v) : table_(0) { *this = v; }
  Property(const std::string& v) : type_(Type_String), table_(0) { string_ = v; }
  Property(const char* v) : type_(Type_String), table_(0), string_(v) {}
  Property(double v) : type_(Type_Number), table_(0), number_(v) {}
  explicit Property(bool v) : type_(Type_Bool), table_(0), bool_(v) {}
  Property(const PropertyTable& v) : type_(Type_Null), table_(0) { *this = v; }
  ~Property() { DeleteTable(); }

  bool IsNull() const { return type_ == Type_Null; }
  bool IsString() const { return type_ == Type_String; }
  bool IsNumber() const { return type_ == Type_Number; }
  bool IsBool() const { return type_ == Type_Bool; }
  bool IsTable() const { return type_ == Type_Table; }
  
  Property& operator=(const Property& p)
  {
    DeleteTable();
    type_ = p.type_;
    switch(type_)
    {
      case Type_Null: break;
      case Type_String: string_ = p.string_; break;
      case Type_Number: number_ = p.number_; break;
      case Type_Bool: bool_ = p.bool_; break;
      case Type_Table: table_ = NewPropertyTable(*p.table_); break;
      default: Assert(false); break;
    }
    return *this;
  }
  
  Property& operator=(const std::string& v)
  {
    DeleteTable();
    type_ = Type_String;
    string_ = v;
    return *this;
  }
  Property& operator=(const char* v)
  {
    DeleteTable();
    type_ = Type_String;
    string_ = v;
    return *this;
  }
  Property& operator=(double v)
  {
    DeleteTable();
    type_ = Type_Number;
    number_ = v;
    return *this;
  }
  Property& operator=(bool v)
  {
    DeleteTable();
    type_ = Type_Bool;
    bool_ = v;
    return *this;
  }
  Property& operator=(const PropertyTable& v)
  {
    DeleteTable();
    type_ = Type_Table;
    table_ = NewPropertyTable(v);
    return *this;
  }

  operator bool () const { return GetBool(); }
  operator double () const {return GetNumber(); }
  operator const std::string& () const { return GetString(); }
  operator const PropertyTable& () const { return GetTable(); }

  const std::string& GetString() const { return string_; }
  double GetNumber() const { return number_; }
  bool GetBool() const { return bool_; }
  const PropertyTable& GetTable() const { return *table_; }
  PropertyTable* GetTablePtr() { return table_; }
  const PropertyTable* GetTablePtr() const { return const_cast<Property*>(this)->GetTablePtr(); }
  
  std::string Stringify(const std::string& indent = "") const;
  static bool Unstringify(lua_State* L, const std::string& str, size_t& index, Property& to);
  
  void PushOnLuaStack(lua_State* L) const;
  static void FromLuaStack(lua_State* L, int index, Property& ret);

private:
  PropertyTable* NewPropertyTable(const PropertyTable& p);
  void DeleteTable();
  
private:
  enum Type { Type_Null=0, Type_String=1, Type_Number=2, Type_Bool=3, Type_Table=4 };
  Type type_;
  PropertyTable* table_;
  std::string string_;
  double number_;
  bool bool_;
};

} // Maxx
