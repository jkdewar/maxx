#include "engine/property.h"
#include "engine/property-table.h"
#include "cross-platform/lua-headers.h"
#include <sstream>

namespace Maxx {

//--------------------------------------------------------------------
std::string Property::Stringify(const std::string& indent) const
{
  std::stringstream ss;
  if (IsNull())
  {
    ss << "nil";
  }
  else if (IsString())
  {
    ss << "\"" << GetString() << "\"";
  }
  else if (IsNumber())
  {
    ss << GetNumber();
  }
  else if (IsBool())
  {
    if (GetBool())
      ss << "true";
    else
      ss << "false";
  }
  else if (IsTable())
  {
    ss << GetTable().Stringify(indent);
  }
  else
  {
    Assert(false);
  }
  return ss.str();
}

//--------------------------------------------------------------------
void Property::PushOnLuaStack(lua_State* L) const
{
  if (IsNull())
    lua_pushnil(L);
  else if (IsString())
    lua_pushstring(L, GetString().c_str());
  else if (IsNumber())
    lua_pushnumber(L, GetNumber());
  else if (IsBool())
    lua_pushboolean(L, GetBool());
  else if (IsTable())
    GetTable().PushOnLuaStack(L);
  else
  {
    Assert(false);
    lua_pushnil(L);
  }
}

//--------------------------------------------------------------------
/*static*/ void Property::FromLuaStack(lua_State* L, int index, Property& value)
{
  int type = lua_type(L, index);
  if (type == LUA_TNIL)
    value = Property();
  else if (type == LUA_TSTRING)
    value = lua_tostring(L, index);
  else if (type == LUA_TNUMBER)
    value = lua_tonumber(L, index);
  else if (type == LUA_TBOOLEAN)
    value = Property((bool)(lua_toboolean(L, index) != 0));
  else if (type == LUA_TTABLE)
  {
    value = PropertyTable();
    PropertyTable::FromLuaStack(L, index, *value.GetTablePtr());
  }
  else if (type == LUA_TUSERDATA)
  {
    value = "<userdata>";
  }
  else if (type == LUA_TFUNCTION)
  {
    value = "<function>";
  }
  else
    Assert(false);
}

//--------------------------------------------------------------------
/*static*/ bool Property::Unstringify(lua_State* L, const std::string& input, size_t& index, Property& to)
{
  // This intentially does not use Maxx::RunLuaCode.
  // Property should not depend on Maxx.
  // Julie currently uses Property.

  std::string s = "return " + input;
  int loadResult = luaL_loadstring(L, s.c_str());
  if (loadResult != 0)
  {
    Trace("%s\n", lua_tostring(L, -1));
    lua_pop(L, lua_gettop(L));
    return false;
  }
  int callResult = lua_pcall(L, 0, LUA_MULTRET, 0);
  if (callResult != 0)
  {
    lua_pop(L, lua_gettop(L));
    return false;
  }
  Property::FromLuaStack(L, -1, to);
  lua_pop(L, lua_gettop(L));
  return true;
}

} // Maxx
