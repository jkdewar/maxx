#pragma once

#include <map>
#include <string>

namespace Maxx {
  
class Entity;

//--------------------------------------------------------------------
class ScriptManager
{
public:
  ScriptManager();
  ~ScriptManager();
  
  void RunAll(const std::string& path, bool recursive = true);
};

} // Maxx
