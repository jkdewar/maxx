#if !defined(__IPHONEOS__)
#include "engine/server.h"
#include "engine/maxx.h"
#include "engine/property.h"
#include "utils/trace.h"

namespace Maxx {
  
//--------------------------------------------------------------------
class Connection
{
public:
  Connection(TCPsocket sock);
  ~Connection();
  void Update();
  bool Send(const std::string& s);
  bool IsClosed() const { return !sock_; }
private:
  bool ExecuteBufferedCommand();
  TCPsocket sock_;
  std::string buffer_;
};

//--------------------------------------------------------------------
Connection::Connection(TCPsocket sock)
  : sock_(sock)
{
}

//--------------------------------------------------------------------
Connection::~Connection()
{
  if (sock_)
  {
    SDLNet_TCP_Close(sock_); 
    sock_ = 0;
  }
}

//--------------------------------------------------------------------
void Connection::Update()
{
  if (!sock_)
    return;

  // Check if our socket has data available, without blocking.
  SDLNet_SocketSet set;
  set = SDLNet_AllocSocketSet(16);
  SDLNet_TCP_AddSocket(set, sock_);
  SDLNet_CheckSockets(set, 0);
  SDLNet_FreeSocketSet(set);
  set = 0;
  if (!SDLNet_SocketReady(sock_))
    return;

  // Data is available. Read it.
  const int maxlen = 1024*4;
  char data[maxlen+1];
  int bytesReceived = SDLNet_TCP_Recv(sock_, data, maxlen);
  if (bytesReceived <= 0)
  {
    // Connection lost
    SDLNet_TCP_Close(sock_);
    sock_ = 0;
    return;
  }
  //if (bytesReceived>0){Trace("%d bytes received",bytesReceived);}
  data[bytesReceived] = '\0';
  buffer_ += data;
  
  while (ExecuteBufferedCommand())
    ;
}

//--------------------------------------------------------------------
bool Connection::ExecuteBufferedCommand()
{
  // Look for a \26 character which marks the
  // end of a command string.
  size_t endPos = buffer_.find_first_of((char)26);
  if (endPos == std::string::npos)
    return false;

  // Found one. Pull the command out of our buffer.
  std::string command = buffer_.substr(0, endPos);
  buffer_ = buffer_.substr(endPos + 1, std::string::npos);
  //Trace("command: %s\n", command.c_str());
  
  // Execute the command in Lua.
  Property result;
  bool runOk = Maxx::Get()->RunLuaCode(command, &result);
  std::string strResult;
  if (runOk)
    strResult = result.Stringify();
  else
    strResult = ""; // means there was an error
  strResult.push_back((char)26); // send \26 too
  //Trace("result: '%s'\n", strResult.c_str());

  // Send the result.
  if (!Send(strResult))
    return false;
  
  return true;
}

//--------------------------------------------------------------------
bool Connection::Send(const std::string& s)
{
  int bytesLeft = s.size();
  int i = 0;
  while (bytesLeft > 0)
  {
    int bytesSent = SDLNet_TCP_Send(sock_, &s.c_str()[i], bytesLeft);
    if (bytesSent <= 0)
    {
      // Connection lost
      SDLNet_TCP_Close(sock_);
      sock_ = 0;
      return false;
    }
    bytesLeft -= bytesSent;
    i += bytesSent;
  }
  return true;
}

//--------------------------------------------------------------------
Server::Server(int port)
  : connection_(0)
{
  IPaddress ip;
  if (SDLNet_ResolveHost(&ip, NULL, port) < 0)
  {
    Trace("ERROR: local SDLNet_ResolveHost failed (port %d)\n", port);
    return;
  }
  listenSocket_ = SDLNet_TCP_Open(&ip);
  if (!listenSocket_)
  {
    Trace("ERROR: SDLNet_TCP_Open: %s\n", SDLNet_GetError());
    return;
  }
}

//--------------------------------------------------------------------
Server::~Server()
{
  delete connection_; connection_ = 0;
}

//--------------------------------------------------------------------
void Server::Update()
{
  if (!listenSocket_)
    return;

  if (connection_)
  {
    connection_->Update();
    if (connection_->IsClosed())
    {
      Trace("Lost connection\n");
      delete connection_; connection_ = 0;
    }
    return;
  }

  // Accept an incoming connection, if there is one
  TCPsocket sock = SDLNet_TCP_Accept(listenSocket_);
  if (sock)
  {
    IPaddress* ip = SDLNet_TCP_GetPeerAddress(sock);
    unsigned char byte1 = (ip->host&(0xff<<(8*0)))>>(8*0);
    unsigned char byte2 = (ip->host&(0xff<<(8*1)))>>(8*1);
    unsigned char byte3 = (ip->host&(0xff<<(8*2)))>>(8*2);
    unsigned char byte4 = (ip->host&(0xff<<(8*3)))>>(8*3);
    Trace("Got a connection! %d.%d.%d.%d\n",byte1,byte2,byte3,byte4);
    if (byte1!=127||byte2!=0||byte3!=0||byte4!=1)
    {
      Trace("Connection rejected because it is not coming from localhost (127.0.0.1)\n");
      SDLNet_TCP_Close(sock);
    }
    else
    {
      connection_ = new Connection(sock);
    }
  }
}

//--------------------------------------------------------------------
bool Server::Send(const std::string& s)
{
  if (connection_)
    return connection_->Send(s);
  return false;
}

} // namespace Maxx
#endif
