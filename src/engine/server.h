#if !defined(__IPHONEOS__)
#pragma once
#include "cross-platform/sdl-net-headers.h"
#include <string>

namespace Maxx {

class Connection;

class Server
{
public:
  Server(int port = 15355);
  ~Server();

  void Update();
  
  bool Send(const std::string& s);

private:
  TCPsocket listenSocket_;
  Connection* connection_;
};

} // namespace Maxx
#endif