#include "file-cache.h"
#include "cross-platform/file-sys.h"
#include "utils/trace.h"

namespace Maxx {
  
FileCache::FileCache()
{
}

FileCache::~FileCache()
{
}

bool FileCache::ReadFile(const std::string& fileName, std::string& fileContents)
{
  FileNameToContents::iterator i = fileNameToContents_.find(fileName);
  if (i != fileNameToContents_.end()) 
  {
    fileContents = i->second;
    //Trace("cached %s\n", fileName.c_str());
    return true;
  }
  bool readOk = ::ReadFile(fileName, fileContents);
  if (!readOk)
    return false;
  fileNameToContents_.insert(std::make_pair(fileName, fileContents));
  return true;
}

} // Maxx
