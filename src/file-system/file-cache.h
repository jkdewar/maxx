#pragma once

#include <string>
#include <map>

namespace Maxx {

class FileCache
{
public:
  FileCache();
  ~FileCache();
  
  bool ReadFile(const std::string& fileName, std::string& fileContents);
  
private:
  typedef std::map<std::string, std::string> FileNameToContents;
  FileNameToContents fileNameToContents_;
};

} // Maxx
