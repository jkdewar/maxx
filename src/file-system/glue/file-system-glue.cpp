#include "file-system/glue/file-system-glue.h"
#include "cross-platform/file-sys.h"

namespace Maxx {
namespace Glue {
  
//--------------------------------------------------------------------
// returns saveFolder
int FileSystem_getSaveFolder(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 0);
  std::string saveFolder = GetSaveFolder();
  lua_pushstring(L, saveFolder.c_str());
  return 1;
}


//--------------------------------------------------------------------
int luaopen_FileSystem(lua_State* L)
{
  static luaL_Reg table[] =
  {
   {"getSaveFolder",FileSystem_getSaveFolder},
   {0, 0}
  };
  Maxx::Glue::RegisterType<FileSystem_DummyClass>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}

} // Glue
} // Maxx
