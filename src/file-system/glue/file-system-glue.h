#pragma once

#include "engine/glue.h"

namespace Maxx {
namespace Glue {

int luaopen_FileSystem(lua_State* L);

class FileSystem_DummyClass{};

} // Glue
} // Maxx
