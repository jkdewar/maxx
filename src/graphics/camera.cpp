#include "graphics/camera.h"
#include "graphics/display.h"
#include "engine/maxx.h"
#include "cross-platform/opengl-headers.h"
#include "math/math.h"

namespace Maxx {

//--------------------------------------------------------------------
Camera::Camera()
  : x_(0.0)
  , y_(0.0)
  , rotation_(0.0)
  , scaleX_(1.0)
  , scaleY_(1.0)
{
}

//--------------------------------------------------------------------
Camera::~Camera()
{
}

//--------------------------------------------------------------------
void Camera::SetOpenGlMatrix()
{
  Display* display = Maxx::Get()->GetDisplay();
  double ssx = display->GetScreenSizeX();
  double ssy = display->GetScreenSizeY();
  glTranslatef((float)ssx/2.0, (float)ssy/2.0, 0.0f);
  glScalef((float)scaleX_, (float)scaleY_, 0.0f);  
  glTranslatef((float)(-x_), (float)(-y_), 0.0);
  glTranslatef((float)(+x_), (float)(+y_), 0.0);
  glRotatef((float)Math::RadiansToDegrees(rotation_), 0.0f, 0.0f, 1.0f);
  glTranslatef((float)(-x_), (float)(-y_), 0.0);
}

} // Maxx
