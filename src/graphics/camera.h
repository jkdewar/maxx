#pragma once

namespace Maxx {

//--------------------------------------------------------------------
class Camera
{
public:
  Camera();
  ~Camera();
  
  double GetX() const { return x_; }
  double GetY() const { return y_; }
  void MoveBy(double dx, double dy) { x_ += dx; y_ += dy; }
  void MoveTo(double x, double y) { x_ = x; y_ = y; }

  double GetRotation() const { return rotation_; }
  void SetRotation(double rotation) { rotation_ = rotation; }
  void RotateBy(double amount) { rotation_ += amount; }
  
  double GetScaleX() const { return scaleX_; }
  void SetScaleX(double sx) { scaleX_ = sx; }
  double GetScaleY() const { return scaleY_; }
  void SetScaleY(double sy) { scaleY_ = sy; }
  
  void SetOpenGlMatrix();
  
private:
  double x_, y_; // center of the screen in world coords
  double rotation_;
  double scaleX_, scaleY_;
};

} // Maxx
