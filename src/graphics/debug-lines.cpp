#include "graphics/debug-lines.h"
#include "graphics/camera.h"
#include "graphics/graphics.h"
#include "utils/assert.h"
#include "engine/maxx.h"

namespace Maxx {

//--------------------------------------------------------------------
DebugLines::DebugLines()
  : lineList_()
  , finished_(false)
  , verts_(0)
  , colors_(0)
  , totalVerts_(0)
{
}

//--------------------------------------------------------------------
DebugLines::~DebugLines()
{
  delete [] verts_; verts_ = 0;
  delete [] colors_; colors_ = 0;
}

//--------------------------------------------------------------------
void DebugLines::AddLine(double x1, double y1, double x2, double y2, double r, double g, double b, double a)
{
  Line line;
  line.p1.x = x1;
  line.p1.y = y1;
  line.p1.r = r;
  line.p1.g = g;
  line.p1.b = b;
  line.p1.a = a;
  line.p2.x = x2;
  line.p2.y = y2;
  line.p2.r = r;
  line.p2.g = g;
  line.p2.b = b;
  line.p2.a = a;
  lineList_.push_back(line);
}

//--------------------------------------------------------------------
void DebugLines::AddBox(double x1, double y1, double x2, double y2, double r, double g, double b, double a)
{
  AddLine(x1, y1, x2, y1, r,g,b,a);
  AddLine(x2, y1, x2, y2, r,g,b,a);
  AddLine(x2, y2, x1, y2, r,g,b,a);
  AddLine(x1, y2, x1, y1, r,g,b,a);
}

//--------------------------------------------------------------------
void DebugLines::Finish()
{
  Assert(!finished_);
  if (finished_)
    return;
  finished_ = true;
  
  totalVerts_ = (int)lineList_.size() * 2; // p1,p2
  delete [] verts_; verts_ = 0;
  delete [] colors_; colors_ = 0;
  verts_ = new GLfloat [totalVerts_ * 2]; // x,y
  colors_ = new GLfloat [totalVerts_ * 4]; // r,g,b,a
  GLfloat* vptr = verts_;
  GLfloat* cptr = colors_;
    
  for (LineList::const_iterator i = lineList_.begin(); i != lineList_.end(); ++i)
  {
    const Line& l = *i;

    *vptr++ = (GLfloat)l.p1.x;
    *vptr++ = (GLfloat)l.p1.y;
    *cptr++ = (GLfloat)l.p1.r;
    *cptr++ = (GLfloat)l.p1.g;
    *cptr++ = (GLfloat)l.p1.b;
    *cptr++ = (GLfloat)l.p1.a;

    *vptr++ = (GLfloat)l.p2.x;
    *vptr++ = (GLfloat)l.p2.y;
    *cptr++ = (GLfloat)l.p2.r;
    *cptr++ = (GLfloat)l.p2.g;
    *cptr++ = (GLfloat)l.p2.b;
    *cptr++ = (GLfloat)l.p2.a;    
  }
}

//--------------------------------------------------------------------
void DebugLines::Draw() const
{
  Assert(finished_);
  if (!finished_)
    return;
  if (totalVerts_ == 0)
    return;
  
  Camera& camera = Maxx::Get()->GetGraphics()->GetCamera();
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  camera.SetOpenGlMatrix();
    
  glDisable(GL_TEXTURE_2D);
  glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
  glEnableClientState(GL_VERTEX_ARRAY);
  glVertexPointer(2, GL_FLOAT, 0, verts_);
  glEnableClientState(GL_COLOR_ARRAY);
  glColorPointer(4, GL_FLOAT, 0, colors_);
  glDrawArrays(GL_LINES, 0, totalVerts_);
  glDisableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);
  glEnable(GL_TEXTURE_2D);
  
  glPopMatrix();
}

} // Maxx
