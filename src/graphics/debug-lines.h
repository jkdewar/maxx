#pragma once

#include "cross-platform/opengl-headers.h"
#include <vector>

namespace Maxx {

class DebugLines
{
public:
  DebugLines();
  ~DebugLines();

  void AddLine(double x1, double y1, double x2, double y2, double r = 1.0, double g = 1.0, double b = 1.0, double a = 1.0);
  void AddBox(double x1, double y1, double x2, double y2, double r = 1.0, double g = 1.0, double b = 1.0, double a = 1.0);
  void Finish();
  void Draw() const;
  
private:
  struct Point
  {
    double x, y;
    double r, g, b, a;
  };
  struct Line
  {
    Point p1;
    Point p2;
  };
  
  typedef std::vector<Line> LineList;
  LineList lineList_;
  bool finished_;
  GLfloat* verts_;
  GLfloat* colors_;
  int totalVerts_;
};

} // Maxx
