#if defined(__IPHONEOS__)
#include "graphics/display.h"
#import <UIKit/UIKit.h>

namespace Maxx {

//--------------------------------------------------------------------
double Display::GetScreenScale() const
{
  if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)])
  {
    return [UIScreen mainScreen].scale;
  }
  return 1.0;
}

} // Maxx
#endif
