#include "graphics/display.h"
#include "graphics/image.h"
#include "graphics/graphics.h"
#include "cross-platform/sdl-headers.h"
#include "cross-platform/opengl-headers.h"
#include "math/math.h"
#include "utils/assert.h"
#include "engine/maxx.h"
#include <iostream>
#include <algorithm>

using namespace Maxx::Math;

namespace Maxx {

//--------------------------------------------------------------------
Display* Display::Create()
{
  bool success = false;
  Display* renderSys = new Display(success);
  if (!success)
  {
    delete renderSys; renderSys = 0;
  }
  return renderSys;
}

//--------------------------------------------------------------------
bool Display::SetGraphicsMode(int sizeX, int sizeY, int windowSizeX, int windowSizeY, bool fullscreen)
{
  Maxx::Get()->GetGraphics()->GetImageManager().FreeTexturesBeforeReload();

#if defined(__IPHONEOS__)
  windowSizeX = 0;
  windowSizeY = 0;
  fullscreen = false;
#endif
    
  if (windowSizeX == 0 || windowSizeY == 0)
  {
    SDL_DisplayMode mode;
    SDL_GetDisplayMode(0, 0, &mode);
    windowSizeX = mode.w;
    windowSizeY = mode.h;
#if defined(__IPHONEOS__)
    if (sizeX < sizeY)
      std::swap(windowSizeX, windowSizeY);
#endif
  }
  
  Trace("SetGraphicsMode wsx=%d wsy=%d\n",windowSizeX,windowSizeY);
  mScreenSizeX = sizeX;
  mScreenSizeY = sizeY;
  mActualScreenSizeX = windowSizeX;
  mActualScreenSizeY = windowSizeY;
  mFullscreen = fullscreen;
  
#if defined(__IPHONEOS__)
  double realAspect = (double)mActualScreenSizeX / (double)mActualScreenSizeY;
  mScreenSizeX = mScreenSizeY * realAspect;
#endif
   
  // Don't set color bit sizes (SDL_GL_RED_SIZE, etc)
  //  Mac OS X will always use 8-8-8-8 ARGB for 32-bit screens and
  //  5-5-5 RGB for 16-bit screens

  // Request a 16-bit depth buffer (without this, there is no depth buffer)
  //SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 0);

  // Request double-buffered OpenGL
  //int doubleBuffer = 1;
  //SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, doubleBuffer);

  Uint32 flags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;

  if (fullscreen)
    flags |= SDL_WINDOW_FULLSCREEN;

#if defined (__IPHONEOS__)
  flags |= SDL_WINDOW_BORDERLESS;
#endif

  // use OpenGL ES 1
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 1);
  
  if (glcontext_)
  {
    SDL_GL_DeleteContext(glcontext_);
    glcontext_ = 0;
  }
  if (window_)
  {
    SDL_DestroyWindow(window_);
    window_ = 0;
  }
  
  int mungedX = mActualScreenSizeX;
  int mungedY = mActualScreenSizeY;
#if defined(__IPHONEOS__)
  //std::swap(mungedX, mungedY);
#endif
  window_ = SDL_CreateWindow("", 
                             SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
                             mungedX, mungedY,
                             flags);
  if (!window_)
    return false;
    
  glcontext_ = SDL_GL_CreateContext(window_);
  if (!glcontext_)
    return false;
  
  int result = SDL_GL_MakeCurrent(window_, glcontext_);
  Assert(result == 0);
  
  // Enable vsync
  SDL_GL_SetSwapInterval(1);  
  
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

  glDisable(GL_DEPTH_TEST);
  
  glMatrixMode(GL_MODELVIEW);

  Assert(glGetError() == GL_NO_ERROR);
  
  Maxx::Get()->GetGraphics()->GetImageManager().ReloadTextures();

  return true;
}

#if !defined(__IPHONEOS__)
//--------------------------------------------------------------------
double Display::GetScreenScale() const
{
  return 1.0;
}
#endif

//--------------------------------------------------------------------
void Display::SetClearColor(double r, double g, double b)
{
  mClearR = r;
  mClearG = g;
  mClearB = b;
}

//--------------------------------------------------------------------
void Display::SetBorderColor(double r, double g, double b)
{
  mBorderR = r;
  mBorderG = g;
  mBorderB = b;
}

//--------------------------------------------------------------------
void Display::Clear()
{
#if !defined(__IPHONEOS__)
  glClearColor((GLclampf)mClearR, (GLclampf)mClearG, (GLclampf)mClearB, 0.0f);
  glClear(/*GL_DEPTH_BUFFER_BIT |*/ GL_COLOR_BUFFER_BIT);
#endif

  // calc viewport
  mViewportScale = Min((double)mActualScreenSizeX/mScreenSizeX, (double)mActualScreenSizeY/mScreenSizeY);
  mViewportX = (mActualScreenSizeX - mScreenSizeX*mViewportScale) / 2.0;
  mViewportY = (mActualScreenSizeY - mScreenSizeY*mViewportScale) / 2.0;
  mViewportSizeX = mScreenSizeX*mViewportScale;
  mViewportSizeY = mScreenSizeY*mViewportScale;
  
 #if !defined(__IPHONEOS__)
   // draw border around viewport
   glViewport(0, 0, mActualScreenSizeX, mActualScreenSizeY);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
   glOrtho(0., static_cast<GLdouble>(mActualScreenSizeX), static_cast<GLdouble>(mActualScreenSizeY), 0., -1., 1.);
   glColor4f(mBorderR, mBorderG, mBorderB, 1.0);
   //glColor4f(124.f/255.f, 112.f/255.f, 238.f/255.f, 1.0f); // c64 border colour
   //glColor4f(0.0f, 0.0f, 0.0f, 1.0f); // black border
   glDisable(GL_TEXTURE_2D);
   glBegin(GL_QUADS);
   // top quad
   glVertex2i(0, 0);
   glVertex2i(mActualScreenSizeX, 0);
   glVertex2i(mActualScreenSizeX, mViewportY);
   glVertex2i(0, mViewportY);
   // bottom quad
   glVertex2i(0, mActualScreenSizeY);
   glVertex2i(0, mViewportY + mViewportSizeY);
   glVertex2i(mActualScreenSizeX, mViewportY + mViewportSizeY);
   glVertex2i(mActualScreenSizeX, mActualScreenSizeY);
   // left quad
   glVertex2i(0, mViewportY);
   glVertex2i(mViewportX, mViewportY);
   glVertex2i(mViewportX, mActualScreenSizeY);
   glVertex2i(0, mActualScreenSizeY);
   // right quad
   glVertex2i(mActualScreenSizeX, mActualScreenSizeY);
   glVertex2i(mViewportX + mViewportSizeX, mActualScreenSizeY);
   glVertex2i(mViewportX + mViewportSizeX, 0);
   glVertex2i(mActualScreenSizeX, 0);
   glEnd();
   Assert(glGetError() == GL_NO_ERROR);
 #endif
  
  if (IsIosLandscape())
    glViewport(0, 0, mActualScreenSizeX, mActualScreenSizeY);
  else
    glViewport(mViewportX, mViewportY, mViewportSizeX, mViewportSizeY);
  
  // Set up projection matrix.
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  // (0, 0) is top-left
  // coordinates are in pixels
  if (IsIosLandscape())
    glOrthof(0.0, (GLfloat)mScreenSizeY, (GLfloat)mScreenSizeX, 0.0, -1., 1.);
  else
  {
#if defined(__LINUX__)
    glOrtho(0.0, (GLfloat)mScreenSizeX, (GLfloat)mScreenSizeY, 0.0, -1., 1.);
#else
    glOrthof(0.0, (GLfloat)mScreenSizeX, (GLfloat)mScreenSizeY, 0.0, -1., 1.);
#endif
  }
  //// (0, 0) is center
  // coordinates are in pixels
  //GLfloat hsx = (GLfloat)mActualScreenSizeX / 2.0;
  //GLfloat hsy = (GLfloat)mActualScreenSizeY / 2.0;
  //glOrthof(-hsx, +hsx, +hsy, -hsy, -1.0, 1.0);
  
  glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
  
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  if (IsIosLandscape())
  {
    glRotatef(90.0, 0.0, 0.0, 1.0);
    glTranslatef(0, -GetScreenSizeY(), 0.0);
  }
}

//--------------------------------------------------------------------
void Display::Flip()
{
  SDL_GL_SwapWindow(window_);
}

//--------------------------------------------------------------------
Display::Display(bool& success)
  : window_(0)
  , glcontext_(0)
  , mScreenSizeX(0)
  , mScreenSizeY(0)
  , mActualScreenSizeX(0)
  , mActualScreenSizeY(0)
	, mFullscreen(false)
  , mTextureHandle(-1)
  , mClearR(0.0f), mClearG(0.0f), mClearB(0.0f)
  , mBorderR(0.0f), mBorderG(0.0f), mBorderB(0.0f)
  , mViewportX(0), mViewportY(0), mViewportSizeX(0), mViewportSizeY(0), mViewportScale(0)
{
  int sdlInitResult = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER);
  if (sdlInitResult < 0)
  {
		std::cout << SDL_GetError() << std::endl;
    success = false;
    return;
  }
  //int ttfInitResult = TTF_Init();
  //if (ttfInitResult < 0)
  //{
  //  success = false;
  //  return;
  //}
  success = true;
}

//--------------------------------------------------------------------
Display::~Display()
{
  if (glcontext_)
  {
    SDL_GL_DeleteContext(glcontext_);
    glcontext_ = 0;
  }
  if (window_)
  {
    SDL_DestroyWindow(window_);
    window_ = 0;
  }
  //TTF_Quit();
}

//--------------------------------------------------------------------
void Display::SetMouseGrab(bool on)
{
#if !defined(__MACOSX__)
  SDL_SetWindowGrab(window_, on?SDL_TRUE:SDL_FALSE);
#endif
}

//--------------------------------------------------------------------
void Display::ConvertMouseCoords(int& mouseX, int& mouseY)
{
  mouseX -= mViewportX;
  mouseY -= mViewportY;
  mouseX /= mViewportScale;
  mouseY /= mViewportScale;
}

////--------------------------------------------------------------------
//Image* Display::TakeScreenshot() const
//{
//  SDL_Surface* surface = 0;
//  unsigned char* pixels = 0;
//  TakeScreenshot(surface, pixels);   
//  Image* image = new Image(surface);
//  SDL_FreeSurface(surface);
//  surface = 0;
//  delete [] pixels;
//  pixels = 0;
//  return image;
//}
//
////--------------------------------------------------------------------
//void Display::TakeScreenshot(SDL_Surface*& surface, unsigned char*& pixels) const
//{
//  int sx = GetScreenSizeX();
//  int sy = GetScreenSizeY();
//  pixels = new unsigned char [sx * sy * 4];
//  glReadBuffer(GL_BACK);
//  Assert(glGetError() == GL_NO_ERROR);
//  glReadPixels(0, 0, sx, sy, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
//  Assert(glGetError() == GL_NO_ERROR);
//#if SDL_BYTEORDER == SDL_BIG_ENDIAN
//  Uint32 rmask = 0xff000000;
//  Uint32 gmask = 0x00ff0000;
//  Uint32 bmask = 0x0000ff00;
//  Uint32 amask = 0x000000ff;
//#else
//  Uint32 rmask = 0x000000ff;
//  Uint32 gmask = 0x0000ff00;
//  Uint32 bmask = 0x00ff0000;
//  Uint32 amask = 0xff000000;
//#endif
//  surface = SDL_CreateRGBSurfaceFrom(pixels, sx, sy, 32, sx*4, rmask, gmask, bmask, amask);
//}

} // Maxx
