#pragma once

#include "cross-platform/sdl-headers.h"
#include "cross-platform/opengl-headers.h"

namespace Maxx {

class Image;

class Display
{
public:
  static Display* Create();
  ~Display();

  bool SetGraphicsMode(int sizeX, int sizeY, int windowSizeX, int windowSizeY, bool fullscreen);

  int GetScreenSizeX() const { return mScreenSizeX; }
  int GetScreenSizeY() const { return mScreenSizeY; }
  int GetActualScreenSizeX() const { return mActualScreenSizeX; }
  int GetActualScreenSizeY() const { return mActualScreenSizeY; }
  double GetScreenScale() const;
  bool IsFullscreen() const { return mFullscreen; }
  
  bool IsIosLandscape() const
  { 
#if defined(__IPHONEOS__)
    return (mScreenSizeX > mScreenSizeY);
#else
    return false;
#endif
  }
  
  void SetClearColor(double r, double g, double b);
  void SetBorderColor(double r, double g, double b);
  void Clear();
  void Flip();
  
  void SetMouseGrab(bool on);
  void ConvertMouseCoords(int& mouseX, int& mouseY);

  SDL_Window* GetSdlWindow() const { return window_; }
  
  //Image* TakeScreenshot() const;
  //void TakeScreenshot(SDL_Surface*& surface, unsigned char*& pixels) const;
  
private:
  Display(bool& success);

  SDL_Window* window_;
  SDL_GLContext glcontext_;
  int mScreenSizeX, mScreenSizeY;
  int mActualScreenSizeX, mActualScreenSizeY;
  bool mFullscreen;
  GLuint mTextureHandle;
  double mClearR, mClearG, mClearB;
  double mBorderR, mBorderG, mBorderB;
  int mViewportX, mViewportY, mViewportSizeX, mViewportSizeY;
  double mViewportScale;
};

} // Maxx
