#include "graphics/draw-image-params.h"
#include "graphics/image.h"
#include "utils/trace.h"

namespace Maxx {

//----------------------------------------------------------------------
// This is probably the most speed-critical code in the engine. Careful!
/*inline*/ void DrawImageParams::CalcBuffers()
{
  const Image& image = *image_;
  DrawImageParams& params = *this;
  
  const int numVertices = 4;
  
  if (dirty_)
  {
    verts[0].pos.x = -0.5;
    verts[0].pos.y = -0.5;
    verts[1].pos.x = +0.5;
    verts[1].pos.y = -0.5; 
    verts[2].pos.x = +0.5;
    verts[2].pos.y = +0.5;
    verts[3].pos.x = -0.5;
    verts[3].pos.y = +0.5;
    
    const ImageSheet* imageSheet = params.GetImageSheet();
    if (imageSheet)
    {
      bool foundImageInSheet = imageSheet->GetUvs(params.GetSheetImageName(), params);
      if (!foundImageInSheet)
        Trace("ERROR: couldn't find image '%s' in sheet\n", params.GetSheetImageName().c_str());
    }
    
    const double su = image.GetScaleU();
    const double sv = image.GetScaleV();
    if (params.flipU)
    {
      verts[0].u = params.u2*su;
      verts[1].u = params.u1*su;
      verts[2].u = params.u4*su;
      verts[3].u = params.u3*su;
    }
    else
    {
      verts[0].u = params.u1*su;
      verts[1].u = params.u2*su;
      verts[2].u = params.u3*su;
      verts[3].u = params.u4*su;
    }
    if (params.flipV)
    {
      verts[0].v = params.v4*sv;
      verts[1].v = params.v3*sv;
      verts[2].v = params.v2*sv;
      verts[3].v = params.v1*sv;
    }
    else
    {
      verts[0].v = params.v1*sv;
      verts[1].v = params.v2*sv;
      verts[2].v = params.v3*sv;
      verts[3].v = params.v4*sv;
    }
    
    //if (params.offsetU != 0)
    {
      verts[0].u += params.offsetU;
      verts[1].u += params.offsetU;
      verts[2].u += params.offsetU;
      verts[3].u += params.offsetU;
    }
    //if (params.offsetV != 0)
    {
      verts[0].v += params.offsetV;
      verts[1].v += params.offsetV;
      verts[2].v += params.offsetV;
      verts[3].v += params.offsetV;
    }
    
    double sizeX,sizeY;
    if (imageSheet_)
    {
      bool ok = imageSheet_->GetSize(sheetImageName_, sizeX, sizeY);
      if (!ok)
        Trace("ERROR: getting size of image %s in sheet\n", sheetImageName_.c_str());
    }
    else
    {
      sizeX = image_->GetSizeX();
      sizeY = image_->GetSizeY();
    }
    
//    Matrix t1 = Matrix::Translate(-params.anchorX * sizeX/2.0 *params.scaleX - params.offsetX,
//                                  -params.anchorY * sizeY/2.0 *params.scaleY - params.offsetY);  
//    Matrix r = Matrix::Rotate(params.rotation);
//    Matrix s = Matrix::Scale(params.scaleX * sizeX, params.scaleY * sizeY);
//    matrix_ = r * t1 * s;
    /*
     R = [c,-s,0,
          s,c,0,
          0,0,1]
     
     T = [1,0,tx,
          0,1,ty,
          0,0,1]
     
     S = [sx,0,0,
          0,sy,0,
          0,0,1]
     
     T * S = [1,0,tx, * [sx,0,0, = [sx,0,tx,
              0,1,ty,    0,sy,0,    0,sy,ty,
              0,0,1]     0,0,1]     0,0,1]
     
     R * (T * S) = [c,-s,0, * [sx,0,tx, = [c*sx, -s*sy, c*tx-s*ty,
                    s,c,0,     0,sy,ty,    s*sx, c*sy,  s*tx+c*ty,
                    0,0,1]     0,0,1]      0,    0,     1         ]
     */
    {
      const double tx = -params.anchorX * sizeX/2.0 *params.scaleX - params.offsetX;
      const double ty = -params.anchorY * sizeY/2.0 *params.scaleY - params.offsetY;
      const double c = Math::Cos(params.rotation);
      const double s = Math::Sin(params.rotation);
      const double sx = params.scaleX * sizeX;
      const double sy = params.scaleY * sizeY;
      matrix_.m[0] = c*sx;
      matrix_.m[1] = -s*sy;
      matrix_.m[2] = c*tx-s*ty;
      matrix_.m[3] = s*sx;
      matrix_.m[4] = c*sy;
      matrix_.m[5] = s*tx+c*ty;
      matrix_.m[6] = 0;
      matrix_.m[7] = 0;
      matrix_.m[8] = 1;
    }
    dirty_ = false;
    
    // transform positions
    for (int i = 0; i < numVertices; ++i)
    {
      //verts[i].pos = matrix_ * verts[i].pos;
      matrix_.Transform(verts[i].pos);
    }
  }
  
  GLfloat* vptr = verts_;
  GLfloat* tptr = uvs_;
  GLubyte* cptr = colors_;
  
#define AWESOME(i)                      \
*vptr++ = (GLfloat)verts[i].pos.x + x;  \
*vptr++ = (GLfloat)verts[i].pos.y + y;  \
*tptr++ = (GLfloat)verts[i].u;          \
*tptr++ = (GLfloat)verts[i].v;          \
*cptr++ = (GLubyte)(params.r*255);      \
*cptr++ = (GLubyte)(params.g*255);      \
*cptr++ = (GLubyte)(params.b*255);      \
*cptr++ = (GLubyte)(params.alpha*255);  \

  AWESOME(0)
  AWESOME(1)
  AWESOME(2)
  AWESOME(2)
  AWESOME(3)
  AWESOME(0)
  
#undef AWESOME
}

} // Maxx
