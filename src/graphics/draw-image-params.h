#pragma once
#include "cross-platform/opengl-headers.h"
#include "math/matrix.h"
#include "graphics/image-sheet.h"
#include "graphics/image.h"
#include "math/vector.h"
#include <string>

namespace Maxx {
  
class Image;
class ImageSheet;

//----------------------------------------------------------------------
class DrawImageParams
{
public:
  DrawImageParams()
    : image_(0)
    , imageSheet_(0)
    , sheetImageName_()
    , x(0.0), y(0.0)
    , anchorX(0.0), anchorY(0.0)
    , offsetX(0.0), offsetY(0.0)
    , scaleX(1.0), scaleY(1.0)
    , rotation(0.0)
    , u1(0.0), v1(0.0)
    , u2(1.0), v2(0.0)
    , u3(1.0), v3(1.0)
    , u4(0.0), v4(1.0)
    , flipU(false), flipV(false)
    , offsetU(0.0), offsetV(0.0)
    , r(1.0), g(1.0), b(1.0), alpha(1.0)
    , filter(true)
    , layer()
    , layerZ(0.0)
    , effect()
    , dirty_(true)
  {
  }
  
  const Image* GetImage() const { return image_; }
  const ImageSheet* GetImageSheet() const { return imageSheet_; }
  const std::string& GetSheetImageName() const { return sheetImageName_; }
  double GetX() const { return x; }
  double GetY() const { return y; }
  double GetAnchorX() const { return anchorX; }
  double GetAnchorY() const { return anchorY; }
  double GetOffsetX() const { return offsetX; }
  double GetOffsetY() const { return offsetY; }
  double GetScaleX() const { return scaleX; }
  double GetScaleY() const { return scaleY; }
  double GetRotation() const { return rotation; }
  bool GetFlipU() const { return flipU; }
  bool GetFlipV() const { return flipV; }
  double GetOffsetU() const { return offsetU; }
  double GetOffsetV() const { return offsetV; }
  double GetR() const { return r; }
  double GetG() const { return g; }
  double GetB() const { return b; }
  double GetAlpha() const { return alpha; }
  bool GetFilter() const { return filter; }
  const std::string& GetLayer() const { return layer; }
  double GetLayerZ() const { return layerZ; }
  const std::string& GetEffect() const { return effect; }
  
  void SetImage(const Image* i)           { if(image_==i)return; image_=i; imageSheet_=0; dirty_=true; }
  void SetImageSheet(const ImageSheet* i) { if(imageSheet_==i)return; imageSheet_=i; image_=i?i->GetImage():0; dirty_=true; }
  void SetSheetImageName(const std::string& v) { if(sheetImageName_==v)return; sheetImageName_=v; dirty_=true; }
  void SetX(double v) { x=v; }
  void SetY(double v) { y=v; }
  void SetAnchorX(double v) { if (anchorX!=v) {anchorX=v; dirty_=true;} }
  void SetAnchorY(double v) { if (anchorY!=v) {anchorY=v; dirty_=true;} }
  void SetOffsetX(double v) { if (offsetX!=v) {offsetX=v; dirty_=true;} }
  void SetOffsetY(double v) { if (offsetY!=v) {offsetY=v; dirty_=true;} }
  void SetScaleX(double v) { if (scaleX!=v) {scaleX=v; dirty_=true;} }
  void SetScaleY(double v) { if (scaleY!=v) {scaleY=v; dirty_=true;} }
  void SetRotation(double v) { if (rotation!=v) {rotation=v; dirty_=true;} }
  void SetFlipU(bool v) { if (flipU!=v) {flipU=v; dirty_=true;} }
  void SetFlipV(bool v) { if (flipV!=v) {flipV=v; dirty_=true;} }
  void SetOffsetU(double v) { if (offsetU!=v) {offsetU=v; dirty_=true;} }
  void SetOffsetV(double v) { if (offsetV!=v) {offsetV=v; dirty_=true;} }
  void SetR(double v) { r=v; }
  void SetG(double v) { g=v; }
  void SetB(double v) { b=v; }
  void SetAlpha(double v) { alpha=v; }
  void SetFilter(bool v) { filter=v; }
  void SetLayer(const std::string& v) { layer=v; }
  void SetLayerZ(double v) { layerZ=v; }
  void SetEffect(const std::string& v) { effect=v; }

  void CalcBuffers();
  const GLfloat* GetVerts() const { return verts_; }
  const GLfloat* GetUvs() const { return uvs_; }
  const GLubyte* GetColors() const { return colors_; }
  
  bool IsDirty() const { return dirty_; }
  
private:
  struct Vertex
  {
    Vector pos;
    double u,v;
    GLubyte r,g,b,a;
  };
  
  friend class ImageSheet; // TODO (needed for GetUvs currently...)
  const Image* image_;
  const ImageSheet* imageSheet_;
  std::string sheetImageName_;
  double x, y;
  double anchorX, anchorY;
  double offsetX, offsetY;
  double scaleX, scaleY;
  double rotation; // in radians
  double u1,v1,u2,v2,u3,v3,u4,v4; // top-left, top-right, bottom-right, bottom-left
  bool flipU, flipV;
  double offsetU, offsetV;
  double r, g, b, alpha;
  bool filter;
  std::string layer;
  double layerZ;            // z value within layer
  std::string effect;
  
  // two triangles forming a rectangle
  // * 3 verts each
  // = 6 verts total
  enum { kNumVerts = 6 };

  Vertex verts[4];
  Matrix matrix_;
  GLfloat verts_[kNumVerts * 2];  // each vert has x,y
  GLfloat uvs_[kNumVerts * 2];    // each vert has u,v
  GLubyte colors_[kNumVerts * 4]; // each vert has r,g,b,a
  bool dirty_;
};
  

} // Maxx
