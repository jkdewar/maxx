#include "graphics/glue/camera-glue.h"
#include "graphics/camera.h"
#include "utils/assert.h"

namespace Maxx {
namespace Glue {

//--------------------------------------------------------------------
// self
// returns x
int Camera_getX(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Camera* camera = Maxx::Glue::GetUserData<Camera>(L, 1);
  GLUE_VERIFY(camera);
  double x = camera->GetX();
  lua_pushnumber(L, x);
  return 1;
}

//--------------------------------------------------------------------
// self
// returns y
int Camera_getY(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Camera* camera = Maxx::Glue::GetUserData<Camera>(L, 1);
  GLUE_VERIFY(camera);
  double y = camera->GetY();
  lua_pushnumber(L, y);
  return 1;
}

//--------------------------------------------------------------------
// self, dx, dy
int Camera_moveBy(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 3);
  Camera* camera = Maxx::Glue::GetUserData<Camera>(L, 1);
  GLUE_VERIFY(camera);
  double dx = lua_tonumber(L, 2);
  double dy = lua_tonumber(L, 3);
  camera->MoveBy(dx, dy);
  return 0;
}

//--------------------------------------------------------------------
// self, x, y
int Camera_moveTo(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 3);
  Camera* camera = Maxx::Glue::GetUserData<Camera>(L, 1);
  GLUE_VERIFY(camera);
  double x = lua_tonumber(L, 2);
  double y = lua_tonumber(L, 3);
  camera->MoveTo(x, y);
  return 0;
}

//--------------------------------------------------------------------
// self
// returns rotation
int Camera_getRotation(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Camera* camera = Maxx::Glue::GetUserData<Camera>(L, 1);
  GLUE_VERIFY(camera);
  double rotation = camera->GetRotation();
  lua_pushnumber(L, rotation);
  return 1;
}

//--------------------------------------------------------------------
// self, amount
int Camera_setRotation(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Camera* camera = Maxx::Glue::GetUserData<Camera>(L, 1);
  GLUE_VERIFY(camera);
  double rotation = lua_tonumber(L, 2);
  camera->SetRotation(rotation);
  return 0;
}

//--------------------------------------------------------------------
// self, amount
int Camera_rotateBy(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Camera* camera = Maxx::Glue::GetUserData<Camera>(L, 1);
  GLUE_VERIFY(camera);
  double amount = lua_tonumber(L, 2);
  camera->RotateBy(amount);
  return 0;
}

//--------------------------------------------------------------------
// self
// returns scaleX
int Camera_getScaleX(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Camera* camera = Maxx::Glue::GetUserData<Camera>(L, 1);
  GLUE_VERIFY(camera);
  double scaleX = camera->GetScaleX();
  lua_pushnumber(L, scaleX);
  return 1;
}

//--------------------------------------------------------------------
// self, scaleX
int Camera_setScaleX(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Camera* camera = Maxx::Glue::GetUserData<Camera>(L, 1);
  GLUE_VERIFY(camera);
  double scaleX = lua_tonumber(L, 2);
  camera->SetScaleX(scaleX);
  return 0;
}

//--------------------------------------------------------------------
// self
// returns scaleY
int Camera_getScaleY(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Camera* camera = Maxx::Glue::GetUserData<Camera>(L, 1);
  GLUE_VERIFY(camera);
  double scaleY = camera->GetScaleY();
  lua_pushnumber(L, scaleY);
  return 1;
}

//--------------------------------------------------------------------
// self, scaleY
int Camera_setScaleY(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Camera* camera = Maxx::Glue::GetUserData<Camera>(L, 1);
  GLUE_VERIFY(camera);
  double scaleY = lua_tonumber(L, 2);
  camera->SetScaleY(scaleY);
  return 0;
}

//--------------------------------------------------------------------
int luaopen_Camera(lua_State* L)
{
  static luaL_Reg table[] =
  {
    {"getX", Camera_getX},
    {"getY", Camera_getY},
    {"moveBy", Camera_moveBy},
    {"moveTo", Camera_moveTo},
    {"getRotation", Camera_getRotation},
    {"setRotation", Camera_setRotation},
    {"rotateBy", Camera_rotateBy},
    {"getScaleX", Camera_getScaleX},
    {"setScaleX", Camera_setScaleX},
    {"getScaleY", Camera_getScaleY},
    {"setScaleY", Camera_setScaleY},
    {0, 0},
  };
  Maxx::Glue::RegisterType<Camera>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}

} // Glue
} // Maxx
