#pragma once

#include "engine/glue.h"

namespace Maxx {
namespace Glue {

int luaopen_Camera(lua_State*);

} // Glue
} // Maxx
