#include "glue-display.h"
#include "graphics/display.h"
#include "graphics/image.h"
#include <string>

using namespace Maxx;

namespace Maxx {
namespace Glue {

//--------------------------------------------------------------------
int Display_new(lua_State* L)
{
  Display* renderSys = Display::Create();
  Maxx::Glue::NewUserData<Display>(L, renderSys, /*own=*/true);
  return 1;
}

//--------------------------------------------------------------------
int Display_setGraphicsMode(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 6);
  Display* renderSys = Maxx::Glue::GetUserData<Display>(L);
  GLUE_VERIFY(renderSys);
  int sizeX = (int)lua_tonumber(L, 2);
  int sizeY = (int)lua_tonumber(L, 3);
  int windowSizeX = (int)lua_tonumber(L, 4);
  int windowSizeY = (int)lua_tonumber(L, 5);
  bool fullscreen = lua_toboolean(L, 6) != 0;
  bool ok = renderSys->SetGraphicsMode(sizeX, sizeY, windowSizeX, windowSizeY, fullscreen);
  lua_pushboolean(L, ok);
  return 1;
}

//--------------------------------------------------------------------
int Display_getScreenSizeX(lua_State* L)
{
  Display* renderSys = Maxx::Glue::GetUserData<Display>(L, 1);
  GLUE_VERIFY(renderSys);
  lua_pushnumber(L, renderSys->GetScreenSizeX());
  return 1;
}

//--------------------------------------------------------------------
int Display_getScreenSizeY(lua_State* L)
{
  Display* renderSys = Maxx::Glue::GetUserData<Display>(L, 1);
  GLUE_VERIFY(renderSys);
  lua_pushnumber(L, renderSys->GetScreenSizeY());
  return 1;
}

//--------------------------------------------------------------------
int Display_getActualScreenSizeX(lua_State* L)
{
  Display* renderSys = Maxx::Glue::GetUserData<Display>(L, 1);
  GLUE_VERIFY(renderSys);
  lua_pushnumber(L, renderSys->GetActualScreenSizeX());
  return 1;
}

//--------------------------------------------------------------------
int Display_getActualScreenSizeY(lua_State* L)
{
  Display* renderSys = Maxx::Glue::GetUserData<Display>(L, 1);
  GLUE_VERIFY(renderSys);
  lua_pushnumber(L, renderSys->GetActualScreenSizeY());
  return 1;
}

//--------------------------------------------------------------------
int Display_isFullscreen(lua_State* L)
{
  Display* renderSys = Maxx::Glue::GetUserData<Display>(L, 1);
  GLUE_VERIFY(renderSys);
  lua_pushboolean(L, renderSys->IsFullscreen());
  return 1;
}

//--------------------------------------------------------------------
int Display_setClearColor(lua_State* L)
{
  Display* renderSys = Maxx::Glue::GetUserData<Display>(L, 1);
  GLUE_VERIFY(renderSys);
  double r = lua_tonumber(L, 2);
  double g = lua_tonumber(L, 3);
  double b = lua_tonumber(L, 4);
  renderSys->SetClearColor(r, g, b);
  return 0;
}

//--------------------------------------------------------------------
int Display_setBorderColor(lua_State* L)
{
  Display* renderSys = Maxx::Glue::GetUserData<Display>(L, 1);
  GLUE_VERIFY(renderSys);
  double r = lua_tonumber(L, 2);
  double g = lua_tonumber(L, 3);
  double b = lua_tonumber(L, 4);
  renderSys->SetBorderColor(r, g, b);
  return 0;
}

//--------------------------------------------------------------------
int Display_clear(lua_State* L)
{
  Display* renderSys = Maxx::Glue::GetUserData<Display>(L, 1);
  GLUE_VERIFY(renderSys);
  renderSys->Clear();
  return 0;
}

//--------------------------------------------------------------------
int Display_flip(lua_State* L)
{
  Display* renderSys = Maxx::Glue::GetUserData<Display>(L, 1);
  GLUE_VERIFY(renderSys);
  renderSys->Flip();
  return 0;
}

//--------------------------------------------------------------------
int Display_setMouseGrab(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Display* display = Maxx::Glue::GetUserData<Display>(L, 1);
  GLUE_VERIFY(display);
  bool grab = lua_toboolean(L, 2);
  display->SetMouseGrab(grab);
  return 0;
}

////--------------------------------------------------------------------
//// self, fileName
//int Display_saveScreenshot(lua_State* L)
//{
//  GLUE_VERIFY(lua_gettop(L) == 2);
//  Display* display = Maxx::Glue::GetUserData<Display>(L, 1);
//  GLUE_VERIFY(display);
//  GLUE_VERIFY(lua_isstring(L, 2));
//  const char* fileName = lua_tostring(L, 2);
//  Image* image = display->TakeScreenshot();
//  if (!image)
//  {
//    Trace("ERROR: TakeScreenShot failed\n");
//    return 0;
//  }
//  SDL_Surface* surface = image->GetSDLSurface();
//  png_save_surface((char*)fileName, surface);
//  delete image; image = 0;
//  return 0;
//}

//--------------------------------------------------------------------
int luaopen_Display(lua_State* L)
{
  static luaL_Reg table[] =
  {
    {"new", Display_new},
    {"setGraphicsMode", Display_setGraphicsMode},
    {"getScreenSizeX", Display_getScreenSizeX},
    {"getScreenSizeY", Display_getScreenSizeY},
    {"getActualScreenSizeX", Display_getActualScreenSizeX},
    {"getActualScreenSizeY", Display_getActualScreenSizeY},
    {"isFullscreen", Display_isFullscreen},
    {"setClearColor", Display_setClearColor},
    {"setBorderColor", Display_setBorderColor},
    {"clear", Display_clear},
    {"flip", Display_flip},
    {"setMouseGrab", Display_setMouseGrab},
    //{"saveScreenshot", Display_saveScreenshot},
    {0, 0}
  };
  Maxx::Glue::RegisterType<Display>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}

} // Glue
} // Maxx
