#pragma once

#include "engine/glue.h"

namespace Maxx {
namespace Glue {

int luaopen_Display(lua_State*);

} // Glue
} // Maxx
