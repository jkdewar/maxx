#include "glue-graphics.h"
#include "graphics/image.h"
#include "graphics/debug-lines.h"
#include "graphics/hsb.h"
#include "engine/maxx.h"
#include <string>

namespace Maxx {
namespace Glue {

//--------------------------------------------------------------------
// self, layer
int Graphics_addLayer(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  GLUE_VERIFY(lua_isstring(L, 1));
  std::string layer = lua_tostring(L, 1);
  GLUE_VERIFY(lua_isboolean(L, 2));
  bool scrollable = lua_toboolean(L, 2) != 0;
  Maxx::Get()->GetGraphics()->AddLayer(layer, scrollable);
  return 0;
}

//--------------------------------------------------------------------
// x1,y1, x2,y2, (r,g,b,a)
int Graphics_drawLine(lua_State* L)
{
  int numParams = lua_gettop(L);
  GLUE_VERIFY(numParams >= 4 && numParams <= 8);
  GLUE_VERIFY(lua_isnumber(L, 1));
  GLUE_VERIFY(lua_isnumber(L, 2));
  GLUE_VERIFY(lua_isnumber(L, 3));
  GLUE_VERIFY(lua_isnumber(L, 4));
  double x1 = lua_tonumber(L, 1);
  double y1 = lua_tonumber(L, 2);
  double x2 = lua_tonumber(L, 3);
  double y2 = lua_tonumber(L, 4);
  double r = 1.0, g = 1.0, b = 1.0, a = 1.0;
  if (numParams >= 5) r = lua_tonumber(L, 5);
  if (numParams >= 6) g = lua_tonumber(L, 6);
  if (numParams >= 7) b = lua_tonumber(L, 7);
  if (numParams >= 8) a = lua_tonumber(L, 8);
  Maxx::Get()->GetGraphics()->GetDebugLines()->AddLine(x1,y1,x2,y2,r,g,b,a);
  return 0;
}

//--------------------------------------------------------------------
// x1,y1, x2,y2, (r,g,b,a)
int Graphics_drawBox(lua_State* L)
{
  int numParams = lua_gettop(L);
  GLUE_VERIFY(numParams >= 4 && numParams <= 8);
  GLUE_VERIFY(lua_isnumber(L, 1));
  GLUE_VERIFY(lua_isnumber(L, 2));
  GLUE_VERIFY(lua_isnumber(L, 3));
  GLUE_VERIFY(lua_isnumber(L, 4));
  double x1 = lua_tonumber(L, 1);
  double y1 = lua_tonumber(L, 2);
  double x2 = lua_tonumber(L, 3);
  double y2 = lua_tonumber(L, 4);
  double r = 1.0, g = 1.0, b = 1.0, a = 1.0;
  if (numParams >= 5) r = lua_tonumber(L, 5);
  if (numParams >= 6) g = lua_tonumber(L, 6);
  if (numParams >= 7) b = lua_tonumber(L, 7);
  if (numParams >= 8) a = lua_tonumber(L, 8);
  Maxx::Get()->GetGraphics()->GetDebugLines()->AddBox(x1,y1,x2,y2,r,g,b,a);
  return 0;
}

//--------------------------------------------------------------------
void Graphics_getDrawImageParamsFromLuaTable(lua_State* L, int index, DrawImageParams& params)
{
  int oldTop = lua_gettop(L);
  std::string s;
  Assert(lua_istable(L, index));
  lua_getfield(L, index, "x"); if (lua_isnumber(L, -1)) { params.SetX(lua_tonumber(L, -1)); }  
  lua_getfield(L, index, "y"); if (lua_isnumber(L, -1)) { params.SetY(lua_tonumber(L, -1)); }
  lua_getfield(L, index, "anchorX"); if (lua_isnumber(L, -1)) { params.SetAnchorX(lua_tonumber(L, -1)); }
  lua_getfield(L, index, "anchorY"); if (lua_isnumber(L, -1)) { params.SetAnchorY(lua_tonumber(L, -1)); }
  lua_getfield(L, index, "offsetX"); if (lua_isnumber(L, -1)) { params.SetOffsetX(lua_tonumber(L, -1)); }
  lua_getfield(L, index, "offsetY"); if (lua_isnumber(L, -1)) { params.SetOffsetY(lua_tonumber(L, -1)); }
  lua_getfield(L, index, "offsetU"); if (lua_isnumber(L, -1)) { params.SetOffsetU(lua_tonumber(L, -1)); }
  lua_getfield(L, index, "offsetV"); if (lua_isnumber(L, -1)) { params.SetOffsetV(lua_tonumber(L, -1)); }
  lua_getfield(L, index, "scaleX"); if (lua_isnumber(L, -1)) { params.SetScaleX(lua_tonumber(L, -1)); }
  lua_getfield(L, index, "scaleY"); if (lua_isnumber(L, -1)) { params.SetScaleY(lua_tonumber(L, -1)); }
  lua_getfield(L, index, "rotation"); if (lua_isnumber(L, -1)) { params.SetRotation(lua_tonumber(L, -1)); }
  lua_getfield(L, index, "flipU"); if (lua_isboolean(L, -1)) { params.SetFlipU(lua_toboolean(L, -1) != 0); }
  lua_getfield(L, index, "flipV"); if (lua_isboolean(L, -1)) { params.SetFlipV(lua_toboolean(L, -1) != 0); }
  lua_getfield(L, index, "r"); if (lua_isnumber(L, -1)) { params.SetR(lua_tonumber(L, -1)); }
  lua_getfield(L, index, "g"); if (lua_isnumber(L, -1)) { params.SetG(lua_tonumber(L, -1)); }
  lua_getfield(L, index, "b"); if (lua_isnumber(L, -1)) { params.SetB(lua_tonumber(L, -1)); }
  lua_getfield(L, index, "alpha"); if (lua_isnumber(L, -1)) { params.SetAlpha(lua_tonumber(L, -1)); }
  lua_getfield(L, index, "filter"); if (lua_isboolean(L, -1)) { params.SetFilter(lua_toboolean(L, -1) != 0); }
  lua_getfield(L, index, "layer"); if (lua_isstring(L, -1)) { params.SetLayer(lua_tostring(L, -1)); }
  lua_getfield(L, index, "effect"); if (lua_isstring(L, -1)) { params.SetEffect(lua_tostring(L, -1)); }
  lua_pop(L, lua_gettop(L) - oldTop);
}

//--------------------------------------------------------------------
int Graphics_drawImage(lua_State* L)
{
  Image* image = Glue::GetUserData<Image>(L, 1);
  GLUE_VERIFY(image);

  DrawImageParams* params = new DrawImageParams;
  params->SetImage(image);
  
  GLUE_VERIFY(lua_istable(L, 2));
  Graphics_getDrawImageParamsFromLuaTable(L, 2, *params);
  
  Maxx::Get()->GetGraphics()->DrawImage(params, /*deleteParamsForMe=*/true);
  
  return 0;
}

//--------------------------------------------------------------------
// h, s, b
// returns r, g, b
int Graphics_hsbToRgb(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 3);
  GLUE_VERIFY(lua_isnumber(L, 1));
  GLUE_VERIFY(lua_isnumber(L, 2));
  GLUE_VERIFY(lua_isnumber(L, 3));
  double h = lua_tonumber(L, 1);
  double s = lua_tonumber(L, 2);
  double B = lua_tonumber(L, 3);
  double r = 0.0, g = 0.0, b = 0.0;
  HsbToRgb(h,s,B, r,g,b);
  lua_pushnumber(L, r);
  lua_pushnumber(L, g);
  lua_pushnumber(L, b);
  return 3;
}

//--------------------------------------------------------------------
int luaopen_Graphics(lua_State* L)
{
  static luaL_Reg table[] =
  {
    {"addLayer", Graphics_addLayer},
    {"drawImage", Graphics_drawImage},
    {"drawLine", Graphics_drawLine},
    {"drawBox", Graphics_drawBox},
    {"hsbToRgb", Graphics_hsbToRgb},
    {0, 0}
  };
  Maxx::Glue::RegisterType<Graphics_DummyClass>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}

} // Glue
} // Maxx
