#pragma once

#include "engine/glue.h"
#include "graphics/graphics.h"

namespace Maxx {
namespace Glue {

int luaopen_Graphics(lua_State*);
void Graphics_getDrawImageParamsFromLuaTable(lua_State* L, int index, DrawImageParams& params);

class Graphics_DummyClass{};

} // Glue
} // Maxx
