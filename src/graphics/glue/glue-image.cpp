#include "glue-image.h"
#include "graphics/image.h"
#include "graphics/image-manager.h"
#include "graphics/graphics.h"
#include "engine/maxx.h"
#include <string>

using namespace Maxx;

namespace Maxx {
namespace Glue {


//--------------------------------------------------------------------
int Image_load(lua_State* L)
{
  std::string fileName = lua_tostring(L, 1);
  Image* image = (Image*)Maxx::Get()->GetGraphics()->GetImageManager().GetImage(fileName);
  if (!image)
  {
    // load failed
    lua_pushnil(L);
    return 1;
  }
  Maxx::Glue::NewUserData<Image>(L, image, /*own=*/false);
  return 1;
}

//--------------------------------------------------------------------
int Image_unload(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  GLUE_VERIFY(lua_isstring(L, 1));
  std::string fileName = lua_tostring(L, 1);
  Maxx::Get()->GetGraphics()->GetImageManager().UnloadImage(fileName);
  return 0;
}

//--------------------------------------------------------------------
int Image_getSizeX(lua_State* L)
{
  Image* image = Maxx::Glue::GetUserData<Image>(L);
  GLUE_VERIFY(image);
  int sizeX = image->GetSizeX();
  lua_pushnumber(L, sizeX);
  return 1;
}

//--------------------------------------------------------------------
int Image_getSizeY(lua_State* L)
{
  Image* image = Maxx::Glue::GetUserData<Image>(L);
  GLUE_VERIFY(image);
  int sizeY = image->GetSizeY();
  lua_pushnumber(L, sizeY);
  return 1;
}

//--------------------------------------------------------------------
int Image_getScaleU(lua_State* L)
{
  Image* image = Maxx::Glue::GetUserData<Image>(L);
  GLUE_VERIFY(image);
  double scaleU = image->GetScaleU();
  lua_pushnumber(L, scaleU);
  return 1;
}

//--------------------------------------------------------------------
int Image_getScaleV(lua_State* L)
{
  Image* image = Maxx::Glue::GetUserData<Image>(L);
  GLUE_VERIFY(image);
  double scaleV = image->GetScaleV();
  lua_pushnumber(L, scaleV);
  return 1;
}

//--------------------------------------------------------------------
int luaopen_Image(lua_State* L)
{
  static luaL_Reg table[] =
  {
    {"load", Image_load},
    {"unload", Image_unload},
    {"getSizeX", Image_getSizeX},
    {"getSizeY", Image_getSizeY},
    {"getScaleU", Image_getScaleU},
    {"getScaleV", Image_getScaleV},
    {0, 0}
  };
  Maxx::Glue::RegisterType<Image>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}

} // Glue
} // Maxx
