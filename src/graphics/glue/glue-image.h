#pragma once

#include "engine/glue.h"

namespace Maxx {
namespace Glue {

int luaopen_Image(lua_State*);

} // Glue
} // Maxx
