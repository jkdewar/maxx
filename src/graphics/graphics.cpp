#include "graphics/graphics.h"
#include "graphics/image.h"
#include "graphics/image-batcher.h"
#include "graphics/debug-lines.h"
#include "math/vector.h"
#include "math/matrix.h"
#include <vector>
#include "utils/assert.h"

namespace Maxx {
  
//--------------------------------------------------------------------
Graphics::Graphics()
  : imageBatcher_(0)
  , debugLines_(0)
{
}

//--------------------------------------------------------------------
Graphics::~Graphics()
{
  delete imageBatcher_; imageBatcher_ = 0;
  delete debugLines_; debugLines_ = 0;
  // for (ParamsList::iterator i = paramsToDelete_.begin(); i != paramsToDelete_.end(); ++i)
  //   delete *i;
}

static int imageCount = 0;

//--------------------------------------------------------------------
void Graphics::DrawImage(DrawImageParams* params, bool deleteParamsForMe)
{
  if (params->GetAlpha() == 0)
  {
    if (deleteParamsForMe)
      delete params;
    return;
  }
  ++imageCount;
  imageBatcher_->Add(*params);
  if (deleteParamsForMe)
    paramsToDelete_.push_back(params);
}

//--------------------------------------------------------------------
void Graphics::BeginFrame()
{ 
  imageCount=0;
  for (ParamsList::iterator i = paramsToDelete_.begin(); i != paramsToDelete_.end(); ++i)
    delete *i;
  paramsToDelete_.clear();
  
  delete imageBatcher_;
  imageBatcher_ = new ImageBatcher;
  delete debugLines_;
  debugLines_ = new DebugLines;
  
  glEnable(GL_TEXTURE_2D);
  glEnable(GL_BLEND);
  glDisable(GL_ALPHA_TEST);
}

//--------------------------------------------------------------------
void Graphics::EndFrame()
{
  //Trace("images drawn: %d\n", imageCount);
  imageBatcher_->Finish();
  debugLines_->Finish();
}

//--------------------------------------------------------------------
void Graphics::Draw() const
{
  imageBatcher_->Draw();
  debugLines_->Draw();
}

//--------------------------------------------------------------------
void Graphics::AddLayer(const std::string& layerName, bool scrollable)
{
  int z = (int)layerMap_.size();
  Layer layer(z, scrollable);
  layerMap_.insert(std::make_pair(layerName, layer));
}

//--------------------------------------------------------------------
int Graphics::GetLayerZ(const std::string& layerName) const
{
  const Layer* layer = GetLayer(layerName);
  return layer ? layer->z : 0;
}

//--------------------------------------------------------------------
const Graphics::Layer* Graphics::GetLayer(const std::string& layerName) const
{
  //return &(layerMap_.find(layerName)->second);
  LayerMap::const_iterator i = layerMap_.find(layerName);
  if (i == layerMap_.end())
  {
    Trace("ERROR: Unknown layer \"%s\"\n", layerName.c_str());
    return 0;
  }
  return &i->second;
}

//--------------------------------------------------------------------
void Graphics::DrawLine(double x1, double y1, double x2, double y2, double r, double g, double b, double a)
{
  GetDebugLines()->AddLine(x1,y1,x2,y2,r,g,b,a);
}

} // Maxx