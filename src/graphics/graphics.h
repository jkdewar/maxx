#pragma once

#include "graphics/image-manager.h"
#include "graphics/camera.h"
#include <memory.h>
#include <string>
#include <map>
#include <vector>
#include "cross-platform/hash-map.h"
#include "graphics/draw-image-params.h"

namespace Maxx {

class Image;
class ImageBatcher;
class DebugLines;

//--------------------------------------------------------------------
class Graphics
{
public:
  Graphics();
  ~Graphics();
  
  struct Layer
  {
    Layer(int z, bool scrollable) : z(z), scrollable(scrollable) {}
    int z;
    bool scrollable;
  };
  
  void AddLayer(const std::string& layerName, bool scrollable);
  int GetLayerZ(const std::string& layerName) const;
  const Layer* GetLayer(const std::string& layerName) const;
  
  void DrawImage(DrawImageParams* params, bool deleteParamsForMe = false);
  void DrawLine(double x1, double y1, double x2, double y2, double r = 0.0, double g = 1.0, double b = 0.0, double a = 1.0);

  void BeginFrame();
  void EndFrame();
  void Draw() const;

  DebugLines* GetDebugLines() { return debugLines_; }
  ImageManager& GetImageManager() { return imageManager_; }
  Camera& GetCamera() { return camera_; }
  ImageBatcher* GetImageBatcher() { return imageBatcher_; }
  
private:
  typedef std::hash_map<std::string, Layer> LayerMap;
  typedef std::vector<DrawImageParams*> ParamsList;
  
  LayerMap layerMap_;
  ImageBatcher* imageBatcher_;
  ImageManager imageManager_;
  DebugLines* debugLines_;
  Camera camera_;
  ParamsList paramsToDelete_;
};

} // Maxx
