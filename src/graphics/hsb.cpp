// thanks to 3d Computer Graphics by Allan Watt
#include "graphics/hsb.h"
#include <cstdio>

namespace Maxx {

//--------------------------------------------------------------------
static double QqhToRgb(double q1, double q2, double hue)
{
  while (hue > 360.0)
    hue = hue - 360.0;
  while  (hue < 0.0)
    hue = hue + 360.0;
  if (hue < 60.0)
    return q1 + (q2 - q1) * hue / 60.0;
  else if (hue < 180.0)
    return q2;
  else if (hue < 240.0)
    return q1 + (q2 - q1) * (240.0 - hue) / 60.0;
  else
    return q1;
}

//--------------------------------------------------------------------
void HsbToRgb(double h, double s, double B, double& r, double& g, double& b)
{
  double p1(0), p2(0);
  if (B <= 0.5)
  {
    p2 = B * (B + s);
  }
  else
  {
    p2 = B + s - B * s;
  }
  p1 = 2.0 * B - p2;
  if (s == 0)
  {
    r = B;
    g = B;
    b = B;
  }
  else
  {
    r = QqhToRgb(p1, p2, h*360.0 + 120.0);
    g = QqhToRgb(p1, p2, h*360.0);
    b = QqhToRgb(p1, p2, h*360.0 - 120.0);
  }
}

} // Maxx
