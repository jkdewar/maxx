#pragma once

namespace Maxx {

void HsbToRgb(double h, double s, double B, double& r, double& g, double& b);

} // Maxx
