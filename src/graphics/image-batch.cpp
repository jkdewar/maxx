#include "graphics/image-batch.h"
#include "engine/maxx.h"
#include "math/vector.h"
#include "math/matrix.h"
#include "cross-platform/opengl-headers.h"
#include "utils/assert.h"

namespace Maxx {

//--------------------------------------------------------------------
ImageBatch::ImageBatch(const Image* image)
  : image_(image)
  , finished_(false)
  , verts_(0)
  , uvs_(0)
  , colors_(0)
  , vptr_(0)
  , tptr_(0)
  , cptr_(0)
{
  Assert(image);
}

//--------------------------------------------------------------------
ImageBatch::~ImageBatch()
{
  drawImageParamsList_.clear();
  delete [] verts_; verts_ = 0;
  delete [] uvs_; uvs_ = 0;
  delete [] colors_; colors_ = 0;
  vptr_ = 0;
  tptr_ = 0;
  cptr_ = 0;
  image_ = 0;
}
  
//--------------------------------------------------------------------
void ImageBatch::Add(const DrawImageParams& drawImageParams)
{
  drawImageParamsList_.push_back((DrawImageParams*)&drawImageParams);
}

//--------------------------------------------------------------------
void ImageBatch::Finish()
{
  if (finished_)
    return;
  finished_ = true;
  
  // Allocate vertex, uv and color arrays
  int totalVerts = (int)drawImageParamsList_.size() * 2*3; // each quad has 2 triangles, 3 verts each
  Assert(!verts_);
  Assert(!uvs_);
  Assert(!colors_);
      
  verts_ = new GLfloat [totalVerts * 2]; // each vert has x,y
  uvs_ = new GLfloat[totalVerts * 2]; // each vert has u,v
  colors_ = new GLubyte[totalVerts * 4]; // each vert has r,g,b,a
  vptr_ = verts_;
  tptr_ = uvs_;
  cptr_ = colors_;
  
  // Build arrays
  for (DrawImageParamsList::iterator
       i = drawImageParamsList_.begin();
       i != drawImageParamsList_.end();
       ++i)
  {
    DrawImageParams& params = **i;
    
    params.CalcBuffers();
    memcpy(vptr_, params.GetVerts(), 6*2*sizeof(GLfloat));
    vptr_ += 6*2;
    memcpy(tptr_, params.GetUvs(), 6*2*sizeof(GLfloat));
    tptr_ += 6*2;
    memcpy(cptr_, params.GetColors(), 6*4*sizeof(GLubyte));
    cptr_ += 6*4;
  }

  vptr_ = 0;
  tptr_ = 0;
  cptr_ = 0;
}

//--------------------------------------------------------------------
void ImageBatch::Draw() const
{
  if (drawImageParamsList_.empty()) return;
  
  glEnable(GL_TEXTURE_2D);
  image_->Bind();
  //glEnable(GL_BLEND);
  const std::string& effect = (*drawImageParamsList_.begin())->GetEffect();
  if (effect == "screen")
  {
    //glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
  }
  else
  {
    //glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  }
  if ((*drawImageParamsList_.begin())->GetFilter() == false)
  {
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  }
     
  int totalVerts = (int)drawImageParamsList_.size() * 2*3; // each quad 2 triangles, 3 verts each

  //glColor4f(1.0f, 1.0f, 1.0f, 1.0);

  glEnableClientState(GL_VERTEX_ARRAY);
  glVertexPointer(2, GL_FLOAT, 0, verts_);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
  glTexCoordPointer(2, GL_FLOAT, 0, uvs_);
  glEnableClientState(GL_COLOR_ARRAY);
  glColorPointer(4, GL_UNSIGNED_BYTE, 0, colors_);

  glDrawArrays(GL_TRIANGLES, 0, totalVerts);
  Assert(glGetError() == GL_NO_ERROR);
  
  glDisableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY); 
}

} // Maxx
 