#pragma once

#include "graphics/image.h"
#include "graphics/graphics.h"
//#include <list>
#include <vector>

namespace Maxx {

//--------------------------------------------------------------------
class ImageBatch
{
public:
  ImageBatch(const Image* image);
  ~ImageBatch();

  void Add(const DrawImageParams& drawImageParams);
  void Finish();
  void Draw() const;
  
private:
  typedef std::vector<DrawImageParams*> DrawImageParamsList;
  
  const Image* image_;
  DrawImageParamsList drawImageParamsList_;
  bool finished_;
  GLfloat* verts_;
  GLfloat* uvs_;
  GLubyte* colors_;
  GLfloat* vptr_;
  GLfloat* tptr_;
  GLubyte* cptr_;
  int count_;
};

} // Maxx
