#include "graphics/image-batcher.h"
#include "graphics/image-batch.h"
#include "utils/assert.h"
#include "engine/maxx.h"

namespace Maxx {

//--------------------------------------------------------------------
ImageBatcher::ImageBatcher()
  : map_()
  , finished_(false)
{
}

//--------------------------------------------------------------------
ImageBatcher::~ImageBatcher()
{
  for (Map::iterator i = map_.begin(); i != map_.end(); ++i)
    delete i->second;
  map_.clear();
}

//--------------------------------------------------------------------
void ImageBatcher::Add(const DrawImageParams& params)
{
  ImageBatch* batch = GetImageBatch(params);
  batch->Add(params);
}

//--------------------------------------------------------------------
ImageBatch* ImageBatcher::GetImageBatch(const DrawImageParams& params)
{
  ImageBatch* batch = 0;
  const Graphics::Layer* layer = Maxx::Get()->GetGraphics()->GetLayer(params.GetLayer());
  if (!layer)
    return 0;
  Key key(params.GetImage(), layer->z, params.GetLayerZ(), params.GetEffect(), layer->scrollable);
  Map::iterator i = map_.find(key);
  if (i == map_.end())
  {
    batch = new ImageBatch(params.GetImage());
    map_.insert(std::make_pair(key, batch));
  }
  else
  {
    batch = i->second;
  }
  return batch;
}

//--------------------------------------------------------------------
void ImageBatcher::Finish()
{
  finished_ = true;
  for (Map::iterator i = map_.begin(); i != map_.end(); ++i)
  {
    ImageBatch* batch = i->second;
    batch->Finish();
  }
}

//--------------------------------------------------------------------
void ImageBatcher::Draw() const
{
  Assert(finished_);
  if (!finished_)
    return;
    
  glColor4f(1.0f, 1.0f, 1.0f, 1.0);
    
  Camera& camera = Maxx::Get()->GetGraphics()->GetCamera();
  for (Map::const_iterator i = map_.begin(); i != map_.end(); ++i)
  {
    const Key& key = i->first;
    const ImageBatch* batch = i->second;
    if (key.scrollable)
    {
      glPushMatrix();
      camera.SetOpenGlMatrix();
    }
    batch->Draw();
    if (key.scrollable)
    {
      glPopMatrix();
    }
  }
}

} // Maxx
