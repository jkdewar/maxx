#pragma once

#include "graphics/graphics.h"
#include <map>
#include <string>

namespace Maxx {

class Image;
class ImageBatch;
  
class ImageBatcher
{
public:
  ImageBatcher();
  ~ImageBatcher();
  
  void Add(const DrawImageParams& params);
  ImageBatch* GetImageBatch(const DrawImageParams& params);
  void Finish();  
  
  void Draw() const;
  
private:
  struct Key
  {
    Key(const Image* image, int z, double layerZ, const std::string& effect, bool scrollable) 
      : image(image), z(z), layerZ(layerZ), effect(effect), scrollable(scrollable) 
    {
    }
    bool operator<(const Key& k) const
    { 
      if (z < k.z) return true; if (k.z < z) return false; 
      if (image < k.image) return true; if (k.image < image) return false;
      if (effect < k.effect) return true; if (k.effect < effect) return false;
      if (layerZ < k.layerZ) return true; if (k.layerZ < layerZ) return false;
      return false;
    }
    const Image* image;
    int z;
    double layerZ;
    std::string effect;
    bool scrollable;
  };
  typedef std::map<Key, ImageBatch*> Map;
  Map map_;
  bool finished_;
};

} // Maxx