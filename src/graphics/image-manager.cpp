#include "graphics/image-manager.h"
#include "graphics/image.h"
#include "graphics/image-sheet.h"

namespace Maxx {

//--------------------------------------------------------------------
ImageManager::ImageManager()
{
}

//--------------------------------------------------------------------
ImageManager::~ImageManager()
{
  Trace("---ALL USED IMAGES---\n");
	for (std::map<std::string, const Image*>::iterator i = mMap.begin(); i != mMap.end(); ++i)
	{
    Trace("\"%s\",\n", i->first.c_str());
		delete (*i).second;
	}
	mMap.clear();
	
	for (std::map<std::string, const ImageSheet*>::iterator i = mMap2.begin(); i != mMap2.end(); ++i)
	{
	  Trace("\"%s\",\n", i->first.c_str());
		delete (*i).second;
	}
	mMap2.clear();
	Trace("---------------------\n");
  
}

//--------------------------------------------------------------------
const Image* ImageManager::GetImage(const std::string& filename)
{
	std::map<std::string, const Image*>::iterator i = mMap.find(filename);
	if (i != mMap.end())
		return (*i).second;
	Image* newImage = Image::Load(filename);
	mMap[filename] = newImage;
	return newImage;
}

//--------------------------------------------------------------------
const ImageSheet* ImageManager::GetImageSheet(const std::string& filename)
{
	std::map<std::string, const ImageSheet*>::iterator i = mMap2.find(filename);
	if (i != mMap2.end())
		return (*i).second;
	ImageSheet* newImageSheet = ImageSheet::Load(filename);
	mMap2[filename] = newImageSheet;
	return newImageSheet;
}

//--------------------------------------------------------------------
void ImageManager::UnloadImage(const std::string& filename)
{
	std::map<std::string, const Image*>::iterator i = mMap.find(filename);
	if (i != mMap.end())
	{
    Trace("UNLOADED %s\n", filename.c_str());
    delete i->second;
    mMap.erase(i);
  }
}

//--------------------------------------------------------------------
void ImageManager::UnloadImageSheet(const std::string& filename)
{
	std::map<std::string, const ImageSheet*>::iterator i = mMap2.find(filename);
	if (i != mMap2.end())
	{
	  // TODO: Unload the image associated with the .sheet as well?
    delete i->second;
    mMap2.erase(i);
  }
}

//--------------------------------------------------------------------
void ImageManager::FreeTexturesBeforeReload()
{
  for (std::map<std::string, const Image*>::iterator
       i = mMap.begin();
       i != mMap.end();
       ++i)
  {
    Image* image = const_cast<Image*>(i->second);
    image->FreeTextureBeforeReload();
  }
  for (std::map<std::string, const ImageSheet*>::iterator
     i = mMap2.begin();
     i != mMap2.end();
     ++i)
  {
    Image* image = const_cast<Image*>(i->second->GetImage());
    image->FreeTextureBeforeReload();
  }
}

//--------------------------------------------------------------------
void ImageManager::ReloadTextures()
{
  for (std::map<std::string, const Image*>::iterator
       i = mMap.begin();
       i != mMap.end();
       ++i)
  {
    Image* image = const_cast<Image*>(i->second);
    image->ReloadTexture(i->first);
  }
  for (std::map<std::string, const ImageSheet*>::iterator
     i = mMap2.begin();
     i != mMap2.end();
     ++i)
  {
    Image* image = const_cast<Image*>(i->second->GetImage());
    image->ReloadTexture(i->first);
  }
}

} // Maxx
