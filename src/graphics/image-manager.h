#pragma once

#include <string>
#include <map>

namespace Maxx {

class Image;
class ImageSheet;

class ImageManager
{
public:
	ImageManager();
	~ImageManager();

	const Image* GetImage(const std::string& filename);
  const ImageSheet* GetImageSheet(const std::string& filename);
  
  void UnloadImage(const std::string& filename);
  void UnloadImageSheet(const std::string& filename);
  
  void FreeTexturesBeforeReload();
  void ReloadTextures();

private:
	std::map<std::string, const Image*> mMap;
	std::map<std::string, const ImageSheet*> mMap2;
};

} // Maxx