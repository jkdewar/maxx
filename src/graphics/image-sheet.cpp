#include "graphics/image-sheet.h"
#include "graphics/image.h"
#include "graphics/draw-image-params.h"
#include "graphics/graphics.h"
#include "cross-platform/file-sys.h"
#include "utils/trace.h"
#include "engine/maxx.h"

namespace Maxx {

//--------------------------------------------------------------------
/*static*/ ImageSheet* ImageSheet::Load(const std::string& fileName)
{
  bool success = false;
  ImageSheet* i = new ImageSheet(fileName, success);
  if (!success)
  {
    delete i;
    i = 0;
  }
  return i;
}

//--------------------------------------------------------------------
ImageSheet::ImageSheet(const std::string& fileName, bool& success)
  : image_(0)
  , table_()
{
  // Read table from file
  std::string fileContents;
  bool readOk = ReadFile(fileName, fileContents);
  if (!readOk)
  {
    Trace("ERROR: ImageSheet: Couldn't read file '%s'\n", fileName.c_str());
    success = false;
    return;
  }
  size_t dontCare = 0;
  bool unstringifyOk = PropertyTable::Unstringify(Maxx::Get()->GetLuaState(), fileContents, dontCare, table_);
  if (!unstringifyOk)
  {
    Trace("ERROR: ImageSheet: unstringifying '%s'\n", fileName.c_str());
    success = false;
    return;
  }
  
  // Load image
  std::string imageFileName = table_.GetString("imageFilename","");
  if (imageFileName.empty())
  {
    Trace("ERROR: ImageSheet: imageFilename missing\n");
    success = false;
    return;
  } 
  image_ = Maxx::Get()->GetGraphics()->GetImageManager().GetImage(imageFileName);
  if (!image_)
  {
    Trace("ERROR: ImageSheet: loading image '%s'\n", imageFileName.c_str());
    success = false;
    return;
  }
  
  success = true;
  return;
}

//--------------------------------------------------------------------
ImageSheet::~ImageSheet()
{
}

//--------------------------------------------------------------------
bool ImageSheet::GetSize(const std::string& imageName, double& sizeX, double& sizeY) const
{
  const PropertyTable* images = table_.GetTable("images");
  if (!images)
    return false;
  const PropertyTable* info = images->GetTable(imageName);
  if (!info)
    return false;
  double defaultWidth = table_.GetNumber("defaultWidth",0.0);
  double defaultHeight = table_.GetNumber("defaultHeight",0.0);
  sizeX = info->GetNumber("w", defaultWidth);
  sizeY = info->GetNumber("h", defaultHeight);
  //sizeX += 1.0; sizeY += 1.0; // TODO: why?
  return true;
}

//--------------------------------------------------------------------
bool ImageSheet::GetUvs(const std::string& imageName, DrawImageParams& drawImageParams) const
{
  const PropertyTable* images = table_.GetTable("images");
  if (!images)
    return false;
  const PropertyTable* info = images->GetTable(imageName);
  if (!info)
    return false;
  double x = info->GetNumber("x", 0.0);
  double y = info->GetNumber("y", 0.0);
  double defaultWidth = table_.GetNumber("defaultWidth",0.0);
  double defaultHeight = table_.GetNumber("defaultHeight",0.0);
  double w = info->GetNumber("w", defaultWidth);
  double h = info->GetNumber("h", defaultHeight);
  //w += 1.0; h += 1.0; // TODO: why?
  double imageSizeX = (double)image_->GetSizeX();
  double imageSizeY = (double)image_->GetSizeY();
  drawImageParams.u1 = x / imageSizeX;
  drawImageParams.v1 = y / imageSizeY;
  drawImageParams.u2 = (x + w) / imageSizeX;
  drawImageParams.v2 = drawImageParams.v1;
  drawImageParams.u3 = drawImageParams.u2;
  drawImageParams.v3 = (y + h) / imageSizeY;
  drawImageParams.u4 = drawImageParams.u1;
  drawImageParams.v4 = drawImageParams.v3;
  return true;
}

  
} // Maxx
