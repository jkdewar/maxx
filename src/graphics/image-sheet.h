#pragma once
#include "engine/property-table.h"
#include <string>

namespace Maxx {
  
class Image;
class DrawImageParams;

//--------------------------------------------------------------------
class ImageSheet
{
public:
  static ImageSheet* Load(const std::string& fileName);
  ~ImageSheet();
  
  const Image* GetImage() const { return image_; }
  bool GetSize(const std::string& imageName, double& sizeX, double& sizeY) const;
  bool GetUvs(const std::string& imageName, DrawImageParams& drawImageParams) const;
  double GetDefaultHeight() const { return table_.GetNumber("defaultHeight", 0.0); }
  
private:
  ImageSheet(const std::string& fileName, bool& success);
  
private:
  const Image* image_;
  PropertyTable table_;
};
  
} // Maxx
