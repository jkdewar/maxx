#include "graphics/image.h"
#include "cross-platform/sdl-headers.h"
#include "cross-platform/sdl-image-headers.h"
#include "math/math.h"
#include "graphics/display.h"
#include "graphics/image-manager.h"
#include "utils/assert.h"
#include <algorithm>

using namespace Maxx::Math;

namespace Maxx {

//--------------------------------------------------------------------
Image* Image::Load(const std::string& name)
{
	bool success = false;
	Image* image = new Image(name, success);
	if (!success)
	{
		delete image;
		image = 0;
	}
	return image;
}

//--------------------------------------------------------------------
Image::Image(const std::string& filename, bool& success)
	: mSurface(0), mHandle(0), mImageSizeX(0), mImageSizeY(0), mTextureSizeX(0), mTextureSizeY(0)
{
    success = LoadInternal(filename);
}

//--------------------------------------------------------------------
Image::Image(SDL_Surface* surface)
    : mSurface(0), mHandle(0), mImageSizeX(0), mImageSizeY(0), mTextureSizeX(0), mTextureSizeY(0)
{
	//glEnable(GL_TEXTURE_2D);
  Assert(glGetError() == GL_NO_ERROR);

	glGenTextures(1, &mHandle);
  Assert(glGetError() == GL_NO_ERROR);

  CreateFromSurface(surface);
}

//--------------------------------------------------------------------
Image::Image(int sizeX, int sizeY)
	: mSurface(0), mHandle(0), mImageSizeX(0), mImageSizeY(0), mTextureSizeX(0), mTextureSizeY(0)
{	
	//glEnable(GL_TEXTURE_2D);

	glGenTextures(1, &mHandle);
	Assert(glGetError() == GL_NO_ERROR);

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    Uint32 rmask = 0xff000000;
    Uint32 gmask = 0x00ff0000;
    Uint32 bmask = 0x0000ff00;
    Uint32 amask = 0x000000ff;
#else
    Uint32 rmask = 0x000000ff;
    Uint32 gmask = 0x0000ff00;
    Uint32 bmask = 0x00ff0000;
    Uint32 amask = 0xff000000;
#endif
	SDL_Surface* surface = SDL_CreateRGBSurface(SDL_SWSURFACE, sizeX, sizeY, 32, rmask, gmask, bmask, amask);
    CreateFromSurface(surface);
}

//--------------------------------------------------------------------
Image::~Image()
{
	glDeleteTextures(1, &mHandle);
}

//--------------------------------------------------------------------
static void myBuild2DMipmaps(SDL_Surface* textureSurface)
{
  int mipLevel= 0;
  glTexImage2D(GL_TEXTURE_2D, mipLevel, 4, textureSurface->w, textureSurface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureSurface->pixels);
  ++mipLevel;
  int sizeX = textureSurface->w;
  int sizeY = textureSurface->h;
  while (sizeX > 1 || sizeY > 1)
  {
    if (sizeX > 1)
    {
      sizeX /= 2;
    }
    if (sizeY > 1)
    {
      sizeY /= 2;
    }
    SDL_Surface* mipSurface = SDL_CreateRGBSurface(textureSurface->flags, sizeX, sizeY, 32, textureSurface->format->Rmask, textureSurface->format->Gmask, textureSurface->format->Bmask, textureSurface->format->Amask);
    SDL_BlitSurface(textureSurface, 0, mipSurface, 0);
    glTexImage2D(GL_TEXTURE_2D, mipLevel, 4, mipSurface->w, mipSurface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, mipSurface->pixels);
    ++mipLevel;
    SDL_FreeSurface(mipSurface);
    mipSurface = 0;
  }
}

//----------------------------------------------------------------------
Uint32 getpixel(SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to retrieve */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
    case 1:
        return *p;
        break;

    case 2:
        return *(Uint16 *)p;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            return p[0] << 16 | p[1] << 8 | p[2];
        else
            return p[0] | p[1] << 8 | p[2] << 16;
        break;

    case 4:
        return *(Uint32 *)p;
        break;

    default:
        return 0;       /* shouldn't happen, but avoids warnings */
    }
}

//--------------------------------------------------------------------
static void DumpSurface(SDL_Surface* s)
{
  printf("======================================\n");
  Uint32 pixel;
  Uint8 r,g,b,a;
  int count=0;
  for (int y=0;y<s->h;++y) {
    for (int x=0;x<s->w;++x) {
      pixel=getpixel(s,x,y);
      SDL_GetRGBA(pixel,s->format,&r,&g,&b,&a);
      printf("[r:%02x g:%02x b:%02x a:%02x] ",r,g,b,a);
      ++count;
      if (count>=1000) return;
    }
  }
}

//--------------------------------------------------------------------
void Image::CreateFromSurface(SDL_Surface* imageSurface)
{
  mSurface = imageSurface;

	mImageSizeX = imageSurface->w;
	mImageSizeY = imageSurface->h;
	mTextureSizeX = NearestPowerOf2RoundUp(mImageSizeX);
	mTextureSizeY = NearestPowerOf2RoundUp(mImageSizeY);

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
  Uint32 rmask = 0xff000000;
  Uint32 gmask = 0x00ff0000;
  Uint32 bmask = 0x0000ff00;
  Uint32 amask = 0x000000ff;
#else
  Uint32 rmask = 0x000000ff;
  Uint32 gmask = 0x0000ff00;
  Uint32 bmask = 0x00ff0000;
  Uint32 amask = 0xff000000;
#endif
	SDL_Surface* textureSurface = SDL_CreateRGBSurface(SDL_SWSURFACE, mTextureSizeX, mTextureSizeY, 32, rmask, gmask, bmask, amask);
  SDL_FillRect(textureSurface,0,SDL_MapRGBA(textureSurface->format,0,0,0,0));

  SDL_Rect destRect;
  destRect.x = 0;
  destRect.y = 0;
  destRect.w = imageSurface->w;
  destRect.h = imageSurface->h;
  SDL_SetSurfaceBlendMode(imageSurface, SDL_BLENDMODE_NONE);
  SDL_SetSurfaceBlendMode(textureSurface, SDL_BLENDMODE_NONE);
  SDL_BlitSurface(imageSurface, 0, textureSurface, 0);
  //DumpSurface(textureSurface);
    
  //glEnable(GL_TEXTURE_2D);
  Bind();

#if !defined(__WIN32__) // TODO
  glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
#endif
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureSurface->w, textureSurface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureSurface->pixels);
  //gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, textureSurface->w, textureSurface->h, GL_RGBA, GL_UNSIGNED_BYTE, textureSurface->pixels);
  //myBuild2DMipmaps(textureSurface);
  Assert(glGetError() == GL_NO_ERROR);
	
  SDL_FreeSurface(textureSurface);
  textureSurface = 0;
  SDL_FreeSurface(mSurface);
  mSurface = 0;
}

//--------------------------------------------------------------------
bool Image::LoadInternal(const std::string& filename)
{
	//glEnable(GL_TEXTURE_2D);

	glGenTextures(1, &mHandle);
	Assert(glGetError() == GL_NO_ERROR);

	SDL_Surface* imageSurface = IMG_Load(filename.c_str());
	if (imageSurface == 0)
	{
		Trace("Load failed: %s\n", filename.c_str());
		return false;
	}

  Trace("LOADING %s\n", filename.c_str());
  CreateFromSurface(imageSurface);
  return true;
}

//--------------------------------------------------------------------
void Image::Bind() const
{
  //glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, mHandle);
	Assert(glGetError() == GL_NO_ERROR);
  
#if defined(__WIN32__) // TODO
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
#else
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
#endif
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

//--------------------------------------------------------------------
void Image::FreeTextureBeforeReload()
{
  glDeleteTextures(1, &mHandle);
}

//--------------------------------------------------------------------
void Image::ReloadTexture(const std::string& filename)
{
  bool ok = LoadInternal(filename);
  Assert(ok);
}

} // Maxx
