#pragma once

#include "cross-platform/opengl-headers.h"
#include "cross-platform/sdl-headers.h"
#include <string>

namespace Maxx {

class Image
{
public:
	static Image* Load(const std::string& filename);
  Image(SDL_Surface* surface);
	Image(int sizeX, int sizeY);
	~Image();

	int GetSizeX() const { return mImageSizeX; }
  int GetSizeY() const { return mImageSizeY; }
  float GetScaleU() const { return (float)mImageSizeX / (float)mTextureSizeX; }
  float GetScaleV() const { return (float)mImageSizeY / (float)mTextureSizeY; }

  SDL_Surface* GetSDLSurface() const { return mSurface; }
  int GetOpenGLHandle() const { return mHandle; }
  
  void Bind() const;
  
  void FreeTextureBeforeReload();
  void ReloadTexture(const std::string& filename);

private:
	Image(const std::string& filename, bool& success);
  void CreateFromSurface(SDL_Surface* surface);
  bool LoadInternal(const std::string& filename);

  SDL_Surface* mSurface;
	GLuint mHandle;
	int mImageSizeX, mImageSizeY;
	int mTextureSizeX, mTextureSizeY;
};

} // Maxx
