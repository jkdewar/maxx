#if 0
#include "graphics/shader.h"
#include <fstream>
#include <cassert>

namespace Maxx {
  
#define VERIFY_GL_OK() do { GLenum error = glGetError(); if (error != GL_NO_ERROR) { printf("GL ERROR at %s:%d\n'%s'", __FILE__, __LINE__, gluErrorString(error)); } } while(false)

Shader::Shader()
  : vertShaderFileName_("")
  , fragShaderFileName_("")
  , vertShader_(0)
  , fragShader_(0)
{
}

Shader::~Shader()
{
  // TODO:jkd free vert and frag shaders
}

bool Shader::Load(const std::string& vertShaderFileName, const std::string& fragShaderFileName)
{
  VERIFY_GL_OK();
  // TODO:jkd free vert and frag shaders
  // Build shader objects
  vertShader_ = glCreateShader(GL_VERTEX_SHADER);
  fragShader_ = glCreateShader(GL_FRAGMENT_SHADER);

  // Load code from .vs and .fs files
  std::ifstream vsFile(vertShaderFileName.c_str());
  if (!vsFile)
    return false;
  std::ifstream fsFile(fragShaderFileName.c_str());
  if (!fsFile)
    return false;
  std::string vsCode((std::istreambuf_iterator<char>(vsFile)), std::istreambuf_iterator<char>());
  std::string fsCode((std::istreambuf_iterator<char>(fsFile)), std::istreambuf_iterator<char>());

  // Compile
  const GLchar* vsSource[1]; vsSource[0] = vsCode.c_str();
  const GLchar* fsSource[1]; fsSource[0] = fsCode.c_str();
  const GLint vsLen = (GLint)vsCode.length();
  const GLint fsLen = (GLint)fsCode.length();
  GLint compiled;
  glShaderSource(vertShader_, 1, (const GLchar**)&vsSource, (const GLint*)&vsLen);
  VERIFY_GL_OK();
  glCompileShader(vertShader_);
  glGetProgramiv(vertShader_, GL_COMPILE_STATUS, &compiled);
  VERIFY_GL_OK();
  if (!compiled)
   return false;
  glShaderSource(fragShader_, 1, (const GLchar**)&fsSource, (const GLint*)&fsLen);
  VERIFY_GL_OK();
  glCompileShader(fragShader_);
  VERIFY_GL_OK();
  glGetProgramiv(fragShader_, GL_COMPILE_STATUS, &compiled);
  VERIFY_GL_OK();
  if (!compiled)
   return false;

  // Build shader program
  // TODO:jkd free program
  program_ = glCreateProgram();
  glAttachShader(program_, vertShader_);
  VERIFY_GL_OK();
  glAttachShader(program_, fragShader_);
  VERIFY_GL_OK();
  glLinkProgram(program_);
  VERIFY_GL_OK();
  GLint linked;
  glGetProgramiv(program_, GL_LINK_STATUS, &linked);
  if (!linked)
    return false;
  return true;
}

bool Shader::TurnOn()
{
  VERIFY_GL_OK();
  glUseProgram(program_);
  VERIFY_GL_OK();
  return true;
}

bool Shader::TurnOff()
{
  VERIFY_GL_OK();
  glUseProgram(0);
  VERIFY_GL_OK();
  return true;
}

} // Maxx
#endif
