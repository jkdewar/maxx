#if 0
#pragma once

#include "cross-platform/opengl-headers.h"
#include <string>

namespace Maxx {

class Shader
{
public:

  Shader();
  ~Shader();

  bool Load(const std::string& vertShaderFileName, const std::string& fragShaderFileName);

  bool TurnOn();
  bool TurnOff();

private:
  std::string vertShaderFileName_;
  std::string fragShaderFileName_;
  GLuint vertShader_;
  GLuint fragShader_;
  GLuint program_;
};

} // Maxx
#endif
