#include "graphics/text/bitmap-font.h"
#include "graphics/draw-image-params.h"
#include "utils/assert.h"
#include "utils/trace.h"

namespace Maxx {
  
static const double kSpaceBetweenCharacters = 1.0;

//--------------------------------------------------------------------
BitmapFont* BitmapFont::Load(const std::string& fileName, float scale)
{
  bool success = false;
  BitmapFont* font = new BitmapFont(fileName, scale, success);
  if (!success)
  {
    delete font;
    font = 0;
  }
  return font;
}

//--------------------------------------------------------------------
BitmapFont::~BitmapFont()
{
}

//--------------------------------------------------------------------
void BitmapFont::BuildText(const std::string& textIn, const DrawImageParams& params, DrawImageParamsList& dips) const
{
  static std::string space(" ");
  const std::string& text = textIn.empty() ? space : textIn;
  Assert(imageSheet_);
  if (!imageSheet_)
    return;
  size_t size = text.size();
  double x = params.GetX() + (-0.5-params.GetAnchorX()/2.0)*GetTextWidth(text, params);
  double y = params.GetY() + (-0.5-params.GetAnchorY()/2.0)*GetHeight();
  for (size_t i = 0; i < size; ++i)
  {
    char c = text[i];
    std::string letterName;
    letterName.push_back(c);
    double w,h;
    bool ok = imageSheet_->GetSize(letterName, w, h);
    if (!ok)
    {
      letterName = ::toupper(c);
      ok = imageSheet_->GetSize(letterName, w, h);
    }
    if (!ok)
    {
      letterName = ::tolower(c);
      ok = imageSheet_->GetSize(letterName, w, h);
    }
    if (!ok)
      continue;
    DrawImageParams p = params;
    p.SetImageSheet(imageSheet_);
    p.SetSheetImageName(letterName);
    p.SetOffsetX(-x);
    p.SetOffsetY(-y);
    p.SetScaleX(params.GetScaleX());
    p.SetScaleY(params.GetScaleY());
    p.SetFilter(params.GetFilter());
    p.SetAnchorX(-1);
    p.SetAnchorY(-1);
    ok = imageSheet_->GetUvs(letterName, p);
    Assert(ok);
    dips.push_back(p);
    x += w*params.GetScaleX() + kSpaceBetweenCharacters;
  }
}

//--------------------------------------------------------------------
int BitmapFont::GetTextWidth(const std::string& text, const DrawImageParams& p) const
{
  Assert(imageSheet_);
  if (!imageSheet_)
    return 0;
  
  double totalWidth = 0.0;
  size_t size = text.size();
  for (size_t i = 0; i < size; ++i)
  {
    char c = text[i];
    std::string letterName;
    letterName.push_back(c);
    double w,h;
    bool ok = imageSheet_->GetSize(letterName, w, h);
    if (!ok)
    {
      letterName = ::toupper(c);
      ok = imageSheet_->GetSize(letterName, w, h);
    }
    if (!ok)
    {
      letterName = ::tolower(c);
      ok = imageSheet_->GetSize(letterName, w, h);
    }
    if (!ok)
      continue;
    totalWidth += w*p.GetScaleX() + kSpaceBetweenCharacters;
  }
  return totalWidth;
}

//--------------------------------------------------------------------
int BitmapFont::GetHeight() const
{
  if (!imageSheet_)
    return 0;
  return imageSheet_->GetDefaultHeight();
}

//--------------------------------------------------------------------
BitmapFont::BitmapFont(const std::string& fileName, float scale, bool& success)
  : imageSheet_(0)
{
  imageSheet_ = ImageSheet::Load(fileName);
  if (!imageSheet_)
  {
    Trace("ERROR: BitmapFont: couldn't load image sheet '%s'\n", fileName.c_str());
    success = false;
    return;
  }
  success = true;
  return;
}

} // Maxx
