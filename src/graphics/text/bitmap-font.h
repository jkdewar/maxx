#pragma once

#include "graphics/text/font.h"

namespace Maxx {

class Image;
class ImageSheet;

//--------------------------------------------------------------------
class BitmapFont : public Font
{
public:
  // .sheet file with each image named by character
  static BitmapFont* Load(const std::string& fileName, float scale = 1.0f);
  virtual ~BitmapFont();

  virtual void BuildText(const std::string& text, const DrawImageParams& p, DrawImageParamsList& dips) const;

  virtual int GetTextWidth(const std::string& text, const DrawImageParams& p) const;
  virtual int GetHeight() const;

private:

  BitmapFont(const std::string& fileName, float scale, bool& success);

  const ImageSheet* imageSheet_;
  
  // const Image* mImage;
  // int mHeight;
  // std::map<char, Letter> mMap;
  // float mScale;
};

} // Maxx
