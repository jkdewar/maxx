#include "graphics/text/font-manager.h"
#include "graphics/text/font.h"
#include "graphics/text/tt-font.h"
#include "graphics/text/bitmap-font.h"
#include "utils/assert.h"
#include "cross-platform/file-sys.h"

namespace Maxx {

//--------------------------------------------------------------------
FontManager::FontManager()
{
}

//--------------------------------------------------------------------
FontManager::~FontManager()
{
  for (NameToFont::iterator i = nameToFont_.begin(); i != nameToFont_.end(); ++i)
  {
    delete i->second;
  }
  nameToFont_.clear();
}

//--------------------------------------------------------------------
Font* FontManager::RegisterFont(const std::string& name, const std::string& fileName, double pointSize)
{
  {
    NameToFont::iterator i = nameToFont_.find(name);
    if (i != nameToFont_.end())
      nameToFont_.erase(i);
  }
  
  std::string extension = GetExtension(fileName);
  Font* font = 0;
  if (extension == "sheet")
  {
    font = BitmapFont::Load(fileName, pointSize);
  }
  else
  {
    font = TTFont::Load(fileName, pointSize);
  }
  Assert(font);
  nameToFont_.insert(std::make_pair(name, font));
  return font;
}

//--------------------------------------------------------------------
Font* FontManager::GetFont(const std::string& name)
{
  NameToFont::const_iterator i = nameToFont_.find(name);
  if (i == nameToFont_.end())
  {
    Trace("FontManager::GetFont: ERROR: Unknown font \"%s\"\n", name.c_str());
    return 0;
  }
  return i->second;
}

} // Maxx
