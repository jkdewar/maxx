#pragma once

#include <map>
#include <string>

namespace Maxx {

class Font;
  
//--------------------------------------------------------------------
class FontManager
{
public:
  FontManager();
  ~FontManager();
  
  Font* RegisterFont(const std::string& name, const std::string& fileName, double pointSize);
  
  Font* GetFont(const std::string& name);

private:  
  typedef std::map<std::string, Font*> NameToFont;  
  NameToFont nameToFont_;
};

} // Maxx
