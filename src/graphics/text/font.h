#pragma once

#include <string>
#include <vector>

namespace Maxx {

class Image;
class DrawImageParams;

//--------------------------------------------------------------------
class Font
{
public:
  virtual ~Font() {}

  typedef std::vector<DrawImageParams> DrawImageParamsList;
  virtual void BuildText(const std::string& text, const DrawImageParams& p, DrawImageParamsList& dips) const = 0;

  virtual int GetTextWidth(const std::string& text, const DrawImageParams& p) const = 0;
  virtual int GetHeight() const = 0;
};

} // Maxx
