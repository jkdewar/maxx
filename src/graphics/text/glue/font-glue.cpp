#include "graphics/text/glue/font-glue.h"
#include "graphics/text/font.h"
#include "graphics/text/tt-font.h"
#include "graphics/text/font-manager.h"
#include "graphics/image.h"
#include "engine/maxx.h"

namespace Maxx {
namespace Glue {

// //--------------------------------------------------------------------
// // fileName, pointSize
// // returns Font*
// int Font_load(lua_State* L)
// {
//   GLUE_VERIFY(lua_gettop(L) == 2);
//   GLUE_VERIFY(lua_isstring(L, 1));
//   std::string fileName = lua_tostring(L, 1);
//   GLUE_VERIFY(lua_isnumber(L, 2));
//   int pointSize = (int)lua_tonumber(L, 2);
//   Font* font = TTFont::Create(fileName, pointSize);
//   Assert(font);
//   Maxx::Glue::NewUserData<Font>(L, font, /*own=*/true);
//   return 1;
// }

//--------------------------------------------------------------------
// self=font, text
int Font_drawText(lua_State* L)
{
  //TODO: remove
  // GLUE_VERIFY(lua_gettop(L) == 2);
  // 
  // Font* font = Maxx::Glue::GetUserData<Font>(L, 1);
  // GLUE_VERIFY(font);
  // 
  // GLUE_VERIFY(lua_isstring(L, 2));
  // std::string text = lua_tostring(L, 2);
  // 
  // font->DrawText(text);

  return 0;
}

//--------------------------------------------------------------------
int Font_getFont(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  GLUE_VERIFY(lua_isstring(L, 1));
  std::string name = lua_tostring(L, 1);
  FontManager* fontMan = Maxx::Get()->GetFontManager();
  Font* font = fontMan->GetFont(name);
  Maxx::Glue::NewUserData<Font>(L, font, /*own=*/false);
  return 1;
}

//--------------------------------------------------------------------
// self
// returns height
int Font_getHeight(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Font* font = Maxx::Glue::GetUserData<Font>(L, 1);
  GLUE_VERIFY(font);
  double height = font->GetHeight();
  lua_pushnumber(L, height);
  return 1;
}

//--------------------------------------------------------------------
int luaopen_Font(lua_State* L)
{
  static luaL_Reg table[] =
  {
    // {"load", Font_load},
    {"drawText", Font_drawText},
    {"getFont", Font_getFont},
    {"getHeight", Font_getHeight},
    {0, 0}
  };
  Maxx::Glue::RegisterType<Font>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}

} // Glue
} // Maxx
