#pragma once

#include "engine/glue.h"

namespace Maxx {
namespace Glue {

int luaopen_Font(lua_State* L);

} // Glue
} // Maxx
