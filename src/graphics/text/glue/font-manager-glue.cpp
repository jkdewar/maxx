#include "graphics/text/glue/font-manager-glue.h"
#include "graphics/text/font-manager.h"
#include "engine/maxx.h"

namespace Maxx {
namespace Glue {

//--------------------------------------------------------------------
// self, name, fileName, pointSize
int FontManager_registerFont(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 4);
  
  FontManager* fontManager = Maxx::Glue::GetUserData<FontManager>(L, 1);
  GLUE_VERIFY(fontManager);
  GLUE_VERIFY(lua_isstring(L, 2));
  std::string name = lua_tostring(L, 2);
  GLUE_VERIFY(lua_isstring(L, 3));
  std::string fileName = lua_tostring(L, 3);
  GLUE_VERIFY(lua_isnumber(L, 4));
  double pointSize = lua_tonumber(L, 4);
  fontManager->RegisterFont(name, fileName, pointSize);
  return 0;
}

//--------------------------------------------------------------------
int luaopen_FontManager(lua_State* L)
{
  static luaL_Reg table[] =
  {
    {"registerFont", FontManager_registerFont},
    {0, 0}
  };
  Maxx::Glue::RegisterType<FontManager>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}

} // Glue
} // Maxx
