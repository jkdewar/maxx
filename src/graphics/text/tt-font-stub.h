#pragma once
#include "graphics/text/font.h"
#include <string>

namespace Maxx {
  
class Image;

//--------------------------------------------------------------------
class TTFont : public Font
{
public:
  static TTFont* Load(const std::string& fileName, int pointSize) { return 0; }
  virtual ~TTFont() {}

  virtual Image* DrawToImage(const std::string& text, double r, double g, double b) const { return 0; }
  
  virtual int GetTextWidth(const std::string& text) const { return 0; }
  virtual int GetHeight() const { return 0; }

  int GetPointSize() const { return 0; }
};

} // Maxx
