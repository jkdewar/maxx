#if 0//!defined(__IPHONEOS__)
#include "graphics/text/tt-font.h"
#include "graphics/image.h"
#include "graphics/draw-image-params.h"
#include "utils/assert.h"
#include <string>

namespace Maxx {

//--------------------------------------------------------------------
/*static*/ TTFont* TTFont::Load(const std::string& fileName, int pointSize)
{
	bool success = false;
	TTFont* font = new TTFont(fileName, pointSize, success);
	if (!success)
	{
    delete font; font = 0;
	}
	return font;
}

//--------------------------------------------------------------------
TTFont::TTFont(const std::string& fileName, int pointSize, bool& success)
    :   pointSize_(pointSize), ttf_(0)
{
    ttf_ = TTF_OpenFont(fileName.c_str(), pointSize);
    if (!ttf_)
      Trace("TTF_OpenFont: %s\n", TTF_GetError());
    success = (ttf_ != 0);
}

//--------------------------------------------------------------------
TTFont::~TTFont()
{
	TTF_CloseFont(ttf_); 
    ttf_ = 0; 
}

//--------------------------------------------------------------------
int TTFont::GetTextWidth(const std::string& text, const DrawImageParams& p) const
{
	int width, height;
	TTF_SizeText(ttf_, text.c_str(), &width, &height);
	return width;
}

//--------------------------------------------------------------------
int TTFont::GetHeight() const
{
	return TTF_FontHeight(ttf_);
}

//--------------------------------------------------------------------
int TTFont::GetPointSize() const
{
	return pointSize_;
}

//--------------------------------------------------------------------
void TTFont::BuildText(const std::string& textIn, const DrawImageParams& params, DrawImageParamsList& dips) const
{
  std::string text = textIn;
  if (text.empty())
    text = " ";
	
	SDL_Color fgColour;
  fgColour.r = (Uint8)(255);
  fgColour.g = (Uint8)(255);
  fgColour.b = (Uint8)(255);
	fgColour.unused = 255;
	SDL_Surface* textSurface = 0;
	//if (GetPointSize() < 16)
	//{
	//	textSurface = TTF_RenderText_Solid(ttf_, text.c_str(), fgColour);
	//}
	//else
	{
		textSurface = TTF_RenderText_Blended(ttf_, text.c_str(), fgColour);
	}
	Assert(textSurface);

  Image* image = new Image(textSurface);
  SDL_FreeSurface(textSurface);

  DrawImageParams p;
  p.SetAnchorX(-1);
  p.SetAnchorY(-1);
  p.SetImage(image); // TODO: this image is currently never deleted leak leak leak
  dips.push_back(p);
}

} // Maxx

#endif
