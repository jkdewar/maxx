#pragma once

#if 1//defined(__IPHONEOS__) // no ttf support at the moment
#include "tt-font-stub.h"
#else

#include "graphics/text/font.h"
#include "cross-platform/sdl-ttf-headers.h"
#include <string>

namespace Maxx {
  
class Image;

//--------------------------------------------------------------------
class TTFont : public Font
{
public:
	static TTFont* Load(const std::string& fileName, int pointSize);
  virtual ~TTFont();

  virtual void BuildText(const std::string& text, const DrawImageParams& p, DrawImageParamsList& dips) const;
  
  virtual int GetTextWidth(const std::string& text, const DrawImageParams& p) const;
  virtual int GetHeight() const;

  int GetPointSize() const;

private:
  TTFont(const std::string& fileName, int pointSize, bool& success);

private:
	int pointSize_;
  TTF_Font* ttf_;
};

} // Maxx

#endif
