#include "input/accelerometer.h"

namespace Maxx {

Accelerometer::Accelerometer()
  : joy_(0)
{
#if defined(__IPHONEOS__)
  joy_ = SDL_JoystickOpen(0);
#endif
}

Accelerometer::~Accelerometer()
{
#if defined(__IPHONEOS__)
  SDL_JoystickClose(joy_);
  joy_ = 0;
#endif
}

double Accelerometer::GetXAxis() const
{
#if defined(__IPHONEOS__)
  return SDL_JoystickGetAxis(joy_, 0);
#else
  return 0.0;
#endif
}

double Accelerometer::GetYAxis() const
{
#if defined(__IPHONEOS__)
  return SDL_JoystickGetAxis(joy_, 1);
#else
  return 0.0;
#endif
}

} // Maxx