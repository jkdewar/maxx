#pragma once
#include "cross-platform/sdl-headers.h"

namespace Maxx {

class Accelerometer
{
public:
  Accelerometer();
  ~Accelerometer();
  
  double GetXAxis() const;
  double GetYAxis() const;
  
private:
  SDL_Joystick* joy_;
};

} // Maxx