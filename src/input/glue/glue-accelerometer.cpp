#include "glue-accelerometer.h"
#include "input/accelerometer.h"

using namespace Maxx;

namespace Maxx {
namespace Glue {

//--------------------------------------------------------------------
int Accelerometer_getXAxis(lua_State* L)
{
	Accelerometer* accelerometer = Maxx::Glue::GetUserData<Accelerometer>(L);
  GLUE_VERIFY(accelerometer);
  lua_pushnumber(L, accelerometer->GetXAxis());
  return 1;
}

//--------------------------------------------------------------------
int Accelerometer_getYAxis(lua_State* L)
{
	Accelerometer* accelerometer = Maxx::Glue::GetUserData<Accelerometer>(L);
  GLUE_VERIFY(accelerometer);
  lua_pushnumber(L, accelerometer->GetYAxis());
  return 1;
}

//--------------------------------------------------------------------
int luaopen_Accelerometer(lua_State* L)
{
  static luaL_Reg table[] =
  {
    {"getXAxis", Accelerometer_getXAxis},
    {"getYAxis", Accelerometer_getYAxis},
    {0, 0}
  };
  Maxx::Glue::RegisterType<Accelerometer>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}

} // Glue
} // Maxx
