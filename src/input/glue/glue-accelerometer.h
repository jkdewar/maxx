#pragma once

#include "engine/glue.h"

namespace Maxx {
namespace Glue {

int luaopen_Accelerometer(lua_State*);

} // Glue
} // Maxx
