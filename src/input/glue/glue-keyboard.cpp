#include "glue-keyboard.h"
#include "input/keyboard.h"
#include <string>
#include <map>
#include <algorithm>

using namespace Maxx;

namespace Maxx {
namespace Glue {

//--------------------------------------------------------------------
int Keyboard_new(lua_State* L)
{
  Keyboard* keyboard = new Keyboard;
  Maxx::Glue::NewUserData<Keyboard>(L, keyboard, /*own=*/true);
  return 1;
}

//--------------------------------------------------------------------
int Keyboard_update(lua_State* L)
{
  Keyboard* keyboard = Maxx::Glue::GetUserData<Keyboard>(L);
  GLUE_VERIFY(keyboard);
  keyboard->Update();
  return 0;
}

//--------------------------------------------------------------------
static SDL_Scancode StringToSdlScancode(const std::string& s)
{
  static std::map<std::string, SDL_Scancode> m;
  static bool initialized = false;
  if (!initialized)
  {
    m["a"] = SDL_SCANCODE_A;
    m["b"] = SDL_SCANCODE_B;
    m["c"] = SDL_SCANCODE_C;
    m["d"] = SDL_SCANCODE_D;
    m["e"] = SDL_SCANCODE_E;
    m["f"] = SDL_SCANCODE_F;
    m["g"] = SDL_SCANCODE_G;
    m["h"] = SDL_SCANCODE_H;
    m["i"] = SDL_SCANCODE_I;
    m["j"] = SDL_SCANCODE_J;
    m["k"] = SDL_SCANCODE_K;
    m["l"] = SDL_SCANCODE_L;
    m["m"] = SDL_SCANCODE_M;
    m["n"] = SDL_SCANCODE_N;
    m["o"] = SDL_SCANCODE_O;
    m["p"] = SDL_SCANCODE_P;
    m["q"] = SDL_SCANCODE_Q;
    m["r"] = SDL_SCANCODE_R;
    m["s"] = SDL_SCANCODE_S;
    m["t"] = SDL_SCANCODE_T;
    m["u"] = SDL_SCANCODE_U;
    m["v"] = SDL_SCANCODE_V;
    m["w"] = SDL_SCANCODE_W;
    m["x"] = SDL_SCANCODE_X;
    m["y"] = SDL_SCANCODE_Y;
    m["z"] = SDL_SCANCODE_Z;
    m["0"] = SDL_SCANCODE_0;
    m["1"] = SDL_SCANCODE_1;
    m["2"] = SDL_SCANCODE_2;
    m["3"] = SDL_SCANCODE_3;
    m["4"] = SDL_SCANCODE_4;
    m["5"] = SDL_SCANCODE_5;
    m["6"] = SDL_SCANCODE_6;
    m["7"] = SDL_SCANCODE_7;
    m["8"] = SDL_SCANCODE_8;
    m["9"] = SDL_SCANCODE_9;
    m["lalt"] = SDL_SCANCODE_LALT;
    m["ralt"] = SDL_SCANCODE_RALT;
    m["lctrl"] = SDL_SCANCODE_LCTRL;
    m["rctrl"] = SDL_SCANCODE_RCTRL;    
    m["lshift"] = SDL_SCANCODE_LSHIFT;
    m["rshift"] = SDL_SCANCODE_RSHIFT;
    m["space"] = SDL_SCANCODE_SPACE;
    m["up"] = SDL_SCANCODE_UP;
    m["down"] = SDL_SCANCODE_DOWN;
    m["left"] = SDL_SCANCODE_LEFT;
    m["right"] = SDL_SCANCODE_RIGHT;
    m["period"] = SDL_SCANCODE_PERIOD;
    m["delete"] = SDL_SCANCODE_DELETE;
    m["escape"] = SDL_SCANCODE_ESCAPE;
    m["backspace"] = SDL_SCANCODE_BACKSPACE;
    m["period"] = SDL_SCANCODE_PERIOD;
    m["-"] = SDL_SCANCODE_MINUS;
    m["="] = SDL_SCANCODE_EQUALS;
    initialized = true;
  }
  std::string lowercase = s;
  std::transform(lowercase.begin(), lowercase.end(), lowercase.begin(), ::tolower);
  std::map<std::string, SDL_Scancode>::iterator i = m.find(lowercase);
  if (i != m.end())
    return i->second;
  return (SDL_Scancode)0;
}

//--------------------------------------------------------------------
int Keyboard_isDown(lua_State* L)
{
  Keyboard* keyboard = Maxx::Glue::GetUserData<Keyboard>(L, 1);
  GLUE_VERIFY(keyboard);
  const char* button = lua_tostring(L, 2);
  GLUE_VERIFY(button);
  SDL_Scancode sdlkey = StringToSdlScancode(button);
  bool result = keyboard->IsDown(sdlkey);
  lua_pushboolean(L, result);
  return 1;
}

//--------------------------------------------------------------------
int Keyboard_isUp(lua_State* L)
{
  Keyboard* keyboard = Maxx::Glue::GetUserData<Keyboard>(L, 1);
  GLUE_VERIFY(keyboard);
  const char* button = lua_tostring(L, 2);
  GLUE_VERIFY(button);
  SDL_Scancode sdlkey = StringToSdlScancode(button);
  bool result = keyboard->IsUp(sdlkey);
  lua_pushboolean(L, result);
  return 1;
}

//--------------------------------------------------------------------
int Keyboard_isPressed(lua_State* L)
{
  Keyboard* keyboard = Maxx::Glue::GetUserData<Keyboard>(L, 1);
  GLUE_VERIFY(keyboard);
  const char* button = lua_tostring(L, 2);
  GLUE_VERIFY(button);
  SDL_Scancode sdlkey = StringToSdlScancode(button);
  bool result = keyboard->IsPressed(sdlkey);
  lua_pushboolean(L, result);
  return 1;
}

//--------------------------------------------------------------------
int Keyboard_isReleased(lua_State* L)
{
  Keyboard* keyboard = Maxx::Glue::GetUserData<Keyboard>(L, 1);
  GLUE_VERIFY(keyboard);
  const char* button = lua_tostring(L, 2);
  GLUE_VERIFY(button);
  SDL_Scancode sdlkey = StringToSdlScancode(button);
  bool result = keyboard->IsReleased(sdlkey);
  lua_pushboolean(L, result);
  return 1;
}

//--------------------------------------------------------------------
// self
int Keyboard_show(lua_State* L)
{
  Keyboard* keyboard = Maxx::Glue::GetUserData<Keyboard>(L, 1);
  GLUE_VERIFY(keyboard);
  keyboard->Show();
  return 0;
}

//--------------------------------------------------------------------
// self
int Keyboard_hide(lua_State* L)
{
  Keyboard* keyboard = Maxx::Glue::GetUserData<Keyboard>(L, 1);
  GLUE_VERIFY(keyboard);
  keyboard->Hide();
  return 0;
}

//--------------------------------------------------------------------
// self
// returns true if is shown
int Keyboard_isShown(lua_State* L)
{
  Keyboard* keyboard = Maxx::Glue::GetUserData<Keyboard>(L, 1);
  GLUE_VERIFY(keyboard);
  lua_pushboolean(L, keyboard->IsShown());
  return 1;
}

//--------------------------------------------------------------------
int luaopen_Keyboard(lua_State* L)
{
  static luaL_Reg table[] =
  {
    {"new", Keyboard_new},
    {"update", Keyboard_update},
    {"isDown", Keyboard_isDown},
    {"isUp", Keyboard_isUp},
    {"isPressed", Keyboard_isPressed},
    {"isReleased", Keyboard_isReleased},
    {"show", Keyboard_show},
    {"hide", Keyboard_hide},
    {"isShown", Keyboard_isShown},
    {0, 0}
  };
  Maxx::Glue::RegisterType<Keyboard>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}

} // Glue
} // Maxx
