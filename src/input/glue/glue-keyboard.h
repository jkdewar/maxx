#pragma once

#include "engine/glue.h"

namespace Maxx {
namespace Glue {

int luaopen_Keyboard(lua_State*);

} // Glue
} // Maxx
