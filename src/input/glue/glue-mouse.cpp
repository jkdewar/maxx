#include "glue-mouse.h"
#include "input/mouse.h"

using namespace Maxx;

namespace Maxx {
namespace Glue {

//--------------------------------------------------------------------
int Mouse_new(lua_State* L)
{
  Mouse* mouse = new Mouse;
  Maxx::Glue::NewUserData<Mouse>(L, mouse, /*own=*/true);
  return 1;
}

//--------------------------------------------------------------------
int Mouse_update(lua_State* L)
{
	Mouse* mouse = Maxx::Glue::GetUserData<Mouse>(L);
  GLUE_VERIFY(mouse);
	mouse->Update();
  return 0;
}

//--------------------------------------------------------------------
int Mouse_getX(lua_State* L)
{
	Mouse* mouse = Maxx::Glue::GetUserData<Mouse>(L);
  GLUE_VERIFY(mouse);
  lua_pushnumber(L, mouse->GetX());
  return 1;
}

//--------------------------------------------------------------------
int Mouse_getY(lua_State* L)
{
	Mouse* mouse = Maxx::Glue::GetUserData<Mouse>(L);
  GLUE_VERIFY(mouse);
  lua_pushnumber(L, mouse->GetY());
  return 1;
}

//--------------------------------------------------------------------
int Mouse_isDown(lua_State* L)
{
	Mouse* mouse = Maxx::Glue::GetUserData<Mouse>(L, 1);
  GLUE_VERIFY(mouse);
  int button = (int)lua_tonumber(L, 2);
  bool result = mouse->IsDown(button);
  lua_pushboolean(L, result);
  return 1;
}

//--------------------------------------------------------------------
int Mouse_isUp(lua_State* L)
{
  Mouse* mouse = Maxx::Glue::GetUserData<Mouse>(L, 1);
  GLUE_VERIFY(mouse);
  int button = (int)lua_tonumber(L, 2);
  bool result = mouse->IsUp(button);
  lua_pushboolean(L, result);
  return 1;
}

//--------------------------------------------------------------------
int Mouse_isPressed(lua_State* L)
{
  Mouse* mouse = Maxx::Glue::GetUserData<Mouse>(L, 1);
  GLUE_VERIFY(mouse);
  int button = (int)lua_tonumber(L, 2);
  bool result = mouse->IsPressed(button);
  lua_pushboolean(L, result);
  return 1;
}

//--------------------------------------------------------------------
int Mouse_isReleased(lua_State* L)
{
  Mouse* mouse = Maxx::Glue::GetUserData<Mouse>(L, 1);
  GLUE_VERIFY(mouse);
  int button = (int)lua_tonumber(L, 2);
  bool result = mouse->IsReleased(button);
  lua_pushboolean(L, result);
  return 1;
}

//--------------------------------------------------------------------
int luaopen_Mouse(lua_State* L)
{
  static luaL_Reg table[] =
  {
    {"new", Mouse_new},
    {"update", Mouse_update},
    {"getX", Mouse_getX},
    {"getY", Mouse_getY},
    {"isDown", Mouse_isDown},
    {"isUp", Mouse_isUp},
    {"isPressed", Mouse_isPressed},
    {"isReleased", Mouse_isReleased},
    {0, 0}
  };
  Maxx::Glue::RegisterType<Mouse>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}

} // Glue
} // Maxx
