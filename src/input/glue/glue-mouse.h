#pragma once

#include "engine/glue.h"

namespace Maxx {
namespace Glue {

int luaopen_Mouse(lua_State*);

} // Glue
} // Maxx
