#include "glue-touch-input.h"
#include "input/touch-input.h"

using namespace Maxx;

namespace Maxx {
namespace Glue {

//--------------------------------------------------------------------
int TouchInput_getTouches(lua_State* L)
{
	TouchInput* touchInput = Maxx::Glue::GetUserData<TouchInput>(L);
  GLUE_VERIFY(touchInput);
  const TouchInput::Map& touches = touchInput->GetTouches();
  
  // put touches into a lua table on the stack
  {
    lua_newtable(L);
    int index = 0;
    for (TouchInput::Map::const_iterator
         i = touches.begin(); i != touches.end(); ++i)
    {
      const TouchInfo& touchInfo = i->second;
      lua_newtable(L);
      int tableIndex = lua_gettop(L);
      
      lua_pushstring(L, "touchId");
      lua_pushstring(L, touchInfo.touchId.c_str());
      lua_rawset(L, tableIndex);
      
      lua_pushstring(L, "fingerId");
      lua_pushstring(L, touchInfo.fingerId.c_str());
      lua_rawset(L, tableIndex);
      
      lua_pushstring(L, "x");
      lua_pushnumber(L, touchInfo.x);
      lua_rawset(L, tableIndex);
      
      lua_pushstring(L, "y");
      lua_pushnumber(L, touchInfo.y);
      lua_rawset(L, tableIndex);
      
      lua_pushstring(L, "state");
      if (touchInfo.state == TouchInfo::Pressed)
        lua_pushstring(L, "pressed");
      else if (touchInfo.state == TouchInfo::Released)
        lua_pushstring(L, "released");
      else if (touchInfo.state == TouchInfo::Held)
        lua_pushstring(L, "held");
      else
      {
        Assert(false);
        lua_pushstring(L, "");
      }
      lua_rawset(L, tableIndex);
      
      lua_rawseti(L, -2, index);
      ++index;
    }
  }
  
  return 1;
}

//--------------------------------------------------------------------
int luaopen_TouchInput(lua_State* L)
{
  static luaL_Reg table[] =
  {
    {"getTouches", TouchInput_getTouches},
    {0, 0}
  };
  Maxx::Glue::RegisterType<TouchInput>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}

} // Glue
} // Maxx
