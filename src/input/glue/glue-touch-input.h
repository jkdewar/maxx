#pragma once

#include "engine/glue.h"

namespace Maxx {
namespace Glue {

int luaopen_TouchInput(lua_State*);

} // Glue
} // Maxx
