#include "input/keyboard.h"
#include "input/scancode-map.h"
#include "engine/maxx.h"
#include "engine/property-table.h"
#include "engine/entity.h"
#include "messaging/postmaster.h"
#include <cstring>
#include <algorithm>
#include "utils/assert.h"
#include "graphics/display.h"

namespace Maxx {

//--------------------------------------------------------------------
Keyboard::Keyboard()
  : curKeys_(keys1_)
  , prevKeys_(keys2_)
{
  memset(keys1_, 0, MAX_KEYS * sizeof(keys1_[0]));
  memset(keys2_, 0, MAX_KEYS * sizeof(keys2_[0]));

  //SDL_EnableUNICODE(SDL_ENABLE);
  //SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);

  UpdateState();
}

//--------------------------------------------------------------------
Keyboard::~Keyboard()
{
}

//--------------------------------------------------------------------
void Keyboard::Update()
{
  UpdateState();
  SendMessages();
}

//--------------------------------------------------------------------
bool Keyboard::IsDown(SDL_Scancode key) const
{
  return (curKeys_[key] != 0);
}

//--------------------------------------------------------------------
bool Keyboard::IsUp(SDL_Scancode key) const
{
  return (curKeys_[key] == 0);
}

//--------------------------------------------------------------------
bool Keyboard::IsPressed(SDL_Scancode key) const
{
  bool isPressed = (curKeys_[key] != 0 && prevKeys_[key] == 0);
  return isPressed;
}

//--------------------------------------------------------------------
bool Keyboard::IsReleased(SDL_Scancode key) const
{
  return (curKeys_[key] == 0 && prevKeys_[key] != 0);
}

//--------------------------------------------------------------------
static ScancodeMap& GetScancodeMap()
{
  static ScancodeMap scancodeMap;
  return scancodeMap;
}

//--------------------------------------------------------------------
/*static*/ std::string Keyboard::ScancodeToString(SDL_Scancode scancode)
{
  return GetScancodeMap().ScancodeToString(scancode);
}

//--------------------------------------------------------------------
/*static*/ SDL_Scancode Keyboard::StringToScancode(const std::string& str)
{
  return GetScancodeMap().StringToScancode(str);
}

//--------------------------------------------------------------------
static char UnicodeToAscii(int unicode)
{
  // taken from SDL_keysym API doc
  char ascii = unicode;
  if ((unicode & 0xFF80) == 0)
  {
    ascii = (char)(unicode & 0x7F);
  }
  return ascii;
}

//--------------------------------------------------------------------
void Keyboard::HandleEvent(SDL_Event ev)
{
  if (ev.type == SDL_TEXTINPUT)
  {
    Entity* focus = Maxx::Get()->GetFocus();
    
    PropertyTable msg;
    msg.Set("text", std::string(ev.text.text));
    msg.Insert("handled", Property(false));
    if (focus)
      focus->OnMsg("TextTyped", msg, /*sender=*/0);
  }
}

//--------------------------------------------------------------------
void Keyboard::UpdateState()
{
  int numKeys = 0;
  Uint8* keys = SDL_GetKeyboardState(&numKeys);
  std::swap(prevKeys_, curKeys_);
  Assert(numKeys <= MAX_KEYS);
  memset(curKeys_, 0, MAX_KEYS * sizeof(curKeys_[0]));
  memcpy(curKeys_, keys, numKeys * sizeof(curKeys_[0]));
}

//--------------------------------------------------------------------
void Keyboard::SendMessages()
{
#if !defined(__IPHONEOS__)
  Entity* focus = Maxx::Get()->GetFocus();
  Postmaster* postmaster = Maxx::Get()->GetPostmaster();
  
  for (int i = 0; i < MAX_KEYS; ++i)
  {
    SDL_Scancode scancode = (SDL_Scancode)i;
    if (IsPressed(scancode))
    {
      PropertyTable msg;
      //msg.Insert("scancode", (double)i);
      msg.Insert("key", ScancodeToString((SDL_Scancode)i));
      msg.Insert("handled", Property(false));
      if (focus)
        focus->OnMsg("KeyPressed", msg, /*sender=*/0);
      else
        postmaster->SendMessage("KeyPressedGlobal", msg, /*sender=*/0);
    }
    if (IsReleased(scancode))
    {
      PropertyTable msg;
      //msg.Insert("scancode", (double)i);
      msg.Insert("key", ScancodeToString((SDL_Scancode)i));
      postmaster->SendMessage("KeyReleased", msg, /*sender=*/0);
    }
  }
#endif
}

//--------------------------------------------------------------------
void Keyboard::Show()
{
#if defined(__IPHONEOS__)
  SDL_iPhoneKeyboardShow(Maxx::Get()->GetDisplay()->GetSdlWindow());
#endif
}

//--------------------------------------------------------------------
void Keyboard::Hide()
{
#if defined(__IPHONEOS__)
  SDL_iPhoneKeyboardHide(Maxx::Get()->GetDisplay()->GetSdlWindow());
#endif
}

//--------------------------------------------------------------------
bool Keyboard::IsShown() const
{
#if defined(__IPHONEOS__)
  return SDL_iPhoneKeyboardIsShown(Maxx::Get()->GetDisplay()->GetSdlWindow());
#else
  return false;
#endif
}

} // Maxx
