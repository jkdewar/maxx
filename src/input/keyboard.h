#pragma once

#include "cross-platform/sdl-headers.h"
#include <string>

namespace Maxx {

class Keyboard
{
public:

  Keyboard();
  ~Keyboard();

  void Update();

  bool IsDown(SDL_Scancode key) const;
  bool IsUp(SDL_Scancode key) const;
  bool IsPressed(SDL_Scancode key) const;
  bool IsReleased(SDL_Scancode key) const;

  static std::string ScancodeToString(SDL_Scancode scancode);
  static SDL_Scancode StringToScancode(const std::string& str);
  
  void HandleEvent(SDL_Event ev);
  
  void Show();
  void Hide();
  bool IsShown() const;
  
private:
  void UpdateState();
  void SendMessages();

private:
  enum { MAX_KEYS = 4096 };

  Uint8 keys1_[MAX_KEYS];
  Uint8 keys2_[MAX_KEYS];
  Uint8* curKeys_;
  Uint8* prevKeys_;
};

} // Maxx
