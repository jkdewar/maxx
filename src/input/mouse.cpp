#include "input/mouse.h"
#include "engine/maxx.h"
#include "engine/property-table.h"
#include "messaging/postmaster.h"
#include "physics/physics.h"
#include "graphics/graphics.h"
#include "graphics/display.h"
#include "graphics/camera.h"

namespace Maxx {

//--------------------------------------------------------------------
Mouse::Mouse()
  : prevButtons_(0)
  , curButtons_(0)
{
}

//--------------------------------------------------------------------
Mouse::~Mouse()
{
}

//--------------------------------------------------------------------
void Mouse::Update()
{
  prevButtons_ = curButtons_;
  curButtons_ = SDL_GetMouseState(&x_, &y_);
  
  SendMessages();
}

//--------------------------------------------------------------------
int Mouse::GetX() const
{
  if (Maxx::Get()->GetDisplay()->IsIosLandscape())
    return GetRealY();
  else
    return GetRealX();
}

//--------------------------------------------------------------------
int Mouse::GetY() const
{
  if (Maxx::Get()->GetDisplay()->IsIosLandscape())
  {
    int screenSizeY = Maxx::Get()->GetDisplay()->GetScreenSizeY();
    return (screenSizeY - 1) - GetRealX();
  }
  else
  {
    return GetRealY();
  }
}

//--------------------------------------------------------------------
// Translates from maxx button convention to sdl
// maxx: 1=left, 2=right, 3=middle
// sdl:  1=left, 2=middle, 3=right
static int TranslateButton(int button)
{
  if (button == 1) return 1;
  if (button == 2) return 3;
  if (button == 3) return 2;
  return button;
}

//--------------------------------------------------------------------
// Translates from sdl to maxx
static int UntranslateButton(int button)
{
  if (button == 1) return 1;
  if (button == 3) return 2;
  if (button == 2) return 3;
  return button;
}

//--------------------------------------------------------------------
bool Mouse::IsDown(int button) const
{
  return (curButtons_ & SDL_BUTTON(TranslateButton(button))) != 0;
}

//--------------------------------------------------------------------
bool Mouse::IsUp(int button) const
{
  return (curButtons_ & SDL_BUTTON(TranslateButton(button))) == 0;
}

//--------------------------------------------------------------------
bool Mouse::IsPressed(int button) const
{
  return ((curButtons_ & SDL_BUTTON(TranslateButton(button))) != 0 && 
         (prevButtons_ & SDL_BUTTON(TranslateButton(button))) == 0);
}

//--------------------------------------------------------------------
bool Mouse::IsReleased(int button) const
{
  return ((curButtons_ & SDL_BUTTON(TranslateButton(button))) == 0 && 
         (prevButtons_ & SDL_BUTTON(TranslateButton(button))) != 0);
}
           
//--------------------------------------------------------------------
int Mouse::GetRealX() const
{
  int cameraX = (int)Maxx::Get()->GetGraphics()->GetCamera().GetX();
  int screenSizeX = Maxx::Get()->GetDisplay()->GetScreenSizeX();
  int actualScreenSizeX = Maxx::Get()->GetDisplay()->GetActualScreenSizeX();
  double scale = Maxx::Get()->GetDisplay()->GetScreenScale();
  int x = x_, y = y_;
  x *= (double)actualScreenSizeX / screenSizeX;
  Maxx::Get()->GetDisplay()->ConvertMouseCoords(x, y);  
  int ret = x * scale + cameraX - screenSizeX/2.0;
  return ret;
}

//--------------------------------------------------------------------
int Mouse::GetRealY() const
{
  int cameraY = (int)Maxx::Get()->GetGraphics()->GetCamera().GetY();
  int screenSizeY = Maxx::Get()->GetDisplay()->GetScreenSizeY();
  int actualScreenSizeY = Maxx::Get()->GetDisplay()->GetActualScreenSizeY();
  double scale = Maxx::Get()->GetDisplay()->GetScreenScale();
   int x = x_, y = y_;
  Maxx::Get()->GetDisplay()->ConvertMouseCoords(x, y);  
  int ret = y * scale /** screenSizeY / actualScreenSizeY*/ + cameraY - screenSizeY/2.0;
  return ret;
}

//--------------------------------------------------------------------
void Mouse::SendMessages()
{
  Postmaster* postmaster = Maxx::Get()->GetPostmaster();

  for (int i = 1; i <= 3; ++i)
  {
    int button = UntranslateButton(i);
    if (IsPressed(button))
    {
      // Perform point collision query
      Physics* physics = Maxx::Get()->GetPhysics();
      Physics::FixtureList fixtureList;
      physics->QueryPoint(GetX(), GetY(), fixtureList);

      PropertyTable msg;
      msg.Insert("x", GetX());
      msg.Insert("y", GetY());
      msg.Insert("button", (double)button);
      msg.Insert("handled", Property(false));
      for (Physics::FixtureList::iterator i = fixtureList.begin(); i != fixtureList.end(); ++i)
      {
        FixtureComponent* fixtureComponent = *i;
        msg.Insert("fixtureName", fixtureComponent->GetName());
        fixtureComponent->GetEntity()->OnMsg("MousePressedSelf", msg, /*sender=*/0);
        postmaster->SendMessage("MousePressedOther", msg, /*sender=*/fixtureComponent->GetEntity());
      }
      if (!msg.GetBool("handled", false))
        postmaster->SendMessage("MousePressed", msg, /*sender=*/0);
    }
    if (IsReleased(button))
    {
      PropertyTable msg;
      msg.Insert("x", GetX());
      msg.Insert("y", GetY());
      msg.Insert("button", (double)button);
      postmaster->SendMessage("MouseReleased", msg, /*sender=*/0);
    }
  }
}

} // Maxx
