#pragma once

#include "cross-platform/sdl-headers.h"

namespace Maxx {

class Mouse
{
public:
  Mouse();
  ~Mouse();

  void Update();

  int GetX() const;
  int GetY() const;

  bool IsDown(int button) const;
  bool IsUp(int button) const;
  bool IsPressed(int button) const;
  bool IsReleased(int button) const;

private:
  int GetRealX() const;
  int GetRealY() const;
  void SendMessages();
  
private:
  Uint8 prevButtons_;
  Uint8 curButtons_;
  int x_, y_;
};

} // Maxx
