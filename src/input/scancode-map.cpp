#include "input/scancode-map.h"

namespace Maxx {

//--------------------------------------------------------------------
ScancodeMap::ScancodeMap()
{
  std::string str;
#define SCANCODE(s)                               \
  str = #s;                                       \
  for (size_t i = 0; i < str.size(); ++i)         \
    str[i] = tolower(str[i]);                     \
  scancodeToStringMap_[SDL_SCANCODE_ ## s] = str; \
  stringToScancodeMap_[str] = SDL_SCANCODE_ ## s;
#include "input/scancodes.h"
}

//--------------------------------------------------------------------
std::string ScancodeMap::ScancodeToString(SDL_Scancode scancode) const
{
  ScancodeToStringMap::const_iterator i = scancodeToStringMap_.find(scancode);
  if (i == scancodeToStringMap_.end())
    return "";
  return i->second;
}

//--------------------------------------------------------------------
SDL_Scancode ScancodeMap::StringToScancode(const std::string& str) const
{
  StringToScancodeMap::const_iterator i = stringToScancodeMap_.find(str);
  if (i == stringToScancodeMap_.end())
    return (SDL_Scancode)0;
  return i->second;
}

} // Maxx
