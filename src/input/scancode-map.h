#pragma once

#include "cross-platform/sdl-headers.h"
#include <map>
#include <string>

namespace Maxx {

//--------------------------------------------------------------------
class ScancodeMap
{
public:
  ScancodeMap();
  
  std::string ScancodeToString(SDL_Scancode scancode) const;
  SDL_Scancode StringToScancode(const std::string& str) const;
  
private:
  typedef std::map<SDL_Scancode, std::string> ScancodeToStringMap;
  typedef std::map<std::string, SDL_Scancode> StringToScancodeMap;
  ScancodeToStringMap scancodeToStringMap_;
  StringToScancodeMap stringToScancodeMap_;
};

} // Maxx
