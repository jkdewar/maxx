#include "input/touch-input.h"
#include "utils/trace.h"
#include "engine/maxx.h"
#include "messaging/postmaster.h"
#include "graphics/graphics.h"
#include "graphics/display.h"
#include "graphics/camera.h"
#include <sstream>
#include <algorithm>

namespace Maxx {

//--------------------------------------------------------------------
TouchInput::TouchInput()
{
}

//--------------------------------------------------------------------
TouchInput::~TouchInput()
{
}

//--------------------------------------------------------------------
void TouchInput::HandleEvent(SDL_Event ev)
{
  switch (ev.type) 
  {
    case SDL_FINGERDOWN: HandleFingerDownEvent(ev); break;
    case SDL_FINGERUP: HandleFingerUpEvent(ev); break;
    case SDL_FINGERMOTION: HandleFingerMotionEvent(ev); break;
    default: break;
  }
}

//--------------------------------------------------------------------
void TouchInput::Update()
{
  for (Map::iterator i = map_.begin(); i != map_.end(); /**/)
  {
    if (i->second.state == TouchInfo::Released)
    {
      map_.erase(i++);
    }
    else
    {
      if (i->second.state == TouchInfo::Pressed)
      {
        i->second.state = TouchInfo::Held;
      }
      ++i;
    }
  }
}

//--------------------------------------------------------------------
void TouchInput::Reset()
{
  map_.clear();
}

//--------------------------------------------------------------------
static void ConvertCoords(SDL_Event ev, double& x, double& y, double& dx, double& dy)
{
  Display* d = Maxx::Get()->GetDisplay();
  double cx = Maxx::Get()->GetGraphics()->GetCamera().GetX();
  double cy = Maxx::Get()->GetGraphics()->GetCamera().GetY();
  double s = d->GetScreenScale();
  double ssx = (double)d->GetScreenSizeX();
  double ssy = (double)d->GetScreenSizeY();
  double tx = (double)ev.tfinger.x;
  double ty = (double)ev.tfinger.y;
  if (d->IsIosLandscape())
    std::swap(tx, ty);
  x = s * ssx * tx / 32768.0 + cx - ssx/2.0;
  y = s * ssy * ty / 32768.0 + cy - ssy/2.0;
  dx = ev.tfinger.dx;
  dy = ev.tfinger.dy;
  if (d->IsIosLandscape())
    y = ssy - y;
}

//--------------------------------------------------------------------
void TouchInput::BuildMessage(SDL_Event ev, PropertyTable& msg)
{
  std::stringstream ss;
  ss.clear(); ss << ev.tfinger.touchId; msg.Insert("touchId", ss.str());
  ss.clear(); ss << ev.tfinger.fingerId; msg.Insert("fingerId", ss.str());
  double x, y, dx, dy;
  ConvertCoords(ev, x, y, dx, dy);
  msg.Insert("x", x);
  msg.Insert("y", y);
  msg.Insert("dx", dx);
  msg.Insert("dy", dy);
  msg.Insert("pressure", (double)ev.tfinger.pressure);
}

//--------------------------------------------------------------------
void TouchInput::HandleFingerDownEvent(SDL_Event ev)
{
  PropertyTable msg;
  BuildMessage(ev, msg);

  TouchInfo touchInfo;
  touchInfo.touchId = msg.GetString("touchId");
  touchInfo.fingerId = msg.GetString("fingerId");
  touchInfo.x = msg.GetNumber("x");
  touchInfo.y = msg.GetNumber("y");
  touchInfo.state = TouchInfo::Pressed;
  map_.insert(std::make_pair(std::make_pair(touchInfo.touchId, touchInfo.fingerId), touchInfo));

  Maxx::Get()->GetPostmaster()->SendMessage("FingerDown", msg, 0);
}

//--------------------------------------------------------------------
void TouchInput::HandleFingerUpEvent(SDL_Event ev)
{
  PropertyTable msg;
  BuildMessage(ev, msg);

  Map::iterator i = map_.find(std::make_pair(msg.GetString("touchId"), msg.GetString("fingerId")));
  if (i != map_.end())
  {
    i->second.x = msg.GetNumber("x");
    i->second.y = msg.GetNumber("y");
    i->second.state = TouchInfo::Released;
  }
  
  Maxx::Get()->GetPostmaster()->SendMessage("FingerUp", msg, 0);
}

//--------------------------------------------------------------------
void TouchInput::HandleFingerMotionEvent(SDL_Event ev)
{
  PropertyTable msg;
  BuildMessage(ev, msg);
  
  Map::iterator i = map_.find(std::make_pair(msg.GetString("touchId"), msg.GetString("fingerId")));
  if (i != map_.end())
  {
    i->second.x = msg.GetNumber("x");
    i->second.y = msg.GetNumber("y");
  }
  
  Maxx::Get()->GetPostmaster()->SendMessage("FingerMotion", msg, 0);
}


} // Maxx
