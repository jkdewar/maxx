#pragma once
#include "cross-platform/sdl-headers.h"
#include <string>
#include <map>

namespace Maxx {
  
class PropertyTable;

struct TouchInfo
{
  enum State
  {
    Pressed,
    Held,
    Released,
  };
  
  std::string touchId;
  std::string fingerId;
  double x, y;
  State state;
};

class TouchInput
{
public:
  TouchInput();
  ~TouchInput();
  
  void HandleEvent(SDL_Event ev);

  void Update();
  
  void Reset();

  typedef std::pair<std::string, std::string> Ids; // touchId, fingerId
  typedef std::map<Ids, TouchInfo> Map;
  
  const Map& GetTouches() const { return map_; }
   
private:
  void BuildMessage(SDL_Event ev, PropertyTable& msg);
  void HandleFingerDownEvent(SDL_Event ev);
  void HandleFingerUpEvent(SDL_Event ev);
  void HandleFingerMotionEvent(SDL_Event ev);
  
  Map map_;
};

} // Maxx
