#include "glue-matrix.h"
#include "math/matrix.h"
#include "math/vector.h"
#include <sstream>

using namespace Maxx;

namespace Maxx {
namespace Glue {

//--------------------------------------------------------------------
int Matrix_new(lua_State* L)
{
  if (lua_gettop(L) == 9)
  {
    double m[9];
    for (int i = 0; i < 9; ++i)
      m[i] = lua_tonumber(L, i + 1);
    Matrix* newMatrix = new Matrix(m);
    Maxx::Glue::NewUserData<Matrix>(L, newMatrix);
    return 1;
  }
  Matrix* matrix1 = Maxx::Glue::GetUserData<Matrix>(L, 1);
  if (matrix1)
  {
    // copy construct
    Matrix* newMatrix = new Matrix(*matrix1);
    Maxx::Glue::NewUserData<Matrix>(L, newMatrix);
    return 1;
  }
  else
  {
    // set to identity
    Matrix* newMatrix = new Matrix(Matrix::Identity());
    Maxx::Glue::NewUserData<Matrix>(L, newMatrix);
    return 1;    
  }
}

//--------------------------------------------------------------------
int Matrix__add(lua_State* L)
{
  Matrix* matrix1 = Maxx::Glue::GetUserData<Matrix>(L, 1);
  GLUE_VERIFY(matrix1);
  Matrix* matrix2 = Maxx::Glue::GetUserData<Matrix>(L, 2);
  Matrix* newMatrix = 0;
  if (matrix2)
  {
    newMatrix = new Matrix(*matrix1 + *matrix2);
  }
  GLUE_VERIFY(newMatrix);
  Maxx::Glue::NewUserData<Matrix>(L, newMatrix);
  return 1;
}

//--------------------------------------------------------------------
int Matrix__sub(lua_State* L)
{
  Matrix* matrix1 = Maxx::Glue::GetUserData<Matrix>(L, 1);
  GLUE_VERIFY(matrix1);
  Matrix* matrix2 = Maxx::Glue::GetUserData<Matrix>(L, 2);
  GLUE_VERIFY(matrix2);
  Matrix* newMatrix = new Matrix(*matrix1 - *matrix2);
  Maxx::Glue::NewUserData<Matrix>(L, newMatrix);
  return 1;
}

//--------------------------------------------------------------------
int Matrix__mul(lua_State* L)
{
  Matrix* matrix1 = Maxx::Glue::GetUserData<Matrix>(L, 1);
  GLUE_VERIFY(matrix1);
  Matrix* matrix2 = Maxx::Glue::GetUserData<Matrix>(L, 2);
  Vector* vector2 = Maxx::Glue::GetUserData<Vector>(L, 2);
  if (matrix2)
  {
    // Matrix * Matrix
    Matrix* newMatrix = new Matrix(*matrix1 * *matrix2);
    Maxx::Glue::NewUserData<Matrix>(L, newMatrix);
    return 1;
  }
  else if (vector2)
  {
    // Matrix * Vector
    Vector* newVector = new Vector(*matrix1 * *vector2);
    Maxx::Glue::NewUserData<Vector>(L, newVector);
    return 1;
  }
  else
  {
    // Matrix * scalar
    double scalar2 = lua_tonumber(L, 2);
    Matrix* newMatrix = new Matrix(*matrix1 * scalar2);
    Maxx::Glue::NewUserData<Matrix>(L, newMatrix);
    return 1;
  }
}

//--------------------------------------------------------------------
int Matrix_inverse(lua_State* L)
{
  Matrix* matrix = Maxx::Glue::GetUserData<Matrix>(L, 1);
  GLUE_VERIFY(matrix);
  Matrix* inverse = new Matrix(matrix->Inverse());
  Maxx::Glue::NewUserData<Matrix>(L, inverse);
  return 1;
}

//--------------------------------------------------------------------
int Matrix_identity(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 0);
  Matrix* matrix = new Matrix(Matrix::Identity());
  Maxx::Glue::NewUserData<Matrix>(L, matrix);
  return 1;
}

//--------------------------------------------------------------------
int Matrix_zero(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 0);
  Matrix* matrix = new Matrix(Matrix::Zero());
  Maxx::Glue::NewUserData<Matrix>(L, matrix);
  return 1;
}

//--------------------------------------------------------------------
int Matrix_scale(lua_State* L)
{
  double scaleX = 1.0, scaleY = 1.0;
  if (lua_gettop(L) == 1)
  {
    // vector passed
    Vector* vector1 = Maxx::Glue::GetUserData<Vector>(L, 1);
    GLUE_VERIFY(vector1);
    scaleX = vector1->x;
    scaleY = vector1->y;
  }
  else if (lua_gettop(L) == 2)
  {
    // two numbers passed
    scaleX = lua_tonumber(L, 1);
    scaleY = lua_tonumber(L, 2);
  }
  else
  {
    GLUE_VERIFY(false);
  }
  
  Matrix* matrix = new Matrix(Matrix::Scale(scaleX, scaleY));
  Maxx::Glue::NewUserData<Matrix>(L, matrix);
  return 1;
}

//--------------------------------------------------------------------
int Matrix_translate(lua_State* L)
{
  double translateX = 0.0, translateY = 0.0;
  if (lua_gettop(L) == 1)
  {
    // vector passed
    Vector* vector1 = Maxx::Glue::GetUserData<Vector>(L, 1);
    GLUE_VERIFY(vector1);
    translateX = vector1->x;
    translateY = vector1->y;
  }
  else if (lua_gettop(L) == 2)
  {
    // two numbers passed
    translateX = lua_tonumber(L, 1);
    translateY = lua_tonumber(L, 2);
  }
  else
  {
    GLUE_VERIFY(false);
  }
  
  Matrix* matrix = new Matrix(Matrix::Translate(translateX, translateY));
  Maxx::Glue::NewUserData<Matrix>(L, matrix);
  return 1;
}

//--------------------------------------------------------------------
int Matrix_rotate(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  double radians = lua_tonumber(L, 1);
  Matrix* matrix = new Matrix(Matrix::Rotate(radians));
  Maxx::Glue::NewUserData<Matrix>(L, matrix);
  return 1;
}

//--------------------------------------------------------------------
int Matrix__tostring(lua_State* L)
{
  Matrix* matrix = Maxx::Glue::GetUserData<Matrix>(L, 1);
  GLUE_VERIFY(matrix);
  std::stringstream s;
  double* m = matrix->m;
  s << "[" << m[0] << "\t" << m[1] << "\t" << m[2] << " \n";
  s << " " << m[3] << "\t" << m[4] << "\t" << m[5] << " \n";
  s << " " << m[6] << "\t" << m[7] << "\t" << m[8] << "]";
  lua_pushstring(L, s.str().c_str());
  return 1;
}

//--------------------------------------------------------------------
int luaopen_Matrix(lua_State* L)
{
  static luaL_Reg table[] =
  {
    {"new", Matrix_new},
    {"__add", Matrix__add},
    {"__sub", Matrix__sub},
    {"__mul", Matrix__mul},
    {"inverse", Matrix_inverse},
    {"identity", Matrix_identity},
    {"zero", Matrix_zero},
    {"scale", Matrix_scale},
    {"rotate", Matrix_rotate},
    {"translate", Matrix_translate},
    {"__tostring", Matrix__tostring},
    {0, 0}
  };
  Maxx::Glue::RegisterType<Matrix>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}

} // Glue
} // Maxx
