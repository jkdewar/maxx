#include "glue-vector.h"
#include "math/vector.h"
#include <sstream>

using namespace Maxx;

namespace Maxx {
namespace Glue {

//--------------------------------------------------------------------
int Vector_new(lua_State* L)
{
  double x = 0, y = 0;
  if (lua_gettop(L) == 0)
  {
    // no arguments
  }
  else if (lua_gettop(L) == 1)
  {
    // vector argument
    Vector* vector1 = Maxx::Glue::GetUserData<Vector>(L, 1);
    GLUE_VERIFY(vector1);
    x = vector1->x;
    y = vector1->y;
  }
  else if (lua_gettop(L) == 2)
  {
    x = lua_tonumber(L, 1);
    y = lua_tonumber(L, 2);
  }
  else
  {
    GLUE_VERIFY(false);
  }
  
  Vector* newVector = new Vector(x, y);
  Maxx::Glue::NewUserData<Vector>(L, newVector);
  return 1;
}

//--------------------------------------------------------------------
int Vector_getX(lua_State* L)
{
  Vector* vector = Maxx::Glue::GetUserData<Vector>(L, 1);
  GLUE_VERIFY(vector);
  lua_pushnumber(L, vector->x);
  return 1;
}

//--------------------------------------------------------------------
int Vector_getY(lua_State* L)
{
  Vector* vector = Maxx::Glue::GetUserData<Vector>(L, 1);
  GLUE_VERIFY(vector);
  lua_pushnumber(L, vector->y);
  return 1;
}

//--------------------------------------------------------------------
int Vector_setX(lua_State* L)
{
  Vector* vector = Maxx::Glue::GetUserData<Vector>(L, 1);
  GLUE_VERIFY(vector);
  vector->x = lua_tonumber(L, 2);
  return 0;
}

//--------------------------------------------------------------------
int Vector_setY(lua_State* L)
{
  Vector* vector = Maxx::Glue::GetUserData<Vector>(L, 1);
  GLUE_VERIFY(vector);
  vector->y = lua_tonumber(L, 2);
  return 0;
}

//--------------------------------------------------------------------
int Vector_set(lua_State* L)
{
  Vector* vector = Maxx::Glue::GetUserData<Vector>(L, 1);
  GLUE_VERIFY(vector);
  vector->x = lua_tonumber(L, 2);
  vector->y = lua_tonumber(L, 3);
  return 0;
}

//--------------------------------------------------------------------
int Vector_length(lua_State* L)
{
  Vector* vector = Maxx::Glue::GetUserData<Vector>(L, 1);
  GLUE_VERIFY(vector);
  double length = vector->Length();
  lua_pushnumber(L, length);
  return 1;
}

//--------------------------------------------------------------------
int Vector__add(lua_State* L)
{
  Vector* v1 = Maxx::Glue::GetUserData<Vector>(L, 1);
  GLUE_VERIFY(v1);
  Vector* v2 = Maxx::Glue::GetUserData<Vector>(L, 2);
  GLUE_VERIFY(v2);
  Vector* result = new Vector(*v1 + *v2);
  Maxx::Glue::NewUserData<Vector>(L, result);
  return 1;
}

//--------------------------------------------------------------------
int Vector__sub(lua_State* L)
{
  Vector* v1 = Maxx::Glue::GetUserData<Vector>(L, 1);
  GLUE_VERIFY(v1);
  Vector* v2 = Maxx::Glue::GetUserData<Vector>(L, 2);
  GLUE_VERIFY(v2);
  Vector* result = new Vector(*v1 - *v2);
  Maxx::Glue::NewUserData<Vector>(L, result);
  return 1;
}

//--------------------------------------------------------------------
int Vector__mul(lua_State* L)
{
  Vector* v1 = Maxx::Glue::GetUserData<Vector>(L, 1);
  GLUE_VERIFY(v1);
  Vector* v2 = Maxx::Glue::GetUserData<Vector>(L, 2);
  if (v2)
  {
    // dot product
    double dot = *v1 * *v2;
    lua_pushnumber(L, dot);
    return 1;
  }
  else
  {
    // multiply by scalar
    double scalar = lua_tonumber(L, 2);
    Vector* result = new Vector(*v1 * scalar);
    Maxx::Glue::NewUserData<Vector>(L, result);
    return 1;
  }
}

//--------------------------------------------------------------------
int Vector_cross(lua_State* L)
{
  Vector* vector1 = Maxx::Glue::GetUserData<Vector>(L, 1);
  GLUE_VERIFY(vector1);
  Vector* vector2 = Maxx::Glue::GetUserData<Vector>(L, 2);
  GLUE_VERIFY(vector2);
  double cross = (*vector1) % (*vector2);
  lua_pushnumber(L, cross);
  return 1;
}

//--------------------------------------------------------------------
int Vector__tostring(lua_State* L)
{
  Vector* vector = Maxx::Glue::GetUserData<Vector>(L, 1);
  GLUE_VERIFY(vector);
  std::stringstream s;
  s << "(" << vector->x << ", " << vector->y << ")";
  lua_pushstring(L, s.str().c_str());
  return 1;
}

//--------------------------------------------------------------------
int luaopen_Vector(lua_State* L)
{
  static luaL_Reg table[] =
  {
    {"new", Vector_new},
    {"getX", Vector_getX},
    {"getY", Vector_getY},
    {"setX", Vector_setX},
    {"setY", Vector_setY},
    {"set", Vector_set},
    {"length", Vector_length},
    {"__add", Vector__add},
    {"__sub", Vector__sub},
    {"__mul", Vector__mul},
    {"cross", Vector_cross},
    {"__tostring", Vector__tostring},
    {0, 0}
  };
  Maxx::Glue::RegisterType<Vector>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}

} // Glue
} // Maxx
