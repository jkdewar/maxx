#pragma once

#include "engine/glue.h"

namespace Maxx {
namespace Glue {

int luaopen_Vector(lua_State*);

} // Glue
} // Maxx
