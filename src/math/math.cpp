#include "math/math.h"
#include "utils/assert.h"
#include <cstdlib>
#include <cstdio>

namespace Maxx {
namespace Math {

//--------------------------------------------------------------------
int SimpleLog2(int input)
{
	Assert(input > 0);
	int retval = 0;
	while (input > 1)
	{
		input >>= 1;
		++retval;
	}
	return retval;
}

//--------------------------------------------------------------------
int NearestPowerOf2RoundUp(int input)
{
	const int p = SimpleLog2(input);
	int output = 1;
	for (int i = 0; i < p; ++i)
	{
		output *= 2;
	}
	if (output == input)
	{
		return output;
	}
	else
	{
		return output * 2;
	}
}

//--------------------------------------------------------------------
int Pow(int num, int pow)
{
    Assert(pow >= 0);
    int result = 1;
    for (int i = 0; i < pow; ++i)
    {
        result *= num;
    }
    return result;
}

//--------------------------------------------------------------------
double AngleDist(double from, double to)
{
    double angleDist = UnwindAngle(to - from);
    return angleDist;
}

//static unsigned long g_randomSeed = 0;

//--------------------------------------------------------------------
int Random()
{
    //gRandomSeed = g_randomSeed * 1103515245 + 12345; 
    //return (int)g_randomSeed;
    return rand();
}

//--------------------------------------------------------------------
int RandomBetween(int min, int max)
{
    Assert(max >= min);
    int r = Random();
    if (r < 0) { r = -r; }
    r %= (max - min + 1);
    r += min;
    return r;
}

//--------------------------------------------------------------------
double RandomDoubleBetween(double min, double max)
{
  double d = RandomBetween(0, 32000) / 32000.0;
  return min + (max - min) * d;
}

//--------------------------------------------------------------------
void SeedRandom(unsigned int seed)
{
    //g_randomSeed = seed;
    srand(seed);
}

//--------------------------------------------------------------------
bool IsBasicallyZero(double d)
{
  const double reallySmall = 0.0000000000001;
  if (d>= -reallySmall && d <= +reallySmall)
    return true;
  return false;
}

//--------------------------------------------------------------------
int Sign(int x)
{
  if (x == 0)
    return 0;
  if (x > 0)
    return +1;
  return -1;
}

//--------------------------------------------------------------------
int Sign(double x)
{
  if (x > 0)
    return +1;
  if (x < 0)
    return -1;
  return 0;
}

//--------------------------------------------------------------------
bool TestPointInAabb(double x, double y, double x1, double y1, double x2, double y2)
{
  double minX = Min(x1, x2);
  double minY = Min(y1, y2);
  double maxX = Max(x1, x2);
  double maxY = Max(y1, y2);
  if (x < minX) return false;
  if (x > maxX) return false;
  if (y < minY) return false;
  if (y > maxY) return false;
  return true;
}

} // Math
} // Maxx
