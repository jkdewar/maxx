#pragma once
#include <math.h>
#include "utils/assert.h"

namespace Maxx {
namespace Math {
  
const double Pi = 3.1415926535897932384626433832795;
const double HalfPi = Pi/2.0;
const double TwoPi = 2.0*Pi;

//--------------------------------------------------------------------
// Unwinds angle to the range [-Pi, +Pi]
inline double UnwindAngle(double angle)
{
	while (angle > Pi)
	{
		angle -= 2.0 * Pi;
	}
	while (angle < -Pi)
	{
		angle += 2.0 * Pi;
	}
	return angle;
}

//--------------------------------------------------------------------
inline double DegreesToRadians(double degrees)
{
  return degrees * Pi / 180.0;
}

//--------------------------------------------------------------------
inline double RadiansToDegrees(double radians)
{
  return radians * 180.0 / Pi;
}

//--------------------------------------------------------------------
template <class T>
T Min(const T& a, const T& b)
{
  return a <= b ? a : b;
}

//--------------------------------------------------------------------
template <class T>
T Max(const T& a, const T& b)
{
  return a > b ? a : b;
}

//--------------------------------------------------------------------
template <class T>
T Abs(T x)
{
  return x >= 0 ? x : -x;
}

//--------------------------------------------------------------------
inline double Floor(double f)
{
	return floor(f);
}

//--------------------------------------------------------------------
inline double Ceil(double f)
{
	return ceil(f);
}

//--------------------------------------------------------------------
inline double Sin(double a)
{
  return sin(a);
}

//--------------------------------------------------------------------
inline double Cos(double a)
{
  return cos(a);
}

//--------------------------------------------------------------------
inline double Atan2(double y, double x)
{
  return atan2(y, x);
}

//--------------------------------------------------------------------
inline double Sqrt(double f)
{
  return sqrt(f);
}

//--------------------------------------------------------------------
// x = i + f where -1 < f < 1
// for example: 
// x = 2.9 => i = 2, f = 0.9
// x = -3.1 => i = -3, f = -0.1
inline void Splitdouble(double x, int* i, double* f)
{
  if (x >= 0)
  {
    *i = (int)Floor(x);
  }
  else
  {
    *i = (int)Ceil(x);
  }
  *f = x - *i;
}

template <class T>
void Clamp(T& val, const T& min, const T& max)
{
  Assert(min <= max);
  if (val < min) val = min;
  if (val > max) val = max;
}

//--------------------------------------------------------------------

int SimpleLog2(int input);
int NearestPowerOf2RoundUp(int input);
int Pow(int num, int pow);
double AngleDist(double from, double to);
int Random();
int RandomBetween(int min, int max);
double RandomDoubleBetween(double min, double max);
inline double Random01() { return RandomDoubleBetween(0.0, 1.0); }
void SeedRandom(unsigned int seed);
bool IsBasicallyZero(double d);
int Sign(int x);
int Sign(double x);

bool TestPointInAabb(double x, double y, double x1, double y1, double x2, double y2);

} // Math
} // Maxx