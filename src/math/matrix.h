#pragma once
#include "math/vector.h"
#include "math/math.h"

// column vectors convention
// v2 = m3*m2*m1*v1
// i think...

namespace Maxx {
  
class Vector;

//--------------------------------------------------------------------
class Matrix
{
public:
  Matrix();
  Matrix(double m0, double m1, double m2, 
         double m3, double m4, double m5, 
         double m6, double m7, double m8);
  Matrix(double m[9]);
  Matrix(const Matrix& rhs);
  ~Matrix();

  Matrix& operator=(const Matrix& rhs);
  Matrix operator+(const Matrix& rhs) const;
  Matrix operator-(const Matrix& rhs) const;
  Matrix operator*(const Matrix& rhs) const;  // Matrix * Matrix
  Vector operator*(const Vector& v) const;    // Matrix * Vector
  Matrix operator*(double d) const;           // Matrix * scalar

  void Transform(Vector& v) const;
  
  Matrix Inverse() const;

  static Matrix Identity();
  static Matrix Zero();
  static Matrix Scale(double sx, double sy);
  static Matrix Scale(const Vector& v);
  static Matrix Rotate(double radians);
  static Matrix Translate(double tx, double ty);
  static Matrix Translate(const Vector& v);

  double m[9];
};
  
//--------------------------------------------------------------------
inline Matrix::Matrix()
{
  for (int i = 0; i < 9; ++i)
    m[i] = 0.0;
}

//--------------------------------------------------------------------
inline  Matrix::Matrix(double m0, double m1, double m2, 
               double m3, double m4, double m5, 
               double m6, double m7, double m8)
{
  m[0] = m0; m[1] = m1; m[2] = m2;
  m[3] = m3; m[4] = m4; m[5] = m5;
  m[6] = m6; m[7] = m7; m[8] = m8;
}

//--------------------------------------------------------------------
inline Matrix::Matrix(double arr[9])
{
  for (int i = 0; i < 9; ++i)
    m[i] = arr[i];
}

//--------------------------------------------------------------------
inline Matrix::Matrix(const Matrix& rhs)
{
  *this = rhs;
}

//--------------------------------------------------------------------
inline Matrix::~Matrix()
{
}

//--------------------------------------------------------------------
inline Matrix& Matrix::operator=(const Matrix& rhs)
{
  for (int i = 0; i < 9; ++i)
    m[i] = rhs.m[i];
  return *this;
}

//--------------------------------------------------------------------
inline Matrix Matrix::operator+(const Matrix& rhs) const
{
  const double* a = this->m;
  const double* b = rhs.m;
  Matrix result;
  double* r = result.m;
  r[0] = a[0] + b[0];
  r[1] = a[1] + b[1];
  r[2] = a[2] + b[2];
  r[3] = a[3] + b[3];
  r[4] = a[4] + b[4];
  r[5] = a[5] + b[5];
  r[6] = a[6] + b[6];
  r[7] = a[7] + b[7];
  r[8] = a[8] + b[8];
  return result;
}

//--------------------------------------------------------------------
inline Matrix Matrix::operator-(const Matrix& rhs) const
{
  const double* a = this->m;
  const double* b = rhs.m;
  Matrix result;
  double* r = result.m;
  r[0] = a[0] - b[0];
  r[1] = a[1] - b[1];
  r[2] = a[2] - b[2];
  r[3] = a[3] - b[3];
  r[4] = a[4] - b[4];
  r[5] = a[5] - b[5];
  r[6] = a[6] - b[6];
  r[7] = a[7] - b[7];
  r[8] = a[8] - b[8];
  return result;
}

//--------------------------------------------------------------------
inline Matrix Matrix::operator*(double d) const
{
  const double* a = this->m;
  Matrix result;
  double* r = result.m;
  r[0] = a[0] * d;
  r[1] = a[1] * d;
  r[2] = a[2] * d;
  r[3] = a[3] * d;
  r[4] = a[4] * d;
  r[5] = a[5] * d;
  r[6] = a[6] * d;
  r[7] = a[7] * d;
  r[8] = a[8] * d;
  return result;
}

//--------------------------------------------------------------------
inline void Matrix::Transform(Vector& v) const
{
  double newx = m[0] * v.x + m[1] * v.y + m[2];
  double newy = m[3] * v.x + m[4] * v.y + m[5];
  v.x = newx;
  v.y = newy;
}
  
//--------------------------------------------------------------------
inline Matrix Matrix::Inverse() const
{
  // http://everything2.com/title/How+to+find+the+inverse+of+a+matrix
  const double& a = m[0];
  const double& b = m[1];
  const double& c = m[2];
  const double& d = m[3];
  const double& e = m[4];
  const double& f = m[5];
  const double& g = m[6];
  const double& h = m[7];
  const double& i = m[8];
  double det = a*(e*i-f*h) - b*(d*i-f*g) + c*(d*h-e*g);
  if (Math::IsBasicallyZero(det))
    return *this;
  double invdet = 1/det;
  return Matrix(e*i-f*h, c*h-b*i, b*f-c*e,
                f*g-d*i, a*i-c*g, c*d-a*f,
                d*h-e*g, b*g-a*h, a*e-b*d) * invdet;
}

//--------------------------------------------------------------------
inline  /*static*/ Matrix Matrix::Identity()
{
  static double identity[9] =
  {
    1.0,  0.0,  0.0,
    0.0,  1.0,  0.0,
    0.0,  0.0,  1.0
  };
  return Matrix(identity);
}

//--------------------------------------------------------------------
inline /*static*/ Matrix Matrix::Zero()
{
  static double zero[9] =
  {
    0.0,  0.0,  0.0,
    0.0,  0.0,  0.0,
    0.0,  0.0,  0.0
  };
  return Matrix(zero);
}

//--------------------------------------------------------------------
inline /*static*/ Matrix Matrix::Scale(double sx, double sy)
{
  return Matrix(sx,     0.0,    0.0, 
                0.0,    sy,     0.0, 
                0.0,    0.0,    1.0);
}

//--------------------------------------------------------------------
inline /*static*/ Matrix Matrix::Scale(const Vector& v)
{
  return Matrix(v.x,    0.0,    0.0, 
                0.0,    v.y,    0.0, 
                0.0,    0.0,    1.0);
}

//--------------------------------------------------------------------
inline /*static*/ Matrix Matrix::Rotate(double radians)
{
  double c = Math::Cos(radians);
  double s = Math::Sin(radians);
  return Matrix(c,     -s,      0.0, 
                s,      c,      0.0, 
                0.0,    0.0,    1.0);
}

//--------------------------------------------------------------------
inline /*static*/ Matrix Matrix::Translate(double tx, double ty)
{
  return Matrix(1.0,    0.0,    tx,
                0.0,    1.0,    ty,
                0.0,    0.0,    1.0);
}

//--------------------------------------------------------------------
inline /*static*/ Matrix Matrix::Translate(const Vector& v)
{
  return Matrix(1.0,    0.0,    v.x,
                0.0,    1.0,    v.y,
                0.0,    0.0,    1.0);
}
  
//--------------------------------------------------------------------
inline Matrix Matrix::operator*(const Matrix& rhs) const
{
  const double* a = this->m;
  const double* b = rhs.m;
  Matrix result;
  double* r = result.m;
  r[0] = a[0]*b[0] + a[1]*b[3] + a[2]*b[6];
  r[1] = a[0]*b[1] + a[1]*b[4] + a[2]*b[7];
  r[2] = a[0]*b[2] + a[1]*b[5] + a[2]*b[8];
  r[3] = a[3]*b[0] + a[4]*b[3] + a[5]*b[6];
  r[4] = a[3]*b[1] + a[4]*b[4] + a[5]*b[7];
  r[5] = a[3]*b[2] + a[4]*b[5] + a[5]*b[8];
  r[6] = a[6]*b[0] + a[7]*b[3] + a[8]*b[6];
  r[7] = a[6]*b[1] + a[7]*b[4] + a[8]*b[7];
  r[8] = a[6]*b[2] + a[7]*b[5] + a[8]*b[8];
  return result;
}

//--------------------------------------------------------------------
inline Vector Matrix::operator*(const Vector& v) const
{
  return Vector(m[0] * v.x + m[1] * v.y + m[2],
                m[3] * v.x + m[4] * v.y + m[5]);
}

} // Maxx