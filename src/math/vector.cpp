#include "vector.h"
#include "math/math.h"

using namespace Maxx::Math;

namespace Maxx {

//--------------------------------------------------------------------
Vector::Vector()
  : x(0), y(0)
{
}

//--------------------------------------------------------------------
Vector::Vector(double x, double y)
  : x(x), y(y)
{
}

//--------------------------------------------------------------------
Vector::Vector(const Vector& v)
{
  *this = v;
}

//--------------------------------------------------------------------
Vector::~Vector()
{
}

//--------------------------------------------------------------------
double Vector::Length() const
{
  double length = Sqrt(x * x + y * y);
  return length;
}

//--------------------------------------------------------------------
Vector Vector::Normalized() const
{
  double length = Length();
  if (length > 0)
  {
    return Vector(x / length, y / length);
  }
  else
  {
    return Vector(0, 0);
  }
}

//--------------------------------------------------------------------
Vector& Vector::operator=(const Vector& v)
{
  x = v.x;
  y = v.y;
  return *this;
}

//--------------------------------------------------------------------
Vector Vector::operator*(double f) const
{
  return Vector(x * f, y * f);
}

//--------------------------------------------------------------------
double Vector::operator*(const Vector& v) const
{
  return x * v.x + y * v.y;
}

//--------------------------------------------------------------------
Vector Vector::operator+(const Vector& v) const
{
  return Vector(x + v.x, y + v.y);
}

//--------------------------------------------------------------------
Vector Vector::operator-(const Vector& v) const
{
  return Vector(x - v.x, y - v.y);
}

//--------------------------------------------------------------------
double Vector::operator%(const Vector& v) const
{
  return x * v.y - v.x * y;
}

} // Maxx