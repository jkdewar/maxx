#pragma once

namespace Maxx {

class Vector
{
public:
  Vector();
  Vector(double x, double y);
  Vector(const Vector& v);
  ~Vector();

  double Length() const;
  Vector Normalized() const;

  Vector& operator=(const Vector& v);       // assignment
  Vector operator+(const Vector& v) const;
  Vector operator-(const Vector& v) const;  
  Vector operator*(double f) const;         // scalar multiply
  double operator*(const Vector& v) const;  // dot product
  double operator%(const Vector& v) const;  // cross product

  double x, y;
};

} // Maxx
