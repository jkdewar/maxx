#include "messaging/glue/glue-postmaster.h"
#include "messaging/postmaster.h"
#include "engine/property-table.h"

namespace Maxx {
namespace Glue {
  
//--------------------------------------------------------------------
// postmaster, msgname, msg
int Postmaster_sendMessage(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 3);
  Postmaster* postmaster = Maxx::Glue::GetUserData<Postmaster>(L, 1);
  GLUE_VERIFY(postmaster);
  GLUE_VERIFY(lua_isstring(L, 2));
  std::string msgname = lua_tostring(L, 2);
  GLUE_VERIFY(lua_istable(L, 3));
  PropertyTable msg;
  PropertyTable::FromLuaStack(L, -1, msg);
  postmaster->SendMessage(msgname, msg, 0);
  return 0;
}

//--------------------------------------------------------------------
int luaopen_Postmaster(lua_State* L)
{
  static luaL_Reg table[] =
  {
    {"sendMessage", Postmaster_sendMessage},
    {0, 0}
  };
  Maxx::Glue::RegisterType<Postmaster>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}

} // Glue
} // Maxx
