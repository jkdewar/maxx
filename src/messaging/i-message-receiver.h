#pragma once

#include "engine/property-table.h"

namespace Maxx {
  
//--------------------------------------------------------------------
class IMessageReceiver
{
public:
  virtual ~IMessageReceiver() {}
  virtual void OnMsg(const std::string& msgname, PropertyTable& msg, IMessageReceiver* sender) = 0;
};

} // Maxx
