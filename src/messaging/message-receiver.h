#pragma once

#include <string>
#include <map>
#include <set>
#include "engine/property-table.h"
#include "messaging/i-message-receiver.h"
#include "messaging/postmaster.h"
#include "engine/maxx.h"

namespace Maxx {

//--------------------------------------------------------------------
template <class T>
class MessageReceiver : public IMessageReceiver
{    
public:
  typedef void (T::*MsgFunction)(PropertyTable& msg, IMessageReceiver* sender);
  typedef std::map<std::string, MsgFunction> MessageToFunction;
  typedef std::set<std::string> MessageSet;

  virtual void OnMsg(const std::string& msgname, PropertyTable& msg, IMessageReceiver* sender)
  {
    if (messageSet_.find(msgname) == messageSet_.end())
      return; // message not handled
    MsgFunction msgFunction = messageToFunction_[msgname];
    T* t = dynamic_cast<T*>(this);
    (t->*msgFunction)(msg, sender);
  }

  virtual ~MessageReceiver()
  {
    Maxx::Get()->GetPostmaster()->UnsubscribeAll(this);
  }

protected:
  MessageToFunction messageToFunction_;
  MessageSet messageSet_;
};

} // Maxx

//--------------------------------------------------------------------
#define BEGIN_ONMSG(T)                \
  virtual void SubscribeToMessages()  \
  {                                   \
    typedef T CLASS;

//--------------------------------------------------------------------
#define ONMSG(NAME, MEMBER_FN)                                              \
    Maxx::Get()->GetPostmaster()->SubscribeToMessage(NAME, this);           \
    messageToFunction_.insert(std::make_pair(NAME, &CLASS::MEMBER_FN));     \
    messageSet_.insert(NAME);                                               \

//--------------------------------------------------------------------
#define END_ONMSG()                 \
  }

//--------------------------------------------------------------------
#define SUBSCRIBE_ALL()             \
  SubscribeToMessages();            \

