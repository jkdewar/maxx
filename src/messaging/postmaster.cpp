#include "messaging/postmaster.h"
#include "messaging/message-receiver.h"
#include <algorithm>

namespace Maxx {
  
//--------------------------------------------------------------------
Postmaster::Postmaster()
{
}

//--------------------------------------------------------------------
Postmaster::~Postmaster()
{
  for (MessageToReceiverListMap::iterator
       i = messageToReceiverListMap_.begin();
       i != messageToReceiverListMap_.end();
       ++i)
  {
    delete i->second;
  }
  messageToReceiverListMap_.clear();
  
  for (ReceiverToMessageListMap::iterator
       i = receiverToMessageListMap_.begin();
       i != receiverToMessageListMap_.end();
       ++i)
  {
    MessageList* messageList = i->second;
    delete messageList;
  }
  receiverToMessageListMap_.clear();
}

//--------------------------------------------------------------------
void Postmaster::SendMessage(const std::string& msgname, PropertyTable& msg, IMessageReceiver* sender)
{
  MessageToReceiverListMap::iterator i = messageToReceiverListMap_.find(msgname);
  if (i == messageToReceiverListMap_.end())
    return;
  ReceiverList* receiverList = i->second;
  for (ReceiverList::iterator j = receiverList->begin(); j != receiverList->end(); ++j)
  {
    IMessageReceiver* receiver = *j;
    receiver->OnMsg(msgname, msg, sender);
  }
  //Trace("global msg \"%s\"\n", msgname.c_str());
}

//--------------------------------------------------------------------
void Postmaster::SubscribeToMessage(const std::string& msgname, IMessageReceiver* receiver)
{  
  // Add to forward map
  {
    ReceiverList* receiverList = 0;
    MessageToReceiverListMap::iterator i = messageToReceiverListMap_.find(msgname);
    if (i == messageToReceiverListMap_.end())
    {
      receiverList = new ReceiverList;
      messageToReceiverListMap_.insert(std::make_pair(msgname, receiverList));
    }
    else
    {
      receiverList = i->second;
    }
    if (std::find(receiverList->begin(), receiverList->end(), receiver) == receiverList->end())
      receiverList->push_back(receiver);
    else
      Assert(false);
  }
  
  // Add to reverse map
  {
    MessageList* messageList = 0;
    ReceiverToMessageListMap::iterator i = receiverToMessageListMap_.find(receiver);
    if (i == receiverToMessageListMap_.end())
    {
      messageList = new MessageList;
      receiverToMessageListMap_.insert(std::make_pair(receiver, messageList));
    }
    else
    {
      messageList = i->second;
    }
    messageList->push_back(msgname);
  }
}

//--------------------------------------------------------------------
void Postmaster::UnsubscribeFromMessage(const std::string& msgname, IMessageReceiver* receiver)
{
  MessageToReceiverListMap::iterator i = messageToReceiverListMap_.find(msgname);
  if (i == messageToReceiverListMap_.end())
  {
    Assert(false);
    return;
  }
  ReceiverList* receiverList = i->second;
  ReceiverList::iterator j = std::find(receiverList->begin(), receiverList->end(), receiver);
  if (j != receiverList->end())
    j = receiverList->erase(j);
  else
    Assert(false);
}

//--------------------------------------------------------------------
void Postmaster::UnsubscribeAll(IMessageReceiver* receiver)
{
  ReceiverToMessageListMap::iterator i = receiverToMessageListMap_.find(receiver);
  if (i == receiverToMessageListMap_.end())
    return;
  MessageList* messageList = i->second;
  for (MessageList::iterator j = messageList->begin(); j != messageList->end(); ++j)
  {
    const std::string& msgname = *j;
    MessageToReceiverListMap::iterator k = messageToReceiverListMap_.find(msgname);
    if (k == messageToReceiverListMap_.end())
    {
      Assert(false);
      continue;
    }
    ReceiverList* receiverList = k->second;
    ReceiverList::iterator l = std::find(receiverList->begin(), receiverList->end(), receiver);
    if (l == receiverList->end())
    {
      Assert(false);
      continue;
    }
    l = receiverList->erase(l);
  }
  receiverToMessageListMap_.erase(i);
}

} // Maxx
