#pragma once

#include <string>
#include <list>
#include <map>
#include "messaging/i-message-receiver.h"

namespace Maxx {
  
class IMessageReceiver;
class PropertyTable;

//--------------------------------------------------------------------
class Postmaster
{
public:
  Postmaster();
  ~Postmaster();
  
  void SendMessage(const std::string& msgname, PropertyTable& msg, IMessageReceiver* sender);
  void SendMessageTo(const std::string& msgname, PropertyTable& msg, IMessageReceiver* to);
  void SubscribeToMessage(const std::string& msgname, IMessageReceiver* receiver);
  void UnsubscribeFromMessage(const std::string& msgname, IMessageReceiver* receiver);
  void UnsubscribeAll(IMessageReceiver* receiver);

private:
  typedef std::list<IMessageReceiver*> ReceiverList;
  typedef std::list<std::string> MessageList;
  typedef std::map<std::string, ReceiverList*> MessageToReceiverListMap;
  typedef std::map<IMessageReceiver*, MessageList*> ReceiverToMessageListMap;
  
  MessageToReceiverListMap messageToReceiverListMap_;
  ReceiverToMessageListMap receiverToMessageListMap_;
};

} // Maxx
