#include "physics/box2d-debug-draw.h"
#include "graphics/graphics.h"
#include "graphics/debug-lines.h"
#include "math/math.h"
#include "engine/maxx.h"

namespace Maxx {
  
void Box2dDebugDraw::DrawPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color)
{
  const b2Vec2* vptr = vertices;
  for (int32 i = 0; i < vertexCount - 1; ++i)
  {
    DrawSegment(*vptr, *(vptr + 1), color);
    vptr += 1;
  }
  DrawSegment(vertices[vertexCount-1], vertices[0], color);
}

void Box2dDebugDraw::DrawSolidPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color)
{
  // TODO
  DrawPolygon(vertices, vertexCount, color);
}

void Box2dDebugDraw::DrawCircle(const b2Vec2 &center, float32 radius, const b2Color &color)
{
  const int pixelsPerLine = 16;

  double radiusInPixels = radius * scale;
  int numLines = (int)(radiusInPixels / pixelsPerLine);
  Math::Clamp(numLines, 8, 256);
  for (int i = 0; i < numLines; ++i)
  {
    double angle1 = Math::Pi*2.0 * i / numLines;
    double angle2 = Math::Pi*2.0 * (i + 1) / numLines;
    double x1 = center.x + Math::Cos(angle1) * radius;
    double y1 = center.y + Math::Sin(angle1) * radius;
    double x2 = center.x + Math::Cos(angle2) * radius;
    double y2 = center.y + Math::Sin(angle2) * radius;
    DrawSegment(b2Vec2((float32)x1,(float32)y1), b2Vec2((float32)x2, (float32)y2), color);
  }
}

void Box2dDebugDraw::DrawSolidCircle(const b2Vec2 &center, float32 radius, const b2Vec2 &axis, const b2Color &color)
{
  // TODO
  DrawCircle(center, radius, color);
}

void Box2dDebugDraw::DrawSegment(const b2Vec2 &p1, const b2Vec2 &p2, const b2Color &color)
{
  double s = scale;
  Maxx::Get()->GetGraphics()->GetDebugLines()->AddLine(p1.x*s, p1.y*s, p2.x*s, p2.y*s, color.r, color.g, color.b, 1.0);
}

void Box2dDebugDraw::DrawTransform(const b2Transform &xf)
{
}

} // Maxx
