#pragma once

#include "cross-platform/box2d-headers.h"

namespace Maxx {

class Box2dDebugDraw : public b2Draw
{
public:
 	Box2dDebugDraw() : scale(1.0) {}
  virtual ~Box2dDebugDraw() {}
  
  virtual void DrawPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color);
  virtual void DrawSolidPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color);
  virtual void DrawCircle(const b2Vec2 &center, float32 radius, const b2Color &color);
  virtual void DrawSolidCircle(const b2Vec2 &center, float32 radius, const b2Vec2 &axis, const b2Color &color);
  virtual void DrawSegment(const b2Vec2 &p1, const b2Vec2 &p2, const b2Color &color);
  virtual void DrawTransform(const b2Transform &xf);
  
  double scale;
};

} // Maxx
