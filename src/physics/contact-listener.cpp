#include "physics/contact-listener.h"
#include "engine/entity.h"
#include "components/fixture-component.h"

namespace Maxx {

//--------------------------------------------------------------------
ContactListener::ContactListener()
{
}

//--------------------------------------------------------------------
ContactListener::~ContactListener()
{
}

//--------------------------------------------------------------------
void ContactListener::RegisterFixtureComponent(FixtureComponent* fixtureComponent)
{
  Assert(fixtureComponent);
  if (!fixtureComponent)
    return;
  b2Fixture* b2fixture = fixtureComponent->Getb2Fixture();
  Assert(b2fixture);
  if (!b2fixture)
    return;

  if (componentTob2_.find(fixtureComponent) != componentTob2_.end())
    return; // Already registered. (This is not an error.)
  componentTob2_.insert(std::make_pair(fixtureComponent, b2fixture));
  b2ToComponent_.insert(std::make_pair(b2fixture, fixtureComponent));
}

//--------------------------------------------------------------------
void ContactListener::UnregisterFixtureComponent(FixtureComponent* fixtureComponent)
{
  {
    ComponentTob2Map::iterator i = componentTob2_.find(fixtureComponent);
    if (i != componentTob2_.end())
      componentTob2_.erase(i);
  }
  
  {
    b2Fixture* b2fixture = fixtureComponent->Getb2Fixture();
    Assert(b2fixture);
    b2ToComponentMap::iterator i = b2ToComponent_.find(b2fixture);
    Assert(i != b2ToComponent_.end());
    if (i != b2ToComponent_.end())
      b2ToComponent_.erase(i);
  }
}

//--------------------------------------------------------------------
void ContactListener::BeginContact(b2Contact* contact)
{
  FixtureComponent* fixtureComponentA = 0;
  FixtureComponent* fixtureComponentB = 0;
  Entity* entityA = 0;
  Entity* entityB = 0;
  FindComponentsAndEntities(contact, fixtureComponentA, fixtureComponentB, entityA, entityB);
  if (fixtureComponentA && fixtureComponentB && entityA && entityB)
    Entity::OnBeginContact(contact, entityA, entityB, fixtureComponentA, fixtureComponentB);
}

//--------------------------------------------------------------------
void ContactListener::EndContact(b2Contact* contact)
{
  FixtureComponent* fixtureComponentA = 0;
  FixtureComponent* fixtureComponentB = 0;
  Entity* entityA = 0;
  Entity* entityB = 0;
  FindComponentsAndEntities(contact, fixtureComponentA, fixtureComponentB, entityA, entityB);
  if (fixtureComponentA && fixtureComponentB && entityA && entityB)
    Entity::OnEndContact(contact, entityA, entityB, fixtureComponentA, fixtureComponentB);
}

//--------------------------------------------------------------------
void ContactListener::PreSolve(b2Contact* contact, const b2Manifold* oldManifold)
{
  FixtureComponent* fixtureComponentA = 0;
  FixtureComponent* fixtureComponentB = 0;
  Entity* entityA = 0;
  Entity* entityB = 0;
  FindComponentsAndEntities(contact, fixtureComponentA, fixtureComponentB, entityA, entityB);
  if (fixtureComponentA && fixtureComponentB && entityA && entityB)
    Entity::OnPreSolveContact(contact, entityA, entityB, fixtureComponentA, fixtureComponentB);
}

//--------------------------------------------------------------------
void ContactListener::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse)
{
}

//--------------------------------------------------------------------
FixtureComponent* ContactListener::b2FixtureToFixtureComponent(b2Fixture* b2fixture) const
{
  b2ToComponentMap::const_iterator i = b2ToComponent_.find(b2fixture);
  if (i == b2ToComponent_.end())
    return 0;
  return i->second;
}

//--------------------------------------------------------------------
void ContactListener::FindComponentsAndEntities(b2Contact* contact, 
                                                FixtureComponent*& fixtureComponentA,
                                                FixtureComponent*& fixtureComponentB,
                                                Entity*& entityA,
                                                Entity*& entityB)
{
  fixtureComponentA = fixtureComponentB = 0;
  entityA = entityB = 0;
  b2ToComponentMap::iterator iterA = b2ToComponent_.find(contact->GetFixtureA());
  b2ToComponentMap::iterator iterB = b2ToComponent_.find(contact->GetFixtureB());
  if (iterA != b2ToComponent_.end())
    fixtureComponentA = iterA->second;
  if (iterB != b2ToComponent_.end())
    fixtureComponentB = iterB->second;
  if (fixtureComponentA)
    entityA = fixtureComponentA->GetEntity();
  if (fixtureComponentB)
    entityB = fixtureComponentB->GetEntity();
}


} // Maxx
