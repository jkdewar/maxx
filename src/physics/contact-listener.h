#pragma once

#include <map>
#include <set>
#include "cross-platform/box2d-headers.h"

namespace Maxx {

class Entity;
class FixtureComponent;

//--------------------------------------------------------------------
class ContactListener : public b2ContactListener
{
public:
  ContactListener();
  virtual ~ContactListener();

  void RegisterFixtureComponent(FixtureComponent* fixtureComponent);
  void UnregisterFixtureComponent(FixtureComponent* fixtureComponent);
  
  virtual void BeginContact(b2Contact* contact);
  virtual void EndContact(b2Contact* contact);
  virtual void PreSolve(b2Contact* contact, const b2Manifold* oldManifold);
  virtual void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse);
  
  FixtureComponent* b2FixtureToFixtureComponent(b2Fixture* b2fixture) const;

private:
  void FindComponentsAndEntities(b2Contact* contact, 
                                 FixtureComponent*& fixtureA,
                                 FixtureComponent*& fixtureB,
                                 Entity*& entityA,
                                 Entity*& entityB);
private:
  typedef std::map<b2Fixture*, FixtureComponent*> b2ToComponentMap;
  typedef std::map<FixtureComponent*, b2Fixture*> ComponentTob2Map;

  b2ToComponentMap b2ToComponent_;
  ComponentTob2Map componentTob2_;
};

} // Maxx
