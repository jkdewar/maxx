#include "physics/glue/physics-glue.h"
#include "physics/physics.h"
#include "components/fixture-component.h"

namespace Maxx {
namespace Glue {

//--------------------------------------------------------------------
// self
// returns boolean
int Physics_getDebugDraw(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Physics* physics = Maxx::Glue::GetUserData<Physics>(L, 1);
  bool draw = physics->GetDebugDraw();
  lua_pushboolean(L, draw);
  return 1;
}

//--------------------------------------------------------------------
// self, enabled
int Physics_setDebugDraw(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Physics* physics = Maxx::Glue::GetUserData<Physics>(L, 1);
  GLUE_VERIFY(physics);
  GLUE_VERIFY(lua_isboolean(L, 2));
  bool draw = lua_toboolean(L, 2) != 0;
  physics->SetDebugDraw(draw);
  return 0;
}

//--------------------------------------------------------------------
// self, ppm
int Physics_setPixelsPerMeter(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Physics* physics = Maxx::Glue::GetUserData<Physics>(L, 1);
  GLUE_VERIFY(physics);
  GLUE_VERIFY(lua_isnumber(L, 2));
  double ppm = lua_tonumber(L, 2);
  physics->SetPixelsPerMeter(ppm);
  return 0;
}

//--------------------------------------------------------------------
// self
// returns ppm
int Physics_getPixelsPerMeter(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Physics* physics = Maxx::Glue::GetUserData<Physics>(L, 1);
  GLUE_VERIFY(physics);
  double ppm = physics->GetPixelsPerMeter();
  lua_pushnumber(L, ppm);
  return 1;
}

//--------------------------------------------------------------------
// self, gravityX, gravityY
int Physics_setGravity(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 3);
  Physics* physics = Maxx::Glue::GetUserData<Physics>(L, 1);
  GLUE_VERIFY(physics);
  double gravityX = lua_tonumber(L, 2);
  double gravityY = lua_tonumber(L, 3);
  physics->SetGravity(gravityX, gravityY);
  return 0;
}

//--------------------------------------------------------------------
// self
// returns gravityX
int Physics_getGravityX(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Physics* physics = Maxx::Glue::GetUserData<Physics>(L, 1);
  GLUE_VERIFY(physics);
  double gravityX = physics->GetGravityX();
  lua_pushnumber(L, gravityX);
  return 1;
}

//--------------------------------------------------------------------
// self
// returns gravityY
int Physics_getGravityY(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Physics* physics = Maxx::Glue::GetUserData<Physics>(L, 1);
  GLUE_VERIFY(physics);
  double gravityY = physics->GetGravityY();
  lua_pushnumber(L, gravityY);
  return 1;
}

//--------------------------------------------------------------------
// self
// returns boolean
int Physics_isPaused(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Physics* physics = Maxx::Glue::GetUserData<Physics>(L, 1);
  GLUE_VERIFY(physics);
  bool isPaused = physics->IsPaused();
  lua_pushboolean(L, isPaused);
  return 1;
}

//--------------------------------------------------------------------
// self, paused
int Physics_setPaused(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Physics* physics = Maxx::Glue::GetUserData<Physics>(L, 1);
  GLUE_VERIFY(physics);
  bool paused = lua_toboolean(L, 2);
  physics->SetPaused(paused);
  return 0;
}

//--------------------------------------------------------------------
// self, timeScale
int Physics_setTimeScale(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 2);
  Physics* physics = Maxx::Glue::GetUserData<Physics>(L, 1);
  GLUE_VERIFY(physics);
  double timeScale = lua_tonumber(L, 2);
  physics->SetTimeScale(timeScale);
  return 0;
}

//--------------------------------------------------------------------
// self
// returns timeScale
int Physics_getTimeScale(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 1);
  Physics* physics = Maxx::Glue::GetUserData<Physics>(L, 1);
  GLUE_VERIFY(physics);
  double timeScale = physics->GetTimeScale();
  lua_pushnumber(L, timeScale);
  return 1;
}


//--------------------------------------------------------------------
// self, centerX,centerY, halfSizeX,halfSizeY
// returns table of fixtures
int Physics_queryAabb(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 5);
  Physics* physics = Maxx::Glue::GetUserData<Physics>(L, 1);
  GLUE_VERIFY(physics);
  GLUE_VERIFY(lua_isnumber(L, 2));
  GLUE_VERIFY(lua_isnumber(L, 3));
  GLUE_VERIFY(lua_isnumber(L, 4));
  GLUE_VERIFY(lua_isnumber(L, 5));
  Physics::AABB aabb(0,0,0,0);
  aabb.centerX = lua_tonumber(L, 2);
  aabb.centerY = lua_tonumber(L, 3);
  aabb.halfSizeX = lua_tonumber(L, 4);
  aabb.halfSizeY = lua_tonumber(L, 5);
  
  Physics::FixtureList fixtures;
  physics->QueryAABB(aabb, fixtures);
  
  lua_newtable(L);
  int index = 1;
  for (Physics::FixtureList::iterator i = fixtures.begin(); i != fixtures.end(); ++i)
  {
    FixtureComponent* fixtureComponent = *i;
    Maxx::Glue::NewUserData<Component>(L, fixtureComponent, /*own=*/false);
    lua_rawseti(L, -2, index);
    ++index;
  }

  return 1;
}

//--------------------------------------------------------------------
// self, x,y
// returns table of fixtures
int Physics_queryPoint(lua_State* L)
{
  GLUE_VERIFY(lua_gettop(L) == 3);
  Physics* physics = Maxx::Glue::GetUserData<Physics>(L, 1);
  GLUE_VERIFY(physics);
  GLUE_VERIFY(lua_isnumber(L, 2));
  GLUE_VERIFY(lua_isnumber(L, 3));
  double x = lua_tonumber(L, 2);
  double y = lua_tonumber(L, 3);
  Physics::FixtureList fixtures;
  physics->QueryPoint(x, y, fixtures);
  
  lua_newtable(L);
  int index = 1;
  for (Physics::FixtureList::iterator i = fixtures.begin(); i != fixtures.end(); ++i)
  {
    FixtureComponent* fixtureComponent = *i;
    Maxx::Glue::NewUserData<Component>(L, fixtureComponent, /*own=*/false);
    lua_rawseti(L, -2, index);
    ++index;
  }

  return 1;
}

//--------------------------------------------------------------------
int luaopen_Physics(lua_State* L)
{
  static luaL_Reg table[] =
  {
    {"getDebugDraw", Physics_getDebugDraw},
    {"setDebugDraw", Physics_setDebugDraw},
    {"setGravity", Physics_setGravity},
    {"getGravityX", Physics_getGravityX},
    {"getGravityY", Physics_getGravityY},
    {"setPixelsPerMeter", Physics_setPixelsPerMeter},
    {"getPixelsPerMeter", Physics_getPixelsPerMeter},
    {"setPaused", Physics_setPaused},
    {"isPaused", Physics_isPaused},
    {"getTimeScale", Physics_getTimeScale},
    {"setTimeScale", Physics_setTimeScale},
    {"queryAabb", Physics_queryAabb},
    {"queryPoint", Physics_queryPoint},
    {0, 0}
  };
  Maxx::Glue::RegisterType<Physics>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}

} // Glue
} // Maxx
