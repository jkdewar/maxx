#pragma once

#include "engine/glue.h"

namespace Maxx {
namespace Glue {

int luaopen_Physics(lua_State*);

} // Glue
} // Maxx
