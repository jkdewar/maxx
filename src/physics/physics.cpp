#include "physics/physics.h"
#include "physics/contact-listener.h"
#include "physics/box2d-debug-draw.h"
#include "engine/maxx.h"
#include "input/keyboard.h"
#include "utils/assert.h"

namespace Maxx {

//--------------------------------------------------------------------
Physics::Physics()
  : world_(0)
  , contactListener_(0)
  , ppm_(105.0)
  , queryFixtureList_(0)
  , updating_(false)
  , debugDraw_(false)
  , paused_(false)
  , timeScale_(1.0)
{
  b2Vec2 gravity(0.0, 2.0);
  world_ = new b2World(gravity);

  contactListener_ = new ContactListener;
  world_->SetContactListener(contactListener_);
}

//--------------------------------------------------------------------
Physics::~Physics()
{
  delete contactListener_; contactListener_ = 0;
  delete world_; world_ = 0;
}

//--------------------------------------------------------------------
void Physics::SetGravity(double fx, double fy)
{
  if (world_)
    world_->SetGravity(b2Vec2((float32)fx, (float32)fy));
}

//--------------------------------------------------------------------
double Physics::GetGravityX() const
{
  if (!world_)
    return 0.0;
  b2Vec2 gravity = world_->GetGravity();
  return gravity.x;
}

//--------------------------------------------------------------------
double Physics::GetGravityY() const
{
  if (!world_)
    return 0.0;
  b2Vec2 gravity = world_->GetGravity();
  return gravity.y;
}

//--------------------------------------------------------------------
void Physics::QueryAABB(const AABB& aabb, FixtureList& fixtureList) const
{
  if (!world_)
    return;

  b2AABB b2aabb;
  b2aabb.lowerBound = b2Vec2((float32)PixelsToMeters(aabb.centerX - aabb.halfSizeX), 
                             (float32)PixelsToMeters(aabb.centerY - aabb.halfSizeY));
  b2aabb.upperBound = b2Vec2((float32)PixelsToMeters(aabb.centerX + aabb.halfSizeX), 
                             (float32)PixelsToMeters(aabb.centerY + aabb.halfSizeY));
  
  queryFixtureList_ = &fixtureList;
  world_->QueryAABB(const_cast<Physics*>(this), b2aabb);
  queryFixtureList_ = 0;
}

//--------------------------------------------------------------------
void Physics::QueryRaycast(FixtureList& fixtureList) const
{
  if (!world_)
    return;
  Trace("TODO: Physics::QueryRaycast not implemented. (bad programmer!)\n");
  assert(false); // TODO
}

//--------------------------------------------------------------------
void Physics::QueryPoint(double x, double y, FixtureList& fixtureList) const
{
  FixtureList aabbFixtureList;
  QueryAABB(AABB(x, y, 0.01, 0.01), aabbFixtureList);

  for (FixtureList::iterator i = aabbFixtureList.begin(); i != aabbFixtureList.end(); ++i)
  {
    FixtureComponent* fixtureComponent = *i;
    b2Fixture* fixture = fixtureComponent->Getb2Fixture();
    bool pointInFixture = fixture->TestPoint(b2Vec2((float32)PixelsToMeters(x), (float32)PixelsToMeters(y)));
    if (pointInFixture)
      fixtureList.push_back(fixtureComponent);
  }
}

//--------------------------------------------------------------------
bool Physics::ReportFixture(b2Fixture* b2fixture)
{
  FixtureComponent* fixtureComponent = contactListener_->b2FixtureToFixtureComponent(b2fixture);
  if (fixtureComponent)
    queryFixtureList_->push_back(fixtureComponent);
  return true;
}

//--------------------------------------------------------------------
void Physics::Update(double dt)
{
  if (!world_)
    return;

  int velocityIterations = 1;
  int positionIterations = 1;
  updating_ = true;
  // if (Maxx::Get()->GetKeyboard()->IsPressed(SDL_SCANCODE_T))
  //   SetPaused(!IsPaused());
  if (!IsPaused())
    world_->Step((float32)(dt * GetTimeScale()), velocityIterations, positionIterations);
  updating_ = false;
}

//--------------------------------------------------------------------
void Physics::Draw() const
{
  if (!debugDraw_)
    return;
  
  Box2dDebugDraw dd;
  dd.AppendFlags(b2Draw::e_shapeBit);
  dd.scale = MetersToPixels(1.0);
  world_->SetDebugDraw(&dd);
  world_->DrawDebugData();
  world_->SetDebugDraw(0);
}

} // Maxx
