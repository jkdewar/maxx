#pragma once

#include "cross-platform/box2d-headers.h"
#include "components/fixture-component.h"
#include <vector>

namespace Maxx {

class ContactListener;

//--------------------------------------------------------------------
class Physics : public b2QueryCallback
{
public:
  Physics();
  ~Physics();
  
  void SetGravity(double fx, double fy);
  double GetGravityX() const;
  double GetGravityY() const;
  
  //--------------------------------------------------------------------
  // Physics-Time manipulation
  void SetPaused(bool paused) { paused_ = paused; }
  bool IsPaused() const { return paused_; }
  void SetTimeScale(double timeScale) { timeScale_ = timeScale; }
  double GetTimeScale() const { return timeScale_; }

  //--------------------------------------------------------------------
  // Physics scale (pixel <-> meter conversion)
  
  void SetPixelsPerMeter(double ppm) { ppm_ = ppm; }
  double GetPixelsPerMeter() const { return ppm_; }
  double PixelsToMeters(double pixels) const { return pixels / ppm_; }
  double MetersToPixels(double meters) const { return meters * ppm_; }
  
  //--------------------------------------------------------------------
  // Query
  struct AABB
  {
    AABB(double centerX, double centerY, double halfSizeX, double halfSizeY)
      : centerX(centerX), centerY(centerY), halfSizeX(halfSizeX), halfSizeY(halfSizeY) {}
    double centerX, centerY, halfSizeX, halfSizeY;
  };

  typedef std::vector<FixtureComponent*> FixtureList;

  void QueryAABB(const AABB& aabb, FixtureList& fixtureList) const;
  void QueryRaycast(FixtureList& fixtureList) const;
  void QueryPoint(double x, double y, FixtureList& fixtureList) const;

  // Don't call this. It's the Box2D callback.
  virtual bool ReportFixture(b2Fixture* fixture);

  //--------------------------------------------------------------------
  b2World* Getb2World() { return world_; }
  ContactListener* GetContactListener() { return contactListener_; }

  void Update(double dt);
  void Draw() const;
  
  bool GetDebugDraw() const { return debugDraw_; }
  void SetDebugDraw(bool draw) { debugDraw_ = draw; }

  bool UpdateInProgress() const { return updating_; }

private:
  b2World* world_;
  ContactListener* contactListener_;
  double ppm_;
  mutable FixtureList* queryFixtureList_;
  mutable bool updating_;
  bool debugDraw_;
  bool paused_;
  double timeScale_;
};

} // Maxx
