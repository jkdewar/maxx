#pragma once
#include <string>
#include "engine/maxx.h"
#include "profiler/profiler.h"

namespace Maxx {

//--------------------------------------------------------------------
class ProfileScope
{
public:
  //--------------------------------------------------------------------
  ProfileScope(const std::string& description) 
    : startTime_(Maxx::Get()->GetTime()) 
    , description_(description)
  {
  }
  //--------------------------------------------------------------------
  ~ProfileScope()
  {
    double elapsedTime = Maxx::Get()->GetTime() - startTime_;
    Maxx::Get()->GetProfiler()->AddTimedSection(elapsedTime, description_);
  }
  //--------------------------------------------------------------------  
private:
  double startTime_;
  std::string description_;
};

} // Maxx

//--------------------------------------------------------------------
#if defined(MAXX_PROFILER_ENABLED)
#define PROFILE_SCOPE(str) Maxx::ProfileScope profileScoper(str);
#else
#define PROFILE_SCOPE(str)
#endif
