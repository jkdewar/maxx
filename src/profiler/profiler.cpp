#include "profiler/profiler.h"
#include "utils/trace.h"
#include "utils/assert.h"
#include "engine/maxx.h"

namespace Maxx {

//--------------------------------------------------------------------
Profiler::Profiler()
    : frameInProgress_(false)
    , frameStartTime_(0.0)
    , frameElapsedTime_(0.0)
    , sectionList_()
{
}

//--------------------------------------------------------------------
Profiler::~Profiler()
{
}

//--------------------------------------------------------------------
void Profiler::BeginFrame()
{
  Assert(!frameInProgress_);
  if (frameInProgress_)
    return;
  frameInProgress_ = true;
  frameStartTime_ = Maxx::Get()->GetTime();
  sectionList_.clear();
}

//--------------------------------------------------------------------
void Profiler::AddTimedSection(double time, const std::string& description)
{
  Assert(frameInProgress_);
  if (!frameInProgress_)
    return;
  sectionList_.push_back(std::make_pair(time, description));
}

//--------------------------------------------------------------------
double Profiler::EndFrame()
{
  Assert(frameInProgress_);
  if (!frameInProgress_)
    return 0.0;
  frameInProgress_ = false;
  frameElapsedTime_ = Maxx::Get()->GetTime() - frameStartTime_;
  return frameElapsedTime_;
}

//--------------------------------------------------------------------
void Profiler::PrintFrameInfo() const
{
  Trace("--- Profiler ----------------------------------------\n");
  Trace("TOTAL FRAME TIME: %d ms\n", (int)(frameElapsedTime_ * 1000));
  for (SectionList::const_iterator
       i = sectionList_.begin();
       i != sectionList_.end();
       ++i)
  {
    int timeInMs = (int)(i->first * 1000);
    const std::string& description = i->second;
    Trace("%s: %d ms\n", description.c_str(), timeInMs);
  }
  Trace("------------------------------------------------------\n");
}

} // Maxx
