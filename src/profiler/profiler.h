#pragma once
#include <string>
#include <vector>

namespace Maxx {

//--------------------------------------------------------------------
class Profiler
{
public:
  Profiler();
  ~Profiler();

  void BeginFrame();
  void AddTimedSection(double time, const std::string& description);
  double EndFrame();
  void PrintFrameInfo() const;

private:
  typedef std::vector<std::pair<double, std::string> > SectionList;
  bool frameInProgress_;
  double frameStartTime_;
  double frameElapsedTime_;
  SectionList sectionList_;
};

} // Maxx
