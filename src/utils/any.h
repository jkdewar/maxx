#pragma once
#include <typeinfo>


#pragma warning(push)
#pragma warning(disable:4522)

namespace Maxx {
namespace Utils {

class Any
{
public:
  
  //--------------------------------------------------------------------
  Any() : holder_(0)
  {
  }
  
  //--------------------------------------------------------------------
  Any(const Any& rhs) : holder_(0)
  {
    if (rhs.holder_)
      holder_ = rhs.holder_->Clone();
    else
      holder_ = 0;
  }
  
  //--------------------------------------------------------------------
  template <class T>
  Any(const T& rhs) : holder_(0)
  {
    holder_ = new HolderT<T>(rhs);
  }
  
  //--------------------------------------------------------------------
  ~Any()
  {
    delete holder_; holder_ = 0;
  }
  
  //--------------------------------------------------------------------
  bool operator==(const Any& rhs) const
  {
    if (!rhs.holder_ || !holder_)
      return (rhs.holder_ == holder_);
    if (rhs.holder_->GetTypeId() != holder_->GetTypeId())
      return false;
    return holder_->IsEqual(rhs.holder_);
  }
  
  //--------------------------------------------------------------------
  Any& operator=(Any& rhs)
  {
    if (rhs.holder_)
      holder_ = rhs.holder_->Clone();
    else
      holder_ = 0;
    return *this;
  }
  
  //--------------------------------------------------------------------
  template <class T>
  Any& operator=(const T& rhs)
  {
    delete holder_;
    holder_ = new HolderT<T>(rhs);
    return *this;
  }

  //--------------------------------------------------------------------
  Any& operator=(const Any& rhs)
  {
    delete holder_;
    holder_ = rhs.holder_ ? rhs.holder_->Clone() : 0;
    return *this;
  }
  
  //--------------------------------------------------------------------
  template <class T>
  T& Get()
  {
    HolderT<T>* holderT = static_cast<HolderT<T>*>(holder_);
    T* ptr = static_cast<T*>(&holderT->m_value);
    return *ptr;
  }
  
  //--------------------------------------------------------------------
  template <class T>
  const T& Get() const
  {
    const HolderT<T>* holderT = static_cast<HolderT<T>*>(holder_);
    const T* ptr = static_cast<const T*>(&holderT->m_value);
    return *ptr;
  }  
  
  //--------------------------------------------------------------------
  template <class T>
  bool IsType() const
  {
    if (!holder_)
      return false;
    return (typeid(T) == holder_->GetTypeId());
  }
  
  bool IsNull() const
  {
    return (holder_ == 0);
  }
  
  void SetNull()
  {
    delete holder_; holder_ = 0;
  }
  
private:
  
  //--------------------------------------------------------------------
  class Holder
  {
  public:

    virtual ~Holder() {}
    
    virtual const std::type_info& GetTypeId() const
    {
      return typeid(void);
    }
    
    virtual Holder* Clone() const = 0;
    
    virtual bool IsEqual(const Holder* rhs) const = 0;
    
  };
  
  //--------------------------------------------------------------------
  template <class T>
  class HolderT : public Holder
  {
  public:
    
    //--------------------------------------------------------------------
    HolderT(const T& rhs)
    {
      m_value = rhs;
    }
    
    //--------------------------------------------------------------------
    virtual ~HolderT() {}
    
    //--------------------------------------------------------------------
    virtual const std::type_info& GetTypeId() const
    {
      return typeid(T);
    }
    
    //--------------------------------------------------------------------
    virtual Holder* Clone() const
    {
      return new HolderT<T>(m_value);
    }
    
    //--------------------------------------------------------------------
    virtual bool IsEqual(const Holder* rhs) const
    {
      const HolderT<T>* rhsHolderT = static_cast<const HolderT<T>*>(rhs);
      return (m_value == rhsHolderT->m_value);
    }
    
    T m_value;
  };
  
  Holder* holder_;
};

} // Utils
} // Maxx

#pragma warning(pop)