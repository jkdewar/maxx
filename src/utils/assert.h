#pragma once
#include <cstdio>
#include "utils/trace.h"

#if 1
#define Assert(X)                                                 \
  do {                                                            \
  if (!(X)) {                                                     \
    Trace("Assert failed: %s %s:%d\n", #X, __FILE__, __LINE__);   \
  }}  while(false)                                                \

#else
#define Assert(X) 
#endif