#include "glue-utils.h"
#include "utils/guid.h"

namespace Maxx {
namespace Glue {
  
//--------------------------------------------------------------------
int Utils_newGuid(lua_State* L)
{
  std::string s = Maxx::Utils::NewGuid();
  lua_pushstring(L, s.c_str());
  return 1;
}

//--------------------------------------------------------------------
int luaopen_Utils(lua_State* L)
{
  static luaL_Reg table[] =
  {
    {"newGuid", Utils_newGuid},
    {0, 0}
  };
  Maxx::Glue::RegisterType<Utils_DummyClass>(L, table);
  lua_pop(L, 1); // pop class metatable
  return 0;
}

} // Glue
} // Maxx