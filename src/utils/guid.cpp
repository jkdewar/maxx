#include "utils/guid.h"
#include <cstdlib>
#include <cstdio>

namespace Maxx {
namespace Utils {

// S4 and NewGuid are based on code from:
// http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript

//--------------------------------------------------------------------
std::string NewGuid()
{
  int v1 = (1 + (rand() & 0x0ffff));
  int v2 = (1 + (rand() & 0x0ffff));
  int v3 = (1 + (rand() & 0x0ffff));
  int v4 = (1 + (rand() & 0x0ffff));
  int v5 = (1 + (rand() & 0x0ffff));
  int v6 = (1 + (rand() & 0x0ffff));
  int v7 = (1 + (rand() & 0x0ffff));
  int v8 = (1 + (rand() & 0x0ffff));
  
  char buffer[64];
  sprintf(buffer, "%04x%04x-%04x-%04x-%04x-%04x%04x%04x", v1,v2,v3,v4,v5,v6,v7,v8);
  return std::string(buffer);
}

} // Utils
} // Maxx
