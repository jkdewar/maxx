#pragma once

#include <string>

namespace Maxx {
namespace Utils {

std::string NewGuid();

} // Utils
} // Maxx